<?php

class Scripts_mod extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
        }

        function get_scripts_list($seriesID)
        {
                $this->db->select('*')->from('v_surveyScripts')->where('seriesID',$seriesID);

                $query = $this->db->get();
                $result = $query->result_array();
                return $result;
        }

        function getSurveyScripts($scriptsType,$seriesID)
        {
                $this->db->select('*')->from('t_surveyScripts')->where('active',1)->where('scriptsType',$scriptsType)->where('seriesID',$seriesID);

                $query = $this->db->get();
                $result = $query->result_array();
                return $result;
        }
}
