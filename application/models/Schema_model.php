<?php

class Schema_model extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
        }

        public function get_schema($table) {
                $table = trim($table);
               
                if (strlen($table) > 0) {
                        if (strpos($table, ',') !== false) {
                                $table = explode(",", $table);
                        } else {
                                $table = [$table];
                        }
                        $output = [];
                        $o = [];
                        $columns = [];

                        foreach ($table as $k => $v) {
                                if (substr($v, 0, 2) == 'v_') {
                                        $v = str_replace('v_', 't_', $v);
                                }

                                $ic = '';
                                $this->db->select('column_name')->from('information_schema.key_column_usage')->where('table_name', trim($v))->where('TABLE_SCHEMA', $this->db->database);
                                $query = $this->db->get();

                                $row = $query->row_array();
                                $column_key = $row['column_name'];

                                if (strlen($v) > 0) {
//                                $sql = "SELECT COLUMN_NAME, DATA_TYPE,COLUMN_KEY, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION FROM t_ma_column WHERE TABLE_NAME='" . trim($v) . "' ORDER BY TABLE_NAME";
                                        //                                        $this->db->select('COLUMN_NAME, DATA_TYPE,COLUMN_KEY, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION')->from('t_ma_column')->where('TABLE_NAME', trim($v))->orderBy('TABLE_NAME');
                                        //                                $query = $this->db->query($sql);
                                        $this->db->select('COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION,IS_NULLABLE,COLUMN_COMMENT')->from('information_schema.columns')->where('TABLE_NAME', trim($v))->where('TABLE_SCHEMA', $this->db->database);
                                        $query = $this->db->get();
                                        // vd::d($this->db->last_query());
                                        foreach ($query->result() as $row) {
                                                if ($row->CHARACTER_MAXIMUM_LENGTH !== null) {
                                                        $max_length = $row->CHARACTER_MAXIMUM_LENGTH;
                                                }

                                                if ($row->NUMERIC_PRECISION !== null) {
                                                        $max_length = $row->NUMERIC_PRECISION;
                                                }

                                                $o[$row->COLUMN_NAME] = ["column_name" => $row->COLUMN_NAME, "data_type" => $row->DATA_TYPE, "column_key" => $column_key, "max_length" => $max_length, "is_null" => $row->IS_NULLABLE, "comment" => $row->COLUMN_COMMENT];

//                                                if ($row->COLUMN_KEY == 'PRI')
                                                //                                                        $ic = $row->COLUMN_NAME;
                                                if (!in_array($row->COLUMN_NAME, $columns)) {
                                                        array_push($columns, $row->COLUMN_NAME);
                                                }
                                        }
                                }
                        }

                        $output = ['schema' => $o, 'indexColumn' => $column_key, 'columns' => $columns];

//$this->db->close();
                } else {
                        $output = ['schema' => [], 'indexColumn' => '', 'columns' => ''];
                }

                return $output;
        }

}
