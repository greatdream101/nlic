<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Lang_mod extends CI_Model {
        function get_active_lang(){
                $this->db->select('langID,name,code,lang')->from($this->config->item('language_table'))->where('active',1);
                $query=$this->db->get();
                return $query->result_array();
        }
}
