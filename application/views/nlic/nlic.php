<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<? $dirpart = dirname($_SERVER['SCRIPT_NAME']);?>
<link rel="stylesheet" href="../assets/css/nlic.css" type="text/css"  />
<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css"  />
<link rel="stylesheet" href="../assets/css/jquery-ui.css" type="text/css"  />
<link rel="stylesheet" href="../assets/css/responsive.css" type="text/css"  />

<script src="./../assets/js/jquery-3.2.1.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script src="../assets/js/nlic.js"></script>

<!-- chart js -->
<script src="../assets/plugins/chart.js/Chart-2.4.0.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155315684-1"></script>


<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<body>
        <div class="bg1600"></div>
        <div class="head-image-cover-left"> </div>
        <div class="head-image-cover-right"></div>
        <!-- <div class="bg-cover"></div> -->

        <div class="section topbar">
            <div class="row topbar-left">
                <div class="header-nlic col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img style="width: 120px;" src="<?= base_url() ?>assets/img/icon/mol_logo.png" >
                    <span id="nlic_mol_name"><?=$this->mcl->gl("nlic_mol_name")?></span>
                    <span id="button_mol"><a href="http://nlic.mol.go.th/auth"><i class="fa fa-bars"></i></a></span>
                </div>
            </div>
        </div>

        <div class="section menu">
            <div class="row">
                <div class="menu-nlic">
                    <div class="wrapper">
                        <?php
                            // vd::d($menu);
                                foreach($menu as $key=>$value){

                                    if($value['link'] == "#document"){
                                        $target = "";
                                    }else{
                                        $target = "target=_blank";
                                    }

                                    print "<div class='menu-item col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                                        print "<a href=".$value['link']." $target>";
                                        print "<div class='menu-border'>";
                                        print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                        print "<div class='menu-font'>".$value['name']."</div>";
                                        print "</div>";
                                        print "</a>";
                                    print "</div>";
                                }
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="section chartJS">
            <div class="row">
                <div class="chartJS-nlic">
                    <div class="col-lg-12">

                        <div class='chartJS-div col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                            <!-- <a href="javascript:void(0)"> -->
                                <div class='chartJS-border'>
                                    <div id="chart-header"><h2>ข้อมูลสถิติแรงงาน</h2></div>
                                    <div id="chart-subheader">จำนวนผู้ประกันตนในระบบประกันสังคม จำแนกตามมาตรา</div>
                                    <div id="div_year" style="text-align:center;">
                                        <select id="select_year" style="width: 200px; border-radius: 10px; text-align-last:center;">
                                            <?php
                                                // print "<option value=''>SELECT YEAR</option>";
                                                for($year=2019 ; $year <= DATE('Y') ; $year++){
                                                    print "<option value=".$year.">".$year."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <!-- <div><iframe style="border-radius: 10px;" height="500px" width="100%" src="http://20.64.8.197/chart?layout=none"></iframe></div> -->
                                        <div><canvas id="chartColumn" width="100%"></canvas></div>
                                </div>
                            <!-- </a> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="section eservice">
            <div class="row">
                <div class="eservice-nlic">
                    <div class="col-lg-12">
                        <div class='eservice-div col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                <div class='eservice-border'>
                                    <div id="eservice-header"><h2><u>ระบบสารสนเทศบริการประชาชน (e-Services)</u></h2></div>
                                        <?php
                                            // vd::d($eservice);
                                            foreach($eservice as $key=>$value){
                                                print "<div class='eservice-item col-lg-4 col-md-4 col-sm-12 col-xs-12'>";
                                                    print "<a href=".$value['link']." target='_blank'>";
                                                    print "<div class='service-border'>";
                                                    // print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                                    print "<div>".$value['name']."</div>";
                                                    print "</div>";
                                                    print "</a>";
                                                print "</div>";
                                            }
                                        ?>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="section document">
            <div class="row">
                <div class="document-nlic">
                    <div class="col-lg-12">
                        <div class='document-div col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                <div class='document-border'>
                                    <div id="document-header"><h2><u><a name="document"> เอกสารเผยแพร่ </a></u></h2></div>
                                            <!-- modal document -->
                                            <div id="document_modal" style="border-radius: 10px;" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header" style="text-align:center;">
                                                    <u><h3 class="modal-title" id="modal_header">Document List</h3></u>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body" id="document_list" style="height: 100%;"></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                                </div>
                                            </div>
                                            </div>
                                        <?
                                            foreach($document as $key=>$value){
                                                print "<div class='document-item col-lg-4 col-md-4 col-sm-12 col-xs-12'>";
                                                    print "<a href='javascript:void(0)' class='open_modal_document' link_name='".$value['name']."' documentid='".$value['service_id']."'>";
                                                    print "<div class='document-border'>";
                                                    // print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                                    print "<div>".$value['name']."</div>";
                                                    print "</div>";
                                                    print "</a>";
                                                print "</div>";
                                        }
                                        ?>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?
            $this->db->SELECT("COUNT(*) AS total_present")->FROM("t_present_counter");
            $total_present = $this->db->get()->result_array();

            $this->db->SELECT("SUM(NUM) AS total_mount")->FROM("t_summary_counter");
            $this->db->WHERE("DATE_FORMAT(DATE, '%Y-%m') = '".date('Y-m')."' ");
            $total_mount = $this->db->get()->result_array();

            $this->db->SELECT("SUM(NUM) AS total_year")->FROM("t_summary_counter");
            // $this->db->WHERE("DATE_FORMAT(DATE, '%Y') = '".date('Y')."' ");
            $total_year = $this->db->get()->result_array();

            $total_present  = isset($total_present[0]['total_present'])?$total_present[0]['total_present']:'0';
            $total_mount    = isset($total_mount[0]['total_mount'])?$total_mount[0]['total_mount']:'0';
            $total_year     = isset($total_year[0]['total_year'])?$total_year[0]['total_year']:'0';

        ?>

        <div class="section footer">
            <div class="row">
                <div class="footer-nlic">

                    <div class="col-lg-12 footer-img">
                        <!-- ../img/cover/bg-cover.png -->
                        <img style='width: 50%; margin-bottom: 10px;' src=../assets/img/cover/bg-cover.png >
                    </div>

                    <div class="col-lg-12 footer-logo">

                            <div class='footer-div col-lg-10 col-md-10 col-sm-12 col-xs-12'>
                                <?php
                                    foreach($footer as $key=>$value){
                                        print "<div class='footer-item-border col-lg-4 col-md-4 col-sm-6 col-xs-6'>";
                                            print "<a href=".$value['link']." $target>";
                                            print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                            print "<div class='footer-font'>".$value['name']."<br>".$value['nameEN']."</div>";
                                            print "</a>";
                                        print "</div>";
                                    }
                                ?>
                            </div>

                            <div class='col-lg-2 col-md-2 col-sm-12 col-xs-12' style='padding-top: 10px; background: #33318a;'></div>
                            <div class='footer-div stat col-lg-2 col-md-2 col-sm-12 col-xs-12'>
                                <div class="stat-id stat-head"><b>สถิติ</b></div>
                                <div class="stat-id"><u>สถิติผู้เข้าชมวันนี้   </u> :  <span> <?=number_format($total_present)?> ครั้ง </span></div>
                                <div class="stat-id"><u>สถิติผู้เข้าชมเดือนนี้ </u> : <span> <?=number_format($total_mount)?>   ครั้ง </span></div>
                                <div class="stat-id"><u>สถิติผู้เข้าชมทั้งหมด </u> :  <span> <?=number_format($total_year)?>    ครั้ง </span></div>
                            </div>
                            

                            <div class='col-lg-2 col-md-2 col-sm-12 col-xs-12' style='padding-top: 10px; background: #33318a;'>
                                <a href="http://intranet.nlic.mol.go.th/" target='_bank'>
                                    <img style='width: 150px; margin-bottom: 10px;' src='../assets/img/icon/footer_logo_07.png' >
                                </a>
                            </div>

                            <div class="footer-copyrigth col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    &copy; 2019 ศูนย์ข้อมูลแรงงานแห่งชาติ กระทรวงแรงงาน ถนนมิตรไมตรี แขวงดินแดง กทม. 10400 โทร.02-232-1436
                            </div>
                    </div>
                </div>
            </div>
        </div>

</body>