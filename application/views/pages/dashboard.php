<div class="p-a-10">
        <div
                id="lt-grid"
                data-arrange="lt-grid"
                class="lt-container
                lt-xs-h-9
                lt-sm-h-6
                lt-md-h-4
                lt-lg-h-3">

                <div
                        draggable="true"
                        class="lt
                        lt-xs-x-0
                        lt-xs-y-0
                        lt-xs-w-1
                        lt-xs-h-1
                        lt-sm-x-0
                        lt-sm-y-0
                        lt-sm-w-1
                        lt-sm-h-1
                        lt-md-x-0
                        lt-md-y-0
                        lt-md-w-1
                        lt-md-h-1
                        lt-lg-x-0
                        lt-lg-y-0
                        lt-lg-w-1
                        lt-lg-h-1">
                        <div class="lt-body note">
                                <button
                                        class="btn btn-primary fa fa-expand btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 3, h: 3});">
                                        (2x2)
                                </button>
                                <button
                                        class="btn btn-primary fa fa-compress btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 1, h: 1});">
                                        (1x1)
                                </button>

                                <table class="table mb-0 table-responsive table-bordered table-condensed table-striped">
                                        <thead>
                                                <tr>
                                                        <th>#</th>
                                                        <th>Project Name</th>
                                                        <th>Start Date</th>
                                                        <th>Due Date</th>
                                                        <th>Status</th>
                                                        <th>Assign</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                                <tr>
                                                        <td>1</td>
                                                        <td>Minton Admin v1</td>
                                                        <td>01/01/2017</td>
                                                        <td>26/04/2017</td>
                                                        <td><span class="badge badge-info">Released</span></td>
                                                        <td>Coderthemes</td>
                                                </tr>
                                                <tr>
                                                        <td>2</td>
                                                        <td>Minton Frontend v1</td>
                                                        <td>01/01/2017</td>
                                                        <td>26/04/2017</td>
                                                        <td><span class="badge badge-success">Released</span></td>
                                                        <td>Minton admin</td>
                                                </tr>
                                                <tr>
                                                        <td>3</td>
                                                        <td>Minton Admin v1.1</td>
                                                        <td>01/05/2017</td>
                                                        <td>10/05/2017</td>
                                                        <td><span class="badge badge-pink">Pending</span></td>
                                                        <td>Coderthemes</td>
                                                </tr>
                                                <tr>
                                                        <td>4</td>
                                                        <td>Minton Frontend v1.1</td>
                                                        <td>01/01/2017</td>
                                                        <td>31/05/2017</td>
                                                        <td><span class="badge badge-purple">Work in Progress</span>
                                                        </td>
                                                        <td>Minton admin</td>
                                                </tr>
                                                <tr>
                                                        <td>5</td>
                                                        <td>Minton Admin v1.3</td>
                                                        <td>01/01/2017</td>
                                                        <td>31/05/2017</td>
                                                        <td><span class="badge badge-warning">Coming soon</span></td>
                                                        <td>Coderthemes</td>
                                                </tr>

                                        </tbody>
                                </table>
                        </div>
                </div>
                <div
                        draggable="true"
                        class="lt
                        lt-xs-x-0
                        lt-xs-y-1
                        lt-xs-w-1
                        lt-xs-h-2
                        lt-sm-x-1
                        lt-sm-y-0
                        lt-sm-w-1
                        lt-sm-h-2
                        lt-md-x-2
                        lt-md-y-0
                        lt-md-w-1
                        lt-md-h-2
                        lt-lg-x-1
                        lt-lg-y-0
                        lt-lg-w-1
                        lt-lg-h-2">
                        <div class="lt-body note">

                        </div>
                </div>
                <div
                        draggable="true"
                        class="lt
                        lt-xs-x-0
                        lt-xs-y-3
                        lt-xs-w-1
                        lt-xs-h-1
                        lt-sm-x-0
                        lt-sm-y-1
                        lt-sm-w-1
                        lt-sm-h-1
                        lt-md-x-1
                        lt-md-y-0
                        lt-md-w-1
                        lt-md-h-1
                        lt-lg-x-0
                        lt-lg-y-1
                        lt-lg-w-1
                        lt-lg-h-1">
                        <div class="lt-body note">
                                <button
                                        class="btn btn-primary fa fa-expand btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 2, h: 2});">
                                        (2x2)
                                </button>
                                <button
                                        class="btn btn-primary fa fa-compress btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 1, h: 1});">
                                        (1x1)
                                </button>
                                <div id="sparkline2"></div>
                                <ul class="list-inline m-t-15 mb-0">
                                        <li>
                                                <h5 class="text-muted m-t-20">Target</h5>
                                                <h4 class="m-b-0">$1000</h4>
                                        </li>
                                        <li>
                                                <h5 class="text-muted m-t-20">Last week</h5>
                                                <h4 class="m-b-0">$523</h4>
                                        </li>
                                        <li>
                                                <h5 class="text-muted m-t-20">Last Month</h5>
                                                <h4 class="m-b-0">$965</h4>
                                        </li>
                                </ul>
                        </div>
                </div>
                <div
                        draggable="true"
                        class="lt
                        lt-xs-x-0
                        lt-xs-y-4
                        lt-xs-w-1
                        lt-xs-h-2
                        lt-sm-x-0
                        lt-sm-y-2
                        lt-sm-w-2
                        lt-sm-h-2
                        lt-md-x-0
                        lt-md-y-1
                        lt-md-w-2
                        lt-md-h-2
                        lt-lg-x-2
                        lt-lg-y-0
                        lt-lg-w-2
                        lt-lg-h-2">
                        <div class="lt-body note">
                                <button
                                        class="btn btn-primary fa fa-expand btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 2, h: 2});">
                                        (2x2)
                                </button>
                                <button
                                        class="btn btn-primary fa fa-compress btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 1, h: 1});">
                                        (1x1)
                                </button>
                                <div id="sparkline3"></div>
                                <ul class="list-inline m-t-15 mb-0">
                                        <li>
                                                <h5 class="text-muted m-t-20">Target</h5>
                                                <h4 class="m-b-0">$1,84,125</h4>
                                        </li>
                                        <li>
                                                <h5 class="text-muted m-t-20">Last week</h5>
                                                <h4 class="m-b-0">$50,230</h4>
                                        </li>
                                        <li>
                                                <h5 class="text-muted m-t-20">Last Month</h5>
                                                <h4 class="m-b-0">$87,451</h4>
                                        </li>
                                </ul>
                        </div>
                </div>
                <div
                        draggable="true"
                        class="lt
                        lt-xs-x-0
                        lt-xs-y-6
                        lt-xs-w-1
                        lt-xs-h-1
                        lt-sm-x-0
                        lt-sm-y-4
                        lt-sm-w-1
                        lt-sm-h-1
                        lt-md-x-2
                        lt-md-y-2
                        lt-md-w-1
                        lt-md-h-1
                        lt-lg-x-0
                        lt-lg-y-2
                        lt-lg-w-1
                        lt-lg-h-1">
                        <div class="lt-body note">
                                <button
                                        class="btn btn-primary fa fa-expand btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 2, h: 2});">
                                        (2x2)
                                </button>
                                <button
                                        class="btn btn-primary fa fa-compress btn-small"
                                        onclick="$('#lt-grid').ltGrid('reposition', $(this).closest('.lt'), {w: 1, h: 1});">
                                        (1x1)
                                </button>
                                <div id="sparkline1"></div>
                                <ul class="list-inline m-t-15 mb-0">
                                        <li>
                                                <h5 class="text-muted m-t-20">Target</h5>
                                                <h4 class="m-b-0">$56,214</h4>
                                        </li>
                                        <li>
                                                <h5 class="text-muted m-t-20">Last week</h5>
                                                <h4 class="m-b-0">$98,251</h4>
                                        </li>
                                        <li>
                                                <h5 class="text-muted m-t-20">Last Month</h5>
                                                <h4 class="m-b-0">$10,025</h4>
                                        </li>
                                </ul>
                        </div>
                </div>
                <div
                        draggable="true"
                        class="lt
                        lt-xs-x-0
                        lt-xs-y-7
                        lt-xs-w-1
                        lt-xs-h-1
                        lt-sm-x-0
                        lt-sm-y-5
                        lt-sm-w-2
                        lt-sm-h-1
                        lt-md-x-1
                        lt-md-y-3
                        lt-md-w-2
                        lt-md-h-1
                        lt-lg-x-1
                        lt-lg-y-2
                        lt-lg-w-2
                        lt-lg-h-1">
                        <div class="lt-body note">

                        </div>
                </div>
                <div
                        draggable="true"
                        class="lt
                        lt-xs-x-0
                        lt-xs-y-8
                        lt-xs-w-1
                        lt-xs-h-1
                        lt-sm-x-1
                        lt-sm-y-4
                        lt-sm-w-1
                        lt-sm-h-1
                        lt-md-x-0
                        lt-md-y-3
                        lt-md-w-1
                        lt-md-h-1
                        lt-lg-x-3
                        lt-lg-y-2
                        lt-lg-w-1
                        lt-lg-h-1">
                        <div class="lt-body note">

                        </div>
                </div>
        </div>
</div>