<?
$ci = & get_instance();
$ci->load->library('addon/message_lib');
$a = $ci->message_lib->get_message();
?>
<div id="timeline" data-breakpoint="1000" class="timeline clearfix add-padding-10">

        <div class="today">
                <div class="timeline-label">                        
                        <span id="Today">Today</span>
                </div>

                <div class="timeline-row">
                        <div class="timeline-leftside">
                                <?
                                $left_comment = $a['left_comment'];
                                $left = $a['left'];
                                foreach ($left as $k => $v) {
                                        ?>
                                        <div id="event-left-0" class="timeline-event">
                                                <div class="timeline-event-box clearfix">
                                                        <header class="timeline-event-header">
                                                                <div class="pull-left">
                                                                        <h3><?= $v['left_name'] . ' - ' . date('Y-m-d H:i:s', $v['created']) ?></h3>
                                                                </div>

                                                               
                                                        </header>
                                                        <div class="timeline-event-content add-padding-10">

                                                                <?= htmlspecialchars($v['message']); ?>
                                                                <?
                                                                if (isset($left_comment[$v['msg_id']])) {
                                                                        $o = '<ul>';
                                                                        foreach ($left_comment[$v['msg_id']] as $kk => $vv) {
                                                                                $o .= '<li><strong>' . $vv['username'] . '</strong>: ' . $vv['comment'] . '</li>';
                                                                                if (count($vv['documents']) > 0) {
                                                                                        $o .= "<div style='margin-top:10px'><ul>";
                                                                                        foreach ($vv['documents'] as $k1 => $v1) {
                                                                                                $o .= "<li class='no-bullet'>" . $this->mcl->show_thumbnail($v1['document_file']) . " <a href='/downloads/downloads?file=" . $v1['document_file'] . "&table_name=t_wall_user_uploads'>" . $v1['document_original_file'] . "</a></li>";
                                                                                        }
                                                                                        $o .= "</ul></div>";
                                                                                }
                                                                        }
                                                                        $o .= '</ul>';
                                                                        print $o;
                                                                }
                                                                ?>
                                                        </div>
                                                </div><!-- End .timeline-event-box --> 
                                                <span class="timeline-connector-arrow"></span>
                                        </div><!-- End .timeline-event --> 
                                        <?
                                }
                                ?>

                        </div>  
                        <div class="timeline-rightside">
                                <?
                                $right = $a['right'];
                                $right_comment = $a['right_comment'];
                                foreach ($right as $k => $v) {
                                        ?>
                                        <div id="event-right-0" class="timeline-event">
                                                <div class="timeline-event-box clearfix">
                                                        <header class="timeline-event-header">
                                                                <div class="pull-left">
                                                                        <h3><?= $v['right_name'] . ' - ' . date('Y-m-d H:i:s', $v['created']) ?></h3>
                                                                </div>
                                                               
                                                        </header>
                                                        <div class="timeline-event-content add-padding-10">
                                                                <?= html_entity_decode($v['message']) ?>
                                                                <?
                                                                if (isset($right_comment[$v['msg_id']])) {
                                                                        $o = '<ul>';
                                                                        foreach ($right_comment[$v['msg_id']] as $kk => $vv) {
                                                                                $o .= '<li><strong>' . $vv['username'] . '</strong>: ' . $vv['comment'] . '</li>';
                                                                                if (count($vv['documents']) > 0) {
                                                                                        $o .= "<div style='margin-top:10px'><ul>";
                                                                                        foreach ($vv['documents'] as $k1 => $v1) {
                                                                                                $o .= "<li class='no-bullet'>" . $this->mcl->show_thumbnail($v1['document_file']) . " <a href='/downloads/downloads?file=" . $v1['document_file'] . "&table_name=t_wall_user_uploads'>" . $v1['document_original_file'] . "</a></li>";
                                                                                        }
                                                                                        $o .= "</ul></div>";
                                                                                }
                                                                        }
                                                                        $o .= '</ul>';
                                                                        print $o;
                                                                }
                                                                ?>

                                                        </div>
                                                </div><!-- End .timeline-event-box --> 
                                                <span class="timeline-connector-arrow"></span>
                                                <span class="timeline-connector-circle">
                                                        <span>
                                                                <i class="fa fa-calendar-o"></i>
                                                        </span>
                                                </span>
                                        </div><!-- End .timeline-event --> 
                                        <?
                                }
                                ?>
                        </div>  
                </div><!-- End .timeline-row -->  
        </div>
</div>
<script language='javascript'>
        $(document).ready(function () {
                $('span[id=Today]').on('click', function (e) {
                        $(this).closest('.today').find('.timeline-row').toggle();
                })
        })
</script>