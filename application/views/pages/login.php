<!DOCTYPE html>
<html >
<style>
  body {
    /* background: #e9e9e9; */
    background-image: url("../assets/img/bg/bg1600.png");
    background-position: center;
    /* background-repeat: no-repeat; */
    /* background-size: cover; */
    color: #666666;
    font-family: 'Poppins', 'Roboto', 'sans-serif' !important;
    font-size: 14px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  .img-responsive{
          margin: 0 auto;
          width: 230px;
  }
  
  /* Pen Title */
  .pen-title {
    padding: 0px 0;
    text-align: center;
    letter-spacing: 2px;
  }
  .pen-title h1 {
    margin: 0 0 20px;
    font-size: 48px;
    font-weight: 300;
  }
  .pen-title span {
    font-size: 12px;
  }
  .pen-title span .fa {
    color: #33b5e5;
  }
  .pen-title span a {
    color: #33b5e5;
    font-weight: 600;
    text-decoration: none;
  }

  /* Form Module */
  .form-module {
    position: relative;
    /* background: #e8f0fead; */
    border-radius: 10px;
    border-radius: 10px;
    max-width: 400px;
    width: 100%;
    /* border-top: 5px solid #33b5e5; */
    /* box-shadow: 0 0 3px rgba(0, 0, 0, 0.1); */
    margin: 0 auto;
  }
  body h2{
    color: #e6e5e6;
  }
  body div.text-center{
    color: #e6e5e6;
  }
  .form-module .toggle {
    cursor: pointer;
    position: absolute;
    top: -0;
    right: -0;
    /* background: #33b5e5; */
    width: 30px;
    height: 30px;
    margin: -5px 0 0;
    color: #ffffff;
    font-size: 12px;
    line-height: 30px;
    text-align: center;
  }
  .form-module .toggle .tooltip {
    position: absolute;
    top: 5px;
    right: -65px;
    display: block;
    background: rgba(0, 0, 0, 0.6);
    width: auto;
    padding: 5px;
    font-size: 10px;
    line-height: 1;
    text-transform: uppercase;
  }
  .form-module .toggle .tooltip:before {
    content: '';
    position: absolute;
    top: 5px;
    left: -5px;
    display: block;
    border-top: 5px solid transparent;
    border-bottom: 5px solid transparent;
    border-right: 5px solid rgba(0, 0, 0, 0.6);
  }
  .form-module .form {
    display: none;
    padding: 20px;
  }
  .form-module .form:nth-child(2) {
    display: block;
  }
  .form-module h2 {
    text-align: center;
    margin: 10px auto;
    color: ##404046;
    font-size: 40px;
    font-weight: 400;
    line-height: 1;
    padding: 10px;
    font-family: sans-serif;
    color: #ffffff;
    text-shadow: 5px 6px 10px rgb(122, 114, 213);
  }
  .form-module input {
    outline: none;
    display: block;
    width: 100%;
    border: 1px solid #d9d9d9;
    margin: 0 0 20px;
    padding: 10px 15px;
    box-sizing: border-box;
    font-wieght: 400;
    -webkit-transition: 0.3s ease;
    transition: 0.3s ease;
    border-radius: 30px;
    /* background: #042451; */
    font: icon;
    margin: 30px auto;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5)
  }
  .form-module input:focus {
    border: 1px solid #33b5e5;
    color: #333333;
  }
  .form-module input[type="submit"] {
    cursor: pointer;
    background: #376ea2;
    width: 50%;
    border: 0;
    padding: 10px 15px;
    color: #ffffff;
    -webkit-transition: 0.3s ease;
    transition: 0.3s ease;
    margin: 20px auto;
  }
  .form-module input[type="submit"]:hover {
    background: #0a3f70;
  }
  .form-module .cta {
    /* background: #f2f2f2; */
    width: 100%;
    padding: 15px 40px;
    box-sizing: border-box;
    color: #666666;
    font-size: 12px;
    text-align: center;
  }
  .form-module .cta a {
    color: #333333;
    text-decoration: none;
  }
  @font-face {
      font-family: 'Poppins';
      src: url('/mzgift/assets/fonts/Poppins-Regular.woff2') format('woff2'),
          url('/mzgift/assets/fonts/Poppins-Regular.woff') format('woff');
      font-weight: normal;
      font-style: normal;
  }

  @font-face {
      font-family: 'Roboto';
      src: url('/mzgift/assets/fonts/Roboto-Regular.woff2') format('woff2'),
          url('/mzgift/assets/fonts/Roboto-Regular.woff') format('woff');
      font-weight: normal;
      font-style: normal;
  }

  @media screen and (max-width: 450px) {
    body {
      /* width: 100%; */
      background-repeat: no-repeat;
      /* background-size: 270% 145%; */
      padding-left: 2%;
      text-align: center;
    }
    body h2{
      color: #e6e5e6;
    }
    body div.text-center{
      color: #e6e5e6;
    }
    .img-responsive {
        margin: 0 auto;
        width: 150px;
    }
    .form-module {
        position: relative;
        background: none;
        border-radius: 10px;
        border-radius: 10px;
        box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);
    }
  }
</style>
<header>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</header>
<body>
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->
<?php echo form_open("auth/login", array('class' => 'form-horizontal m-t-20')); ?>
<div class="pen-title">
  <h1><?//=isset($_SESSION['projectName'])?$_SESSION['projectName']:'Flat Login Form'?></h1>
  <?=img(array('src'=>base_url()."assets/img/icon/mol_logo.png", "class"=>"img-responsive"))?>
</div>
<!-- Form Module-->
<div class="module form-module">
  <div class="toggle"><i class="fa fa-times fa-pencil"></i>
    <div class="tooltip">Click Me</div>
  </div>
  <div class="form">
    <h2>Login</h2>
    <form>
        
       <input type="text" id="identity" class="fadeIn second" name="identity" placeholder="login" autofocus>
       <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>
  </div>
  
  
</div>
<?= form_close() ?>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<!-- <script src='https://codepen.io/andytran/pen/vLmRVp.js'></script> -->

<?//$this->load->view('templates/minton/style.tpl.php') ?>

    <!-- <script  src="js/index.js"></script> -->
    <div class="text-center">
        &copy; ศูนย์ข้อมูลแรงงานแห่งชาติ <br> (NATIONAL LABOUR INFORMATION CENTER)
    </div>


<!-- Needed by BSE.Mq.screen() helper in utils.js -->
<div id="mq"></div>

<!-- Needed by EmVars helper in _emerald.js -->
<div id="emvars"></div>

</body>

        <?php echo form_open("auth/login", array('class'=>'form-horizontal m-t-20')); ?>
        
        <?= form_close() ?>

</body>
</html>

<script type="text/javascript">
  $('#actionable-sign-in').on('acted.actionable', function(){
    var url = $('#form-sign-in-demo').attr('action');
    function go() { Turbolinks.visit(url); }
    setTimeout(go, 1000);
  });
</script>