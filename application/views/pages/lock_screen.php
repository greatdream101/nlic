


<div class="wrapper-page">

        <div class="text-center">
                <a href="index.html" class="logo-lg"><i class="md md-equalizer"></i> <span>Minton</span> </a>
        </div>

        <?php echo form_open("auth/login", array('class' => 'text-center form-horizontal m-t-20')); ?>
        <div class="user-thumb">
                <img src="<?=base_url()?>assets/images/users/avatar-2.jpg" class="img-responsive img-circle img-thumbnail"
                     alt="thumbnail">
        </div>
        <div class="form-group">
                <h3><?= $this->ion_auth->get_user_name() ?></h3>
                <p class="text-muted">Enter your password to access the admin.</p>
                <div class="input-group m-t-30">
                        <input name="password" id="password" type="password" class="form-control" placeholder="Password">
                        <i class="md md-vpn-key form-control-feedback l-h-34" style="left:6px;z-index: 99;"></i>
                        <span class="input-group-btn"> <button type="submit"
                                                               class="btn btn-email btn-primary waves-effect waves-light">
                                        Log In
                                </button> </span>
                </div>
        </div>
        <div class="text-right">
                <a href="pages-login.html" class="text-muted">Not <?= $this->ion_auth->get_user_name() ?> ?</a>
        </div>
        <?=$this->mcl->hd('identity', $this->ion_auth->get_user_name())?>
        <?= form_close() ?>
</div>


<? $this->load->view('templates/minton/style.tpl.php') ?>