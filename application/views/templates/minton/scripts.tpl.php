<script>
        var resizefunc = [];
</script>

<!-- jQuery  -->
<!--<script src="../assets/js/jquery-3.2.1.min.js"></script>-->
<script src="../assets/js/jquery-1.12.4.min.js"></script>
<script src="../assets/js/jquery-ui.min.js"></script>

<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/detect.js"></script>
<script src="../assets/js/fastclick.js"></script>
<script src="../assets/js/jquery.slimscroll.js"></script>
<script src="../assets/js/jquery.blockUI.js"></script>
<script src="../assets/js/waves.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/jquery.nicescroll.js"></script>
<script src="../assets/js/jquery.scrollTo.min.js"></script>
<script src="../assets/plugins/switchery/switchery.min.js"></script>
<script type="text/javascript" src="../assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script src="../assets/plugins/layout-grid/dist/js/layout-grid.min.js"></script>
<script src="../assets/plugins/printThis/printThis.js"></script>
<!-- peity charts -->
<!--<script src="../assets/plugins/peity/jquery.peity.min.js"></script>-->

<!-- Counter Up  -->
<!--<script src="../assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="../assets/plugins/counterup/jquery.counterup.min.js"></script>-->


<!-- skycons -->
<!--<script src="../assets/plugins/skyicons/skycons.min.js" type="text/javascript"></script>-->

<script src="../assets/plugins/toastr/toastr.min.js"></script>

<script src="../assets/plugins/bootstrap-datatables/datatables.js"></script>
<script src="../assets/plugins/bootstrap-datatables/reloadAjax.min.js"></script>
<script src="../assets/plugins/bootstrap-datatables/fakeRowSpan.min.js"></script>
<!--<script src="../assets/plugins/spin.js"></script>-->
<script src="../assets/plugins/jquery-cookie/jquery.cookie.min.js"></script>
<script src="../assets/plugins/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
<script src="../assets/plugins/summernote/dist/summernote.js"></script>
<script src="../assets/plugins/html-clean/jquery.htmlClean.js"></script>

<script src="../assets/plugins/jquery-sortable/jquery.tablesorter.min.js"></script>
<script src="../assets/plugins/jquery-sortable/jquery.tablesorter.widgets.min.js"></script>
<script src="../assets/plugins/jquery-sortable/jquery.tablesorter.pager.min.js"></script>
<script src="../assets/plugins/jquery-sortable/jquery.sortable.js"></script>

<script src="../assets/plugins/select2/select2.js"></script>
<script src="../assets/plugins/popupwindow/popupwindow.min.js"></script>
<script src="../assets/plugins/jquery-countable/jquery.simplyCountable.min.js"></script>
<script src="../assets/plugins/xlsx.core.min.js" type="text/javascript"></script>
<script src="../assets/plugins/moment-develop/min/moment.min.js" type="text/javascript"></script>
<script src="../assets/plugins/moment-develop/min/locales.min.js" type="text/javascript"></script>

<script src="../assets/plugins/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="../assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script src="../assets/plugins/jquery.timeelapse.min.js"></script>

<script src="../assets/plugins/smartwidgets/jarvis.widget.min.js"></script>
<script src="../assets/plugins/smart-wizard/js/jquery.smartWizard.js"></script>
<script src="../assets/plugins/highcharts/js/highcharts.js"></script>
<!--<script src="../assets/plugins/highcharts/js/regression.js"></script>-->
<script src="../assets/plugins/highcharts/js/modules/exporting.js"></script>
<script src="../assets/plugins/highcharts/js/export-csv.js"></script>
<script src="../assets/plugins/jquery.highchartTable-min.js"></script>
<script src="../assets/js/jquery.app.js"></script>
<script type="text/javascript" src="../assets/plugins/d3.js"></script>
<script type="text/javascript" src="../assets/plugins/c3.js"></script>
<script src="../assets/plugins/pivottable-master/dist/pivot.js"></script>
<script src="../assets/plugins/pivottable-master/dist/c3_renderers.js"></script>
<script src="../assets/js/app.js"></script>