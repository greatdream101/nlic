<!--User dropdown-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<?= form_hidden('userID', $this->ion_auth->get_userID()); ?>
<li id="dropdown-user" class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                <span class="pull-right">
                        <img class="img-circle img-user media-object" src="<?= base_url() ?>assets/img/av1.png" alt="Profile Picture">
                </span> 
                <div class="username hidden-xs"><?=$this->ion_auth->get_user_name()?></div>
        </a>


        <div class="dropdown-menu dropdown-menu-right panel-default">
                <div class="pad-all text-right">
                        <ul class="head-list">
                                <li>
                                        <a href="<?=base_url()?>pages/profile" class="btn btn-primary">
                                                <i class="fa fa-user"></i> Profile
                                        </a>
                                </li>
                                <li>
                                        <a href="<?=base_url()?>auth/logout" class="btn btn-primary">
                                                <i class="pli-unlock"></i> Logout
                                        </a>
                                </li>
                        </ul>


                </div>
        </div>
</li>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End user dropdown-->
