<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
		<div id="sidebar-menu">
			<nav>
	            <?php
	            if (count($function_tree) > 0)
	                print $this->tree_menu->process_sub_nav0($function_tree);
	            ?>
            </nav>
            <div class="clearfix"></div>
		</div>
		<div class="clearfix">
		</div>
    </div>
</div>
<!-- Left Sidebar End --> 