<?php
$userID = isset($t['edit_data']['userID'])?$t['edit_data']['userID']:0;
$tab_1 = array();
//array_push($tab_1, $this->mcl->fi('avatar', $t, array('class' => 'image full-wdith')));
array_push($tab_1, $this->mcl->tb('fname', $t, array('class' => 'half-width')));
array_push($tab_1, $this->mcl->tb('lname', $t, array('class' => 'half-width')));
array_push($tab_1, $this->mcl->tb('username', $t, array('class' => 'full-width')));
array_push($tab_1, $this->mcl->tb('email', $t, array('class' => 'full-width')));
array_push($tab_1, $this->mcl->pb('password', $t, array('class' => 'full-width')));
array_push($tab_1, $this->mcl->sb('groupID', $t, array('class' => 'required no-cookie full-width')));
array_push($tab_1, $this->mcl->hd('table_name', 't_ma_user'));
array_push($tab_1, $this->mcl->hd('id', $userID));

//array_push($tab_1, $this->mcl->bt('save_data', 'save_data'));

print $this->mcl->input_page(array($tab_1), $t);
?>

<script language="javascript">


      $(document).ready(function () {

            $("#btn_save_data").off('click').on('click', function (e) {
                  //debugger;
                  e.preventDefault();
                  var data = get_data_serialize('#tab_1');
                  var url = get_base_url() + 'admin_user/save_data';
                  //print(url);   
                        $.ajax({
                              type: "POST",
                              url: url,
                              cache: false,
                              async: false,
                              data: data,
                              beforeSend: function () {

                              },
                              success: function (html) {
                                    end_saving();
                                    return;
                              }
                        });
            });


      });


</script>