<?
$tab_1 = array();
array_push($tab_1, $this->mcl->sb('groupID', $t, array('class' => 'required full-width')));
array_push($tab_1, $this->mcl->bt('view_group_function', 'submit', array('class'=>'search')));
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->div('group_function_area'));
$o = $this->mcl->input_page(array($tab_1), $t);
$o .= $this->mcl->bt('save', 'Save', array('class' => 'footer'));

print $o;
?>
<script language="javascript">
      $(document).ready(function () {
            $('button[id=btn_view_group_function]').off('click').on('click', function (e) {
                  e.preventDefault();
                  if (pass_validation()) {
                        var groupID = attr($(cc).find('input[type=hidden][name=groupID]').val());
                        var start = $.now();
                        $('button[id=btn_save]').removeClass('hide');
                        var data_string = 'groupID=' + groupID
                        $.ajax({
                              type: "POST",
                              url: get_base_url() + get_uri() + '/get_group_function',
                              data: data_string,
                              cache: false,
                              async: false,
                              beforeSend: function () {
                                    start_loading();
                              },
                              success: function (html) {
                                    $(cc).find('#group_function_area').html(html);
                                    var a = [];
                                    $('#group_function_area').find('tr').each(function () {
                                          if ($.inArray($(this).attr('id'), a) == -1) {
                                                a.push($(this).attr('id'));
                                          } else {
                                                $(this).remove();
                                          }
                                    })
                                    handleCheckBox();
                                    end_loading();
                                    handleElapsedTime(start);
                              }
                        });
                        data_string = null;
                        show_footer(1);
                  } else {
                        show_footer(0);
                  }
                  groupID = null;

                  return false;
            });
      });
</script>
