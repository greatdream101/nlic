<style>
    .button_area{
        display: none;
    }
    #btn_save_coverpage{
        margin: 20px 0 20px 0;
    }
</style>
<?php

    $tab_1 = array();
    array_push($tab_1, $this->mcl->tb('name', $t));
    array_push($tab_1, $this->mcl->tb('nameEN', $t));
    array_push($tab_1, $this->mcl->tb('link', $t));
    array_push($tab_1, $this->mcl->tb('comment', $t));

     $t['addonData']['flag_cover']['data'][0] = array('name'=>'inactive', 'value'=>'0');
     $t['addonData']['flag_cover']['data'][1] = array('name'=>'active', 'value'=>'1');
    array_push($tab_1, $this->mcl->rb('flag_cover', $t)); 

    // vd::d($t);

    if( isset($t['edit_data']['cover_id'])){
       
        $cover_id = $t['edit_data']['cover_id'];
        $filePath = base_url(). $t['edit_data']['file_upload'] . '/' . $t['edit_data']['filename'];
        array_push($tab_1, $this->mcl->hd('cover_id', $t['edit_data']['cover_id']));
        array_push($tab_1, "<div id='div_edit_file' style='display:none;'>
                                <input type='file' name='fileattach' id='fileattach' />
                            </div>"
                        );
        array_push($tab_1, $this->mcl->import_edit($cover_id, $filePath , 'no-description no-cookie'));

    }else{
        // array_push($tab_1, $this->mcl->import('fileattach', 'no-description no-cookie'));
        array_push($tab_1, "<input type='file' name='fileattach' id='fileattach' />");
    }

    $button_coverpage = $this->mcl->bt('save_coverpage', 'save').$this->mcl->bt('delete_coverpage', 'delete');

    array_push($tab_1, $button_coverpage);

    print $this->mcl->input_page(array($tab_1), $t);

?>

<script>
    $(document).ready(function(){
           

        $("#btn_save_coverpage").on("click", function(e){

            // var id   = $('input[type=hidden][name=id]').val();
            var url  = get_base_url()+"Setup_coverpage/save_coverpage";
            var data = get_data_serialize('#ajax_content');            

            $.ajax({
                type: "POST",
                url : url,
                data: data,
                success: function(result){

                    var cover_id = parseInt(result);

                    if (window.File && window.FileReader && window.FileList && window.Blob && cover_id>0) {
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', get_base_url() + 'Setup_coverpage/upload_attachments', true);
                        var form = new FormData();

                            var i = 0;
                            var e = 0;

                            form.append('cover_id', cover_id);
                            
                            // $('div#fileattach').each(function() {
                                // var values = $(this).closest('.form-group').find('input[type=file]').val();
                                var values = $('#fileattach').val();
                                if(values != ''){
                                    // var id = $(this).closest('.form-group').find('input[type=file]').attr('id');
                                    // var keyword = $(this).closest('.form-group').find('input[type=text]').val();
                                    // var files = document.getElementById(id).files;
                                    var data = $('#fileattach')[0];
                                        form.append('file' + i, data.files[0]);
                                        // form.append('keyword' + i, keyword);
                                }
                                i += 1;
                            // });
                            $('div#fileattach_edit').each(function() {
                                    var keywordEdit = $(this).closest('.form-group').find('input[type=text]').val();
                                    var keywordID   = $(this).closest('.form-group').find('input[type=text]').attr('attachmentsid');
                                            form.append('keywordID' + e, keywordID);
                                            form.append('keyword_edit' + e, keywordEdit);
                                    e += 1;
                            });
                            xhr.send(form);

                        success = true;
                    }
                    showSystemMessage(1, "Save Success");
                    window.location.replace(get_base_url()+'pages/index#Setup_coverpage');
                }
            });
        });

        $("#btn_delete_coverpage").on("click", function(e){

            var cover_id = $('#cover_id').val();
            var url      = get_base_url()+"Setup_coverpage/delete_coverpage";       

            $.ajax({
                type: "POST",
                url : url,
                data: {'cover_id':cover_id},
                success: function(result){
                    showSystemMessage(1, "Deleted Already");
                    window.location.replace(get_base_url()+'pages/index#Setup_coverpage');
                }
            });
        });

        setTimeout(() => {
            $('#edit_file').on('click', function(){
                $('#div_edit_file').show('slow');
                $('#fileattach_edit').hide();
            });
        }, 1000);
        
    })
</script>
