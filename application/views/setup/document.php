<style>
    .footer{
        display: none;
    }
    .btn{
        margin: 10px 10px 10px 10px;
    }
    input[type="file"]{
        margin: 0 0 0 20px;
        width: 80%;
    }
</style>
<?

$tab_1 = array();
array_push($tab_1, $this->mcl->tb('name', $t, array('class'=>'required')));
array_push($tab_1, $this->mcl->tb('nameEN', $t));
// array_push($tab_1, $this->mcl->tb('link', $t));
// array_push($tab_1, $this->mcl->tb('iconpart', $t));
array_push($tab_1, $this->mcl->tb('comment', $t));

if(isset($t['edit_data']['service_id'])){
    $referID = $t['edit_data']['service_id'];
    foreach($t["addonData"]['attachmentsID']['data'] as $key=>$value){
        //vd::d($value);
        if($referID == $value['referID']){
        $attachmentsID = "file_".$value["attachmentsID"];
        $keyword = $value['keyword'];
        $link    = $value['link'];
        $url = base_url(). $value['filePath'] . '/' . $value['code'] . '.' . $value['extensionFile'];
        // $filePath = base_url(). $value['filePath'] . '/' . $value['code'] . '.' . $value['extensionFile'];
        $filePath = base_url().'/assets/images/document/';
        if(in_array($value['extensionFile'], array('png', 'jpeg', 'jpg', 'gif'))){
            $filePath .= 'pic.png';
        }elseif(in_array($value['extensionFile'], array('pdf', 'docx', 'xlsx'))){
            $filePath .= $value['extensionFile'].'.png';
        }else{
            $filePath .= 'pic.png';
        }

        array_push($tab_1, $this->mcl->document_import_edit($attachmentsID, $filePath , $keyword , $referID, $url, 'no-description no-cookie', $link));
        }
      }
}

$button = "<div>".$this->mcl->bt("approve_save", "approve_save", array('')).$this->mcl->bt("delete_data", "delete_data", array('btn-danger'))."</div>";

array_push($tab_1, $this->mcl->import('file_1', 'no-description no-cookie'));
array_push($tab_1, $this->mcl->div('importfiles', '', ''));
array_push($tab_1, $this->mcl->bt("add_uploadfile", "add_uploadfile", array("class" => "btn_sm")));
array_push($tab_1, $button);

print $this->mcl->input_page(array($tab_1), $t);

?>

<script language="javascript">
    $(document).ready(function () {
    
        handleRemoveAttach();

        $("#btn_add_uploadfile").on("click", function(){
                var url = get_base_url()+"Setup_document/add_uploadfile";
                var data = $("input[type=file]").length;
                $.ajax({
                        type: "POST",
                        url: url,
                        data: {data},
                        success: function(result){
                                $("#importfiles").append(result);
                                // showSystemMessage(1, "Update Success");
                                handleRemoveAttach();
                        }
                });
        });

        $("#btn_approve_save").off('click').on("click", function(){

            var id      = $('input[type=hidden][name=id]').val();
            var url     = get_base_url()+"Setup_document/save_document";
            var data    = '';
            var name    = attr($('input#name').val(),'');
            var nameEN  = attr($('input#nameEN').val(),'');
            var comment = attr($('input#comment').val(),'');

            data += 'name='+name+'&nameEN='+nameEN+'&comment='+comment+'&id='+id;

            if(name!=""){
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function(result){
                            var service_id = parseInt(result);
                            if (service_id>0) {
                                var xhr = new XMLHttpRequest();
                                url = get_base_url()+'Setup_document/upload_attachments';
                                xhr.open('POST', url, true);
                                var form = new FormData();
                                    var i = 0;
                                    form.append('service_id', service_id);
                                    $('div#fileattach').each(function() {
                                        var values   = $(this).closest('.form-group').find('input[type=file]').val();
                                        var link_val = $(this).closest('.form-group').find('input[type=text][id*=link]').val();
                                        if(values != ''){
                                            var id      = $(this).closest('.form-group').find('input[type=file]').attr('id');
                                            var files   = document.getElementById(id).files;
                                            var keyword = $(this).closest('.form-group').find('input[type=text][id*=keyword]').val();
                                            var link    = $(this).closest('.form-group').find('input[type=text][id*=link]').val();
                                                form.append('file' + i, files[0]);
                                                form.append('keyword' + i, keyword);
                                                form.append('link' + i, link);
                                        }else if(link_val != ''){
                                            var id      = $(this).closest('.form-group').find('input[type=file]').attr('id');
                                            var keyword = $(this).closest('.form-group').find('input[type=text][id*=keyword]').val();
                                            var link    = $(this).closest('.form-group').find('input[type=text][id*=link]').val();
                                                form.append('keyword' + i, keyword);
                                                form.append('link' + i, link);
                                                form.append('row', i);
                                        }
                                        i += 1;
                                    });
                                    xhr.send(form);
                                success = true;
                                showSystemMessage(1, "Save Success");
                                window.location.replace(get_base_url()+'pages/index#Setup_document');
                            }//end upload file
                        }
                    });
                }else{
                    showSystemMessage(2, "กรุณาระบุชื่อหัวข้อ เอกสารเผยแพร่!");
                }
            });

            $('#btn_delete_data').on('click', function(){
                var id  = $('input[type=hidden][name=id]').val();
                var url = get_base_url()+'Setup_document/delete_document';
                if(id>0){
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {'service_id':id},
                        success: function(result){
                            showSystemMessage(1, "Save Success");
                            window.location.replace(get_base_url()+'pages/index#Setup_document');
                        }
                    });

                }else{
                    window.location.replace(get_base_url()+'pages/index#Setup_document');
                }
            });

            $('.edit_attachments_data').each(function(){
                $(this).on('keyup', function(){
                    var attachmentsID = $(this).attr('attachmentsID');
                    var data          = $(this).val();
                    var type          = $(this).attr('attachmentType');
                    var url           = get_base_url()+'Setup_document/edit_attachments';

                    $.ajax({
                        type : "POST",
                        url  : url,
                        data : 'attachmentsID='+attachmentsID+'&'+type+'='+data,
                        success:function(){
                            //do nothing
                        }
                    })
                });
            });


        function handleRemoveAttach(){
            $('#ajax_content').find("button[id*=del_file_]").off("click").on("click", function(k, v) {
                $(this).closest('div.col').hide('slow');
            });

            $('#ajax_content').find("button[id*=del_edit_file_]").off("click").on("click", function(k, v) {

                var id = $(this).attr("attachmentsID");
                var data = '';
                data += 'attachmentsID='+id;
                var url = get_base_url()+"Setup_document/delete_attachments";
                $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function(result){
                        }
                    });
                    $(this).closest('div.col').hide('slow');
            });
        }
    });
</script>