
<!DOCTYPE html>
<html>

      <!-- Mirrored from coderthemes.com/minton_2.1/blue_PHP/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Jun 2017 06:40:43 GMT -->
      <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
            <meta name="author" content="Coderthemes">

            <link rel="shortcut icon" href="../assets/img/icon/mol_logo.png">

            <title>ศูนย์ข้อมูลแรงงานแห่งชาติ</title>

            <?= $this->load->view('templates/minton/style.tpl.php', array(), true) ?>

            <script src="<?= base_url() ?>assets/js/modernizr.min.js"></script>

      </head>


      <body class="fixed-left">


            <!-- Begin page -->
            <div id="wrapper">

                  <!-- Top Bar Start -->

                  <? $this->load->view('templates/minton/header.tpl.php', $web) ?>
                  <!-- Top Bar End -->


                  <!-- ========== Left Sidebar Start ========== -->
                  <? $this->load->view('templates/minton/left-sidebar.tpl.php', $web) ?>
                  <!-- Left Sidebar End -->



                  <!-- ============================================================== -->
                  <!-- Start right Content here -->
                  <!-- ============================================================== -->                      
                  <div class="content-page">
                        <!-- Start content -->
                        <div id="ajax_content" class="content">
                        </div>
                        <!-- end content -->
                        <!-- FOOTER -->
                        <? $this->load->view('templates/minton/footer.tpl.php', $web) ?>
                        <!-- End FOOTER -->
                  </div>
                  <!-- ============================================================== -->
                  <!-- End Right content here -->
                  <!-- ============================================================== -->
                  <!-- Right Sidebar -->
                  <? $this->load->view('templates/minton/right-sidebar.tpl.php', $web) ?>
                  <!-- /Right-bar -->

            </div>
            <!-- END wrapper -->


            <?= $this->load->view('templates/minton/scripts.tpl.php', array(), true) ?>
            <?= form_hidden('base_url', base_url()) ?>
      </body>

      <!-- Mirrored from coderthemes.com/minton_2.1/blue_PHP/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Jun 2017 06:41:34 GMT -->
</html>