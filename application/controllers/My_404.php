<?php

class My_404 extends CI_Controller {

        var $output = '';

        public function __construct() {
                parent::__construct();
        }

        public function index() {
                $this->output->set_status_header('404');
                $data['heading'] = 'error_404'; // View name 
                $data['message'] = 'Page not found.';
                $this->load->view('errors/html/error_404', $data); //loading in my template 
               // redirect('/');
        }

}

?>