<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Mct extends CI_Controller {

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         * 		http://example.com/index.php/welcome
         * 	- or -
         * 		http://example.com/index.php/welcome/index
         * 	- or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */
        function __construct() {
                parent::__construct();
//                $this->mdb->set_translation();
//                $this->mcl->load_lang();
//                $this->session->set_userdata('current_url', get_cookie('current_url'));
//                vd::d(uri_string());
        }

        function index() {
                
                $a = $this->mdb->get_website_data();
                if (count($a) == 0) {
                        $a = array('show_init' => true);
                }

                $this->load->view('templates/index', $a);
        }

        function lock_screen(){
                $this->load->view('pages/lock_screen');
        }
        
        function view() {
                $u = $this->uri->segment_array();
                $is_logged_in = $this->ion_auth->is_logged_in();
                if (count($u) > 2) {
                        $mode = $u[3];
                } else {
                        $mode = 'list';
                }

                if ($is_logged_in) {
                        if (null !== $this->input->get('id')) {
                                $id = $this->input->get('id');
                        } else {
                                $id = -1;
                        }

                        if ($id >= 0) {
                                if (strpos(uri_string(), '/edit') !== false) {
                                        $url = str_replace('/edit', '', uri_string());

                                        $tableData = $this->mdb->get_tableData($url, 'edit', $id);
                                } else if (strpos(uri_string(), '/new') !== false) {
                                        $url = str_replace('/new', '', uri_string());
                                        $tableData = $this->mdb->get_tableData($url, 'add', $id);
                                } else {
                                        die('direct access denined');
                                }

                                $o = $this->load->view($u[1] . '/' . $u[2], array('t' => $tableData), true);
                                $o .= $this->load->view('templates/buttons', array('t' => $tableData), true);
                                print $o;
                        } else {
                                $tableData = $this->mdb->get_tableData(uri_string());

                                if (strlen($tableData['tableData']['table_name']) > 0) {
                                        $o = '';
                                        if (file_exists('application/views/' . $u[1] . '/' . $u[2] . '_search' . '.php')) {
                                                $o .= $this->load->view($u[1] . '/' . $u[2] . '_search', array('t' => $tableData), true);
                                        }

                                        $o .= $this->load->view('templates/list', $tableData, true);
                                        print $o;
                                } else {
                                        $this->load->view($u[1] . '/' . $u[2], array('t' => $tableData));
                                }
                        }
                } else {
                        if ($u[1] == 'pages') {
                                $this->load->view($u[1] . '/' . $u[2]);
                        } else {
                                $this->ion_auth->logout();
                        }
                }
        }

        function get_full_calendar() {
                $this->load->model('full_calendar_model');
                $result_value = $this->full_calendar_model->get_event();
                print json_encode($result_value);
        }

        function get_gantt() {
                $uri = $_POST['uri'];
                $id = $_POST['id'];
                $where_id = $_POST['where_id'];
                $o = $this->mcl->get_gantt($uri, $id, $where_id);
                print json_encode($o);
        }

        function get_data() {
                $u = $this->uri->segment_array();
                if ($u[1] !== 'pages') {
                        $is_logged_in = $this->ion_auth->is_logged_in();
                } else {
                        $is_logged_in = true;
                }
                if ($is_logged_in) {
                        $aColumns = explode(',', $_GET['column_name']);

                        ////                /* Indexed column (used for fast and accurate table cardinality) */
                        $sIndexColumn = $_GET['indexColumn'];

                        ////                /* DB table to use */
                        if (strlen($_GET['view_name']) > 0) {
                                $sTable = $_GET['view_name'];
                        } else {
                                $sTable = $_GET['table_name'];
                        }

                        if (isset($_GET['where_value'])) {
                                $where_value = $_GET['where_value'];
                        } else {
                                $where_value = -1;
                        }

                        $orderBy = '';
                        if(isset($_GET['orderBy']))
                                $orderBy = $_GET['orderBy'];
                        
                        return $this->mdb->get_data_table($sTable, $aColumns, $sIndexColumn, $where_value, 'list', $orderBy);
                } else {
                        delete_cookie('ci_session');
                        $this->load->view('pages/signin');
                }
        }

        function get_data_delete() {
                $u = $this->uri->segment_array();
                if ($u[1] !== 'pages') {
                        $is_logged_in = $this->ion_auth->is_logged_in();
                } else {
                        $is_logged_in = true;
                }

                if ($is_logged_in) {
                        $aColumns = explode(',', $_GET['column_name']);

                        ////                /* Indexed column (used for fast and accurate table cardinality) */
                        $sIndexColumn = $_GET['indexColumn'];

                        ////                /* DB table to use */
                        if (strlen($_GET['view_name']) > 0) {
                                $sTable = $_GET['view_name'];
                        } else {
                                $sTable = $_GET['table_name'];
                        }

                        $where_value = $_GET['where_value'];

                        return $this->mdb->get_data_table($sTable, $aColumns, $sIndexColumn, $where_value, 'delete');
                } else {
                        delete_cookie('ci_session');
                        $this->load->view('pages/signin');
                }

        }

        function export_excel() {

                $file_name = $_POST['file_name'];
                $buffer = '';
                $headers = array();
                if (isset($_POST['headers'])) {
                        $headers = explode(',', $_POST['headers']);
                        $buffer .= '<table border=1><thead>';

                        if (count($headers) > 0) {
                                $buffer .= '<tr>';
                                foreach ($headers as $k => $v) {
                                        $buffer .= '<th>' . $this->mcl->gl($v) . '</th>';
                                }
                                $buffer .= '</tr>';
                        }
                        $buffer .= '</thead>';
                        $buffer .= $_POST['csvBuffer'];
                        $buffer .= '</table>';
                } else {
                        $buffer = $_POST['csvBuffer'];
                }

                header("Content-type: application/octet-stream"); //A MIME attachment with the content type "application/octet-stream" is a binary file.
                //Typically, it will be an application or a document that must be opened in an application, such as a spreadsheet or word processor.
                header("Content-Disposition: attachment; filename=export_data(" . $file_name . ").xls"); //with this extension of file name you tell what kind of file it is.
                header("Pragma: no-cache"); //Prevent Caching
                header("Expires: 0"); //Expires and 0 mean that the browser will not cache the page on your hard drive

                try {
                        echo $buffer;
                } catch (exception $e) {
                        
                }
        }
        
        function excel_main() {
        	
        	error_reporting(E_ALL);
        	ini_set('display_errors', TRUE);
        	ini_set('display_startup_errors', TRUE);
        	ini_set('upload_tmp_dir', '/tmp');
        	date_default_timezone_set('Asia/Bangkok');
        	
        	if (PHP_SAPI == 'cli'){
        		die('This example should only be run from a Web Browser');
        	}
        		$this->load->library('PHPExcel/Classes/PHPExcel');
        		
        		$this->cell = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        				'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
        				'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
        				'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
        				'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
        				'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ'
        		);
        		
        		$this->styleRawHeader = array(
        				'borders' => array(
        						'allborders' => array(
        								'style' => PHPExcel_Style_Border::BORDER_THIN,
        								'color' => array(
        										'argb' => 'FF000000'),
        						),
        				),
        				'alignment' => array(
        						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        				)
        		);
        		
        		$this->styleRawContent = array(
        				'borders' => array(
        						'allborders' => array(
        								'style' => PHPExcel_Style_Border::BORDER_THIN,
        								'color' => array(
        										'argb' => 'FF000000'),
        						),
        				),
        		);
        		
        		$callStartTime = microtime(true);
        		
        		$this->post_data = $this->input->post('params');
        		$this->data_array = json_decode($this->post_data, true);
        		
        		$callEndTime = microtime(true);
        		$this->time0 = $callEndTime - $callStartTime;
        		$this->rep_title = $this->input->post('rep_name') !== null ? $this->input->post('rep_name') : 'noname';
        		$this->rep_name = $this->rep_title . '.xls';
        		$this->rep_header = $this->input->post('header') !== null ? $this->input->post('header') : '';
        		
        		$this->rep_template = $this->input->post('rep_template') !== null ? $this->input->post('rep_template') : 'texmplate4.xlsx';
        		$this->objPHPExcel = new PHPExcel();
        		$this->orientation = $this->input->post('orientation') !== null ? $this->input->post('orientation') : '';
        		if (strlen($this->orientation) > 0) {
        			$this->objPHPExcel->getActiveSheet()
        			->getPageSetup()
        			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        			$this->objPHPExcel->getActiveSheet()
        			->getPageSetup()
        			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        			$this->objPHPExcel->getActiveSheet()
        			->getPageMargins()->setTop(1);
        			$this->objPHPExcel->getActiveSheet()
        			->getPageMargins()->setRight(0.75);
        			$this->objPHPExcel->getActiveSheet()
        			->getPageMargins()->setLeft(0.75);
        			$this->objPHPExcel->getActiveSheet()
        			->getPageMargins()->setBottom(1);
        		}
        		
        		if ($this->input->post('rep_template') !== null) {
        			$this->rep_template = $this->input->post('rep_template');
        			$this->excel_template();
        		} else {
        			$this->rep_template = 'template4.xlsx';
        			$this->excel_raw();
        		}
        }
        
        public function excel_template() {
        	if (!file_exists('./application/excel_template/' . $this->rep_template)) {
        		exit("Template not found.");
        	}
        	
        	// Load Template
        	$this->objPHPExcel = PHPExcel_IOFactory::load('./application/excel_template/' . $this->rep_template);
        	
        	// Get Active Sheet
        	$objWorksheet = $this->objPHPExcel->getActiveSheet();
        	
        	// Set document properties
        	$this->objPHPExcel->getProperties()->setCreator("MB")
        	->setLastModifiedBy("MB")
        	->setTitle("Office 2007 XLSX Document")
        	->setSubject("Office 2007 XLSX Document")
        	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        	->setKeywords("office 2007 openxml php")
        	->setCategory("Report template");
        	
        	// Rename worksheet
        	$objWorksheet->setTitle($this->rep_title);
        	
        	// Read Template Setting
        	//================================================
        	$headWidth = $objWorksheet->getCell('C2')->getValue();
        	$headHeight = $objWorksheet->getCell('D2')->getValue();
        	$last_column = $objWorksheet->getCell('B2')->getValue();
        	$last_column = preg_replace("/[0-9 ]/", '', $last_column);
        	
        	$startRow = $headHeight + 1;
        	$rowNo = $headHeight + 1;
        	
        	$formatRow = $objWorksheet->getCell('E2')->getValue();
        	$sumRow = $objWorksheet->getCell('F2')->getValue();
        	$useRowNo = $objWorksheet->getCell('G2')->getValue();
        	
        	//================================================
        	// Remove Setting Rows
        	//var_dump($series,$phase,$date_type_id,$start_date,$end_date,$dealer,$customer_type,$status);exit();
        	$objWorksheet->removeRow(1, 2);
        	// Insert New Row (use this method to copy format ant style from previous row)
        	$len = count($this->data_array);
        	$objWorksheet->insertNewRowBefore($rowNo + 1, $len - 1);
        	$objWorksheet->setCellValue('A1', $this->rep_header);
        	
        	//var_dump($this->date_lib->convert_to_thai_date_sec($this->end_date, 2));exit();
        	
        	$styleArray = array(
        			'borders' => array(
        					'allborders' => array(
        							'style' => PHPExcel_Style_Border::BORDER_THIN
        					)
        			)
        	);
        	$dateformat = ['search_deliverydate','search_releasedate','search_laststatusdate','search_lastcalldate'];
        	// Loop Add Content
        	$currentRow = 0;
        	if ($useRowNo == 'Yes') {
        		$rowCount = 1;
        		
        		foreach ($this->data_array as $dataCell) {
        			$objWorksheet->setCellValue($this->cell[0] . $rowNo, $rowCount);
        			
        			$index = 0;
        			foreach ($dataCell as $key => $value) {
        				$objWorksheet->setCellValue($this->cell[$index + 1] . $rowNo, $value);
        				$index++;
        			}
        			
        			$currentRow++;
        			$rowCount++;
        			$rowNo++;
        		}
        	} else {
        		foreach ($this->data_array as $dataCell) {
        			$index = 0;
        			foreach ($dataCell as $key => $value) {
        				
        				if ((strpos($key, ' Date')!== FALSE)||(in_array($key, $dateformat))){
        					if ($value !== NULL && !empty($value)) {
        						$time = strtotime($this->date_lib->date_convert_to_db($value));
        						$objWorksheet->setCellValue($this->cell[$index] . $rowNo, PHPExcel_Shared_Date::PHPToExcel($time));
        					}else {
        						$objWorksheet->setCellValue($this->cell[$index] . $rowNo, $value);
        					}
        				}else {
        					$objWorksheet->setCellValue($this->cell[$index] . $rowNo, $value);
        				}
        				$index++;
        			}
        			$currentRow++;
        			$rowNo++;
        		}
        	}
        	// Sum Row
        	if ($sumRow > 0) {
        		for ($index = 0; $index < $headWidth; $index++) {
        			if ($objWorksheet->getCell($this->cell[$index] . $rowNo)->getValue() == 'SUM') {
        				$objWorksheet->setCellValue($this->cell[$index] . $rowNo, '=SUM(' . $this->cell[$index] . $startRow . ':' . $this->cell[$index] . ($rowNo - 1) . ')');
        			}
        		}
        	}
        	$last_cell_row = $rowNo - 1;
        	// Auto size columns for each worksheet
        	foreach ($this->objPHPExcel->getWorksheetIterator() as $worksheet) {
        		
        		$this->objPHPExcel->setActiveSheetIndex($this->objPHPExcel->getIndex($worksheet));
        		
        		$sheet = $this->objPHPExcel->getActiveSheet();
        		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        		$cellIterator->setIterateOnlyExistingCells(true);
        		/** @var PHPExcel_Cell $cell */
        		foreach ($cellIterator as $cell) {
        			$sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
        		}
        	}
        	$this->objPHPExcel->getActiveSheet()->getStyle('A9:' . $last_column . $last_cell_row)->applyFromArray($styleArray);
        	unset($styleArray);
        	$this->pop_save_file();
        }

       
        function import() {

                $this->load->model("import_model");
                //$this->load->model("history_model");
                $data = json_decode($_POST["data"]);
                //var_dump($data);
                $value = $this->importCustomer($data);
        }

        function generated_otp($uTokenID) {
                // print("555555");
                $this->load->model("import_model");
                $data = array();
                $otp = "";
                $otpcode = "";
                $TokenID = "";
                if ($uTokenID != "") {
                        $data = $this->import_model->get_token($uTokenID);
                        if ($data != "") {
                                foreach ($data as $val) {
                                        $TokenID = $val["tokenid"];
                                        //  $otpcode = "7539";
                                        for ($i = 0, $otpcode = ""; $i < 4; $i++, $otpcode .= mt_rand(0, 9))
                                                ;
                                        $sta = "1";

                                        // var_dump($otp);
                                        while ($sta == "1") {
                                                $otp = $this->import_model->get_otp($otpcode);
                                                if ($otp != "") {
                                                        // $otpcode = mt_rand(4,4);
                                                        for ($i = 0, $otpcode = ""; $i < 4; $i++, $otpcode .= mt_rand(0, 9))
                                                                ;
                                                        $sta = "1";
                                                } else {
                                                        $otp = $otpcode;
                                                        $sta = "2";
                                                }

                                                //var_dump($sta);
                                        }
                                        $this->import_model->update_otp($TokenID, $otpcode);
                                        $this->import_model->update_link($val["cusid"]);
                                        $message = $this->sent_sms($val["tel"], $otp);
                                        redirect('otp/otp_home/' . $TokenID, 'location');
                                }
                        }
                }
        }

        function log_events() {
                return $this->mdb->save_log_events();
        }
        
        function sent_sms($Tel, $OTP) {

                $username = "sansilvia";
                $password = "admin50";
                $msisdn = $Tel;
                $message = "MBThaiSurvey OTP Code : " . $OTP;
                $smssender = "MBSurvey";
                $value = $this->sms_template($username, $password, $msisdn, $message, $smssender);
                return $value;
        }

        function check_otp() {
                //var_dump($_POST);
                $this->load->model("import_model");
                $value = $this->import_model->get_otp_user($_POST["OTP"], (isset($_POST["TokenID"]) ? $_POST["TokenID"] : ''));
                print $value;
        }

        function sms_template($username, $password, $msisdn, $message, $sender) {
                $url = "http://www.thaibulksms.com/sms_api.php";

                if (extension_loaded('curl')) {
                        $data = array(
                            'username' => $username,
                            'password' => $password,
                            'msisdn' => $msisdn,
                            'message' => $message,
                            'sender' => $sender);
                        $data_string = http_build_query($data);

                        $agent = "ThaiBulkSMS API PHP Client";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
                        $xml_result = curl_exec($ch);
                        $code = curl_getinfo($ch);
                        curl_close($ch);

                        if ($code['http_code'] == 200) {
                                if (function_exists('simplexml_load_string')) {
                                        $sms = new SimpleXMLElement($xml_result);
                                        $count = count($sms->QUEUE);
                                        if ($count > 0) {
                                                $count_pass = 0;
                                                $count_fail = 0;
                                                $used_credit = 0;
                                                $status = "";
                                                for ($i = 0; $i < $count; $i++) {
                                                        if ($sms->QUEUE[$i]->Status) {
                                                                $count_pass++;
                                                                $used_credit += $sms->QUEUE[$i]->UsedCredit;
                                                        } else {
                                                                $count_fail++;
                                                        }
                                                }
                                                if ($count_pass > 0) {
                                                        $msg_string = "สามารถส่งออกได้จำนวน $count_pass หมายเลข, ใช้เครดิตทั้งหมด $used_credit เครดิต";
                                                        $status = "3";
                                                        $this->import_model->insert_smslog($message, $status, $msisdn, $msg_string);
                                                }
                                                if ($count_fail > 0) {
                                                        $msg_string = "ไม่สามารถส่งออกได้จำนวน $count_fail หมายเลข";
                                                        $status = "4";
                                                        $this->import_model->insert_smslog($message, $status, $msisdn, $msg_string);
                                                }
                                        } else {
                                                $msg_string = "เกิดข้อผิดพลาดในการทำงาน, (" . $sms->Detail . ")";
                                        }
                                } else if (function_exists('xml_parse')) {
                                        $xml = sms::xml2array($xml_result);
                                        $count = count($xml['SMS']['QUEUE']);
                                        if ($count > 0) {
                                                $count_pass = 0;
                                                $count_fail = 0;
                                                $used_credit = 0;
                                                $status = "";
                                                for ($i = 0; $i < $count; $i++) {
                                                        if ($xml['SMS']['QUEUE'][$i]['Status']) {
                                                                $count_pass++;
                                                                $used_credit += $xml['SMS']['QUEUE'][$i]['UsedCredit'];
                                                        } else {
                                                                $count_fail++;
                                                        }
                                                }
                                                if ($count_pass > 0) {
                                                        $msg_string = "สามารถส่งออกได้จำนวน $count_pass หมายเลข, ใช้เครดิตทั้งหมด $used_credit เครดิต";
                                                        $status = "3";
                                                        $this->import_model->insert_smslog($message, $status, $msisdn, $msg_string);
                                                }
                                                if ($count_fail > 0) {
                                                        $msg_string = "ไม่สามารถส่งออกได้จำนวน $count_fail หมายเลข";
                                                        $status = "4";
                                                        $this->import_model->insert_smslog($message, $status, $msisdn, $msg_string);
                                                }
                                        } else {
                                                $msg_string = "เกิดข้อผิดพลาดในการทำงาน, (" . $xml['SMS']['Detail'] . ")";
                                        }
                                } else {
                                        $msg_string = "เกิดข้อผิดพลาดในการทำงาน: <br /> ระบบไม่รองรับฟังก์ชั่น XML";
                                }
                        } else {
                                //$http_codes = parse_ini_file("http_code.ini");
                                //$msg_string = "เกิดข้อผิดพลาดในการทำงาน: <br />" . $code['http_code'] . " " . $http_codes[$code['http_code']];
                                $msg_string = "เกิดข้อผิดพลาดในการทำงาน: <br />" . $code['http_code'];
                        }
                } else {
                        if (function_exists('fsockopen')) {
                                $msg_string = $this->sending_fsock($username, $password, $msisdn, $message, $sender, $ScheduledDelivery, $force);
                        } else {
                                $msg_string = "cURL OR fsockopen is not enabled";
                        }
                }
                return $msg_string;
        }

        function assign() {
                $userID = $this->ion_auth->get_userID();
                $t = $this->mdb->get_tableData('assign/assign');
                //vd::d($t);
                $data['data_user'] = $this->mdb->get_calculate();
                $data['t'] = $t;
                // $data['callstatus']['data'] = $this->mdb->get_callstatusforassign();
                //var_dump($data['callstatus']);
                $this->load->view('assign/assign', $data);
        }

        function get_total_insource() {
                $source_id = $_POST['source_id'];
                $this->mdb->get_statusassignamount($source_id);
                print json_encode($this->customer_model->get_district($province_id));
        }
        
        function get_select_data(){
                $data = $this->mdb->get_select_data();
                print json_encode($data);
        }

        function otp($id, $TokenID = '') {
                //$userID = $this->ion_auth->get_userID();
                // $_SESSION['userID'] = '1';
                // $_SESSION['groupID'] = '1';
                var_dump($_SESSION);
                die();
                $data['data_user'] = $TokenID;
                //var_dump($data);
                $this->load->view('otp/otp', $data);
        }

        function retele($TokenID = '') {

                $this->load->model("import_model");
                $value = $this->import_model->update_tele((isset($_POST["TokenID"]) ? $_POST["TokenID"] : ''));
                print $value;
        }

        function cus_info($cus_id) {
                $userID = $this->ion_auth->get_userID();
                // var_dump($cus_id);
                $data['data_user'] = $this->mdb->get_tableData('cus_info/cus_info', 'list', $cus_id);

                //die();callstatus
                //$data['data_user'] = $this->mdb->get_customerdata($cus_id);
                // vd::d($data);
                $this->load->view('cus_info/cus_info', $data);
        }

        function import_pdf() {
                $file = $_POST['file'];
                $pdf = './upload/pdf/' . $file . '.txt';

                $handle = fopen($pdf, "r");
                $a = array();
                $key = '';
                if ($handle) {
                        while (($line = fgets($handle)) !== false) {
                                // process the line read.

                                if (strpos($line, 'FieldName') !== false) {
                                        $e = explode(':', $line);
                                        $key = trim($e[1]);
                                }

                                if (strlen($key) > 0) {
                                        if (strpos($line, 'FieldValue') !== false) {
                                                $ee = explode(':', $line);
                                                $a[$key] = trim($ee[1]);
                                                $key = '';
                                        }
                                }
                        }

                        fclose($handle);
                } else {
                        
                }
                print json_encode($a);
        }

        public function importCustomer($data) {
                $n = $_POST["file_name"];
                $dataSource = array();
                $dataSource["name"] = $n;
                $source_id = $this->import_model->saveSourceName($dataSource);
                // var_dump($source_id);
                $data_success = array();

                $data_failed = array();

                $unique = array();

                foreach ($data as $k => $val) {
                        $val->source_id = $source_id;
                        $error = "0";
                        $error = "0";
                        $error_remark = "";
                        $data_cus = array();

                        $data_casehist = array();

                        ////////////////////////////////// check data
                        if (isset($val->ChassisNo)) {
                                if ($this->import_model->checkdup_chassisno($val->ChassisNo) != "") {
                                        $error = "1";

                                        $error_remark .= "ChassisNo has duplicate in system.";
                                }
                        } else {
                                $error = "1";
                                $error_remark .= "Don't have chassisno info in file excel.";
                        }
                        //var_dump($error_remark);
                        if (isset($val->Model)) {
                                if ($this->import_model->get_model($val->Model) == "") {
                                        $error = "1";
                                        $error_remark .= ($error_remark != "") ? " ," : "";
                                        $error_remark .= "Don't have model name in system.";
                                }
                        } else {
                                $error = "1";
                                $error_remark .= "Don't have model info in file excel.";
                        }

                        if (isset($val->Dealer)) {
                                if ($this->import_model->get_dealer($val->Dealer) == "") {
                                        $error = "1";
                                        $error_remark .= ($error_remark != "") ? " ," : "";
                                        $error_remark .= "Don't have dealer name in system.";
                                }
                        } else {
                                $error = "1";
                                $error_remark .= "Don't have dealer info in file excel.";
                        }

                        if (isset($val->SaleName)) {
                                if ($this->import_model->get_sales($val->SaleName) == "") {
                                        $error = "1";
                                        $error_remark .= ($error_remark != "") ? " ," : "";
                                        $error_remark .= "Don't have sale name in system.";
                                }
                        } else {
                                $error = "1";
                                $error_remark .= "Don't have sale info in file excel.";
                        }

                        if ($error == "0") {

                                $data_cus["sourceid"] = $source_id;
                                if (isset($val->TitleName)) {
                                        $data_cus["titlename"] = $val->TitleName;
                                }if (isset($val->FName)) {
                                        $data_cus["fname"] = $val->FName;
                                }if (isset($val->LName)) {
                                        $data_cus["lname"] = $val->LName;
                                }if (isset($val->Company)) {
                                        $data_cus["company"] = $val->Company;
                                }if (isset($val->Tel)) {
                                        $data_cus["tel"] = $val->Tel;
                                }if (isset($val->Address)) {
                                        $data_cus["address"] = $val->Address;
                                }if (isset($val->Road)) {
                                        $data_cus["road"] = $val->Road;
                                }if (isset($val->SubDistrict)) {
                                        $data_cus["subdistrict"] = $val->SubDistrict;
                                }if (isset($val->District)) {
                                        $data_cus["district"] = $val->District;
                                }if (isset($val->Province)) {
                                        $data_cus["province"] = $val->Province;
                                }if (isset($val->Zip)) {
                                        $data_cus["zip"] = $val->Zip;
                                }if (isset($val->NameSent)) {
                                        $data_cus["namesent"] = $val->NameSent;
                                }if (isset($val->AddressOther)) {
                                        $data_cus["addressother"] = $val->AddressOther;
                                }if (isset($val->Model)) {
                                        $data_cus["model_id"] = $this->import_model->get_model($val->Model);
                                }if (isset($val->EngineNo)) {
                                        $data_cus["engineno"] = $val->EngineNo;
                                }if (isset($val->ChassisNo)) {
                                        $data_cus["chassisno"] = $val->ChassisNo;
                                }if (isset($val->Dealer)) {
                                        $data_cus["dealer_id"] = $this->import_model->get_dealer($val->Dealer);
                                }if (isset($val->SaleName)) {
                                        $data_cus["sale_id"] = $this->import_model->get_sales($val->SaleName);
                                }if (isset($val->DeliveryDate)) {

                                        //$pieces = explode("/", $val->DeliveryDate);
                                        //$new_date = date("Y-m-d",strtotime($pieces[2]."-".$pieces[1]."-".$pieces[0]));
                                        $new_date = date("Y-m-d", strtotime($val->DeliveryDate));
                                        //
                                        // var_dump($new_date);
                                        $data_cus["deliverydate"] = $new_date;
                                }
                                $data_cus["call_status_id"] = '1';

                                $data_cus["call_sub_status_id"] = '1';
                                if (isset($val->OwnerCar)) {
                                        $data_cus["ownercar"] = $val->OwnerCar;
                                }if (isset($val->CarUser)) {
                                        $data_cus["caruser"] = $val->CarUser;
                                }
                                if (isset($val->Ageing)) {
                                        $data_cus["ageing"] = $val->Ageing;
                                }
                                if (isset($val->Gendor)) {
                                        $data_cus["gendor"] = $val->Gendor;
                                }
                                if (isset($val->PeriodTime)) {
                                        $data_cus["periodtime"] = $val->PeriodTime;
                                }
                                if (isset($val->flag_input)) {
                                        $data_cus["flag_input"] = $val->flag_input;
                                }
                                if (isset($val->Remark)) {
                                        $data_cus["remark"] = $val->Remark;
                                }
                                if (isset($val->flag_csi)) {
                                        $data_cus["flag_csi"] = $val->flag_csi;
                                }
                                if (isset($val->flag_cli)) {
                                        $data_cus["flag_cli"] = $val->flag_cli;
                                }
								if(isset($val->phase)){
									$data_cus["phase"] = $val->phase;
								}
                                //print_r($data);
                                if (isset($val->Email)) {
                                        $data_cus["email"] = str_replace(" ", "", $val->Email);
                                }
								
								
                                $cus_id = "";
                                $cus_id = $this->import_model->saveCustomer($data_cus);

                                //insert case history
                                if ($cus_id != "") {

                                        $data_casehist["cusid"] = $cus_id;

                                        $data_casehist["statusid"] = 1;

                                        $case_idhist = $this->import_model->savestatushistory($data_casehist);
                                } else {
                                        print("Error case history");
                                }
                        } else {
                                $data_cus["sourceid"] = $source_id;
                                if (isset($val->TitleName)) {
                                        $data_cus["titlename"] = $val->TitleName;
                                } if (isset($val->FName)) {
                                        $data_cus["fname"] = $val->FName;
                                } if (isset($val->LName)) {
                                        $data_cus["lname"] = $val->LName;
                                } if (isset($val->Company)) {
                                        $data_cus["company"] = $val->Company;
                                } if (isset($val->Tel)) {
                                        $data_cus["tel"] = $val->Tel;
                                } if (isset($val->address)) {
                                        $data_cus["address"] = $val->address;
                                } if (isset($val->Road)) {
                                        $data_cus["road"] = $val->Road;
                                } if (isset($val->SubDistrict)) {
                                        $data_cus["subdistrict"] = $val->SubDistrict;
                                } if (isset($val->District)) {
                                        $data_cus["district"] = $val->District;
                                } if (isset($val->Province)) {
                                        $data_cus["province"] = $val->Province;
                                } if (isset($val->Zip)) {
                                        $data_cus["zip"] = $val->Zip;
                                } if (isset($val->NameSent)) {
                                        $data_cus["namesent"] = $val->NameSent;
                                } if (isset($val->AddressOther)) {
                                        $data_cus["addressother"] = $val->AddressOther;
                                } if (isset($val->Model)) {
                                        $data_cus["model"] = $val->Model;
                                } if (isset($val->EngineNo)) {
                                        $data_cus["engineno"] = $val->EngineNo;
                                } if (isset($val->ChassisNo)) {
                                        $data_cus["chassisno"] = $val->ChassisNo;
                                } if (isset($val->Dealer)) {
                                        $data_cus["dealer"] = $val->Dealer;
                                } if (isset($val->SaleName)) {
                                        $data_cus["salename"] = $val->SaleName;
                                } if (isset($val->DeliveryDate)) {

                                        $pieces = explode("/", $val->DeliveryDate);
                                        $new_date = date("Y-m-d", strtotime($pieces[2] . "-" . $pieces[1] . "-" . $pieces[0]));
                                        //var_dump($new_date);
                                        $data_cus["deliverydate"] = $new_date;
                                }
                                $data_cus["call_status_id"] = '1';

                                $data_cus["call_sub_status_id"] = '1';
                                if (isset($val->OwnerCar)) {
                                        $data_cus["ownercar"] = $val->OwnerCar;
                                } if (isset($val->CarUser)) {
                                        $data_cus["caruser"] = $val->CarUser;
                                }
                                if (isset($val->Ageing)) {
                                        $data_cus["ageing"] = $val->Ageing;
                                }
                                if (isset($val->PeriodTime)) {
                                        $data_cus["periodtime"] = $val->PeriodTime;
                                }
                                if (isset($val->flag_input)) {
                                        $data_cus["flag_input"] = $val->flag_input;
                                }
                                if (isset($val->Remark)) {
                                        $data_cus["remark"] = $val->Remark;
                                }
                                if (isset($val->flag_csi)) {
                                        $data_cus["flag_csi"] = $val->flag_csi;
                                }
                                if (isset($val->flag_cli)) {
                                        $data_cus["flag_cli"] = $val->flag_cli;
                                }
								if(isset($val->phase)){
									$data_cus["phase"] = $val->phase;
								}
                                //print_r($data);
                                if (isset($val->email)) {
                                        $data_cus["email"] = str_replace(" ", "", $val->email);
                                }
                                if (isset($val->gendor)) {
                                        $data_cus["gendor"] = $val->gendor;
                                }
								

                                $data_cus["error_remark"] = $error_remark;


                                $cus_id = "";
                                $cus_id = $this->import_model->saveCustomer_failed($data_cus);
                        }
                        //print("5555");
                }
                //print($source_id);
                $data_success = $this->import_model->get_datasuccess($source_id);
                $table_success = "";
                if (!empty($data_success)) {

                        $table_success = "";

                        foreach ($data_success as $f => $val) {
                                $table_success .= "
						<div class='col-lg-4'>

					        <div class='panel'>
					            <div class='panel-body text-center bg-danger' style='padding: 75px 20px;'>
					                <i class='fa fa-file fa-5x'></i>
					            </div>
					            <div class='pad-all text-center'>
					                <p class='text-semibold text-lg mar-no text-main' style='font-size: 20px;'>Total</p>
					                <p class='h2 text-thin mar-no' style='padding: 22px 0px; font-size: 45px;'>" . $val->total . "</p>

					            </div>
					        </div>


					    </div>";
                                $table_success .= "
						<div class='col-lg-4'>

					        <div class='panel'>
					            <div class='panel-body text-center bg-danger' style='padding: 75px 20px;'>
					                <i class='fa fa-envelope-o fa-5x'></i>
					            </div>
					            <div class='pad-all text-center'>
					                <p class='text-semibold text-lg mar-no text-main' style='font-size: 20px;'>Online</p>
					                <p class='h2 text-thin mar-no' style='padding: 22px 0px; font-size: 45px;'>" . $val->online_ . "</p>

					            </div>
					        </div>


					    </div>";
                                $table_success .= "
						<div class='col-lg-4'>

					        <div class='panel'>
					            <div class='panel-body text-center bg-danger' style='padding: 75px 20px;'>
					                <i class='fa fa-phone-square fa-5x'></i>
					            </div>
					            <div class='pad-all text-center'>
					                <p class='text-semibold text-lg mar-no text-main' style='font-size: 20px;'>Tele</p>
					                <p class='h2 text-thin mar-no' style='padding: 22px 0px; font-size: 45px;'>" . $val->tele . "</p>

					            </div>
					        </div>


					    </div>";
                        }
                }


                $data_failed = $this->import_model->get_datafailed($source_id);
                $table_failed = "";
                if (!empty($data_failed)) {

                        $table_failed = "<div  class='col-md-12'><h3>Failed</h3><table id='tb_f' class='display table2excel table datatable'>";
                        $table_failed .= @" <thead>
                                           <tr>
                                                   <th class='text-center'>Customer </th>
                                                   <th>Chassisno</th>
                                                   <th>Remark</th>
                                           </tr>
                                   </thead>";

                        //print_r($data_failed);
                        $table_failed .= "<tbody>";
                        foreach ($data_failed as $f => $val) {
                                $table_failed .= "<tr>";
                                $table_failed .= "<td>" . $val->fname . ' ' . $val->lname;
                                $table_failed .= "</td>";
                                $table_failed .= "<td>" . $val->chassisno;
                                $table_failed .= "</td>";
                                $table_failed .= "<td>" . $val->error_remark;
                                $table_failed .= "</td>";

                                $table_failed .= "</tr>";
                        }
                        $table_failed .= "</tbody>";


                        $table_failed .= "</table></div>";
                }

                //  print_r($this->import_model->get_data($source_id));
                $value = "";
                print_r($table_success);
                print_r($table_failed);
                if (!empty($table_success) && !empty($table_failed)) {

                        $value = $table_success . '<br/>' . $table_failed; //array_merge($data_success, $data_failed);
                        // $this->savefailed($data_failed);
                } else if (!empty($table_success)) {
                        $value = $table_success;
                } else {
                        $value = $table_failed;
                }
                return $value;
        }

        function export_pdf() {
                ini_set("memory_limit", "256M");
                include "./assets/lib/mpdf/mpdf.php";
                $buffer = '';

                if (isset($_POST['headers'])) {
                        $buffer .= '<table border=1 width=100%>';
                        $headers = explode(',', $_POST['headers']);
                        $buffer .= '<thead><tr>';
                        foreach ($headers as $k => $v) {
                                $buffer .= '<th>' . $this->mcl->gl(trim($v)) . '</th>';
                        }
                        $buffer .= '</tr></thead>';
                        $buffer .= $_POST['csvBuffer'];
                        $buffer .= '</table>';
                } else {
                        $buffer .= $_POST['csvBuffer'];
                }

                if (isset($_POST['orientation'])) {
                        $orientation = strtoupper($_POST['orientation']);
                } else {
                        $orientation = 'P';
                }
                $css = file_get_contents('./assets/css/app_excel.css');
                $mpdf = new mPDF('', 'A4');

                $mpdf->SetAutoFont();
//                $mpdf->SetDisplayMode('fullwidth');

                $mpdf->AddPage($orientation);
                $mpdf->WriteHTML($css, 1);
                $mpdf->WriteHTML($buffer);
                $mpdf->setFooter('Date: {DATE j-m-Y}  Pages: {PAGENO}/ {nb}');
                $mpdf->Output();
                exit();
        }

        function upload_now() {
               
                $file = '';
                $a = array();
                foreach ($_POST as $k => $v) {
                        if (strpos($k, 'document_original_file') !== false) {
                                array_push($a, $k);
                        }
                }

                foreach ($a as $k => $v) {
                        $search = str_replace('document_original_file', 'document_file', $v);
                        if (!isset($_POST[$search])) {
                                if (strpos($v, 'document_original_file') !== false) {

                                        $file = str_replace('document_original_file', 'document_file', $v);
                                }
                        }
                }

                $upload_dir = $this->config->item('upload_dir');

                $upload_dir = $upload_dir[$_POST['uri']];

                $extension = $this->config->item('extension');
                $extension = explode(',', $extension[$_POST['uri']]);

                if (strlen($file) > 0) {
                        // A list of permitted file extensions
                        $allowed = $extension; //array('png', 'jpg', 'gif', 'zip','xls','xlsx');

                        if (isset($_FILES[$file]) && $_FILES[$file]['error'] == 0) {

                                $extension = pathinfo($_FILES[$file]['name'], PATHINFO_EXTENSION);
                                if (!in_array(strtolower($extension), $allowed)) {
                                        echo '{"status":"error"}';
                                        exit;
                                }
                                $old_name = $_FILES[$file]['name'];
                                $e = explode('.', $old_name);
                                $fn = $e[0];
                                $ext = $e[count($e) - 1];
                                $new_name = date('YmdHis') . '.' . $extension;
                                if (move_uploaded_file($_FILES[$file]['tmp_name'], '.' . $upload_dir . '/' . $new_name)) {
//                                                echo '{"status":"success"}';
                                        echo $new_name . ',' . $old_name;
                                        exit;
                                }
                        }
                }

//                echo '{"status":"error"}';
                echo ',';
                exit;
        }

        function upload() {
                $upload_dir = $this->config->item('upload_dir');

                if (isset($_POST['table_name'])) {
                        $upload_dir = $upload_dir[$_POST['table_name']];
                } else {
                        if (isset($_POST['upload_dir'])) {
                                $upload_dir = $_POST['upload_dir'];
                        }
                }

                $upload_name = $_GET['upload_name'];

                $file_save_name = $_GET['file_save_name'];

                $orignial_file_name = $_GET['original_file_name'];

                $sFileName = $_FILES[$upload_name]['name'];
                $sFileType = $_FILES[$upload_name]['type'];
                $sFileSize = $this->bytesToSize1024($_FILES[$upload_name]['size'], 1);

                $ext = strtolower(pathinfo($sFileName, PATHINFO_EXTENSION));
//var_dump($_FILES);
                if (file_exists("." . $upload_dir . "/" . $_FILES[$upload_name]["name"])) {
                        if (strlen($_FILES[$upload_name]["name"]) > 0) {
                                echo $_FILES[$upload_name]["name"] . " already exists. ";
                        } else {
                                echo '';
                        }
                } else {
                        if (move_uploaded_file($_FILES[$upload_name]["tmp_name"], '.' . $upload_dir . "/" . $file_save_name . "." . $ext)) {
                                //$_FILES["upload_file"]["name"]);
                                echo $file_save_name . "." . $ext . ',' . $_FILES[$upload_name]['name'];
                        } else {
                                echo 'error';
                        }
                }
        }

        function bytesToSize1024($bytes, $precision = 2) {
                $unit = array(
                    'B',
                    'KB',
                    'MB');
                return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), $precision) .
                        ' ' . $unit[$i];
        }

        function get_page_404() {
                $a = array('error' => $this->mcl->generate_404());
                $this->load->view('templates/error', $a);
        }

        function get_page_500() {
                $a = array('error' => $this->mcl->generate_500());
                $this->load->view('templates/error', $a);
        }

        function undelete_data() {
                //                if ($this->input->is_ajax_request()) {
                $u = $this->uri->segment_array();
                if ($u[1] !== 'pages') {
                        $is_logged_in = $this->ion_auth->is_logged_in();
                } else {
                        $is_logged_in = true;
                }

                if ($is_logged_in) {
                        $u = explode('/', $_POST['uri']);
                        $u = $u[0] . '/' . $u[1];
                        $t = $this->mdb->get_table_name($u);
                        $_POST['table_name'] = $t['table_name'];
                        $_POST['indexColumn'] = $t['indexColumn'];
                        return $this->mdb->undelete_data();
                } else {
                        return '';
                }
                //                } else {
                //                        die('No Direct Access.');
                //                }
        }

        function delete_data() {
                if ($this->input->is_ajax_request()) {
                        $u = $this->uri->segment_array();
                        if ($u[1] !== 'pages') {
                                $is_logged_in = $this->ion_auth->is_logged_in();
                        } else {
                                $is_logged_in = true;
                        }
                        if ($is_logged_in) {
                                $t = $this->mdb->get_table_name(str_replace('/edit', '', $_POST['uri']));
                                $_POST['table_name'] = $t['table_name'];
                                $_POST['indexColumn'] = $t['indexColumn'];
                                return $this->mdb->delete_data();
                        } else {
                                return '';
                        }
                } else {
                        die('No Direct Access.');
                }
        }

        function save_sub_data() {
                if ($this->input->is_ajax_request()) {
                        $u = $this->uri->segment_array();

                        if ($u[1] !== 'pages') {
                                $is_logged_in = $this->ion_auth->is_logged_in();
                        } else {
                                $is_logged_in = true;
                        }

                        if ($is_logged_in && isset($_POST['uri'])) {
                                $t = $this->mdb->get_table_name($_POST['uri']);
                                $s = $this->mdb->get_schema($t['table_name']);
                                $indexColumn = $_POST['indexColumn'];
                                $id = $_COOKIE['mid'];
                                $result = $this->mdb->save_sub_data($s, $t, $indexColumn, $id);
                                print json_encode($result);
                        } else {
                                return '';
                        }
                } else {
                        die('No Direct Access.');
                }
        }

        function save_data() {

                if ($this->input->is_ajax_request()) {
                        $u = $this->uri->segment_array();

                        if ($u[1] !== 'pages') {
                                $is_logged_in = $this->ion_auth->is_logged_in();
                        } else {
                                $is_logged_in = true;
                        }
                        if ($is_logged_in && isset($_POST['uri'])) {
                                if (isset($_POST['uri'])) {
                                        $uri = $_POST['uri'];

                                        $a = explode('/', $uri);
                                        $fo = $a[0];
                                        $fc = $a[1];
                                }
                                
                                $t = $this->mdb->get_table_name($fo . '/' . $fc);
                                $s = $this->schema->get_schema($t['table_name']);
                                $result = $this->mdb->save_data($s, $t);
                                
                                print json_encode($result);
                        } else {
                                return '';
                        }
                } else {
                        die('No Direct Access.');
                }
        }

        function save_data_custom() {
                if ($this->input->is_ajax_request()) {
                        $u = $this->uri->segment_array();
                        if ($u[1] !== 'pages') {
                                $is_logged_in = $this->ion_auth->is_logged_in();
                        } else {
                                $is_logged_in = true;
                        }
                        if (isset($_POST['uri'])) {
                                $uri = $_POST['uri'];

                                $a = explode('/', $uri);
                                $fo = $a[0];
                                $fc = $a[1];
                                $fc = str_replace('_eng', '', $fc);
                        }

                        if (file_exists('application/libraries/' . ucfirst($fc) . '_lib.php')) {
                                $lib = $fc . "_lib";
                                $this->load->library($lib);
                        } else if (file_exists('application/libraries/addon/' . ucfirst($fc) . '_lib.php')) {
                                $lib = $fc . "_lib";
                                $this->load->library('addon/' . $lib);
                        }

                        $result = $this->$lib->save_custom_data();

                        print json_encode($result);
                } else {
                        die('No Direct Access.');
                }
        }

        function get_group_function() {
                $groupID = $_POST['groupID'];
                $output = $this->mdb->get_group_function($groupID);
                print($output);
        }

        function create_new_term() {
                if ($this->input->is_ajax_request()) {
                        $is_logged_in = $this->ion_auth->is_logged_in();
                        if ($is_logged_in && isset($_POST['uri'])) {

                                $page_history = $this->input->cookie('page_history');
                                if (strpos($page_history, $_POST['from_uri']) === false) {
                                        $page_history .= ',' . substr($_POST['from_uri'], 1);
                                }
                                if (strpos($page_history, $_POST['uri']) === false) {
                                        $page_history .= ',' . $_POST['uri'];
                                }

                                if (substr($page_history, 0, 1) == ',') {
                                        $page_history = substr($page_history, 1);
                                }

                                $_SESSION['page_history'] = $page_history;
                                $_SESSION['page_history_field_name'] = $_POST['newTerm'];
                                //$this->input->set_cookie('page_history', $page_history, time() + 3600 * 24 * 30);
                                //$this->input->set_cookie('page_history_field_name', $_POST['newTerm'], time() + 3600 * 24 * 30);
                        } else {
                                return '';
                        }
                } else {
                        die('No Direct Access.');
                }
        }

        function save_new_term() {
                if ($this->input->is_ajax_request()) {
                        $is_logged_in = $this->ion_auth->is_logged_in();

                        //                        print($is_logged_in);
                        if ($is_logged_in && isset($_POST['uri'])) {
                                $t = $this->mdb->get_table_name($_POST['uri']);
                                $s = $this->mdb->get_schema($t['table_name']);
                                $result = $this->mdb->save_data($s, $t);

                                print json_encode($result);
                        } else {
                                return '';
                        }
                } else {
                        die('No Direct Access.');
                }
        }

        function get_refresh_file() {
                $this->load->model('download_model');
                $result = $this->download_model->get_refresh_file();
                print json_encode($result);
        }

        function get_depth_level() {
                $table_name = $_POST['table_name'];
                $val = $_POST['val'];
                $parent_column = $_POST['parent_column'];
                return $this->mdb->get_depth_level($table_name, $val, $parent_column);
        }

        function message_ajax() {
                if (isset($_POST['update'])) {
                        $uid = $this->ion_auth->get_userID();
                        $update = $this->db->escape_str($_POST['update']);
                        $uploads = $_POST['uploads'];
                        $data = $this->wall_updates->Insert_Update($uid, $update, $uploads);

                        if ($data) {
                                $msg_id = $data['msg_id'];
                                $orimessage = $data['message'];
                                $message = $this->mcl->tolink($this->mcl->htmlcode($data['message']));
                                $time = $data['created'];
                                $mtime = date("c", $time);
                                $uid = $data['uid_fk'];
                                $username = $data['username'];
                                // User Avatar
                                if (isset($gravatar)) {
                                        $face = $this->wall_updates->Gravatar($uid);
                                } else {
                                        $face = $this->wall_updates->Profile_Pic($uid);
                                }

                                // End Avatar
                                ?>
                                <div class="stbody" id="stbody<?= $msg_id; ?>">
                                        <div class="stimg">
                                                <img src="<?= $face; ?>" class='big_face' alt='<?= $username; ?>'/>
                                        </div>
                                        <div class="sttext">
                                                <a class="stdelete" href="#" id="<?= $msg_id; ?>" title='Delete Update'></a>
                                                <b><a href="<?= base_url() . $username; ?>"><?= $username; ?></a></b> <?= $this->mcl->clear($message); ?>
                                                <?php
                                                if ($uploads) {
                                                        echo "<div style='margin-top:10px'>";
                                                        $uploads_array = explode(',', $uploads);
                                                        $uploads = implode(',', array_unique($uploads_array));
                                                        $s = explode(",", $uploads);
                                                        foreach ($s as $a) {
                                                                $newdata = $this->wall_updates->Get_Upload_Image_Id($a);
                                                                if ($newdata) {
                                                                        echo "<img src='uploads/" . $newdata['image_path'] . "' class='imgpreview'/>";
                                                                }
                                                        }
                                                        echo "</div>";
                                                }
                                                ?>
                                                <div class="sttime"><a href='#' class='commentopen' id='<?= $msg_id; ?>' title='Comment'>Comment </a> | <a href='<?= base_url() ?>status/<?= $msg_id; ?>' class="timeago" title='<?= $mtime; ?>'></a>
                                                </div>
                                                <div id="stexpandbox">
                                                        <div id="stexpand">
                                                                <?
                                                                if ($this->mcl->textlink($orimessage)) {
                                                                        $link = $this->mcl->textlink($orimessage);
                                                                        echo Expand_URL($link);
                                                                }
                                                                ?>

                                                        </div>
                                                </div>
                                                <div class="commentcontainer" id="commentload<?= $msg_id; ?>">

                                                </div>
                                                <div class="commentupdate" style='display:none' id='commentbox<?= $msg_id; ?>'>
                                                        <div class="stcommentimg">
                                                                <img src="<?= $face; ?>" class='small_face'/>
                                                        </div>
                                                        <div class="stcommenttext" >
                                                                <form method="post" action="">
                                                                        <textarea name="comment" class="comment" maxlength="200"  id="ctextarea<?= $msg_id; ?>"></textarea>
                                                                        <br />
                                                                        <input type="submit"  value=" Comment "  id="<?= $msg_id; ?>" class="comment_button button"/>
                                                                </form>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                                <?
                        }
                }
        }

        function timeline_message_ajax() {

                $o = '';
                if (isset($_POST['update']) && isset($_POST['uid'])) {
                        $update = $this->db->escape_str($_POST['update']);
                        $uid = $_POST['uid'];
                        if (isset($_POST['document_file'])) {
                                $document_file = $_POST['document_file'];
                        } else {
                                $document_file = '';
                        }

                        if (isset($_POST['document_original_file'])) {
                                $document_original_file = $_POST['document_original_file'];
                        } else {
                                $document_original_file = '';
                        }

                        $uploads = array();

                        if (strlen($document_file) > 0) {
                                $query = mysql_query("insert into t_wall_user_uploads (image_path,uid_fk,document_file, document_original_file,upload_type_id) values('$document_file' ,'$uid','$document_file','$document_original_file','1')") or
                                        die(mysql_error());
                                $ids = mysql_insert_id();
                                array_push($uploads, $ids);
                        } else {
                                $uploads = '';
                        }
                        $data = $this->wall_updates->Insert_Update($uid, $update, $uploads);

                        if ($data) {
                                $msg_id = $data['msg_id'];
                                $orimessage = $data['message'];
                                $message = $this->mcl->tolink($this->mcl->htmlcode($data['message']));
                                $time = $data['created'];
                                $uid = $data['uid_fk'];
                                $username = $data['username'];
                                $file_id = $data['uploads'];

// User Avatar
                                if (isset($gravatar)) {
                                        $face = $this->wall_updates->Gravatar($uid);
                                } else {
                                        $face = $this->wall_updates->Profile_Pic($uid);
                                }

// End Avatar
                                //                                $o.='<div class="item " id="item' . $msg_id . '">';
                                //                                $o.='<a href="#" class="deletebox" id="' . $msg_id . '" title="Delete Update">X</a>';
                                $o .= '<div class="stbody" id="stbody' . $msg_id . '">';
                                $o .= '<div class="stimg">';
                                $o .= '<img src="' . $face . '" class="time_face" alt="' . $username . '" />';
                                $o .= '</div>';
                                $o .= '<div class="sttext">';
                                $o .= '<a id="' . $msg_id .
                                        '" class="stdelete" href="#" original-title="Delete Update"><i class="fa fa-trash-o fa-2x"></i></a>';
                                $o .= '<b><a href="' . base_url() . $username . '">' . $username . '</a></b>' .
                                        $this->mcl->clear($message);

                                $file = $this->wall_updates->Get_Upload_Image_Id($file_id);
                                $file = $file['image_path'];
                                if (strlen($file) > 0) {
                                        $ext = end(explode('.', $file));
                                        $o .= "<div style='margin-top:10px'>";
                                        if (in_array($ext, array(
                                                    IMAGETYPE_GIF,
                                                    IMAGETYPE_JPEG,
                                                    IMAGETYPE_PNG))) {
                                                $o .= "<a href='/downloads/downloads?file=wall/" . $file .
                                                        "' rel='facebox'><img src='./upload/wall/" . $file .
                                                        "' class='imgpreview' /></a>";
                                        } else {
                                                $o .= "<i class='fa fa-file-o'></i>&nbsp;<a href='/downloads/downloads?file=wall/" .
                                                        $file . "'>" . $file . "</a>";
                                        }
                                        $o .= "</div>";
                                }
                                $o .= '<div class="sttime">';
                                $o .= '<a id="' . $msg_id .
                                        '" class="commentopen" title="Comment" href="#">Comment </a>';
                                $o .= '|';
                                $o .= $this->mlr->time_stamp($time) . '</a>';
                                $o .= '</div>';
                                //                                $o.='<div class="sttime">' . $this->mlr->time_stamp($time) . '| <a href="#" class="commentopen" id="' . $msg_id . '" title="Comment">Comment </a></div> ';
                                $o .= '<div id="stexpandbox">';
                                $o .= '<div id="stexpand">';

                                if ($this->mcl->textlink($orimessage)) {
                                        $link = $ths->mcl->textlink($orimessage);
                                        $o .= $this->mlr->Expand_URL($link);
                                }

                                $o .= '</div>';
                                $o .= '</div>';
                                $o .= '<div class="commentcontainer" id="commentload' . $msg_id . '">';

                                $o .= '</div>';
                                $o .= '<div class="commentupdate" style="display:none" id="commentbox' . $msg_id .
                                        '">';
                                $o .= '<div class="stcommentimg">';
                                $o .= '<img src="' . $face . '" class="small_face"/>';
                                $o .= '</div>';
                                $o .= '<div class="stcommenttext" >';
                                //                                $o.='<form method="post" action="">';
                                $o .= '<textarea name="comment" class="comment" maxlength="200"  id="ctextarea' .
                                        $msg_id . '"></textarea>';
                                $o .= '<br />';
                                $o .= '<div  id="cimageupload' . $msg_id . '" class="cimageupload">';
                                $o .= $this->mcl->generate_single_fileinput('Document File', 'document_file_' .
                                        $msg_id, false, '', ''); //label, name, is_disable, validation, value);
                                $o .= '</div>';
                                $o .= '<input type="submit"  value=" Comment "  id="' . $msg_id .
                                        '" class="comment_button btn btn-xs btn-primary"/>';
                                $o .= '<span style="float:right">';
                                $o .= '<a href="javascript:void(0);" id="' . $msg_id .
                                        '" class="ccamera" title="Upload PDF File"><i class="fa fa-cloud-upload fa-2x"></i></a> ';
                                $o .= '</span>';
                                //                                $o.='</form>';
                                $o .= '</div>';
                                $o .= '</div>';
                                $o .= '</div>';
                                $o .= '</div>';
                                //                                $o.='</div>';
                        }
                }
                print $o;
        }

        function view_comment_ajax() {

                if (isset($_POST['msg_id'])) {
                        $msg_id = $this->db->escape_str($_POST['msg_id']);
                        $x = 0;
                        $uid = trim($_POST['uid']);
                        $o = $this->mcl->load_comments($msg_id, $uid);
                        print $o;
                }
        }

        function timeline_moreupdates_ajax() {

                if (isset($_POST['lastid'])) {
                        $lastid = $this->db->escape_str($_POST['lastid']);
//                        $profile_uid = $this->db->escape_str($_POST['profile_uid']);
                        $o = $this->mcl->timeline_load_message($lastid);
                        print $o;
                }
        }

        function comment_ajax() {

                if (isset($_POST['comment'])) {
                        $comment = $this->db->escape_str($_POST['comment']);
                        $msg_id = trim($_POST['msg_id']);

//                                $uid = trim($_POST['uid']);

                        $uid = $this->ion_auth->get_userID();

                        $ip = $_SERVER['REMOTE_ADDR'];

                        if (isset($_POST['document_file_' . $msg_id])) {
                                $document_file = $_POST['document_file_' . $msg_id];
                        } else {
                                $document_file = '';
                        }

                        if (isset($_POST['document_original_file_' . $msg_id])) {
                                $document_original_file = $_POST['document_original_file_' . $msg_id];
                        } else {
                                $document_original_file = '';
                        }

                        $uploads = array();

                        if (strlen($document_file) > 0) {
                                $sql = "insert into t_wall_user_uploads (image_path,uid_fk,document_file, document_original_file,upload_type_id) values('$document_file' ,'$uid','$document_file','$document_original_file','2')";
                                $query = mysql_query($sql) or die(mysql_error());
                                $ids = mysql_insert_id();
                                array_push($uploads, $ids);
                        } else {
                                $uploads = '';
                        }

                        $cdata = $this->wall_updates->Insert_Comment($uid, $msg_id, $comment, $ip, $uploads);
                        $o = '';

                        if ($cdata) {
                                $com_id = $cdata['com_id'];
                                $comment = $this->mcl->tolink(htmlentities($cdata['comment']));
                                $time = $cdata['created'];
                                $mtime = date("c", $time);
                                $username = $cdata['username'];
                                $uid = $cdata['uid_fk'];
                                $file_id = $cdata['uploads'];
// User Avatar
                                if (isset($gravatar)) {
                                        $cface = $this->wall_updates->Gravatar($uid);
                                } else {
                                        $cface = $this->wall_updates->Profile_Pic($uid);
                                }

// End Avatar

                                $o .= '<div class="stcommentbody" id="stcommentbody' . $com_id . '">';
                                $o .= '<div class="stcommentimg">';
                                $o .= '<img src="' . $cface . '" class="small_face" alt="' . $username . '"/>';
                                $o .= '</div> ';

                                $o .= '<div class="stcommenttext">';
                                $o .= '<a class="stcommentdelete" href="#" id="' . $com_id .
                                        '"><i class="fa fa-trash-o fa-2x"></i></a>';
                                $o .= '<b><a href="' . base_url() . $username . '">' . $username . '</a></b>' .
                                        $this->mcl->clear($comment);

                                $file = $this->wall_updates->Get_Upload_Image_Id($file_id);
                                $file = $file['image_path'];
                                if (strlen($file) > 0) {
                                        $ext = end(explode('.', $file));
                                        $o .= "<div style='margin-top:10px'>";
                                        if (in_array($ext, array(
                                                    IMAGETYPE_GIF,
                                                    IMAGETYPE_JPEG,
                                                    IMAGETYPE_PNG))) {
                                                $o .= "<a href='/downloads/downloads?file=wall/" . $file .
                                                        "' rel='facebox'><img src='./upload/wall/" . $file .
                                                        "' class='imgpreview' /></a>";
                                        } else {
                                                $o .= "<i class='fa fa-file-o'></i>&nbsp;<a href='/downloads/downloads?file=wall/" .
                                                        $file . "'>" . $file . "</a>";
                                        }
                                        $o .= "</div>";
                                }

                                $o .= '<div class="stcommenttime" title="' . $mtime . '"></div>';
                                $o .= '</div>';
                                $o .= '</div>';
                                $o .= '<div  id="cimageupload' . $msg_id . '" class="cimageupload">';
                                $o .= $this->mcl->fi('Document File', 'document_file_' .
                                        $msg_id, false, '', ''); //label, name, is_disable, validation, value);
                                $o .= '</div>';
                        }
                        print $o;
                }
        }

        function delete_comment_ajax() {
                if (isset($_POST['com_id'])) {
                        $uid = $this->ion_auth->get_userID();
                        $com_id = $_POST['com_id'];
                        $data = $this->wall_updates->Delete_Comment($uid, $com_id);
                        echo $data;
                }
        }

        function delete_message_ajax() {
                if (isset($_POST['msg_id'])) {
                        $msg_id = $this->db->escape_str($_POST['msg_id']);
                        $uid = $this->ion_auth->get_userID();
                        $data = $this->wall_updates->Delete_Update($uid, $msg_id);
                        echo $data;
                }
        }

        function moreupdates_ajax() {

                if (isSet($_POST['lastid'])) {
                        $lastid = $this->db->escape_str($_POST['lastid']);
//                        $lastmsg = $this->db->escape_str($lastmsg);
                        $this->load_messages();
                }
        }

        function load_messages() {

//Srinivas Tamada http://9lessons.info
                //Loading Comments link with load_updates.php
                if (isset($lastid)) {
                        $lastid = $_POST['lastid'];
                } else {
                        $lastid = 0;
                }

                $profile_uid = $this->ion_auth->get_userID();
                $uid = $profile_uid;
                if ($profile_uid) {
                        $updatesarray = $this->wall_updates->Updates($profile_uid, $lastid);
                        $total = $this->wall_updates->Total_Updates($profile_uid);
                } else {
                        $updatesarray = $this->wall_updates->Friends_Updates($uid, $lastid);
                        $total = $this->wall_updates->Total_Friends_Updates($uid);
                }

                if (isset($gravatar)) {
                        $session_face = $this->wall_updates->Gravatar($uid);
                } else {
                        $session_face = $this->wall_updates->Profile_Pic($uid);
                }

                if ($updatesarray) {
                        foreach ($updatesarray as $data) {
                                $msg_id = $data['msg_id'];
                                $orimessage = $data['message'];
                                $message = $this->mcl->tolink($this->mcl->htmlcode($data['message']));
                                $time = $data['created'];
                                $mtime = date("c", $time);
                                $username = $data['username'];
                                $uploads = $data['uploads'];
                                $msg_uid = $data['uid_fk'];
                                // User Avatar
                                if (isset($gravatar)) {
                                        $face = $this->wall_updates->Gravatar($msg_uid);
                                } else {
                                        $face = $this->wall_updates->Profile_Pic($msg_uid);
                                }

                                // End Avatar
                                ?>
                                <div class="stbody" id="stbody<?= $msg_id; ?>">

                                        <div class="stimg">
                                                <img src="<?= $face; ?>" class='big_face' alt='<?= $username; ?>'/>
                                        </div>
                                        <div class="sttext">
                                                <?php if ($uid == $msg_uid) { ?>
                                                        <a class="stdelete" href="#" id="<?= $msg_id; ?>" title="Delete Update"></a>
                                <?php } ?>
                                                <b><a href="<?= base_url() . $username; ?>"><?= $username; ?></a></b> <?= $this->mcl->clear($message); ?>

                                                <?php
                                                if ($uploads) {
                                                        echo "<div style='margin-top:10px'>";
                                                        $s = explode(",", $uploads);
                                                        foreach ($s as $a) {
                                                                $newdata = $this->wall_updates->Get_Upload_Image_Id($a);
                                                                if ($newdata) {
                                                                        echo "<a href='uploads/" . $newdata['image_path'] . "' rel='facebox'><img src='uploads/" . $newdata['image_path'] . "' class='imgpreview' /></a>";
                                                                }
                                                        }
                                                        echo "</div>";
                                                }
                                                ?>
                                                <div class="sttime"><a href='#' class='commentopen' id='<?= $msg_id; ?>' title='Comment'>Comment </a> | <a href='<?= base_Url() ?>status/<?= $msg_id; ?>' class="timeago" title='<?= $mtime; ?>'></a></div>
                                                <div id="stexpandbox">
                                                        <div id="stexpand<?= $msg_id; ?>">
                                                                <?php
                                                                if ($this->mcl->textlink($orimessage)) {
                                                                        $link = $this->mcl->textlink($orimessage);
                                                                        echo Expand_URL($link);
                                                                }
                                                                ?>
                                                        </div>
                                                </div>
                                                <div class="commentcontainer" id="commentload<?= $msg_id; ?>">
                                                        <?php
                                                        $x = 1;
                                                        $this->load_comments($msg_id);
                                                        ?>
                                                </div>
                                                <div class="commentupdate" style='display:none' id='commentbox<?= $msg_id; ?>'>
                                                        <div class="stcommentimg">
                                                                <img src="<?= $session_face; ?>" class='small_face'/>
                                                        </div>
                                                        <div class="stcommenttext" >
                                                                <form method="post" action="">
                                                                        <textarea name="comment" class="comment" maxlength="200"  id="ctextarea<?= $msg_id; ?>"></textarea>
                                                                        <br />
                                                                        <input type="submit"  value=" Comment "  id="<?= $msg_id; ?>" class="comment_button button"/>
                                                                </form>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                                <?php
                        }

                        if ($total > $this->mcl->perpage) {
                                ?>
                                <!-- More Button here $msg_id values is a last message id value. -->

                                <div id="more<?= $msg_id; ?>" class="morebox">
                                        <a href="#" class="more" id="<?= $msg_id; ?>">More</a>
                                </div>

                                <?
                        }
                } else {
                        echo '<h3 id="noupdates">No Updates</h3>';
                }
        }

        function load_comments($msg_id) {

//Srinivas Tamada http://9lessons.info
                //Loading Comments link with load_updates.php
                $commentsarray = $this->wall_updates->Comments($msg_id, 0);

                if (isset($commentsarray)) {
                        $comment_count = count($commentsarray);
                        $second_count = $comment_count - 2;
                        if ($comment_count > 2) {
                                ?>
                                <div class="comment_ui" id="view<?= $msg_id; ?>">
                                        <a href="#" class="view_comments" id="<?= $msg_id; ?>" vi='<?= $comment_count; ?>'>View all <?= $comment_count; ?> comments</a>
                                </div>
                                <?php
                                $commentsarray = $this->wall_updates->Comments($msg_id, $second_count);
                        }
                }
                if ($commentsarray) {
                        foreach ($commentsarray as $cdata) {
                                $com_id = $cdata['com_id'];
                                $comment = tolink(htmlcode($cdata['comment']));
                                $time = $cdata['created'];
                                $mtime = date("c", $time);
                                $username = $cdata['username'];
                                $com_uid = $cdata['uid_fk'];
                                // User Avatar
                                if (isset($gravatar)) {
                                        $cface = $this->wall_updates->Gravatar($com_uid);
                                } else {
                                        $cface = $this->wall_updates->Profile_Pic($com_uid);
                                }

                                // End Avatar
                                ?>
                                <div class="stcommentbody" id="stcommentbody<?= $com_id; ?>">
                                        <div class="stcommentimg">
                                                <img src="<?= $cface; ?>" class='small_face' alt='<?= $username; ?>'/>
                                        </div>
                                        <div class="stcommenttext">
                                                <?php if ($uid == $com_uid || $uid == $msg_uid) { ?>
                                                        <a class="stcommentdelete" href="#" id='<?= $com_id; ?>' title='Delete Comment'></a>
                                                <?php } ?>
                                                <b><a href="<?= $base_url . $username; ?>"><?= $username; ?></a></b> <?= clear($comment); ?>
                                                <div class="stcommenttime" title="<?= $mtime; ?>"></div>
                                        </div>
                                </div>
                                <?
                        }
                }
        }

        function search() {
                $uri = explode('/', $_POST['uri']);
                $fo = $uri[0];
                $fc = $uri[1];
                $lib = $fc . '_lib';
                $this->load->library('addon/' . $lib);
                $result = $this->$lib->search();
                return $result;
        }

        function filter_sb(){
                $filter_id = isset($_POST["id"]) ? $_POST["id"] : 0;
                $filter = isset($_POST["filter"]) ? $_POST["filter"] : "";
                $search = isset($_POST["search"]) ? $_POST["search"] : "";
                $indexKey = isset($_POST["indexKey"]) ? $_POST["indexKey"] : "";
                
                $table = isset($_POST["table"]) ? $_POST["table"] : "";
                $code = isset($_POST["code"]) ? $_POST["code"] : "";
                $name = isset($_POST["name"]) ? $_POST["name"] : "";
                
                $fil = explode(',',$filter_id);

                $where = array($indexKey=>$filter_id);
                $select = $search.','.$code.','.$name.','.$indexKey;

                $res_data = $this->mdb->get_data($table,$where,$select);
                $table_data['addonData'][$search]['data'] = $res_data;
                $data = $table_data['addonData'][$search]['data'];
                
                if ($filter_id != 0) {
                    $res = array();
                    foreach ($data as $key => $val) {
                        if (in_array($val[$indexKey],$fil)) {    
                            $arr = array(
                                    $search => $val[$search],
                                    'code' => $val[$code],
                                    'name' => $val[$name]
                            );
                            array_push($res, $arr);
                        }
                    }
                    print json_encode($res);
                } else {
                    $res = array();
                    foreach ($data as $key => $val) {
                        $arr = array(
                                $search => $val[$search],
                                'code' => $val[$code],
                                'name' => $val[$name]
                        );
                        array_push($res, $arr);
                    }
                    print json_encode($res);
                }
        }        

}
?>