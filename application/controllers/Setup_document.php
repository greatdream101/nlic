<?php

class Setup_document extends MY_Controller {

        public function __construct() {
                parent::__construct();
                $this->configs = array('detail' => 'ta');
        }

        public function index(){
                $list = $this->mcl->get_list_view($_POST['uri']);
                $a = $this->load->view('setup/documentList', array('list' => $list), true);
                print $a;
        }

        public function save_document(){

                $service_id             = isset($_POST['id'])?$_POST['id']:"0";
                $data                   = $_POST;
                $userID                 = $this->ion_auth->get_userID();
                $computerName           = $this->security_lib->get_computerName();
                $data['computerName']   = $computerName;
                $data['createdID']      = $userID;
                $data['createdDate']    = date('Y-m-d H:i:s');

                if ($service_id == 0) {
                        unset($data['id']);
                        $this->db->insert('t_management_document', $data);
                        $service_id = $this->db->insert_id();

                } else {
                        unset($data['id']);
                        $this->db->where('service_id',$service_id);
                        $this->db->update('t_management_document', $data);
                }

                print $service_id;
        }

        public function add_uploadfile(){
                $count = 1 + $_POST['data'];
                $element_name = "file_" . $count;
                $result = $this->mcl->import($element_name, 'no-description');
                print $result;
        }

        public function delete_attachments(){
                $attachmentsID = $_POST['attachmentsID'];
                $this->db->update('t_attachments', array('active'=>'0'), "attachmentsID=$attachmentsID");
        }
        public function edit_attachments(){
                // vd::d($_POST);
                $attachmentsID = $_POST['attachmentsID'];
                $this->db->update('t_attachments', $_POST, "attachmentsID=$attachmentsID");
        }

        public function delete_document(){
                $service_id = $_POST['service_id'];
                $this->db->update('t_management_document', array('active'=>'0'), "service_id=$service_id");
        }

        public function upload_attachments(){

                if (!empty($_FILES)) {
                $computer_name = $this->security_lib->get_computerName();
                $user_id = $this->ion_auth->get_userID();
                $row = 0;
                foreach ($_FILES as $key => $val) {
                        $service_id = $_POST["service_id"];
                        if (!empty($val['name'])) {
                        $originalName = $val['name'];
                        $extensionFile = pathinfo($originalName, PATHINFO_EXTENSION);

                        //////////////////////////     Gen new name      //////////////////////////
                        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $charactersLength = strlen($characters);

                        $randomString = '';
                        for ($i = 0; $i < 10; $i++) {
                                $randomString .= $characters[rand(0, $charactersLength - 1)];
                        }

                        $code = strtoupper($randomString);
                        $newName = $code . '.' . $extensionFile;

                        $upload = $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER['SCRIPT_NAME']).'/upload';
                        if (!file_exists($upload . "/document/" . $service_id)) {
                                mkdir($upload . "/document/" . $service_id, DIR_READ_WRITE_MODE, true);
                        }

                        $filePath = "/upload/document/" . $service_id;

                        if ($extensionFile !== "") {
                                $fn = $val['tmp_name'];
                                if (in_array($extensionFile, array('pdf', 'docx', 'doc', 'xlsx', 'xls', 'pttx', 'ptt'))) {
                                        // $contentFile = $this->getContentPDF($fn);
                                        $contentFile = '0';
                                } else {
                                        $size = getimagesize($fn);
                                        $ratio = $size[0] / $size[1];
                                                if ($ratio > 1) {
                                                        $width = 500;
                                                        $height = 500 / $ratio;
                                                } else {
                                                        $width = 500 * $ratio;
                                                        $height = 500;
                                                }

                                                $src = imagecreatefromstring(file_get_contents($fn));
                                                $dst = imagecreatetruecolor($width, $height);
                                                imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                                                imagedestroy($src);
                                                imagejpeg($dst, $fn);
                                                imagedestroy($dst);
                                }
                        }

                        if (move_uploaded_file($val["tmp_name"], "./" . $filePath . "/" . $newName)) {
                                $dataFile["referID"] = $service_id;
                                $dataFile["link"] = isset($_POST["link" . $row]) ? $_POST["link" . $row] : '';
                                $dataFile["documentID"] = 1; // is KM
                                $dataFile["keyword"] = isset($_POST["keyword" . $row]) ? $_POST["keyword" . $row] : '';
                                $dataFile["originalName"] = $originalName;
                                $dataFile["code"] = $code;
                                $dataFile["extensionFile"] = $extensionFile;
                                $dataFile["filePath"] = $filePath;
                                $dataFile['contentFile'] = $contentFile;
                                $dataFile["active"] = 1;
                                $dataFile['createdID'] = !empty($user_id) ? $user_id : null;
                                $dataFile['computerName'] = $computer_name;
                                $image_id = $this->db->insert('t_attachments', $dataFile);
                        }
                        }
                        $row++;
                }
                }else{

                        if(!empty($_POST['row'])){
                                $service_id = $_POST["service_id"];
                                $computer_name = $this->security_lib->get_computerName();
                                $user_id = $this->ion_auth->get_userID();

                                for($row = 0 ; $row<=$_POST['row'] ; $row++){
                                        echo $row.'<br>';
                                        $dataFile["referID"] = $service_id;
                                        $dataFile["link"] = isset($_POST["link" . $row]) ? $_POST["link" . $row] : '';
                                        $dataFile["documentID"] = 1; // is KM
                                        $dataFile["keyword"] = isset($_POST["keyword" . $row]) ? $_POST["keyword" . $row] : '';
                                        $dataFile["active"] = 1;
                                        $dataFile['createdID'] = !empty($user_id) ? $user_id : null;
                                        $dataFile['computerName'] = $computer_name;
                                        $image_id = $this->db->insert('t_attachments', $dataFile);
                                }
                        }
                        
                }
        }


}
