<?php

class Setup_coverpage extends MY_Controller {

        public function __construct() {
                parent::__construct();
                $this->configs = array('detail' => 'ta');
        }

        public function save_coverpage()
        {
                $insert = [];
                $table  = "t_management_coverpage";
                $cover_id = isset($_POST['cover_id'])?$_POST['cover_id']:"";

                if(!empty($_POST) && isset($_POST)){
                    foreach($_POST as $k => $v){
                            switch($k){
                                    case "name":
                                            $insert[$k] = $_POST[$k];
                                    break;
                                    case "nameEN":
                                            $insert[$k] = $_POST[$k];
                                    break;
                                    case "link":
                                            $insert[$k] = $_POST[$k];
                                    break;
                                    case "comment":
                                            $insert[$k] = $_POST[$k];
                                    break;
                                    case "flag_cover":
                                        $insert[$k] = $_POST[$k];

                                        if($insert[$k] == 1){
                                            $setFlag = array('flag_cover'=>'0');
                                            $this->db->update($table, $setFlag);
                                        }

                                    break;
                                    default:
                                            #default
                                    break;
                            }
                        }
                    }

            if($cover_id != ""){
                #update data
                $where = array('cover_id'=>$cover_id);
                $this->db->where($where);
                $this->db->update($table, $insert);
                $id_result = $cover_id;
            }else{
                #insert new data
                $id_result = $this->mdb->insert($table, $insert);
            }
                
            print $id_result;
        }

        public function delete_coverpage(){
            $set = ['active'=>0, 'flag_cover'=>0];
            $table  = "t_management_coverpage";
            $cover_id = isset($_POST['cover_id'])?$_POST['cover_id']:"";

            if($cover_id > 0){
                #update data
                $where = array('cover_id'=>$cover_id);
                $this->db->where($where);
                $this->db->update($table, $set);
                $id_result = $cover_id;
            }
        }

        public function upload_attachments(){

        if (!empty($_FILES)) {

            $computer_name = $this->security_lib->get_computerName();
            $user_id = $this->ion_auth->get_userID();
            $row = 0;
            foreach ($_FILES as $key => $val) {
                $cover_id = $_POST["cover_id"];
                if (!empty($val['name'])) {
                    $originalName = $val['name'];
                    $extensionFile = pathinfo($originalName, PATHINFO_EXTENSION);

                    //////////////////////////     Gen new name      //////////////////////////
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);

                    $randomString = '';
                    for ($i = 0; $i < 10; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }

                    $code = strtoupper($randomString);
                    $newName = $code . '.' . $extensionFile;

                    $upload = $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER['SCRIPT_NAME']).'/upload';

                //     if (!file_exists($upload . $cover_id)) {
                //         mkdir($upload . $cover_id, DIR_READ_WRITE_MODE, true);
                //     }

                    $filePath = "upload";

                    if ($extensionFile !== "") {
                        $fn = $val['tmp_name'];
                        if ($extensionFile == 'pdf') {
                            $contentFile = $this->getContentPDF($fn);
                        } else {
                            $size = getimagesize($fn);
                            $ratio = $size[0] / $size[1];
                            if ($ratio > 1) {
                                $width = 1200;
                                $height = 1200 / $ratio;
                            } else {
                                $width = 1200 * $ratio;
                                $height = 1200;
                            }

                            $src = imagecreatefromstring(file_get_contents($fn));
                            $dst = imagecreatetruecolor($width, $height);
                            imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                            imagedestroy($src);
                            imagejpeg($dst, $fn);
                            imagedestroy($dst);
                        }
                    }

                    if (move_uploaded_file($val["tmp_name"], "./" . $filePath . "/" . $newName)) {

                        $this->db->select('*')->from('t_management_coverpage');
                        $this->db->where('cover_id', $cover_id);
                        $result = $this->db->get()->result_array();
                        // print $val["tmp_name"]. "/" . $result[0]['file_upload'] . "/" .$result[0]['filename'];
                        if(!empty($result[0]['file_upload'])){
                            unlink("./" . $result[0]['file_upload'] . "/" .$result[0]['filename']);
                        }
                        
                        $dataFile["originalName"]       = $originalName;
                        $dataFile["extensionFile"]      = $extensionFile;
                        $dataFile["filePath"]           = $filePath;

                        $this->db->set('file_upload', $filePath);
                        $this->db->set('filename', $newName);
                        $this->db->set('active', 1);
                        $this->db->where('cover_id', $cover_id);
                        $this->db->update('t_management_coverpage');
                    }
                }
                $row++;
            }
        }else{
            print "no file";
        }
        
    }


}
