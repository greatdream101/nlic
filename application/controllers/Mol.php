<?

defined('BASEPATH') OR exit('No direct script access allowed');

    class Mol extends MY_Controller{
        public function __construct(){
            parent::__construct();
        }

        public function index(){

            $this->db->SELECT('*')->FROM('t_management_coverpage')->WHERE('flag_cover', 1);
            $coverpage = $this->db->get()->result_array();

            if(!empty($coverpage)){
                $this->load->view('nlic/coverpage', array('coverpage'=>$coverpage));
            }else{
                $this->home();
            }
        }

        public function home(){
            $where    = array('active'=>1);
            $menu     = $this->mdb->get("t_management_menu", $where, "*", "", "");
            $eservice = $this->mdb->get("t_management_eservice", $where, "*", "", "");
            $document = $this->mdb->get("t_management_document", $where, "*", "", "");
            $footer   = $this->mdb->get("t_management_footer", $where, "*", "", "");

            $activity = array(
                                'menu'=>$menu
                                ,'eservice'=>$eservice
                                ,'document'=>$document
                                ,'footer'=>$footer
                            );

            $this->load->view('nlic/nlic', $activity);
            $this->stat_counter();
        }

        public function stat_counter(){
            $this->load->view('nlic/stat_counter');
        }


        // function CallAPI(){
            // $year = isset($_POST['year'])?$_POST['year']:DATE('Y');
            // // http://20.64.8.197/api?type=SSO_MONTH_SUMMARY
            // // $body = array('type'=>'REP_SSO_A40','year'=>'2019','month'=>'1');
            // $body = array('type'=>'SSO_MONTH_SUMMARY', 'year'=>$year);
            // $curl = new Curl\Curl();
            // $response_data = $curl->get('http://20.64.8.197:8000/api/', $body);
            // $arrResponse = json_decode($response_data->response, True);
           // $this->sso_summary_data($arrResponse['data']);
        // }

        function sso_summary_data(){

            $data    = array();

            $year    = isset($_POST['year'])?$_POST['year']:DATE('Y');

            $db_mol  = $this->load->database('oracle_mol', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            for($i=1 ; $i<=12 ; $i++){

                $db_mol->select('YEAR, MONTH, SUM(A33) AS A33, SUM(A39) AS A39, SUM(A40) AS A40')->from('SSO_MONTH_SUMMARY');
                $db_mol->where('YEAR', $year);
                $db_mol->where('MONTH', $i);
                $db_mol->group_by('YEAR, MONTH');
                $arrSSOResult = $db_mol->get()->result_array();

                // vd::d($arrSSOResult);
                if(!empty($arrSSOResult) && isset($arrSSOResult)){

                    $data["A33"][$i-1] = $arrSSOResult[0]['A33'];
                    $data["A39"][$i-1] = $arrSSOResult[0]['A39'];
                    $data["A40"][$i-1] = $arrSSOResult[0]['A40'];

                }else{
                    $data["A33"][$i-1] = 0;
                    $data["A39"][$i-1] = 0;
                    $data["A40"][$i-1] = 0;
                }
            }

            $output = array('data'=>$data);
            print json_encode($output);

        }//end sso_summary_data

        function showmap(){
            $this->load->view('nlic/showmap');
        }

        function get_document_list(){

            $output     = "";
            $service_id = isset($_POST['service_id'])?$_POST['service_id']:"0";

            $this->db->select('*')->from('t_attachments');
            $this->db->where('active', 1);
            $this->db->where('referID', $service_id);
            $this->db->orderby('attachmentsID', 'DESC');
            $arrDocument = $this->db->get()->result_array();

                foreach($arrDocument as $key=>$value){

                    $filePath = base_url().'/assets/images/document/';
                        if(in_array($value['extensionFile'], array('png', 'jpeg', 'jpg', 'gif'))){
                            $filePath .= 'pic.png';
                        }elseif(in_array($value['extensionFile'], array('pdf', 'docx', 'xlsx'))){
                            $filePath .= $value['extensionFile'].'.png';
                        }else{
                            $filePath .= 'pic.png';
                        }

                    $link = isset($value['link'])?$value['link']:base_url(). $value['filePath'] . '/' . $value['code'] . '.' . $value['extensionFile'];
                    $output .= "<div
                                    class='col-lg-12 col-md-12 col-sm-12 document_file_list'
                                    style='border: 1px solid #cdcdcd;
                                    background: #fafafa;
                                    margin: 0 0 10px 0;
                                    border-radius: 10px;
                                    padding-bottom: 30px;
                                '>

                                    <a href='".$link."'>
                                        <div class='col-lg-2 col-md-2 col-sm-2'>
                                            <img class='img-responsive' src='".$filePath."' style='width: 50px; height: 50px; margin: 25px 0 0 0;'>
                                        </div>
                                        <div class='col-lg-10 col-md-10 col-sm-10'>
                                            <div style='padding: 45px 0 0 0;'>".$value['keyword']."</div>
                                        </div>
                                    </a>
                                </div>
                            ";
                }

            print $output;

        }

    }

?>