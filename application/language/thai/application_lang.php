<?
$lang['save_refresh'] =   "บันทึกและรีเฟรช";
$lang['name_eng'] =   "ชื่อภาษาอังกฤษ";
$lang['date_type_id'] =   "ประเภทของวันที่";
$lang['start_date'] =   "วันที่เริ่มต้น";
$lang['end_date'] =   "วันที่สิ้นสุด";
$lang['dealer_id'] =   "ผู้จำหน่ายฯ";
$lang['model_id'] =   "รุ่น";
$lang['statusID'] =   "สถานะ";
$lang['flag_input'] =   "ประเภทลูกค้า";
$lang['flag_type_id'] =   "รายงาน";
$lang['series_id'] =   "ชุดแบบสอบถาม";
$lang['select_one_option'] =   "โปรดเลือกเพียงหนึ่งรายการ";
$lang['please_enter_the_date_in_format_of_yyyy-mm-dd'] =   "โปรดใส่วันที่ในรูปแบบ YYYY-MM-DD";
$lang['graph_type_id'] =   "ประเภทของกราฟ";
$lang['cusid'] =   "รหัสลูกค้า";
$lang['titlename'] =   "คำนำหน้า";
$lang['fname'] =   "ชื่อ";
$lang['lname'] =   "นามสกุล";
$lang['company'] =   "บริษัท";
$lang['tel'] =   "เบอร์โทร";
$lang['email'] =   "อีเมล";
$lang['address'] =   "ที่อยู่";
$lang['road'] =   "ถนน";
$lang['subdistrict'] =   "แขวง/ตำบล";
$lang['district'] =   "เขต/อำเภอ";
$lang['province'] =   "จังหวัด";
$lang['zip'] =   "รหัสไปรษณีย์";
$lang['sub_address'] =   "ที่อยู่ย่อย";
$lang['send_ address'] =   "ที่อยู่จัดส่ง";
$lang['send_road'] =   "ถนนที่จัดส่ง";
$lang['send_subdistrict'] =   "ตำบลที่จัดส่ง";
$lang['send_district'] =   "เขต อำเภอที่ส่ง";
$lang['send_province'] =   "จังหวัดที่ส่ง";
$lang['send_zip'] =   "รหัสไปรษณีย์ส่ง";
$lang['engineno'] =   "เครื่องยนต์";
$lang['chassisno'] =   "เลขตัวถัง";
$lang['salename'] =   "พนักงานขาย";
$lang['deliverydate'] =   "วันส่งมอบรถ";
$lang['releasedate'] =   "วันที่ข้อมูลขึ้นระบบ";
$lang['date_type'] =   "ประเภทของข้อมูล";
$lang['call_status'] =   "สถานะการโทร";
$lang['call_sub_status'] =   "สถานะการโทรย่อย";
$lang['survey_status'] =   "สถานะเเบบสอบถาม";
$lang['laststatusdate'] =   "วันที่อัพเดทล่าสุด";
$lang['cntcall'] =   "จำนวนการโทร";
$lang['lastcalldate'] =   "วันที่โทรครั้งสุดท้าย";
$lang['flag_csi'] =   "Flag CSI";
$lang['flag_cli'] =   "Flag CLI";
$lang['document_id'] =   "Document";
$lang['new'] =   "สร้างเพิ่ม";
$lang['document_file'] =   "ไฟล์เอกสาร";
$lang['document_original_file'] =   "ไฟล์เอกสารเดิม";
$lang['category_id'] =   "รหัสเเคตตาล็อก";
$lang['code'] =   "โค้ด";
$lang['active_online'] =   "สถานะการใช้งาน";
$lang['section_id'] =   "Section Id";
$lang['nameEN'] =   "ชื่อข้อมูลในภาษาอังกฤษ";
$lang['yname'] =   "ปี";
$lang['target'] =   "Target";
$lang['bixtaeget'] =   "Bixtaeget";
$lang['model_group_name'] =   "ชื่อโมเดล";
$lang['model_group_id'] =   "รหัสกลุ่มโมเด็ล";
$lang['dealer_group_name'] =   "ชื่อกลุ่มผู้จำหน่ายฯ";
$lang['dealer_group_id'] =   "รหัสกลุ่มตัวเเทนจำหน่าย";
$lang['snippet_id'] =   "รหัสทางลัด";
$lang['snippet_id'] =   "รหัสทางลัด";
$lang['rate_weight_id'] =   "เรทน้ำหนัก";
$lang['namesent'] =   "ชื่อที่ส่ง";
$lang['n_address'] =   "ที่อยู่ที่จัดส่ง";
$lang['n_road'] =   "ถนนที่จัดส่ง";
$lang['n_subdistrict'] =   "ตำบลที่จัดส่ง";
$lang['n_district'] =   "เขต/อำเภอที่จัดส่ง";
$lang['n_province'] =   "จังหวัดที่จัดส่ง";
$lang['n_zip'] =   "รหัสไปรษณีย์ที่จัดส่ง";
$lang['addressother'] =   "ที่อยู่อื่นๆ";
$lang['sourceid'] =   "รหัสแหล่งที่มา";
$lang['createid'] =   "รหัสการสร้าง";
$lang['createdate'] =   "วันที่สร้าง";
$lang['updateid'] =   "อัพเดท";
$lang['updateid'] =   "อัพเดท";
$lang['flag_new'] =   "Flag New";
$lang['runningno'] =   "Running No";
$lang['linkstatus'] =   "ลิ้งค์สเตตัส";
$lang['laststatus'] =   "สถานะล่าสุด";
$lang['periodtime'] =   "ช่วงเวลา";
$lang['ownercar'] =   "เจ้าของรถ";
$lang['caruser'] =   "ผู้ใช้รถ";
$lang['ageing'] =   "อายุ";
$lang['ownid'] =   "Own Id";
$lang['remark'] =   "หมายเหตุ";
$lang['flagoriginal'] =   "Flag Original";
$lang['cntsendemail'] =   "จำนวนที่ส่งอีเมล";
$lang['agent_id'] =   "รหัสพนักงาน TSR";
$lang['view_agent'] =   "ดึงข้อมูลลูกค้า ของ TSR";
$lang['approve'] =   "Approve";
$lang['Page _PAGE_ of _PAGES_'] =   "Page  PAGE  of  PAGES";
$lang['group_id'] =   "รหัสกลุ่ม";
$lang['search'] =   "ค้นหา";
$lang['clear'] =   "คืนค่าตั้งต้น";
$lang['submit'] =   "ส่งข้อมูล";
$lang['save'] =   "บันทึก";
$lang['input_page'] =   "กรุณากรอกข้อมูล";
$lang['translation'] =   "การแปลภาษา";
$lang['back_to_list'] =   "กลับไปหน้ารายการ";
$lang['select_more_option'] =   "กรุณาเลือกอย่างน้อยหนึ่งรายการ";
$lang['rep_showroom'] =   "รายงานโชว์รูม";
$lang['kmadmin'] =   "การจัดการองค์ความรู้";
$lang['category'] =   "ประเภทขององค์ความรู้";
$lang['series'] =   "ชื่อแบบสอบถาม";
$lang['name'] =   "ชื่อ";
$lang['section_id'] =   "เลขที่ส่วน";
$lang['you_have_'] =   "คุณสามารถพิมพ์ได้อีก";
$lang['question'] =   "คำถาม";
$lang['id'] =   "ไอดี";
$lang['dealer'] =   "ผู้จำหน่ายฯ";
$lang['target'] =   "เป้าหมาย";
$lang['bixtarget'] =   "เป้าหมาย BIX";
$lang['nameyear'] =   "ปีข้อมูล";
$lang['section'] =   "ส่วนข้อมูล";
$lang['import_customer'] =   "การนำเข้าข้อมูลลูกค้า";
$lang['user'] =   "บริหารบัญชี";
$lang['translationID'] =   "เลขที่การแปล";
$lang['userID'] =   "เลขที่ผู้ใช้";
$lang['username'] =   "ชื่อผู้ใช้";
$lang['password'] =   "รหัสผ่าน";
$lang['privilege'] =   "การกำหนดสิทธิการใช้งาน";
$lang['flag_type'] =   "ประเภทรายงาน";
$lang['model'] =   "รุ่นรถที่ซื้อ";
$lang['model_group'] =   "หมวดหมู่ของโมเด็ล";
$lang['dealernameold'] =   "ชื่อผู้จำหน่ายฯ (เดิม)";
$lang['dealer_group'] =   "กลุ่มของผู้จำหน่ายฯ";
$lang['snippet'] =   "ทางลัด";
$lang['rate_weight'] =   "เกณฑ์น้ำหนัก";
$lang['tab_i'] =   "หน้าที่ 1";
$lang['customer'] =   "ลูกค้า";
$lang['sale'] =   "พนักงานขาย";
$lang['sale_id'] =   "เลขที่พนักงานขาย";
$lang['customer_approval'] =   "การยืนยันผลการสำรวจ";
$lang['language'] =   "ภาษา";
$lang['loading_list_data'] =   "กำลังโหลดข้อมูล";
$lang['sPrevious'] =   "หน้าก่อนหน้า";
$lang['sNext'] =   "หน้าถัดไป";
$lang['sLast'] =   "หน้าสุดท้าย";
$lang['sFirst'] =   "หน้าแรก";
$lang['colvis'] =   "แสดง/ ซ่อนคอลัมน์";
$lang['info_filter'] =   "(กรองข้อมูลจากจำนวนทั้งหมด _MAX_ รายการ)";
$lang['page_info'] =   "หน้าที่ _PAGE_ จาก _PAGES_ (จากจำนวน _MAX_ รายการ)";
$lang['zero_record'] =   "ไม่พบข้อมูลที่ค้นหา";
$lang['print_all'] =   "พิมพ์ทั้งหมด";
$lang['csv'] =   "CSV";
$lang['selected_print'] =   "พิมพ์เฉพาะรายการที่เลือก";
$lang['excel'] =   "Excel";
$lang['pdf'] =   "PDF";
$lang['_characters_left'] =   "ตัวอักษร";
$lang['group'] =   "กลุ่ม";
$lang['search'] =   "ค้นหา:";
$lang['lengthMenu'] =   "แสดง _MENU_ รายการ";
$lang['rows'] =   "รายการ";
$lang['show_all_records'] =   "แสดงทั้งหมด";
$lang['online_survey'] =   "การสำรวจออนไลน์";
$lang['next'] =   "ถัดไป";
$lang['back'] =   "ย้อนกลับ";
$lang['cus_info_header'] =   "กรุณายืนยันข้อมูลเบื้องต้น หากไม่ใช่ โปรดแจ้งแก้ไข ด้านล่างนี้";
$lang['name_surname'] =   "ชื่อ - นามสกุล";
$lang['phone'] =   "เบอร์ติดต่อ";
$lang['chassis_no'] =   "หมายเลขตัวถัง";
$lang['engine_no'] =   "หมายเลขเครื่องยนต์";
$lang['sell_person'] =   "พนักงานขาย";
$lang['delivery_date'] =   "วันส่งมอบรถ";
$lang['start'] =   "เริ่มแบบสอบถาม";
$lang['save_cusinfo'] =   "ยืนยันและบันทึกข้อมูล";
$lang['address_header'] =   "ท่านจะได้รับของที่ระลึกแทนคำขอบคุณที่กรุณาสละเวลาอันมีค่าให้ความคิดเห็นอันเป็นประโยชน์ต่อการพัฒนาคุณภาพการบริการของเราต่อไป<br><br>ทางบริษัทฯจะจัดส่งของที่ระลึกทางไปรษณีย์ภายใน 3 สัปดาห์ กรุณายืนยันที่อยู่จัดส่งดังนี้";
$lang['district_id'] =   "อำเภอ/ เขต";
$lang['subdistrict_id'] =   "ตำบล/แขวง";
$lang['province_id'] =   "จังหวัด";
$lang['please_provide_reason'] =   "กรุณาระบุเหตุผล เพื่อการพัฒนาของเรา";
$lang['please_select_choice'] =   "กรุณาเลือกคำตอบในข้อนี้ก่อน";
$lang['please_confirm_to_delete'] =   "กรุณายืนยันการลบ";
$lang['please_investigate_the_error'] =   "กรุณาตรวจสอบ การป้อนข้อมูลยังไม่ถูกต้อง";
$lang['please_wait'] =   "กำลังประมวลผล กรุณารอสักครู่";
$lang['confirm'] =   "ยืนยัน";
$lang['cancel'] =   "ยกเลิก";
$lang['are_you_sure_to_delete'] =   "คุณยังต้องการที่จะลบข้อมูลดังต่อไปนี้หรือไม่";
$lang['delete'] =   "ลบข้อมูล";
$lang['name_vn'] =   "ชื่อข้อมูลในภาษาเวียดนาม";
$lang['status_name'] =   "สถานะการโทร";
$lang['amount'] =   "จำนวน";
$lang['percent'] =   "เปอร์เซ็นต์";
$lang['size'] =   "ขนาดข้อมูล";
$lang['view_leads'] =   "ดู Leads";
$lang['allocate'] =   "จัดสรร";
$lang['summary_of_call_status'] =   "สรุปยอด ​Leads ของแต่ละสถานะการโทร";
$lang['select_agents'] =   "กรุณาเลือก Agents";
$lang['click'] =   "กดคลิ้ก";
$lang['allocation_result'] =   "ผลลัพธ์การจัดสรร";
$lang['cus_id'] =   "เลขที่ลูกค้า";
$lang['title_name'] =   "คำนำหน้าชือ";
$lang['agent_name'] =   "ชื่อ Agent";
$lang['gender'] =   "เพศ";
$lang['select_language'] =   "เปลี่ยนภาษาในการแสดงผล/ Change Display Language";
$lang['please_enter_for'] =   "กรุณาระบุข้อมูลของ";
$lang['name_rep'] =   "ชื่อย่อ";
$lang['select_one_radio'] =   "โปรดเลือกเพียงหนึ่งรายการ";
$lang['add_question'] =   "เพิ่มคำถามใหม่";
$lang['update_question'] =   "ปรับปรุงคำถามเดิม";
$lang['add_edit_question'] =   "เพิ่ม/ แก้ไขคำถาม";
$lang['add_answer'] =   "เพิ่มคำตอบ";
$lang['update_answer'] =   "ปรับปรุงคำตอบ";
$lang['value_id'] =   "ค่า";
$lang['weight'] =   "น้ำหนัก";
$lang['add_edit_answer'] =   "เพิ่ม/ แก้ไขคำตอบ";
$lang['please_enter_the_value_of'] =   "กรุณาป้อนข้อมูลของ";
$lang['control_id'] =   "ชนิดของการแสดงผล";
$lang['flag_remark'] =   "มีการป้อนข้อความเหตุผลหรือไม่?";
$lang['jump'] =   "กระโดดไปยัง";
$lang['select_id'] =   "ชนิดของ Select Control";
$lang['select_snippet'] =   "เลือกทางลัด";
$lang['sure'] =   "ยืนยันลบ";
$lang['flag_id'] =   "ชนิดของ Flag";
$lang['start_survey'] =   "เริ่มทำแบบสอบถาม";
$lang['please_enter_the_reason'] =   "กรุณาระบุเหตุผลเพื่อการปรับปรุงของเรา";
$lang['updateDate'] =   "วันที่ปรับปรุงข้อมูล";
$lang['createdDate'] =   "วันที่สร้างข้อมูล";
$lang['data_type'] =   "ชนิดข้อมูล";
$lang['send_address'] =   "ที่อยู่การส่งเอกสาร";
$lang['laststatusdate'] =   "วันที่ปรับปรุงข้อมูลล่าสุด";
$lang['add_customer'] =   "เพิ่มลูกค้าใหม่";
$lang['sale_person'] =   "พนักงานขาย";
$lang['total'] =   "จำนวนที่พบ";
$lang['approve'] =   "ยืนยัน";
$lang['cus_info'] =   "ข้อมูลลูกค้า";
$lang['customer_info'] =   "รายละเอียดลูกค้า";
$lang['online_survey'] =   "แบบสำรวจออนไลน์";
$lang['contact_number'] =   "เบอร์ติดต่อ";
$lang['call_statusID'] =   "สถานะการโทร";
$lang['previous'] =   "ย้อนกลับ";
$lang['save_data_complete'] =   "บันทึกข้อมูลเรียบร้อยแล้ว";
$lang['system_message'] =   "ข้อมูลระบบ";
$lang['please_select_select_id'] =   "กรุณาเลือกรุ่นรถยนต์ / ผู้จำหน่ายฯ";
$lang['select_all'] =   "เลือกทั้งหมด";
$lang['non_select_text'] =   "ไม่มีการเลือก";
$lang['dealer_survey'] =   "ผู้จำหน่ายฯ";
$lang['save_customer_info'] =   "บันทึกข้อมูล";
$lang['save_all'] =   "บันทึกทั้งหมด";
$lang['agent_introduction_1'] =   "“สวัสดีครับ/ค่ะ ผม/ดิฉัน [agentname] ได้รับมอบหมายจากบริษัท เมอร์เซเดส-เบนซ์ (ประเทศไทย) จำกัด สำรวจความพึงพอใจของท่านที่มีต่อการซื้อรถยนต์เมอร์เซเดส-เบนซ์ จาก [dealername] ความคิดเห็นของท่านมีประโยชน์ในการช่วยปรับปรุงและพัฒนาคุณภาพการบริการของเราให้ดียิ่งขึ้นได้ ดังนั้น จึงขออนุญาตสำรวจความพึงพอใจของท่าน ซึ่งใช้เวลาประมาณ 5 นาที เมื่อจบการสำรวจ ท่านจะได้รับของที่ระลึกเป็นการขอบคุณจากทางบริษัทฯ ไม่ทราบว่าตอนนี้สะดวกหรือไม่ครับ/คะ””&lt;br&gt;&lt;br&gt;&lt;u&gt;กรณีลูกค้าไม่สะดวกให้สัมภาษณ์&lt;/u&gt; - ถ้าเช่นนั้น ทางบริษัทฯ ต้องขออภัยเป็นอย่างยิ่งที่รบกวนเวลาของท่าน ขอบพระคุณครับ/ค่ะ";
$lang['agent_introduction_2'] =   "“สวัสดีครับ/ค่ะ ผม/ดิฉัน [agentname] ได้รับมอบหมายจากบริษัทเมอร์เซเดส-เบนซ์ (ประเทศไทย) จำกัด เนื่องจากท่านได้ซื้อรถจาก ผู้จำหน่าย [dealername] ซึ่งเป็นผู้จำหน่ายอย่างเป็นทางการของเรามาประมาณ [phase] เดือน แล้วนั้น (แล้วแต่กลุ่มลูกค้าเป้าหมาย) บริษัทฯจึงขออนุญาตสำรวจทัศนคติและความพึงพอใจของท่านที่มีต่อความภักดีกับผู้จำหน่ายฯดังกล่าวเพื่อการปรับปรุงคุณภาพการบริการต่อไป ซึ่งใช้เวลาประมาณ 2 นาที ไม่ทราบว่าท่านสะดวกหรือไม่คะ”&lt;br&gt;&lt;br&gt;&lt;u&gt;กรณีลูกค้าไม่สะดวกให้สัมภาษณ์&lt;/u&gt; - ถ้าเช่นนั้น ทางบริษัทฯ ต้องขออภัยเป็นอย่างยิ่งที่รบกวนเวลาของท่าน ขอบพระคุณครับ/ค่ะ";
$lang['cus_info_sale_id'] =   "พนักงานขาย";
$lang['cus_info_model_id'] =   "รุ่นรถที่ซื้อ";
$lang['cus_info_flag_input'] =   "Flag Input";
$lang['cus_info_dealer_id'] =   "ผู้จำหน่ายฯ";
$lang['survey_statusID'] =   "สถานะการทำแบบสำรวจ";
$lang['save_qc_status'] =   "บันทึกสถานะการตรวจสอบคุณภาพ";
$lang['check_all_for_approve'] =   "เลือกรายการทั้งหมดเพื่อยืนยัน";
$lang['select_id'] =   "กรุณาเลือกรุ่นรถยนต์ / ผู้จำหน่ายฯ";
$lang['edit_enable'] =   "แก้ไขข้อมูล";
$lang['flag_qc'] =   "สถานะ QC";
$lang['need_to_filter'] =   "เลือกข้อที่ใช้เป็นเกณฑ์ผ่าน";
$lang['need_to_cal'] =   "เลือกข้อที่ใช้เป็นข้อคำนวณ";
$lang['get_detail'] =   "ดูรายละเอียด";
$lang['view_chart'] =   "ดูชาร์ต";
$lang['address_nn'] =   "ที่อยู่จัดส่ง";
$lang['end3'] =   "ผม/ดิฉัน [agentname] ในนามบริษัท เมอร์เซเดส-เบนซ์ (ประเทศไทย) จำกัด ขอขอบพระคุณท่านเป็นอย่างสูงที่สละเวลาอันมีค่า ให้ความคิดเห็นอันเป็นประโยชน์ต่อการพัฒนาคุณภาพการบริการของเราให้ดียิ่งๆขึ้นไปครับ/ค่ะ สวัสดีครับ/ค่ะ";
$lang['Other_detail'] =   "รายละเอียด";
$lang['already_saved'] =   "บันทึกข้อมูลแล้ว";
$lang['save_call_status'] =   "บันทึกสถานะการโทร";
$lang['save_info'] =   "บันทึกข้อมูลลูกค้า";
$lang['tele_survey'] =   "แบบสำรวจทางโทรศัพท์";
$lang['call_status_search_id'] =   "สถานะการโทร";
$lang['cancel_save'] =   "ยกเลิกการบันทึก";
$lang['deletedDate'] =   "วันที่ลบข้อมูล";
$lang['source_id'] =   "ที่มาข้อมูล";
$lang['phase_id'] =   "เฟส";
$lang['lost_contact'] =   "ผู้ที่ติดต่อไม่ได้";
$lang['get_back'] =   "กลับคืน";
$lang['users_can_double_click_to_save_answer'] =   "ผู้ใช้สามารถดับเบิลคลิกเพื่อบันทึกคำตอบได้";
$lang['allowed_extension:_pdf,'] =   "ประเภทของไฟล์ :PDF";
$lang['save_&_next'] =   "บันทึกและไปต่อ ";
$lang['csi_sales_score'] =   "CSI Sales Score";
$lang['calculate_score'] =   "calculate_score";
$lang['csi_sales_score2'] =   "CSI Sales Score2";
$lang['csi_sales_stat'] =   "CSI Sales Stat Table";
$lang['csi_sales_stat_dealer'] =   "CSI Sales Stat Table(Dealer)";
$lang['csi_sales_stat_all'] =   "CSI Sales Stat Table(Thailand)";
$lang['cli_dealer_score'] =   "CLI Score Calculation";
$lang['cli_all_phase'] =   "ALL PHASE";
$lang['cli_all_phase_sum'] =   "SUM PHASE";
$lang['cli_phase'] =   "PHASE";
$lang['cli_question'] =   "QUESTION 1-4";
$lang['cli_question_sum'] =   "QUESTION SUM";
$lang['save'] =   "บันทึก";
$lang['back_to_question'] =   "ย้อนกลับ";
$lang['agent_lname'] =   "นามสกุล agent";
$lang['first_name'] =   "ชื่อ";
$lang['last_name'] =   "นามสกุล";
$lang['back_to_question'] =   "กลับไปยังหน้าคำถาม";
$lang['release_date'] =   "วันที่ขึ้นระบบ";
$lang['customer_information'] =   "ข้อมูลลูกค้า";
$lang['Other_detail'] =   "รายละเอียด";
$lang['csi_showroom_score'] =   "CSI Showroom Score";
$lang['csi_sales_stat_all_vn'] =   "CSI Sales Stat Table(Vietnam)";
$lang['csi_sales_stat_all_th'] =   "CSI Sales Stat Table(Thailand)";
$lang['total_call'] =   "จำนวนการโทรทั้งหมด";
$lang['total_list_usage'] =   "จำนวนรายชื่อที่ใช้ไปทั้งหมด";
$lang['allowed_extension'] =   "นามสกุลไฟล์ที่ใช้ได้";
$lang['callstatus'] =   "สถานะการโทร";
$lang['callstatus'] =   "สถานะการโทร";
$lang['call_sub_statusID'] =   "สถานะการโทรย่อย";
$lang['callsubstatus'] =   "สถานะการโทรย่อย";
$lang['back_to_call_status'] =   "ไปหน้าบันทึกสถานะการโทร";
$lang['name_th'] =   "ชื่อภาษาไทย";
$lang['soft_require'] =   "กรุณาระบุเหตุผล";
$lang['please_investigate_the_errors'] =   "มีข้อผิดพลาดระหว่างการบันทึก";
$lang['user_profile'] =   "ข้อมูลผู้ใช้ระบบ";
$lang['online_fname'] =   "ชื่อต้น (แก้ไข)";
$lang['online_lname'] =   "นามสกุล (แก้ไข)";
$lang['online_ageing'] =   "อายุ (แก้ไข)";
$lang['online_tel'] =   "เบอร์โทรศัพท์ (แก้ไข)";
$lang['showroom'] =   "ผู้จำหน่ายฯ";
$lang['score_csi_sale'] =   "คะแนน CSI-Sales พนักงานขาย";
$lang['number_of_answer'] =   "จำนวนคำตอบ";
$lang['number_of_interviewee'] =   "จำนวนผู้ให้สัมภาษณ์";
$lang['reset'] =   "ล้างค่า";
$lang['gender_id'] =   "เพศ";
$lang['delete_customer'] =   "ลบลูกค้า";
$lang['please_enter_your_password_to_delete'] =   "กรุณากรอกรหัสผ่านในการเข้าระบบ เพื่อยันยันการลบข้อมูล";
$lang['please_select_customer'] =   "กรุณาเลือกลูกค้า";
$lang['wrong_password'] =   "รหัสผ่านไม่ถูกต้อง โปรดลองอีกครั้ง";
$lang['please_fill'] =   "โปรดระบุ";
$lang['q7_1'] =   "อื่นๆ โปรดระบุ";
$lang['call_status_recording'] =   "จบการสัมภาษณ์: ขอขอบคุณสำหรับการมีส่วนร่วมและแบ่งปันเวลาอันมีค่าของท่าน";
$lang['please_enter_long_description_in_this_area._you_can_also_write_in_html_format'] =   "โปรดใส่คำอธิบายยาว ๆ ในบริเวณนี้ คุณยังสามารถเขียนในรูปแบบ HTML";
$lang['please_enter_long_description_in_this_area._you_can_also_write_in_html_format'] =   "โปรดใส่คำอธิบายยาว ๆ ในบริเวณนี้ คุณยังสามารถเขียนในรูปแบบ HTML";
$lang['customer_ssi_agent'] =   "ตัวแทนลูกค้าของ ssi";
$lang['batch_id'] =   "สัปดาห์";
$lang['sub_statusID'] =   "สถานะย่อย";
$lang['sponsor_id'] =   "สปอนเซอร์";
$lang['project_id'] =   "โปรเจค";
$lang['question_type_id'] =   "ประเภทคำถาม";
$lang['customer_ssi'] =   "ลูกค้า  ssi";
$lang['customer_csi'] =   "ลูกค้า csi";
$lang['customer_csi_agent'] =   "ตัวเเทนลูกค้า csi";
$lang['customer_ssi_qc'] =   "ลูกค้า ssi qc";
$lang['customer_csi_qc'] =   "ลูกค้า csi qc";
$lang['ssi_db_summary'] =   "สรุป ssi db";
$lang['report_ssi_raw_data'] =   "รายงานข้อมูลดิบ";
$lang['report_csi_raw_data'] =   "รายงานข้อมูลดิบของ csi";
$lang['report_csi_red_alert'] =   "รายงานข้อมูลดิบของ csi";
$lang['report_csi_status_interview'] =   "รายงานการสัมภาษณ์สถานะ csi";
$lang['title_name_en'] =   "ชื่อเรื่อง";
$lang['titleID'] =   "ชื่อ";
$lang['title_gender'] =   "เพศ";
$lang['bank_name'] =   "ชื่อธนาคาร";
$lang['bank_id'] =   "ธนาคาร";
$lang['call_category_id'] =   "รหัสหมวดหมู่การโทร";
$lang['sub_status_code'] =   "รหัสสถานะย่อย";
$lang['calltype_id'] =   "รหัสโทรเข้า";
$lang['data_type_id'] =   "รหัสประเภทข้อมูล";
$lang['SETUP_KM_FILES'] =   "ตั้งค่าไฟล์ KM";
$lang['file_id'] =   "รหัสฟิล์ล";
$lang['file_name'] =   "ชื่อไฟล์";
$lang['file_parth'] =   "พาร์ทของไฟล์";
$lang['file_folder'] =   "ไฟล์โฟลเดอร์";
$lang['file_type'] =   "ไฟล์โฟลเดอร์";
$lang['aaahกดเะั้'] =   "abc";
$lang['card_id'] =   "รหัสบัตร";
$lang['card_name'] =   "ชื่อบัตร";
$lang['card_type_id'] =   "รหัสประเภทบัตร";
$lang['card_type_name'] =   "ชื่อชนิดของบัตร";
$lang['series_code'] =   "รหัสชุด";
$lang['provider_id'] =   "รหัสผู้ให้บริการ";
$lang['api_link'] =   "api link";
$lang['user_provider_id'] =   "รหัสผู้ให้บริการผู้ใช้";
$lang['provider_name'] =   "ชื่อผู้ให้บริการ";
$lang['sender_id'] =   "รหัสผู้ส่ง";
$lang['sender_name'] =   "ชื่อผู้ส่ง";
$lang['project_name'] =   "ชื่อโปรเจ็ค";
$lang['call_remark'] =   "หมายเหตุการโทร";
$lang['click_to_call'] =   "คลิกเพื่อโทรออก";
$lang['option'] =   "ตัวเลือก";
$lang['cus_name'] =   "ชื่อผู้ใช้";
$lang['sales_group'] =   "sales group";
$lang['mgmt_no'] =   "Management Number";
$lang['technician_name'] =   "ชื่อช่าง";
$lang['technician_tel'] =   "เบอร์โทรช่าง";
$lang['work_start'] =   "วันที่เข้าบริการ";
$lang['start_time'] =   "เวลาเริ่มบริการ";
$lang['end_time'] =   "เวลาบริการเสร็จ";
$lang['customer_technic_state'] =   "การเข้าบริการ";
$lang['technic_statusID'] =   "ผลการสอบถามการเข้าของช่าง";
$lang['save_state_technic'] =   "บันทึก";
$lang['phone_type_id'] =   "ประเภทเบอร์โทร";
$lang['phone_number'] =   "เบอร์โทร";
$lang['service_time'] =   "วันที่เข้าบริการ";
$lang['technic_status_time_id'] =   "ช่างเข้าตรงเวลาหรือไม่";
$lang['technic_status_call_id'] =   "ช่างได้โทรประสานงานก่อนหรือไม่";
$lang['technic_remark'] =   "ช่างเข้าไม่ตรงตามเวลานัดหมาย ได้โทรแจ้งลูกค้าก่อนหรือไม่";
$lang['customer_repair_state'] =   "หลังเข้าให้บริการ";
$lang['repair_statusID'] =   "สถานะการซ่อม";
$lang['success_time'] =   "เวลาซ่อมเสร็จ";
$lang['repair_remark'] =   "หมายเหตุ";
$lang['repair_sub_statusID'] =   "สาเหตุที่ซ่อมไม่เสร็จ";
$lang['save_state_repair'] =   "บันทึก";
$lang['customer_survay'] =   "แบบสอบถามความพึงพอใจ";
$lang['assign_header'] =   "กรุณาเลือก สปอนเซอร์ และ แหล่งข้อมูล";
$lang['report_call_summary'] =   "Report Call Summary";
$lang['report_call_rawdata'] =   "Report Call RawData";
$lang['report_rawdata'] =   "Report RawData";
$lang['phone_check'] =   "กรุณาตรวจสอบเบอร์โทรศัพท์";
$lang['permanentaddress'] =   "ที่อยู่ตามทะเบียนบ้าน";
$lang['bloodid'] =   "กรุ๊ปเลือด";
$lang['permanentroad'] =   "ซอย/ถนน";
$lang['passmilitaryid'] =   "ท่านผ่านการเกณฑ์ทหารมาหรือยัง ?";
$lang['presentaddress'] =   "ที่อยู่ปัจจุบัน";
$lang['permanentsubdistrictid'] =   "ตำบล/แขวง";
$lang['permanentdistrictid'] =   "อำเภอ/เขต";
$lang['permanentprovinceid'] =   "จังหวัด";
$lang['permanentzipcode'] =   "ไปรษณีย์";
$lang['presentroad'] =   "ซอย/ถนน";
$lang['presentsubdistrictid'] =   "ตำบล/แขวง";
$lang['presentdistrictid'] =   "อำเภอ/เขต";
$lang['presentprovinceid'] =   "จังหวัด";
$lang['presentzipcode'] =   "ไปรษณีย์";
$lang['congenitalremark'] =   "มี โปรดระบุ";
$lang['positionid'] =   "ตำแหน่งที่สมัคร";
$lang['expectedsalary'] =   "เงินเดือนที่คาดหวัง";
$lang['chanelid'] =   "ทราบข่าวสมัครงานจาก";
$lang['dateofapp'] =   "วันที่สมัคร";
$lang['surname'] =   "นามสกุล";
$lang['nickname'] =   "ชื่อเล่น";
$lang['age'] =   "อายุ";
$lang['birthdate'] =   "วัน/เดือน/ปีเกิด";
$lang['mobilephone'] =   "โทรศัพท์(ติดต่อสะดวก)";
$lang['homephone'] =   "โทรศัพท์บ้าน";
$lang['e_mail'] =   "E-mail";
$lang['lineid'] =   "Line ID";
$lang['cardid'] =   "เลขบัตรประชาชน";
$lang['issueat'] =   "ออกให้ ณ";
$lang['expireddate'] =   "วันหมดอายุ";
$lang['placebirth'] =   "สถานที่เกิด";
$lang['nationality'] =   "สัญชาติ";
$lang['race'] =   "เชื้อชาติ";
$lang['religionid'] =   "ศาสนา";
$lang['height'] =   "ส่วนสูง";
$lang['yes'] =   "เกณฑ์แล้ว";
$lang['no_specity'] =   "ยังไม่ได้เกณฑ์";
$lang['passmilitaryremark'] =   "ได้รับการยกเว้นเพราะ";
$lang['congenitalid'] =   "โรคประจำตัว";
$lang['non'] =   "ไม่ระบุ";
$lang['sickyear'] =   "เคยป่วยหนักเมื่อปี";
$lang['sickyearremark'] =   "โปรดระบุโรค";
$lang['maritalid'] =   "สถานภาพสมรส";
$lang['single'] =   "โสด";
$lang['married'] =   "สมรส";
$lang['widowed'] =   "หม้าย";
$lang['divorced'] =   "หย่าร้าง";
$lang['separated'] =   "แยกทางกัน";
$lang['num_child'] =   "จำนวนบุตร";
$lang['son'] =   "ชาย";
$lang['daughter'] =   "หญิง";
$lang['namespouse'] =   "ชื่อ-นามสกุล( สามี/ภรรยา )";
$lang['spoccupation'] =   "อาชีพ/สถานที่ทำงาน";
$lang['spage'] =   "อายุ";
$lang['spmobilephone'] =   "โทรศัพท์";
$lang['namefather'] =   "ชื่อ-นามสกุลบิดา";
$lang['fworkoccupation'] =   "อาชีพ/สถานที่ทำงาน";
$lang['fage'] =   "อายุ";
$lang['fmobilephone'] =   "โทรศัพท์";
$lang['namemother'] =   "ชื่อ-นามสกุลมารดา";
$lang['mworkoccupation'] =   "อาชีพ/สถานที่ทำงาน";
$lang['mage'] =   "อายุ";
$lang['mmobilephone'] =   "โทรศัพท์";
$lang['refname'] =   "ชื่อ-นามสกุล";
$lang['rworkoccupation'] =   "อาชีพ/สถานที่ทำงาน";
$lang['rposition'] =   "ตำแหน่ง";
$lang['rmobilephone'] =   "โทรศัพท์";
$lang['emergenname'] =   "ในกรณีฉุกเฉิน ติดต่อ ชื่อ";
$lang['relationship'] =   "ความสัมพันธ์";
$lang['workplace'] =   "สถานที่ทำงาน";
$lang['emobilephone'] =   "โทรศัพท์";
$lang['genderid'] =   "เพศ";
$lang['gradlevel'] =   "ระดับการศึกษา";
$lang['school'] =   "สถานศึกษา";
$lang['majorcourse'] =   "สาขาวิชา";
$lang['degree'] =   "วุฒิการศึกษา";
$lang['gpax'] =   "คะแนนเฉลี่ย";
$lang['graduatedyear'] =   "ปีที่สำเร็จการศึกษา";
$lang['primarschool'] =   "ประถมศึกษา";
$lang['juniorhigh'] =   "มัธยมศึกษาตอนต้น";
$lang['highschool'] =   "มัธยมศึกษา";
$lang['vocational'] =   "อาชีวศึกษา";
$lang['diploma'] =   "อนุปริญญา/ป.ว.ส.";
$lang['brachelorde'] =   "ปริญญาตรี";
$lang['masterde'] =   "ปริญญาโท";
$lang['doctoratede'] =   "ปริญญาเอก";
$lang['personal_name'] =   "ชื่อผู้สมัคร";
$lang['walk-in'] =   "เวลาสมัคร";
$lang['typewriter'] =   "พิมพ์ดีด";
$lang['round'] =   "ครั้งที่";
$lang['no.'] =   "ลำดับที่";
$lang['evaluationcriteria'] =   "ปัจจัยการประเมินผล";
$lang['notpassinterview'] =   "ไม่ผ่านการสัมภาษณ์";
$lang['saveevaluation'] =   "บันทึกแบบประเมิน";
$lang['savedocumentskill'] =   "บันทึกเอกสารประกอบสมัครงาน";
$lang['commenthr'] =   "หมายเหตุ HR";
$lang['userid'] =   "เลขที่ผู้ใช้";
$lang['evaluationform'] =   "แบบประเมินผลสัมภาษณ์";
$lang['documentskill'] =   "เอกสารประกอบสมัครงานและผลการทดสอบ";
$lang['idcard'] =   "เลขบัตรประชาชน";
$lang['mobile'] =   "เบอร์มือถือ";
$lang['evaluation'] =   "ประเมินการสัมภาษณ์";
$lang['nameen'] =   "ชื่อภาษาอังกฤษ";
$lang['lang'] =   "ภาษา";
$lang['lastlogin'] =   "ครั้งล่าสุด";
$lang['detail'] =   "รายละเอียด";
$lang['titlenameen'] =   "คำนำหน้าชื่อภาษาอังกฤษ";
$lang['titlegender'] =   "คำนำหน้าเพศ";
$lang['sourcename'] =   "ชื่อไฟล์";
$lang['startdate'] =   "วันเริ่มต้น";
$lang['totalrec'] =   "ข้อมูลทั้งหมด";
$lang['enddate'] =   "วันสิ้นสุด";
$lang['totalassigned'] =   "ข้อมูลที่ได้รับ";
$lang['search_rawdata'] =   "ค้นหาข้อมูล";
$lang['name_en'] =   "ชื่อภาษาอังกฤษ";
$lang['departmentname'] =   "แผนก";
$lang['fnameen'] =   "ชื่อ(ภาษาอังกฤษ)";
$lang['lnameen'] =   "นามสกุล(ภาษาอังกฤษ)";
$lang['select_image'] =   "เลือกรูป";
$lang['DISTRICTID'] =   "อำเภอ/เขต";
$lang['trainningyear'] =   "ปี พ.ศ.";
$lang['trainningperiod'] =   "ระยะเวลา";
$lang['trainninginstitute'] =   "สถาบัน";
$lang['trainningcourse'] =   "หลักสูตร / เรื่อง";
$lang['period'] =   "ระยะเวลาทำงาน";
$lang['expfrom'] =   "จากวัน-เดือน - ปี";
$lang['expto'] =   "ถึงวันเดือน - ปี";
$lang['expcompany'] =   "ชื่อบรัษัท";
$lang['expposition'] =   "ตำแหน่ง";
$lang['explatestsalary'] =   "เงินเดือนสุดท้าย";
$lang['expreason'] =   "สาเหตุที่ออก";
$lang['titleid'] =   "คำนำหน้า";
$lang['interviewstatusid'] =   "ผลการการสัมภาษณ์";
$lang['commentdvs'] =   "หมายเหตุ DVS.";
$lang['interviewstatustype'] =   "ผลการการสัมภาษณ์";
$lang['interviewstatusid'] =   "ผลการการสัมภาษณ์";
$lang['chanelremark'] =   "อื่นๆ โปรดระบุ";
$lang['family_data'] =   "ข้อมูลครอบครัว";
$lang['change'] =   "แก้ไข";
$lang['remove'] =   "นำออก";
$lang['personal_detail'] =   "ข้อมูลส่วนบุคคล";
$lang['reference_person'] =   "บุคคลอ้างอิง";
$lang['education'] =   "ประวัติการศึกษา";
$lang['job_training'] =   "ประวัติการฝึกงาน/ งานโครงการต่างๆ";
$lang['working_experience'] =   "ประวัติการทำงาน";
$lang['submit'] =   "ส่ง";
$lang['print_profile'] =   "Search Print Profile";
$lang['loading'] =   "กำลังโหลดข้อมูล กรุณารอ";
$lang['view_questions'] =   "ดูคำถาม";
$lang['numberofdup'] =   "จำนวนการคัดลอก";
$lang['graphtypeid'] =   "ประเภทของกราฟ";
$lang['btn_view_questions'] =   "ดูคำถาม";
$lang['qc_pass'] =   "ผ่านการ QC";
$lang['qc_reject'] =   "ไม่ผ่านการ QC";
$lang['surveyor'] =   "ผู้สำรวจ";
$lang['branch'] =   "สาขา";
$lang['you_have_to_upload_at_least'] =   "คุณต้องอัพโหลดไฟล์อย่างน้อย";
$lang['last_question'] =   "สิ้นสุดคำถาม";
$lang['confirm_save'] =   "ยืนยันการบันทึก";
$lang['admin_translation'] =   "การแปลภาษา";
$lang['translationid'] =   "เลขที่";
$lang['admin_group'] =   "กลุ่มผู้ใช้";
$lang['groupid'] =   "กลุ่ม";
$lang['langid'] =   "เลขที่";
$lang['admin_user'] =   "ผู้ใช้";
$lang['admin_language'] =   "ภาษา";
$lang['setup_title'] =   "คำนำหน้า";
$lang['setup_callstatus'] =   "สถานะการโทรหลัก";
$lang['callstatusid'] =   "เลขที่";
$lang['setup_callsubstatus'] =   "สถานะการโทรรอง";
$lang['callsubstatusid'] =   "เลขที่";
$lang['setup_datasource'] =   "แหล่งที่มา";
$lang['setup_projects'] =   "โครงการ";
$lang['projectid'] =   "เลขที่";
$lang['setup_country'] =   "ประเทศ";
$lang['countryid'] =   "รหัส";
$lang['setup_district'] =   "อำเภอ / เขต";
$lang['districtid'] =   "เลขที่";
$lang['districtcode'] =   "รหัส";
$lang['setup_subdistrict'] =   "ตำบล/ แขวง";
$lang['subdistrictId'] =   "เลขที่";
$lang['setup_gender'] =   "เพศ";
$lang['setup_phonetype'] =   "ประเภทเบอร์โทร";
$lang['phonetypeid'] =   "ประเภทเบอร์โทรศัพท์";
$lang['setup_province'] =   "จังหวัด";
$lang['provinceid'] =   "จังหวัด";
$lang['setup_series'] =   "ชุดแบบสอบถาม";
$lang['seriesid'] =   "ชุดแบบสอบถาม";
$lang['setup_section'] =   "ตอนแบบสอบถาม";
$lang['sectionid'] =   "ตอนแบบสอบถาม";
$lang['sectioncode'] =   "รหัส";
$lang['survey_surveyor'] =   "รายชื่อ OP";
$lang['survey_clients'] =   "รายชื่อ Clients";
$lang['qcstatusid'] =   "สถานะ QC";
$lang['survey_series'] =   "ชุดแบบสอบถาม";
$lang['survey_section'] =   "ตอนแบบสอบถาม";
$lang['groupsname'] =   "กลุ่มผู้ใช้";
$lang['clientsid'] =   "ผู้ขอรับบริการ";
$lang['survey_branch'] =   "สาขา";
$lang['branchid'] =   "สาขา";
$lang['survey_region'] =   "เขตพื้นที่";
$lang['regionid'] =   "เขตพื้นที่";
$lang['survey_list'] =   "รายการแบบสอบถาม";
$lang['updateddate'] =   "วันที่ปรับปรุงข้อมูล";
$lang['view_dashboard'] =   "ดูผลลัพธ์";
$lang['raw_data'] =   "รายงานข้อมูลดิบ";
$lang['score_data'] =   "รายงานคะแนน";
$lang['percentage_data'] =   "รายงานคะแนน (%)";
$lang['questionnaire'] =   "แบบสอบถาม";
$lang['status_id'] =   "สถานะการโทรหลัก";
$lang['excalid'] =   "การคำนวณคะแนน";
$lang['add_reject'] =   "บันทึกไม่ผ่านการ QC";
$lang['close'] =   "ปิด";
$lang['end_survey'] =   "จบการสัมภาษณ์";
$lang['begin'] =   "เริ่มต้น";
$lang['survey_save'] =   "บันทึกแบบสอบถาม";
$lang['please_answer_all_question'] =   "กรุณาตอบคำถามทั้งหมดก่อน";
$lang['datestart'] =   "Date start ";
$lang['dateend'] =   "Date end";
$lang['search_survey'] =   "Export raw data";
$lang['nlic_mol_name'] =   "ศูนย์ข้อมูลแรงงานแห่งชาติ <br> (NATIONAL LABOUR INFORMATION CENTER)";
$lang['flag_cover'] =   "Active Coverpage";
$lang['add_uploadfile'] =   "เพิ่มไฟล์";
$lang['approve_save'] =   "บันทึก";
$lang['delete_data'] =   "ลบ";
$lang['delete_file'] =   "ลบไฟล์";
?>