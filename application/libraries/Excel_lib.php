<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Table $table
 * @property CI_Session $session
 * @property CI_FTP $ftp
 */
require_once APPPATH . "/libraries/PHPExcel/Classes/PHPExcel.php";
require_once APPPATH . "/libraries/PHPExcel/Classes/PHPExcel/IOFactory.php";

class Excel_lib extends PHPExcel {

        function __construct() {
                parent::__construct();
        }
}
