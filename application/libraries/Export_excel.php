<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_excel extends PHPExcel
{
	public $cell;
	public $objPHPExcel;
	public $styleRawHeader;
	public $styleRawContent;
	public $rep_name;
		
	public function __construct()
	{
		parent::__construct();
		
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Asia/Bangkok');
		
		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');
			
			$this->cell = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
					'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
					'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
					'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
					'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ');
			
			$this->styleRawHeader = array(
					'borders' => array(
							'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => 'FF000000'),
							),
					),
					'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
			);
			
			$this->styleRawContent = array(
					'borders' => array(
							'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => 'FF000000'),
							),
					),
			);
			
			// Create new PHPExcel object
			$this->objPHPExcel = new PHPExcel();
	}
	
	public function excel_raw($params){
		$rep_title = $params['rep_title'];
		$this->rep_name = $params['rep_name'];
		$data_array = $params['data_array'];
		// Set Active Sheet
		$objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);
		
		// Rename worksheet
		$objWorksheet->setTitle($rep_title);
		$rowNo = 1;
		$row_header = $data_array[0];
		$index = 0;
		
		// Loop Header
		foreach($row_header as $key => $value){
			$objWorksheet->setCellValue($this->cell[$index].$rowNo,$key);
			$index++;
		}
		
		// Add Header style
		$objWorksheet->getStyle($this->cell[0]."1:".$this->cell[$index-1]."1")->applyFromArray($this->styleRawHeader);
		$objWorksheet->getStyle($this->cell[0]."1:".$this->cell[$index-1]."1")->getFont()->setBold(true);
		
		$startContentRow = 2;
		$rowNo = 2;
		
		// Loop Add Content
		$len = count($data_array);
		$currentRow = 0;
		foreach($data_array as $dataCell){
			$index = 0;
			$currentRow++;
			
			foreach ($dataCell as $key => $value){
				$objWorksheet->setCellValue($this->cell[$index].$rowNo,$value);
				$index++;
			}
			$rowNo++;
		}
		// Add Content style
		$objWorksheet->getStyle($this->cell[0].$startContentRow.":".$this->cell[$index-1].($rowNo-1))->applyFromArray($this->styleRawContent);
		
		$objWorksheet->setAutoFilter($objWorksheet->calculateWorksheetDimension());
		
		$this->pop_save_file();
	}
	
	public function excel_template($params)	{
		$this->rep_name = $params['rep_name'];
		$rep_template = $params['rep_template'];
		$data_array = $params['data_array'];
		
		if (!file_exists('./application/excel_template/'.$rep_template)) {
			exit("Template not found. " .$rep_template);
		}
		
		// Load Template
		$this->objPHPExcel = PHPExcel_IOFactory::load('./application/excel_template/'.$rep_template);
		
		// Get Active Sheet
		$objWorksheet = $this->objPHPExcel->getActiveSheet();
		
		// Set document properties
		$this->objPHPExcel->getProperties()->setCreator("MOCAP")
		->setLastModifiedBy("MOCAP")
		->setTitle("Office 2007 XLSX Document")
		->setSubject("Office 2007 XLSX Document")
		->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("office 2007 openxml php")
		->setCategory("Excel Raw");		
		// Rename worksheet
		//$objWorksheet->setTitle($rep_title);		
		// Read Template Setting
		//================================================
		$headWidth = $objWorksheet->getCell('C2')->getValue();
		$headHeight = $objWorksheet->getCell('D2')->getValue();		
		$startRow = $headHeight+1;
		$rowNo = $headHeight+1;
		// read Formula
		$template_formula = [];
		$formula_row = $objWorksheet->getCell('E2')->getValue();
		
		for($col=0;$col<$headWidth;$col++){
			$template_formula[$col] = $objWorksheet->getCell($this->cell[$col].$formula_row)->getValue();
		}
		
		// read Formula
		$sumRow = $objWorksheet->getCell('F2')->getValue();
		$useRowNo = $objWorksheet->getCell('G2')->getValue();
		
		// Remove Setting Rows
		$objWorksheet->removeRow(1,2);
		
		// Insert New Row (use this method to copy format ant style from previous row)
		$len = count($data_array);
		$objWorksheet->insertNewRowBefore($rowNo + 1, $len - 1);
		
		// Loop Add Content
		$currentRow = 0;				
		if($useRowNo=='Yes'){
			$rowCount = 1;			
			foreach($data_array as $dataCell)
			{
				$objWorksheet->setCellValue($this->cell[0].$rowNo,$rowCount);
				$index = 0;
				foreach ($dataCell as $key => $value)
				{
					if(!is_null($template_formula[$index+1]))
					{
						$objWorksheet->setCellValue($this->cell[$index+1].$rowNo,"=".str_replace('[row]', $rowNo, $template_formula[$index+1]));
					}
					else
					{
						$objWorksheet->setCellValue($this->cell[$index+1].$rowNo,$value);
					}
					
					$index++;
				}
				
				$currentRow++;
				$rowCount++;
				$rowNo++;
			}
		}
		else
		{
			foreach($data_array as $dataCell)
			{
			    $index = 0;
				foreach ($dataCell as $key => $value){
				    if(!is_null($template_formula[$index]))	{
						$objWorksheet->setCellValue($this->cell[$index].$rowNo,"=".str_replace('[row]', $rowNo, $template_formula[$index]));
					}
					else{
						$objWorksheet->setCellValue($this->cell[$index].$rowNo,$value);
					}					
					$index++;
				}
				
				$currentRow++;
				$rowNo++;
			}
		}
		
		// Sum Row
		if($sumRow>0)
		{
			for($index=0;$index<$headWidth;$index++)
			{
				if($objWorksheet->getCell($this->cell[$index].$rowNo)->getValue()=='SUM')
				{
					$objWorksheet->setCellValue($this->cell[$index].$rowNo,'=SUM('.$this->cell[$index].$startRow.':'.$this->cell[$index].($rowNo-1).')');
				}
			}
		}
		
		$this->pop_save_file();
	}

	public function excel_AvgALL_OLD($params, $agency=[], $date=''){	    
	    //use template
	    $this->objPHPExcel = new PHPExcel();
	    $this->rep_name = 'al_aia_report_'.$agency['agencyCD'].'.xlsx';
	    $rep_template = 'al_aia_report.xlsx';
	    if (!file_exists('./application/excel_template/'.$rep_template)) {
	        exit("Template not found. " .$rep_template);
	    }
	    // Load Template
	    $this->objPHPExcel = PHPExcel_IOFactory::load('./application/excel_template/'.$rep_template);
	    
	    // Get Active Sheet
	    $objWorksheet = $this->objPHPExcel->getActiveSheet(0)->setTitle('OverallPerformance');
	    $dateSelect = isset($_POST['dateEnd'])?$_POST['dateEnd']:'';
	    if (!empty($dateSelect)) {
	        $arrKey = explode('-', $dateSelect);
	        $dateKey = intval($arrKey[1]);
	    }else {
	        $dateKey = '';
	    }
	    
	    $mouth = [1,2,3,4,5,6,7,8,9,10,11,12];
	    $arr = [];
	    $arrQ = [];
	    $arrQ['Q1_1']=0;$arrQ['Q1_2']=0;$arrQ['Q1_3']=0; $arrQ['Q1_4']=0;$arrQ['Q1_5']=0;
	    $arrQ['Q2_1_1']=0;$arrQ['Q2_1_2']=0;$arrQ['Q2_1_3']=0; $arrQ['Q2_1_4']=0;$arrQ['Q2_1_5']=0;
	    $arrQ['Q2_2_1']=0;$arrQ['Q2_2_2']=0;$arrQ['Q2_2_3']=0; $arrQ['Q2_2_4']=0;$arrQ['Q2_2_5']=0;
	    $arrQ['Q2_3_1']=0;$arrQ['Q2_3_2']=0;$arrQ['Q2_3_3']=0; $arrQ['Q2_3_4']=0;$arrQ['Q2_3_5']=0;
	    $arrQ['Q2_4_1']=0;$arrQ['Q2_4_2']=0;$arrQ['Q2_4_3']=0; $arrQ['Q2_4_4']=0;$arrQ['Q2_4_5']=0;
	    $arrQ['Q2_5_1']=0;$arrQ['Q2_5_2']=0;$arrQ['Q2_5_3']=0; $arrQ['Q2_5_4']=0;$arrQ['Q2_5_5']=0;
	    $arrQ['Q2_6_1']=0;$arrQ['Q2_6_2']=0;$arrQ['Q2_6_3']=0; $arrQ['Q2_6_4']=0;$arrQ['Q2_6_5']=0;
	    
	    $sheet3Agent = [];
	    $cntSentSMS = [];
	    $cntSMSSS = []; 
	    $countAns = [];
	    $cntSMS_USS = [];
	    $countSurveyQ1[] = 0; $countSurveyQ2_1[]=0; $countSurveyQ2_2[]=0; $countSurveyQ2_3[]=0; $countSurveyQ2_4[]=0; $countSurveyQ2_5[]=0; $countSurveyQ2_6[]=0;
	    $cntSvDQ1[] = 0; $cntSvDQ2_1[]=0; $cntSvDQ2_2[]=0; $cntSvDQ2_3[]=0; $cntSvDQ2_4[]=0; $cntSvDQ2_5[]=0; $cntSvDQ2_6[]=0;
	    $cntSvRQ1[] = 0; $cntSvRQ2_1[]=0; $cntSvRQ2_2[]=0; $cntSvRQ2_3[]=0; $cntSvRQ2_4[]=0; $cntSvRQ2_5[]=0; $cntSvRQ2_6[]=0;
	    $Q1[]=0; $Q2_1[]=0;  $Q2_2[]=0;  $Q2_3[]=0;  $Q2_4[]=0;  $Q2_5[]=0;  $Q2_6[]=0;
	    $DQ1[]=0; $DQ2_1[]=0;  $DQ2_2[]=0;  $DQ2_3[]=0;  $DQ2_4[]=0;  $DQ2_5[]=0;  $DQ2_6[]=0;
	    $RQ1[]=0; $RQ2_1[]=0;  $RQ2_2[]=0;  $RQ2_3[]=0;  $RQ2_4[]=0;  $RQ2_5[]=0;  $RQ2_6[]=0;
	    
	    $ShowAgent = $agency['agencyName'];
	    $arrAgent = [];
	    $dataAgency = [];
	    $haystackAgent = [];
	    	    
	    foreach ($mouth as $lm){
	        $cntSentSMS[$agency['agencyCD']][$lm] = 0;
	        $cntSMSSS[$agency['agencyCD']][$lm] = 0;
	        $countAns[$agency['agencyCD']][$lm] = 0;
	        $cntSMS_USS[$agency['agencyCD']][$lm] = 0;
	        
	        $countSurveyQ1[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_1[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_2[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_3[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_4[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_5[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_6[$agency['agencyCD'].'_'.$lm] = 0;
	        
	        $cntSvDQ1[$agency['division'].'_'.$lm] = 0; $cntSvDQ2_1[$agency['division'].'_'.$lm]=0; 
	        $cntSvDQ2_2[$agency['division'].'_'.$lm]=0; $cntSvDQ2_3[$agency['division'].'_'.$lm]=0; 
	        $cntSvDQ2_4[$agency['division'].'_'.$lm]=0; $cntSvDQ2_5[$agency['division'].'_'.$lm]=0; 
	        $cntSvDQ2_6[$agency['division'].'_'.$lm]=0;
	        
	        $cntSvRQ1[$agency['region'].'_'.$lm] = 0; $cntSvRQ2_1[$agency['region'].'_'.$lm]=0; 
	        $cntSvRQ2_2[$agency['region'].'_'.$lm]=0; $cntSvRQ2_3[$agency['region'].'_'.$lm]=0; 
	        $cntSvRQ2_4[$agency['region'].'_'.$lm]=0; $cntSvRQ2_5[$agency['region'].'_'.$lm]=0; 
	        $cntSvRQ2_6[$agency['region'].'_'.$lm]=0;
	        
	        $Q1[$agency['agencyCD'].'_'.$lm]=0; $Q2_1[$agency['agencyCD'].'_'.$lm]=0;  $Q2_2[$agency['agencyCD'].'_'.$lm]=0;  
	        $Q2_3[$agency['agencyCD'].'_'.$lm]=0;  $Q2_4[$agency['agencyCD'].'_'.$lm]=0;  $Q2_5[$agency['agencyCD'].'_'.$lm]=0;  $Q2_6[$agency['agencyCD'].'_'.$lm]=0;
	        
	        $DQ1[$agency['division'].'_'.$lm]=0; $DQ2_1[$agency['division'].'_'.$lm]=0;  $DQ2_2[$agency['division'].'_'.$lm]=0;  $DQ2_3[$agency['division'].'_'.$lm]=0;  
	        $DQ2_4[$agency['division'].'_'.$lm]=0;  $DQ2_5[$agency['division'].'_'.$lm]=0;  $DQ2_6[$agency['division'].'_'.$lm]=0;
	        
	        $RQ1[$agency['region'].'_'.$lm]=0; $RQ2_1[$agency['region'].'_'.$lm]=0;  $RQ2_2[$agency['region'].'_'.$lm]=0;  $RQ2_3[$agency['region'].'_'.$lm]=0;  
	        $RQ2_4[$agency['region'].'_'.$lm]=0;  $RQ2_5[$agency['region'].'_'.$lm]=0;  $RQ2_6[$agency['region'].'_'.$lm]=0;
	    }
	    
	    foreach ($params as $lsts){
	        $arrKey = explode('-', $lsts['createdDate']);
	        $key = intval($arrKey[1]);
	        $keyChka = $lsts['agentCD'].'_'.$key;
	        $keyChk = $agency['agencyCD'].'_'.$key;
	        $keyChkD = $agency['division'].'_'.$key;
	        $keyChkR = $agency['region'].'_'.$key;
	        if ($lsts['flagID'] == 1 && ($lsts['agencyCD'] == $agency['agencyCD'])) {
	            $dataAgency[$key][$lsts['agencyCD']][$lsts['agentCD']][] = [$lsts['insuredName']
	                                                                  , $lsts['Q1'], $lsts['Q2_1'], $lsts['Q2_2']
	                                                                  , $lsts['Q2_3'], $lsts['Q2_4'], $lsts['Q2_5']
	                                                                  , $lsts['Q2_6'], $lsts['Q3']];
	            
	            if (in_array($keyChka, $haystackAgent)) {
	                $sheet3Agent[$lsts['agencyCD']][$lsts['agentCD']][$key] = [
	                    'agentCD' => $lsts['agentName']
	                    ,'cntSendSMS' => $cntA[$keyChka][0]+=1
	                    ,'cntSMSSS' => ($lsts['sms_status']==1)?$cntA[$keyChka][1]+=1:$cntA[$keyChka][1]+=0
	                    ,'countAns' => (($lsts['Q1']!=''&&$lsts['Q1']!='0')
            	                        ||($lsts['Q2_1']!=''&&$lsts['Q2_1']!='0')
            	                        ||($lsts['Q2_2']!=''&&$lsts['Q2_2']!='0')
            	                        ||($lsts['Q2_3']!=''&&$lsts['Q2_3']!='0')
            	                        ||($lsts['Q2_4']!=''&&$lsts['Q2_4']!='0')
            	                        ||($lsts['Q2_5']!=''&&$lsts['Q2_5']!='0')
            	                        ||($lsts['Q2_6']!=''&&$lsts['Q2_6']!='0')
            	                        ||($lsts['Q3']!=''&&$lsts['Q3']!='0'))?$cntA[$keyChka][2]+=1:$cntA[$keyChka][2]+=0
	                    ,'cntSMSUSS' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=1)?$cntA[$keyChka][3]+=1:$cntA[$keyChka][3]+=0
	                    ,'cntSMS_Error' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=3 && $lsts['sms_status']!=1)?$cntA[$keyChka][4]+=1:$cntA[$keyChka][4]+=0
	                    ,'cntSendSMS_RBK' => ($lsts['flagID'] == 1 && $lsts['sms_status']==3)?$cntA[$keyChka][5]+=1:$cntA[$keyChka][5]+=0
	                    
	                ];
	            }else {
                    $sheet3Agent[$lsts['agencyCD']][$lsts['agentCD']][$key] = [
                        'agentCD' => $lsts['agentName']
                        ,'cntSendSMS' => $cntA[$keyChka][0] = 1
                        ,'cntSMSSS' => ($lsts['sms_status']==1)?$cntA[$keyChka][1]=1:$cntA[$keyChka][1]=0
                        ,'countAns' => (($lsts['Q1']!=''&&$lsts['Q1']!='0')
                                        ||($lsts['Q2_1']!=''&&$lsts['Q2_1']!='0')
                                        ||($lsts['Q2_2']!=''&&$lsts['Q2_2']!='0')
                                        ||($lsts['Q2_3']!=''&&$lsts['Q2_3']!='0')
                                        ||($lsts['Q2_4']!=''&&$lsts['Q2_4']!='0')
                                        ||($lsts['Q2_5']!=''&&$lsts['Q2_5']!='0')
                                        ||($lsts['Q2_6']!=''&&$lsts['Q2_6']!='0')
                                        ||($lsts['Q3']!=''&&$lsts['Q3']!='0'))?$cntA[$keyChka][2]=1:$cntA[$keyChka][2]=0
                        ,'cntSMSUSS' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=1)?$cntA[$keyChka][3]=1:$cntA[$keyChka][3]=0
                        ,'cntSMS_Error' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=3 && $lsts['sms_status']!=1)?$cntA[$keyChka][4]=1:$cntA[$keyChka][4]=0
                        ,'cntSendSMS_RBK' => ($lsts['flagID'] == 1 && $lsts['sms_status']==3)?$cntA[$keyChka][5]=1:$cntA[$keyChka][5]=0
                    ];
                    $haystackAgent[] = $keyChka;
                }
	        }
	        
            $arrAgent[$lsts['agentCD']] = $lsts['agentName'];
	        	        
	        if ($agency['agencyCD'] === $lsts['agencyCD']) {
	            $arr[$key]= [
	                //count Num Answer by Q
	                'TotalQ1' => ($lsts['Q1']!=='0'&&$lsts['Q1']!=='')?$countSurveyQ1[$keyChk]+=1:$countSurveyQ1[$keyChk]+=0
	                ,'TotalQ2_1' => ($lsts['Q2_1']!=='0'&&$lsts['Q2_1']!=='')?$countSurveyQ2_1[$keyChk]+=1:$countSurveyQ2_1[$keyChk]+=0
	                ,'TotalQ2_2' => ($lsts['Q2_2']!=='0'&&$lsts['Q2_2']!=='')?$countSurveyQ2_2[$keyChk]+=1:$countSurveyQ2_2[$keyChk]+=0
	                ,'TotalQ2_3' => ($lsts['Q2_3']!=='0'&&$lsts['Q2_3']!=='')?$countSurveyQ2_3[$keyChk]+=1:$countSurveyQ2_3[$keyChk]+=0
	                ,'TotalQ2_4' => ($lsts['Q2_4']!=='0'&&$lsts['Q2_4']!=='')?$countSurveyQ2_4[$keyChk]+=1:$countSurveyQ2_4[$keyChk]+=0
	                ,'TotalQ2_5' => ($lsts['Q2_5']!=='0'&&$lsts['Q2_5']!=='')?$countSurveyQ2_5[$keyChk]+=1:$countSurveyQ2_5[$keyChk]+=0
	                ,'TotalQ2_6' => ($lsts['Q2_6']!=='0'&&$lsts['Q2_6']!=='')?$countSurveyQ2_6[$keyChk]+=1:$countSurveyQ2_6[$keyChk]+=0
	                //sum value By Q
	                ,'Q1' => $Q1[$keyChk]+=$lsts['Q1']
	                ,'Q2_1' => $Q2_1[$keyChk]+=$lsts['Q2_1']
	                ,'Q2_2' => $Q2_2[$keyChk]+=$lsts['Q2_2']
	                ,'Q2_3' => $Q2_3[$keyChk]+=$lsts['Q2_3']
	                ,'Q2_4' => $Q2_4[$keyChk]+=$lsts['Q2_4']
	                ,'Q2_5' => $Q2_5[$keyChk]+=$lsts['Q2_5']
	                ,'Q2_6' => $Q2_6[$keyChk]+=$lsts['Q2_6']
	            ];
	            
	            ////// count customer Take surveys
	            if (($lsts['Q1']!=''&&$lsts['Q1']!='0')
	                ||($lsts['Q2_1']!=''&&$lsts['Q2_1']!='0')
	                ||($lsts['Q2_2']!=''&&$lsts['Q2_2']!='0')
	                ||($lsts['Q2_3']!=''&&$lsts['Q2_3']!='0')
	                ||($lsts['Q2_4']!=''&&$lsts['Q2_4']!='0')
	                ||($lsts['Q2_5']!=''&&$lsts['Q2_5']!='0')
	                ||($lsts['Q2_6']!=''&&$lsts['Q2_6']!='0')
	                ||($lsts['Q3']!=''&&$lsts['Q3']!='0')) {
	                    $countAns[$lsts['agencyCD']][$key] +=1;
	                }
	                ////// count customer Take surveys
	                ///// count send sms
	                if($lsts['flagID'] == 1){
	                    $cntSentSMS[$lsts['agencyCD']][$key] +=1;
	                }
	                ///// count send sms
	                ///// count send sms success
	                if($lsts['flagID'] == 1 && $lsts['sms_status']==1){
	                    $cntSMSSS[$lsts['agencyCD']][$key] +=1;
	                }
	                ///// count send sms
	                ///// count send sms Un success
	                if($lsts['flagID'] == 1 && $lsts['sms_status']!=1){
	                    $cntSMS_USS[$lsts['agencyCD']][$key] +=1;
	                }
	        }
	        
	        if ($agency['division'] === $lsts['division']) {
	            $arrDivision[$key]= [
	                //count Num Answer by Q
	                'TotalQ1' => ($lsts['Q1']!=='0'&&$lsts['Q1']!=='')?$cntSvDQ1[$keyChkD]+=1:$cntSvDQ1[$keyChkD]+=0
	                ,'TotalQ2_1' => ($lsts['Q2_1']!=='0'&&$lsts['Q2_1']!=='')?$cntSvDQ2_1[$keyChkD]+=1:$cntSvDQ2_1[$keyChkD]+=0
	                ,'TotalQ2_2' => ($lsts['Q2_2']!=='0'&&$lsts['Q2_2']!=='')?$cntSvDQ2_2[$keyChkD]+=1:$cntSvDQ2_2[$keyChkD]+=0
	                ,'TotalQ2_3' => ($lsts['Q2_3']!=='0'&&$lsts['Q2_3']!=='')?$cntSvDQ2_3[$keyChkD]+=1:$cntSvDQ2_3[$keyChkD]+=0
	                ,'TotalQ2_4' => ($lsts['Q2_4']!=='0'&&$lsts['Q2_4']!=='')?$cntSvDQ2_4[$keyChkD]+=1:$cntSvDQ2_4[$keyChkD]+=0
	                ,'TotalQ2_5' => ($lsts['Q2_5']!=='0'&&$lsts['Q2_5']!=='')?$cntSvDQ2_5[$keyChkD]+=1:$cntSvDQ2_5[$keyChkD]+=0
	                ,'TotalQ2_6' => ($lsts['Q2_6']!=='0'&&$lsts['Q2_6']!=='')?$cntSvDQ2_6[$keyChkD]+=1:$cntSvDQ2_6[$keyChkD]+=0
	                //sum value By Q
	                ,'Q1' => $DQ1[$keyChkD]+=$lsts['Q1']
	                ,'Q2_1' => $DQ2_1[$keyChkD]+=$lsts['Q2_1']
	                ,'Q2_2' => $DQ2_2[$keyChkD]+=$lsts['Q2_2']
	                ,'Q2_3' => $DQ2_3[$keyChkD]+=$lsts['Q2_3']
	                ,'Q2_4' => $DQ2_4[$keyChkD]+=$lsts['Q2_4']
	                ,'Q2_5' => $DQ2_5[$keyChkD]+=$lsts['Q2_5']
	                ,'Q2_6' => $DQ2_6[$keyChkD]+=$lsts['Q2_6']
	            ];
	        }
	        
	        if ($agency['region'] === $lsts['region']) {
	            $arrRegion[$key]= [
	                //count Num Answer by Q
	                'TotalQ1' => ($lsts['Q1']!=='0'&&$lsts['Q1']!=='')?$cntSvRQ1[$keyChkR]+=1:$cntSvRQ1[$keyChkR]+=0
	                ,'TotalQ2_1' => ($lsts['Q2_1']!=='0'&&$lsts['Q2_1']!=='')?$cntSvRQ2_1[$keyChkR]+=1:$cntSvRQ2_1[$keyChkR]+=0
	                ,'TotalQ2_2' => ($lsts['Q2_2']!=='0'&&$lsts['Q2_2']!=='')?$cntSvRQ2_2[$keyChkR]+=1:$cntSvRQ2_2[$keyChkR]+=0
	                ,'TotalQ2_3' => ($lsts['Q2_3']!=='0'&&$lsts['Q2_3']!=='')?$cntSvRQ2_3[$keyChkR]+=1:$cntSvRQ2_3[$keyChkR]+=0
	                ,'TotalQ2_4' => ($lsts['Q2_4']!=='0'&&$lsts['Q2_4']!=='')?$cntSvRQ2_4[$keyChkR]+=1:$cntSvRQ2_4[$keyChkR]+=0
	                ,'TotalQ2_5' => ($lsts['Q2_5']!=='0'&&$lsts['Q2_5']!=='')?$cntSvRQ2_5[$keyChkR]+=1:$cntSvRQ2_5[$keyChkR]+=0
	                ,'TotalQ2_6' => ($lsts['Q2_6']!=='0'&&$lsts['Q2_6']!=='')?$cntSvRQ2_6[$keyChkR]+=1:$cntSvRQ2_6[$keyChkR]+=0
	                //sum value By Q
	                ,'Q1' => $RQ1[$keyChkR]+=$lsts['Q1']
	                ,'Q2_1' => $RQ2_1[$keyChkR]+=$lsts['Q2_1']
	                ,'Q2_2' => $RQ2_2[$keyChkR]+=$lsts['Q2_2']
	                ,'Q2_3' => $RQ2_3[$keyChkR]+=$lsts['Q2_3']
	                ,'Q2_4' => $RQ2_4[$keyChkR]+=$lsts['Q2_4']
	                ,'Q2_5' => $RQ2_5[$keyChkR]+=$lsts['Q2_5']
	                ,'Q2_6' => $RQ2_6[$keyChkR]+=$lsts['Q2_6']
	            ];
	        }	        
	    }
	    
	    $arrVal = [];
	    foreach ($arr as $keyMount=>$arrlst){
	        $arrVal[$keyMount] = [
	            'Q1' => ($arrlst['TotalQ1']>0)?$arrlst['Q1']/$arrlst['TotalQ1']:0
	            ,'Q2_1' => ($arrlst['TotalQ2_1']>0)?$arrlst['Q2_1']/$arrlst['TotalQ2_1']:0
	            ,'Q2_2' => ($arrlst['TotalQ2_2']>0)?$arrlst['Q2_2']/$arrlst['TotalQ2_2']:0
	            ,'Q2_3' => ($arrlst['TotalQ2_3']>0)?$arrlst['Q2_3']/$arrlst['TotalQ2_3']:0
	            ,'Q2_4' => ($arrlst['TotalQ2_4']>0)?$arrlst['Q2_4']/$arrlst['TotalQ2_4']:0
	            ,'Q2_5' => ($arrlst['TotalQ2_5']>0)?$arrlst['Q2_5']/$arrlst['TotalQ2_5']:0
	            ,'Q2_6' => ($arrlst['TotalQ2_6']>0)?$arrlst['Q2_6']/$arrlst['TotalQ2_6']:0
	        ];
	    }
	    
	    $arrValDivision = [];
	    foreach ($arrDivision as $keyMount=>$arrlst){
	        $arrValDivision[$keyMount] = [
	            'Q1' => ($arrlst['TotalQ1']>0)?$arrlst['Q1']/$arrlst['TotalQ1']:0
	            ,'Q2_1' => ($arrlst['Q2_1']>0)?$arrlst['Q2_1']/$arrlst['TotalQ2_1']:0
	            ,'Q2_2' => ($arrlst['Q2_2']>0)?$arrlst['Q2_2']/$arrlst['TotalQ2_2']:0
	            ,'Q2_3' => ($arrlst['Q2_3']>0)?$arrlst['Q2_3']/$arrlst['TotalQ2_3']:0
	            ,'Q2_4' => ($arrlst['Q2_4']>0)?$arrlst['Q2_4']/$arrlst['TotalQ2_4']:0
	            ,'Q2_5' => ($arrlst['Q2_5']>0)?$arrlst['Q2_5']/$arrlst['TotalQ2_5']:0
	            ,'Q2_6' => ($arrlst['Q2_6']>0)?$arrlst['Q2_6']/$arrlst['TotalQ2_6']:0
	        ];
	    }
	    
	    $arrValRegion = [];
	    foreach ($arrRegion as $keyMount=>$arrlst){
	        $arrValRegion[$keyMount] = [
	            'Q1' => ($arrlst['TotalQ1']>0)?$arrlst['Q1']/$arrlst['TotalQ1']:0
	            ,'Q2_1' => ($arrlst['Q2_1']>0)?$arrlst['Q2_1']/$arrlst['TotalQ2_1']:0
	            ,'Q2_2' => ($arrlst['Q2_2']>0)?$arrlst['Q2_2']/$arrlst['TotalQ2_2']:0
	            ,'Q2_3' => ($arrlst['Q2_3']>0)?$arrlst['Q2_3']/$arrlst['TotalQ2_3']:0
	            ,'Q2_4' => ($arrlst['Q2_4']>0)?$arrlst['Q2_4']/$arrlst['TotalQ2_4']:0
	            ,'Q2_5' => ($arrlst['Q2_5']>0)?$arrlst['Q2_5']/$arrlst['TotalQ2_5']:0
	            ,'Q2_6' => ($arrlst['Q2_6']>0)?$arrlst['Q2_6']/$arrlst['TotalQ2_6']:0
	        ];
	    }
	    
	    $arrMouth = ['2561','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
	    $arrTotalSurveyQ1 = ['จำนวนลูกค้าที่ตอบแบบสอบถาม'];
	    $avgQ = [];
	    $avgQ['Q1'] = ['ข้อ1. คะแนนความประทับใจโดยรวม'];
	    $avgQ['Q2_1'] = ['ข้อ 2-1'];
	    $avgQ['Q2_2'] = ['ข้อ 2-2'];
	    $avgQ['Q2_3'] = ['ข้อ 2-3'];
	    $avgQ['Q2_4'] = ['ข้อ 2-4'];
	    $avgQ['Q2_5'] = ['ข้อ 2-5'];
	    $avgQ['Q2_6'] = ['ข้อ 2-6'];
	    
	    foreach ($mouth as $lst){
	        $arrTotalSurveyQ1[] = isset($arr[$lst]['TotalQ1'])?$arr[$lst]['TotalQ1']:'';
	        $avgQ['Q1'][] = isset($arrVal[$lst]['Q1'])?number_format($arrVal[$lst]['Q1'],2):'';
	        $avgQ['Q2_1'][] = isset($arrVal[$lst]['Q2_1'])?number_format($arrVal[$lst]['Q2_1'],2):'';
	        $avgQ['Q2_2'][] = isset($arrVal[$lst]['Q2_2'])?number_format($arrVal[$lst]['Q2_2'],2):'';
	        $avgQ['Q2_3'][] = isset($arrVal[$lst]['Q2_3'])?number_format($arrVal[$lst]['Q2_3'],2):'';
	        $avgQ['Q2_4'][] = isset($arrVal[$lst]['Q2_4'])?number_format($arrVal[$lst]['Q2_4'],2):'';
	        $avgQ['Q2_5'][] = isset($arrVal[$lst]['Q2_5'])?number_format($arrVal[$lst]['Q2_5'],2):'';
	        $avgQ['Q2_6'][] = isset($arrVal[$lst]['Q2_6'])?number_format($arrVal[$lst]['Q2_6'],2):'';
	        
	        $avgQD['Q1'][$lst] = isset($arrValDivision[$lst]['Q1'])?number_format($arrValDivision[$lst]['Q1'],2):'';
	        $avgQD['Q2_1'][$lst] = isset($arrValDivision[$lst]['Q2_1'])?number_format($arrValDivision[$lst]['Q2_1'],2):'';
	        $avgQD['Q2_2'][$lst] = isset($arrValDivision[$lst]['Q2_2'])?number_format($arrValDivision[$lst]['Q2_2'],2):'';
	        $avgQD['Q2_3'][$lst] = isset($arrValDivision[$lst]['Q2_3'])?number_format($arrValDivision[$lst]['Q2_3'],2):'';
	        $avgQD['Q2_4'][$lst] = isset($arrValDivision[$lst]['Q2_4'])?number_format($arrValDivision[$lst]['Q2_4'],2):'';
	        $avgQD['Q2_5'][$lst] = isset($arrValDivision[$lst]['Q2_5'])?number_format($arrValDivision[$lst]['Q2_5'],2):'';
	        $avgQD['Q2_6'][$lst] = isset($arrValDivision[$lst]['Q2_6'])?number_format($arrValDivision[$lst]['Q2_6'],2):'';
	        
	        $avgQR['Q1'][$lst] = isset($arrValRegion[$lst]['Q1'])?number_format($arrValRegion[$lst]['Q1'],2):'';
	        $avgQR['Q2_1'][$lst] = isset($arrValRegion[$lst]['Q2_1'])?number_format($arrValRegion[$lst]['Q2_1'],2):'';
	        $avgQR['Q2_2'][$lst] = isset($arrValRegion[$lst]['Q2_2'])?number_format($arrValRegion[$lst]['Q2_2'],2):'';
	        $avgQR['Q2_3'][$lst] = isset($arrValRegion[$lst]['Q2_3'])?number_format($arrValRegion[$lst]['Q2_3'],2):'';
	        $avgQR['Q2_4'][$lst] = isset($arrValRegion[$lst]['Q2_4'])?number_format($arrValRegion[$lst]['Q2_4'],2):'';
	        $avgQR['Q2_5'][$lst] = isset($arrValRegion[$lst]['Q2_5'])?number_format($arrValRegion[$lst]['Q2_5'],2):'';
	        $avgQR['Q2_6'][$lst] = isset($arrValRegion[$lst]['Q2_6'])?number_format($arrValRegion[$lst]['Q2_6'],2):'';
	    }
	    	    
	    $dataSeriesLabels[] = new PHPExcel_Chart_DataSeriesValues('String', 'OverallPerformance!$B$6', NULL, 1);
	    $xAxisTickValues[] = new PHPExcel_Chart_DataSeriesValues('String', 'OverallPerformance!$C$5:$N$5', NULL, 12);
	    $dataSeriesValues = [
	        new PHPExcel_Chart_DataSeriesValues('Number', 'OverallPerformance!$C$6:$N$6', NULL, 12),
	    ];
	    
	    $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);
	    $objWorksheet->fromArray(
	        [   
            	                     [ 'สรุปความพึงพอใจโดยรวมจากแบบสอบถามผ่าน SMS : '.$date],
            	                     ['หน่วย : '.$ShowAgent],
	        ],
	        null,
	        'B1',
	        FALSE
	    );
	    $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);
	    $objWorksheet->fromArray(
	       [    $arrMouth,
	            $avgQ['Q1'],
	            $arrTotalSurveyQ1,
	       ],
	        null,
	        'B5',
	        FALSE
	    );
	    
	    $series = new PHPExcel_Chart_DataSeries(
	        PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
	        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
	        range(0, count($dataSeriesValues)-1),			// plotOrder
	        $dataSeriesLabels,								// plotLabel
	        $xAxisTickValues,								// plotCategory
	        $dataSeriesValues								// plotValues
	    );
	    	    
	    $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);
	    $objWorksheet->fromArray(
	        [   
	            $avgQ['Q2_1'],
	            $avgQ['Q2_2'],
	            $avgQ['Q2_3'],
	            $avgQ['Q2_4'],
	            $avgQ['Q2_5'],
	            $avgQ['Q2_6'],
	        ],
	        null,
	        'B20',
	        FALSE
	   );  
	    
	    $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);	    
	    $plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
	    $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title = new PHPExcel_Chart_Title('ข้อ1. คะแนนความประทับใจโดยรวม');
        $axis = new PHPExcel_Chart_Axis();
        $axis->setAxisOptionsProperties('nextTo', null, null, null, null, null, 0, 5, 1, 1);
	    $secondaryYAxisLabel = new PHPExcel_Chart_Title('');
	    //	Create the chart
	    $chart = new PHPExcel_Chart(
	        'chart1',		// name
	        $title,			// title
	        NULL,		// legend
	        $plotArea,		// plotArea
	        true,			// plotVisibleOnly
	        0,				// displayBlanksAs
	        NULL,			// xAxisLabel
	        NULL,	// yAxisLabel
	        $axis,
	        null,
	        null,
	        null,
	        null    // secondaryYAxisLabel
	    );
	    
	    $chart->setTopLeftPosition('B9');
	    $chart->setBottomRightPosition('O17');
        $objWorksheet->addChart($chart);
///////////////////////////////////////////////////////////////////////////////End OverallPerformance
////////////////////////////////////////////////////////////////////////////////Start PerformanceByAgent
        $agencyName = 'คะแนนเฉลี่ยหน่วย : '.$ShowAgent;
        $agencyDivision = 'คะแนนเฉลี่ย : Division';
        $agencyRegion = 'คะแนนเฉลี่ยหน่วย : Region';
        //var_dump($avgQD['Q1'][$dateKey]);exit();
        if ($dateKey!=='') {
            $avgQAll_agency = [$agencyName, $avgQ['Q1'][$dateKey], $avgQ['Q2_1'][$dateKey], $avgQ['Q2_2'][$dateKey], $avgQ['Q2_3'][$dateKey], $avgQ['Q2_4'][$dateKey], $avgQ['Q2_5'][$dateKey], $avgQ['Q2_6'][$dateKey]];
            $avgQAll_Division = [$agencyDivision, $avgQD['Q1'][$dateKey], $avgQD['Q2_1'][$dateKey], $avgQD['Q2_2'][$dateKey], $avgQD['Q2_3'][$dateKey], $avgQD['Q2_4'][$dateKey], $avgQD['Q2_5'][$dateKey], $avgQD['Q2_6'][$dateKey]];
            $avgQAll_Region = [$agencyRegion, $avgQR['Q1'][$dateKey], $avgQR['Q2_1'][$dateKey], $avgQR['Q2_2'][$dateKey], $avgQR['Q2_3'][$dateKey], $avgQR['Q2_4'][$dateKey], $avgQR['Q2_5'][$dateKey], $avgQR['Q2_6'][$dateKey]];
        }else {
            $avgQAll_agency = [$agencyName];
            $avgQAll_Division = [$agencyDivision];
            $avgQAll_Region = [$agencyRegion];
        }
        
        $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(1)->setTitle('PerformanceByAgent');
        $objWorksheet->fromArray(
            [
                [  'คะแนนรายตัวแทน  : '.$date],
                ['หน่วย : '.$ShowAgent],
            ],
            null,
            'B1',
            FALSE
        );
        $objWorksheet->fromArray(
            [
                $avgQAll_agency,
                $avgQAll_Division,
                $avgQAll_Region,
            ],
            null,
            'C6',
            FALSE
        );
       
        $startRow = 11;
        $haystack = [];
        $haystack1 = [];
        $l=[];
        //var_dump($dataAgency);exit();
        if ($dateKey !='' && isset($dataAgency[$dateKey])) {
            foreach ($dataAgency[$dateKey] as $key1 => $agc){
                if ($agency['agencyCD'] == $key1) {
                    foreach ($agc as $key => $ag){
                        $sum1=0;$sum2=0;$sum3=0;$sum4=0;$sum5=0;$sum6=0;$sum7=0;
                        $count1=0;$count2=0;$count3=0;$count4=0;$count5=0;$count6=0;$count7=0;
                        foreach ($ag as $cus){
                            if (in_array($key1, $haystack1)) {
                                if (in_array($key, $haystack)) {
                                    $l[] = ['',$cus[0],$cus[1],$cus[2],$cus[3],$cus[4],$cus[5],$cus[6],$cus[7],$cus[8]];
                                }else{
                                    $l[] = [$arrAgent[$key],$cus[0],$cus[1],$cus[2],$cus[3],$cus[4],$cus[5],$cus[6],$cus[7],$cus[8]];
                                    $haystack[] =  $key;
                                }
                            }else{
                                if (in_array($key, $haystack)) {
                                    $l[] = ['',$cus[0],$cus[1],$cus[2],$cus[3],$cus[4],$cus[5],$cus[6],$cus[7],$cus[8]];
                                }else{
                                    $l[] = [$arrAgent[$key],$cus[0],$cus[1],$cus[2],$cus[3],$cus[4],$cus[5],$cus[6],$cus[7],$cus[8]];
                                    $haystack[] =  $key;
                                }
                                $haystack1[] = $key1;
                            }
                            //$count0 +=($cus[0]!=0&&$cus[0]!='')?1:0;
                            $count1 +=($cus[1]!=0&&$cus[1]!='')?1:0;
                            $count2 +=($cus[2]!=0&&$cus[2]!='')?1:0;
                            $count3 +=($cus[3]!=0&&$cus[3]!='')?1:0;
                            $count4 +=($cus[4]!=0&&$cus[4]!='')?1:0;
                            $count5 +=($cus[5]!=0&&$cus[5]!='')?1:0;
                            $count6 +=($cus[6]!=0&&$cus[6]!='')?1:0;
                            $count7 +=($cus[7]!=0&&$cus[7]!='')?1:0;
                            $sum1+=$cus[1]; $sum2+=$cus[2]; $sum3+=$cus[3]; $sum4+=$cus[4]; $sum5+=$cus[5]; $sum6+=$cus[6];$sum7+=$cus[7];
                        }
                        $l[] = ['คะแนนเฉลี่ยรายตัวแทน',''
                            ,($sum1>0)?$sum1/$count1:''
                            ,($sum2>0)?$sum2/$count2:''
                            ,($sum3>0)?$sum3/$count3:''
                            ,($sum4>0)?$sum4/$count4:''
                            ,($sum5>0)?$sum5/$count5:''
                            ,($sum6>0)?$sum6/$count6:''
                            ,($sum7>0)?$sum7/$count7:''
                            ,''];
                    }
                }
            }
        }
        $style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'argb' => 'FF000000'),
                ),
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_Sum = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'dcdcdc'),
            ),            
            'font' => array(
                'bold' => true,
            )
        );
        $startRow = 11;
        foreach ($l as $row){
            $objWorksheet->fromArray(
                [
                    $row,
                ],
                null,
                'B'.$startRow,
                FALSE
            );
            $objWorksheet->getStyle('D'.$startRow.':'.'J'.$startRow)->applyFromArray($style_center);
            $objWorksheet->getStyle('B'.$startRow.':'.'K'.$startRow)->applyFromArray($style);
            if ($row[0] == 'คะแนนเฉลี่ยรายตัวแทน') {
                $objWorksheet->mergeCells('B'.$startRow.':'.'C'.$startRow);
                $objWorksheet->getStyle('B'.$startRow.':'.'K'.$startRow)->applyFromArray($style_Sum);
            }
            $startRow++;
        }       
////////////////////////////////////////////////////////////////////////////////End PerformanceByAgent
////////////////////////////////////////////////////////////////////////////////Start IncidentReport
        $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(2)->setTitle('IncidentReport');
        $objWorksheet->fromArray(
            [
                [ 'สรุปผลการส่งเอสเอ็มเอส : '.$date],
                ['หน่วย : '.$ShowAgent],
            ],
            null,
            'B1',
            FALSE
       );
        
        $objWorksheet->fromArray(
            [
                $cntSentSMS[$agency['agencyCD']],
                $cntSMSSS[$agency['agencyCD']],
            ],
            null,
            'C6',
            FALSE
        );
        
        $objWorksheet->fromArray(
            [
                $countAns[$agency['agencyCD']],
            ],
            null,
            'C9',
            FALSE
        );
        
        $objWorksheet->fromArray(
            [
                $cntSMS_USS[$agency['agencyCD']],
            ],
            null,
            'C12',
            FALSE
        );
        
        if ($dateKey != '') {
            $startRow = 19;
            foreach ($sheet3Agent[$agency['agencyCD']] as $key2 => $l2){
                if (isset($l2[$dateKey])) {
                    $agentName = isset($l2[$dateKey]['agentCD'])?$l2[$dateKey]['agentCD']:'';
                    $cntSendSMS = isset($l2[$dateKey]['cntSendSMS'])?$l2[$dateKey]['cntSendSMS']:'';
                    $cntSMSSS = isset($l2[$dateKey]['cntSMSSS'])?$l2[$dateKey]['cntSMSSS']:'';
                    $avgSMSSS = ($cntSendSMS!=0)?number_format(($cntSMSSS/$cntSendSMS)*100,2).'%':'';
                    $countAns = isset($l2[$dateKey]['countAns'])?$l2[$dateKey]['countAns']:'';
                    $avgAns = ($cntSMSSS!=0)?number_format(($countAns/$cntSMSSS)*100,2).'%':'';
                    
                    $cntError = 0;
                    $cntRollback = 0;
                    
                    $objWorksheet->fromArray(
                        [
                            [
                                $agentName
                                , $cntSendSMS
                                , $cntSMSSS
                                , $avgSMSSS
                                , $countAns
                                , $avgAns
                                , $cntError
                                , $cntRollback
                            ],
                        ],
                        null,
                        'B'.$startRow,
                        FALSE
                        );
                    $startRow++;
                }
                
            }
        }
        
////////////////////////////////////////////////////////////////////////////////End IncidentReport        
//////////////////////////////////////////////////////////////////////////////Excel Start 
	    $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
	    $objWriter->setIncludeCharts(TRUE);	    
	    $objWriter->save('./application/aia_share/'.$this->rep_name);
//////////////////////////////////////////////////////////////////////////////Excel End	  
	    unset($this->objPHPExcel);
	    return TRUE;
	}
	
	public function excel_AvgALL($params,$agency=[],$date=''){
	    
	    $mouth = [1,2,3,4,5,6,7,8,9,10,11,12];
	    $q_m = array("1" => "ม.ค.",
	        "2" => "ก.พ.",
	        "3" => "มี.ค.",
	        "4" => "เม.ย.",
	        "5" => "พ.ค.",
	        "6" => "มิ.ย.",
	        "7" => "ก.ค.",
	        "8" => "ส.ค.",
	        "9" => "ก.ย.",
	        "10" => "ต.ค.",
	        "11" => "พ.ย.",
	        "12" => "ธ.ค."
	    );
	    
	    $this->objPHPExcel = new PHPExcel();
	    $this->rep_name = 'al_aia_report_'.$agency['agencyCD'].'.xlsx';
	    $rep_template = 'al_aia_report.xlsx';
	    if (!file_exists('./application/excel_template/'.$rep_template)) {
	        exit("Template not found. " .$rep_template);
	    }
	    // Load Template
	    $this->objPHPExcel = PHPExcel_IOFactory::load('./application/excel_template/'.$rep_template);
	    
	    $arr = [];
	    $arrQ = [];
	    $arrQ['Q1_1']=0;$arrQ['Q1_2']=0;$arrQ['Q1_3']=0; $arrQ['Q1_4']=0;$arrQ['Q1_5']=0;
	    $arrQ['Q2_1_1']=0;$arrQ['Q2_1_2']=0;$arrQ['Q2_1_3']=0; $arrQ['Q2_1_4']=0;$arrQ['Q2_1_5']=0;
	    $arrQ['Q2_2_1']=0;$arrQ['Q2_2_2']=0;$arrQ['Q2_2_3']=0; $arrQ['Q2_2_4']=0;$arrQ['Q2_2_5']=0;
	    $arrQ['Q2_3_1']=0;$arrQ['Q2_3_2']=0;$arrQ['Q2_3_3']=0; $arrQ['Q2_3_4']=0;$arrQ['Q2_3_5']=0;
	    $arrQ['Q2_4_1']=0;$arrQ['Q2_4_2']=0;$arrQ['Q2_4_3']=0; $arrQ['Q2_4_4']=0;$arrQ['Q2_4_5']=0;
	    $arrQ['Q2_5_1']=0;$arrQ['Q2_5_2']=0;$arrQ['Q2_5_3']=0; $arrQ['Q2_5_4']=0;$arrQ['Q2_5_5']=0;
	    $arrQ['Q2_6_1']=0;$arrQ['Q2_6_2']=0;$arrQ['Q2_6_3']=0; $arrQ['Q2_6_4']=0;$arrQ['Q2_6_5']=0;
	    
	    $sheet3Agent = [];
	    $cntSentSMS = [];
	    $cntSMSSS = [];
	    $countAns = [];
	    $cntSMS_USS = [];
	    $countSurveyQ1[] = 0; $countSurveyQ2_1[]=0; $countSurveyQ2_2[]=0; $countSurveyQ2_3[]=0; $countSurveyQ2_4[]=0; $countSurveyQ2_5[]=0; $countSurveyQ2_6[]=0;
	    $cntSvDQ1[] = 0; $cntSvDQ2_1[]=0; $cntSvDQ2_2[]=0; $cntSvDQ2_3[]=0; $cntSvDQ2_4[]=0; $cntSvDQ2_5[]=0; $cntSvDQ2_6[]=0;
	    $cntSvRQ1[] = 0; $cntSvRQ2_1[]=0; $cntSvRQ2_2[]=0; $cntSvRQ2_3[]=0; $cntSvRQ2_4[]=0; $cntSvRQ2_5[]=0; $cntSvRQ2_6[]=0;
	    $Q1[]=0; $Q2_1[]=0;  $Q2_2[]=0;  $Q2_3[]=0;  $Q2_4[]=0;  $Q2_5[]=0;  $Q2_6[]=0;
	    $DQ1[]=0; $DQ2_1[]=0;  $DQ2_2[]=0;  $DQ2_3[]=0;  $DQ2_4[]=0;  $DQ2_5[]=0;  $DQ2_6[]=0;
	    $RQ1[]=0; $RQ2_1[]=0;  $RQ2_2[]=0;  $RQ2_3[]=0;  $RQ2_4[]=0;  $RQ2_5[]=0;  $RQ2_6[]=0;
	    
	    $ShowAgent = $agency['agencyName'];
	    $arrAgent = [];
	    $dataAgency = [];
	    $haystackAgent = [];
	    
	    foreach ($mouth as $lm){
	        $arr[$lm]= [
	            //count Num Answer by Q
	            'TotalQ1' => 0
	            ,'TotalQ2_1' => 0
	            ,'TotalQ2_2' => 0
	            ,'TotalQ2_3' => 0
	            ,'TotalQ2_4' => 0
	            ,'TotalQ2_5' => 0
	            ,'TotalQ2_6' => 0
	            //sum value By Q
	            ,'Q1' => 0
	            ,'Q2_1' => 0
	            ,'Q2_2' => 0
	            ,'Q2_3' => 0
	            ,'Q2_4' => 0
	            ,'Q2_5' => 0
	            ,'Q2_6' => 0
	        ];
	        $cntSentSMS[$agency['agencyCD']][$lm] = 0;
	        $cntSMSSS[$agency['agencyCD']][$lm] = 0;
	        $countAns[$agency['agencyCD']][$lm] = 0;
	        $cntSMS_USS[$agency['agencyCD']][$lm] = 0;
	        
	        $countSurveyQ1[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_1[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_2[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_3[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_4[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_5[$agency['agencyCD'].'_'.$lm] = 0;
	        $countSurveyQ2_6[$agency['agencyCD'].'_'.$lm] = 0;
	        
	        $cntSvDQ1[$agency['division'].'_'.$lm] = 0; $cntSvDQ2_1[$agency['division'].'_'.$lm]=0;
	        $cntSvDQ2_2[$agency['division'].'_'.$lm]=0; $cntSvDQ2_3[$agency['division'].'_'.$lm]=0;
	        $cntSvDQ2_4[$agency['division'].'_'.$lm]=0; $cntSvDQ2_5[$agency['division'].'_'.$lm]=0;
	        $cntSvDQ2_6[$agency['division'].'_'.$lm]=0;
	        
	        $cntSvRQ1[$agency['region'].'_'.$lm] = 0; $cntSvRQ2_1[$agency['region'].'_'.$lm]=0;
	        $cntSvRQ2_2[$agency['region'].'_'.$lm]=0; $cntSvRQ2_3[$agency['region'].'_'.$lm]=0;
	        $cntSvRQ2_4[$agency['region'].'_'.$lm]=0; $cntSvRQ2_5[$agency['region'].'_'.$lm]=0;
	        $cntSvRQ2_6[$agency['region'].'_'.$lm]=0;
	        
	        $Q1[$agency['agencyCD'].'_'.$lm]=0; $Q2_1[$agency['agencyCD'].'_'.$lm]=0;  $Q2_2[$agency['agencyCD'].'_'.$lm]=0;
	        $Q2_3[$agency['agencyCD'].'_'.$lm]=0;  $Q2_4[$agency['agencyCD'].'_'.$lm]=0;  $Q2_5[$agency['agencyCD'].'_'.$lm]=0;  $Q2_6[$agency['agencyCD'].'_'.$lm]=0;
	        
	        $DQ1[$agency['division'].'_'.$lm]=0; $DQ2_1[$agency['division'].'_'.$lm]=0;  $DQ2_2[$agency['division'].'_'.$lm]=0;  $DQ2_3[$agency['division'].'_'.$lm]=0;
	        $DQ2_4[$agency['division'].'_'.$lm]=0;  $DQ2_5[$agency['division'].'_'.$lm]=0;  $DQ2_6[$agency['division'].'_'.$lm]=0;
	        
	        $RQ1[$agency['region'].'_'.$lm]=0; $RQ2_1[$agency['region'].'_'.$lm]=0;  $RQ2_2[$agency['region'].'_'.$lm]=0;  $RQ2_3[$agency['region'].'_'.$lm]=0;
	        $RQ2_4[$agency['region'].'_'.$lm]=0;  $RQ2_5[$agency['region'].'_'.$lm]=0;  $RQ2_6[$agency['region'].'_'.$lm]=0;
	    }
	    
	    foreach ($params as $lsts){
	        $arrKey = explode('/', $lsts['effectiveMonth']);
	        $key = intval($arrKey[1]);
	        $keyChka = $lsts['agentCD'].'_'.$key;
	        $keyChk = $agency['agencyCD'].'_'.$key;
	        $keyChkD = $agency['division'].'_'.$key;
	        $keyChkR = $agency['region'].'_'.$key;
	           
	        if ($lsts['flagID'] == 1 && ($lsts['agencyCD'] == $agency['agencyCD'])) {
	            $dataAgency[$key][$lsts['agencyCD']][$lsts['agentCD']][] = [
	                                                                        $lsts['Q1'], $lsts['Q2_1'], $lsts['Q2_2']
                                                        	                , $lsts['Q2_3'], $lsts['Q2_4'], $lsts['Q2_5']
                                                        	                , $lsts['Q2_6'], $lsts['Q3']
	                
	                                                                       ];
	            
	            if (in_array($keyChka, $haystackAgent)) {
	                $sheet3Agent[$lsts['agencyCD']][$lsts['agentCD']][$key] = [
	                    'agentCD' => $lsts['agentName']
	                    ,'cntSendSMS' => $cntA[$keyChka][0]+=1
	                    ,'cntSMSSS' => ($lsts['sms_status']==1)?$cntA[$keyChka][1]+=1:$cntA[$keyChka][1]+=0
	                    ,'countAns' => (($lsts['Q1']!=''&&$lsts['Q1']!='0')
	                        ||($lsts['Q2_1']!=''&&$lsts['Q2_1']!='0')
	                        ||($lsts['Q2_2']!=''&&$lsts['Q2_2']!='0')
	                        ||($lsts['Q2_3']!=''&&$lsts['Q2_3']!='0')
	                        ||($lsts['Q2_4']!=''&&$lsts['Q2_4']!='0')
	                        ||($lsts['Q2_5']!=''&&$lsts['Q2_5']!='0')
	                        ||($lsts['Q2_6']!=''&&$lsts['Q2_6']!='0')
	                        ||($lsts['Q3']!=''&&$lsts['Q3']!='0'))?$cntA[$keyChka][2]+=1:$cntA[$keyChka][2]+=0
	                    ,'cntSMSUSS' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=1)?$cntA[$keyChka][3]+=1:$cntA[$keyChka][3]+=0
	                    ,'cntSMS_Error' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=3 && $lsts['sms_status']!=1)?$cntA[$keyChka][4]+=1:$cntA[$keyChka][4]+=0
	                    ,'cntSendSMS_RBK' => ($lsts['flagID'] == 1 && $lsts['sms_status']==3)?$cntA[$keyChka][5]+=1:$cntA[$keyChka][5]+=0
	                    ,'Q1' => $q1Ag[$keyChka]+=$lsts['Q1']
	                    ,'Q2_1'=> $q2_1Ag[$keyChka]+=$lsts['Q2_1']
	                    ,'Q2_2' => $q2_2Ag[$keyChka]+=$lsts['Q2_2']
	                    ,'Q2_3' => $q2_3Ag[$keyChka]+=$lsts['Q2_3']
	                    ,'Q2_4' => $q2_4Ag[$keyChka]+=$lsts['Q2_4']
	                    ,'Q2_5' => $q2_5Ag[$keyChka]+=$lsts['Q2_5']
	                    ,'Q2_6' => $q2_6Ag[$keyChka]+=$lsts['Q2_6']  
	                ];
	            }else {
	                $sheet3Agent[$lsts['agencyCD']][$lsts['agentCD']][$key] = [
	                    'agentCD' => $lsts['agentName']
	                    ,'cntSendSMS' => $cntA[$keyChka][0] = 1
	                    ,'cntSMSSS' => ($lsts['sms_status']==1)?$cntA[$keyChka][1]=1:$cntA[$keyChka][1]=0
	                    ,'countAns' => (($lsts['Q1']!=''&&$lsts['Q1']!='0')
	                        ||($lsts['Q2_1']!=''&&$lsts['Q2_1']!='0')
	                        ||($lsts['Q2_2']!=''&&$lsts['Q2_2']!='0')
	                        ||($lsts['Q2_3']!=''&&$lsts['Q2_3']!='0')
	                        ||($lsts['Q2_4']!=''&&$lsts['Q2_4']!='0')
	                        ||($lsts['Q2_5']!=''&&$lsts['Q2_5']!='0')
	                        ||($lsts['Q2_6']!=''&&$lsts['Q2_6']!='0')
	                        ||($lsts['Q3']!=''&&$lsts['Q3']!='0'))?$cntA[$keyChka][2]=1:$cntA[$keyChka][2]=0
	                    ,'cntSMSUSS' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=1)?$cntA[$keyChka][3]=1:$cntA[$keyChka][3]=0
	                    ,'cntSMS_Error' => ($lsts['flagID'] == 1 && $lsts['sms_status']!=3 && $lsts['sms_status']!=1)?$cntA[$keyChka][4]=1:$cntA[$keyChka][4]=0
	                    ,'cntSendSMS_RBK' => ($lsts['flagID'] == 1 && $lsts['sms_status']==3)?$cntA[$keyChka][5]=1:$cntA[$keyChka][5]=0
	                    ,'Q1' => isset($lsts['Q1'])?$q1Ag[$keyChka]=$lsts['Q1']:$q1Ag[$keyChka]=0
	                    ,'Q2_1'=> isset($lsts['Q2_1'])?$q2_1Ag[$keyChka]=$lsts['Q2_1']:$q2_1Ag[$keyChka]=0
	                    ,'Q2_2' => isset($lsts['Q2_2'])?$q2_2Ag[$keyChka]=$lsts['Q2_2']:$q2_2Ag[$keyChka]=0
	                    ,'Q2_3' => isset($lsts['Q2_3'])?$q2_3Ag[$keyChka]=$lsts['Q2_3']:$q2_3Ag[$keyChka]=0
	                    ,'Q2_4' => isset($lsts['Q2_4'])?$q2_4Ag[$keyChka]=$lsts['Q2_4']:$q2_4g[$keyChka]=0
	                    ,'Q2_5' => isset($lsts['Q2_5'])?$q2_5Ag[$keyChka]=$lsts['Q2_5']:$q2_5Ag[$keyChka]=0
	                    ,'Q2_6' => isset($lsts['Q2_6'])?$q2_6Ag[$keyChka]=$lsts['Q2_6']:$q2_6Ag[$keyChka]=0	                    
	                ];
	                $haystackAgent[] = $keyChka;
	            }
	        }
	        
	        $arrAgent[$lsts['agentCD']] = $lsts['agentName'];
	        
	        if ($agency['agencyCD'] === $lsts['agencyCD']) {
	            $arr[$key]= [
	                //count Num Answer by Q
	                'TotalQ1' => ($lsts['Q1']!=='0'&&$lsts['Q1']!=='')?$countSurveyQ1[$keyChk]+=1:$countSurveyQ1[$keyChk]+=0
	                ,'TotalQ2_1' => ($lsts['Q2_1']!=='0'&&$lsts['Q2_1']!=='')?$countSurveyQ2_1[$keyChk]+=1:$countSurveyQ2_1[$keyChk]+=0
	                ,'TotalQ2_2' => ($lsts['Q2_2']!=='0'&&$lsts['Q2_2']!=='')?$countSurveyQ2_2[$keyChk]+=1:$countSurveyQ2_2[$keyChk]+=0
	                ,'TotalQ2_3' => ($lsts['Q2_3']!=='0'&&$lsts['Q2_3']!=='')?$countSurveyQ2_3[$keyChk]+=1:$countSurveyQ2_3[$keyChk]+=0
	                ,'TotalQ2_4' => ($lsts['Q2_4']!=='0'&&$lsts['Q2_4']!=='')?$countSurveyQ2_4[$keyChk]+=1:$countSurveyQ2_4[$keyChk]+=0
	                ,'TotalQ2_5' => ($lsts['Q2_5']!=='0'&&$lsts['Q2_5']!=='')?$countSurveyQ2_5[$keyChk]+=1:$countSurveyQ2_5[$keyChk]+=0
	                ,'TotalQ2_6' => ($lsts['Q2_6']!=='0'&&$lsts['Q2_6']!=='')?$countSurveyQ2_6[$keyChk]+=1:$countSurveyQ2_6[$keyChk]+=0
	                //sum value By Q
	                ,'Q1' => $Q1[$keyChk]+=$lsts['Q1']
	                ,'Q2_1' => $Q2_1[$keyChk]+=$lsts['Q2_1']
	                ,'Q2_2' => $Q2_2[$keyChk]+=$lsts['Q2_2']
	                ,'Q2_3' => $Q2_3[$keyChk]+=$lsts['Q2_3']
	                ,'Q2_4' => $Q2_4[$keyChk]+=$lsts['Q2_4']
	                ,'Q2_5' => $Q2_5[$keyChk]+=$lsts['Q2_5']
	                ,'Q2_6' => $Q2_6[$keyChk]+=$lsts['Q2_6']
	            ];
	            
	            ////// count customer Take surveys start
	            if (($lsts['Q1']!=''&&$lsts['Q1']!='0')
	                ||($lsts['Q2_1']!=''&&$lsts['Q2_1']!='0')
	                ||($lsts['Q2_2']!=''&&$lsts['Q2_2']!='0')
	                ||($lsts['Q2_3']!=''&&$lsts['Q2_3']!='0')
	                ||($lsts['Q2_4']!=''&&$lsts['Q2_4']!='0')
	                ||($lsts['Q2_5']!=''&&$lsts['Q2_5']!='0')
	                ||($lsts['Q2_6']!=''&&$lsts['Q2_6']!='0')
	                ||($lsts['Q3']!=''&&$lsts['Q3']!='0')) {
	                    $countAns[$lsts['agencyCD']][$key] +=1;
	                }
	                ////// count customer Take surveys end
	                ///// count send sms start
	                if($lsts['flagID'] == 1){
	                    $cntSentSMS[$lsts['agencyCD']][$key] +=1;
	                }
	                ///// count send sms end
	                ///// count send sms success start
	                if($lsts['flagID'] == 1 && $lsts['sms_status']==1){
	                    $cntSMSSS[$lsts['agencyCD']][$key] +=1;
	                }
	                ///// count send sms success end
	                ///// count send sms Un success start
	                if($lsts['flagID'] == 1 && $lsts['sms_status']!=1){
	                    $cntSMS_USS[$lsts['agencyCD']][$key] +=1;
	                }
	                ///// count send sms Un success end
	        }
	        
	        if ($agency['division'] === $lsts['division']) {
	            $arrDivision[$key]= [
	                //count Num Answer by Q
	                'TotalQ1' => ($lsts['Q1']!=='0'&&$lsts['Q1']!=='')?$cntSvDQ1[$keyChkD]+=1:$cntSvDQ1[$keyChkD]+=0
	                ,'TotalQ2_1' => ($lsts['Q2_1']!=='0'&&$lsts['Q2_1']!=='')?$cntSvDQ2_1[$keyChkD]+=1:$cntSvDQ2_1[$keyChkD]+=0
	                ,'TotalQ2_2' => ($lsts['Q2_2']!=='0'&&$lsts['Q2_2']!=='')?$cntSvDQ2_2[$keyChkD]+=1:$cntSvDQ2_2[$keyChkD]+=0
	                ,'TotalQ2_3' => ($lsts['Q2_3']!=='0'&&$lsts['Q2_3']!=='')?$cntSvDQ2_3[$keyChkD]+=1:$cntSvDQ2_3[$keyChkD]+=0
	                ,'TotalQ2_4' => ($lsts['Q2_4']!=='0'&&$lsts['Q2_4']!=='')?$cntSvDQ2_4[$keyChkD]+=1:$cntSvDQ2_4[$keyChkD]+=0
	                ,'TotalQ2_5' => ($lsts['Q2_5']!=='0'&&$lsts['Q2_5']!=='')?$cntSvDQ2_5[$keyChkD]+=1:$cntSvDQ2_5[$keyChkD]+=0
	                ,'TotalQ2_6' => ($lsts['Q2_6']!=='0'&&$lsts['Q2_6']!=='')?$cntSvDQ2_6[$keyChkD]+=1:$cntSvDQ2_6[$keyChkD]+=0
	                //sum value By Q
	                ,'Q1' => $DQ1[$keyChkD]+=$lsts['Q1']
	                ,'Q2_1' => $DQ2_1[$keyChkD]+=$lsts['Q2_1']
	                ,'Q2_2' => $DQ2_2[$keyChkD]+=$lsts['Q2_2']
	                ,'Q2_3' => $DQ2_3[$keyChkD]+=$lsts['Q2_3']
	                ,'Q2_4' => $DQ2_4[$keyChkD]+=$lsts['Q2_4']
	                ,'Q2_5' => $DQ2_5[$keyChkD]+=$lsts['Q2_5']
	                ,'Q2_6' => $DQ2_6[$keyChkD]+=$lsts['Q2_6']
	            ];
	        }
	        
	        if ($agency['region'] === $lsts['region']) {
	            $arrRegion[$key]= [
	                //count Num Answer by Q
	                'TotalQ1' => ($lsts['Q1']!=='0'&&$lsts['Q1']!=='')?$cntSvRQ1[$keyChkR]+=1:$cntSvRQ1[$keyChkR]+=0
	                ,'TotalQ2_1' => ($lsts['Q2_1']!=='0'&&$lsts['Q2_1']!=='')?$cntSvRQ2_1[$keyChkR]+=1:$cntSvRQ2_1[$keyChkR]+=0
	                ,'TotalQ2_2' => ($lsts['Q2_2']!=='0'&&$lsts['Q2_2']!=='')?$cntSvRQ2_2[$keyChkR]+=1:$cntSvRQ2_2[$keyChkR]+=0
	                ,'TotalQ2_3' => ($lsts['Q2_3']!=='0'&&$lsts['Q2_3']!=='')?$cntSvRQ2_3[$keyChkR]+=1:$cntSvRQ2_3[$keyChkR]+=0
	                ,'TotalQ2_4' => ($lsts['Q2_4']!=='0'&&$lsts['Q2_4']!=='')?$cntSvRQ2_4[$keyChkR]+=1:$cntSvRQ2_4[$keyChkR]+=0
	                ,'TotalQ2_5' => ($lsts['Q2_5']!=='0'&&$lsts['Q2_5']!=='')?$cntSvRQ2_5[$keyChkR]+=1:$cntSvRQ2_5[$keyChkR]+=0
	                ,'TotalQ2_6' => ($lsts['Q2_6']!=='0'&&$lsts['Q2_6']!=='')?$cntSvRQ2_6[$keyChkR]+=1:$cntSvRQ2_6[$keyChkR]+=0
	                //sum value By Q
	                ,'Q1' => $RQ1[$keyChkR]+=$lsts['Q1']
	                ,'Q2_1' => $RQ2_1[$keyChkR]+=$lsts['Q2_1']
	                ,'Q2_2' => $RQ2_2[$keyChkR]+=$lsts['Q2_2']
	                ,'Q2_3' => $RQ2_3[$keyChkR]+=$lsts['Q2_3']
	                ,'Q2_4' => $RQ2_4[$keyChkR]+=$lsts['Q2_4']
	                ,'Q2_5' => $RQ2_5[$keyChkR]+=$lsts['Q2_5']
	                ,'Q2_6' => $RQ2_6[$keyChkR]+=$lsts['Q2_6']
	            ];
	        }
	    }
	    
	    $arrVal = [];
	    foreach ($arr as $keyMount=>$arrlst){
	        $arrVal[$keyMount] = [
	            'Q1' => ($arrlst['TotalQ1']>0)?$arrlst['Q1']/$arrlst['TotalQ1']:0
	            ,'Q2_1' => ($arrlst['TotalQ2_1']>0)?$arrlst['Q2_1']/$arrlst['TotalQ2_1']:0
	            ,'Q2_2' => ($arrlst['TotalQ2_2']>0)?$arrlst['Q2_2']/$arrlst['TotalQ2_2']:0
	            ,'Q2_3' => ($arrlst['TotalQ2_3']>0)?$arrlst['Q2_3']/$arrlst['TotalQ2_3']:0
	            ,'Q2_4' => ($arrlst['TotalQ2_4']>0)?$arrlst['Q2_4']/$arrlst['TotalQ2_4']:0
	            ,'Q2_5' => ($arrlst['TotalQ2_5']>0)?$arrlst['Q2_5']/$arrlst['TotalQ2_5']:0
	            ,'Q2_6' => ($arrlst['TotalQ2_6']>0)?$arrlst['Q2_6']/$arrlst['TotalQ2_6']:0
	        ];
	    }
	    //var_dump($arrVal);exit();
	    $arrValDivision = [];
	    foreach ($arrDivision as $keyMount=>$arrlst){
	        $arrValDivision[$keyMount] = [
	            'Q1' => ($arrlst['TotalQ1']>0)?$arrlst['Q1']/$arrlst['TotalQ1']:0
	            ,'Q2_1' => ($arrlst['Q2_1']>0)?$arrlst['Q2_1']/$arrlst['TotalQ2_1']:0
	            ,'Q2_2' => ($arrlst['Q2_2']>0)?$arrlst['Q2_2']/$arrlst['TotalQ2_2']:0
	            ,'Q2_3' => ($arrlst['Q2_3']>0)?$arrlst['Q2_3']/$arrlst['TotalQ2_3']:0
	            ,'Q2_4' => ($arrlst['Q2_4']>0)?$arrlst['Q2_4']/$arrlst['TotalQ2_4']:0
	            ,'Q2_5' => ($arrlst['Q2_5']>0)?$arrlst['Q2_5']/$arrlst['TotalQ2_5']:0
	            ,'Q2_6' => ($arrlst['Q2_6']>0)?$arrlst['Q2_6']/$arrlst['TotalQ2_6']:0
	        ];
	    }
	    
	    $arrValRegion = [];
	    foreach ($arrRegion as $keyMount=>$arrlst){
	        $arrValRegion[$keyMount] = [
	            'Q1' => ($arrlst['TotalQ1']>0)?$arrlst['Q1']/$arrlst['TotalQ1']:0
	            ,'Q2_1' => ($arrlst['Q2_1']>0)?$arrlst['Q2_1']/$arrlst['TotalQ2_1']:0
	            ,'Q2_2' => ($arrlst['Q2_2']>0)?$arrlst['Q2_2']/$arrlst['TotalQ2_2']:0
	            ,'Q2_3' => ($arrlst['Q2_3']>0)?$arrlst['Q2_3']/$arrlst['TotalQ2_3']:0
	            ,'Q2_4' => ($arrlst['Q2_4']>0)?$arrlst['Q2_4']/$arrlst['TotalQ2_4']:0
	            ,'Q2_5' => ($arrlst['Q2_5']>0)?$arrlst['Q2_5']/$arrlst['TotalQ2_5']:0
	            ,'Q2_6' => ($arrlst['Q2_6']>0)?$arrlst['Q2_6']/$arrlst['TotalQ2_6']:0
	        ];
	    }
	    	    
 	    $avgQ = [];
	    $avgQ['Q1'] = ['ข้อ1. คะแนนความประทับใจโดยรวม'];
	    $avgQ['Q2_1'] = ['ข้อ 2-1'];
	    $avgQ['Q2_2'] = ['ข้อ 2-2'];
	    $avgQ['Q2_3'] = ['ข้อ 2-3'];
	    $avgQ['Q2_4'] = ['ข้อ 2-4'];
	    $avgQ['Q2_5'] = ['ข้อ 2-5'];
	    $avgQ['Q2_6'] = ['ข้อ 2-6'];
	    
	    foreach ($mouth as $lst){
	        $avgQ['Q1'][] = isset($arrVal[$lst]['Q1'])?number_format($arrVal[$lst]['Q1'],2):0;
	        $avgQ['Q2_1'][] = isset($arrVal[$lst]['Q2_1'])?number_format($arrVal[$lst]['Q2_1'],2):0;
	        $avgQ['Q2_2'][] = isset($arrVal[$lst]['Q2_2'])?number_format($arrVal[$lst]['Q2_2'],2):0;
	        $avgQ['Q2_3'][] = isset($arrVal[$lst]['Q2_3'])?number_format($arrVal[$lst]['Q2_3'],2):0;
	        $avgQ['Q2_4'][] = isset($arrVal[$lst]['Q2_4'])?number_format($arrVal[$lst]['Q2_4'],2):0;
	        $avgQ['Q2_5'][] = isset($arrVal[$lst]['Q2_5'])?number_format($arrVal[$lst]['Q2_5'],2):0;
	        $avgQ['Q2_6'][] = isset($arrVal[$lst]['Q2_6'])?number_format($arrVal[$lst]['Q2_6'],2):0;
	        
	        $avgQD['Q1'][$lst] = isset($arrValDivision[$lst]['Q1'])?number_format($arrValDivision[$lst]['Q1'],2):0;
	        $avgQD['Q2_1'][$lst] = isset($arrValDivision[$lst]['Q2_1'])?number_format($arrValDivision[$lst]['Q2_1'],2):0;
	        $avgQD['Q2_2'][$lst] = isset($arrValDivision[$lst]['Q2_2'])?number_format($arrValDivision[$lst]['Q2_2'],2):0;
	        $avgQD['Q2_3'][$lst] = isset($arrValDivision[$lst]['Q2_3'])?number_format($arrValDivision[$lst]['Q2_3'],2):0;
	        $avgQD['Q2_4'][$lst] = isset($arrValDivision[$lst]['Q2_4'])?number_format($arrValDivision[$lst]['Q2_4'],2):0;
	        $avgQD['Q2_5'][$lst] = isset($arrValDivision[$lst]['Q2_5'])?number_format($arrValDivision[$lst]['Q2_5'],2):0;
	        $avgQD['Q2_6'][$lst] = isset($arrValDivision[$lst]['Q2_6'])?number_format($arrValDivision[$lst]['Q2_6'],2):0;
	        
	        $avgQR['Q1'][$lst] = isset($arrValRegion[$lst]['Q1'])?number_format($arrValRegion[$lst]['Q1'],2):0;
	        $avgQR['Q2_1'][$lst] = isset($arrValRegion[$lst]['Q2_1'])?number_format($arrValRegion[$lst]['Q2_1'],2):0;
	        $avgQR['Q2_2'][$lst] = isset($arrValRegion[$lst]['Q2_2'])?number_format($arrValRegion[$lst]['Q2_2'],2):0;
	        $avgQR['Q2_3'][$lst] = isset($arrValRegion[$lst]['Q2_3'])?number_format($arrValRegion[$lst]['Q2_3'],2):0;
	        $avgQR['Q2_4'][$lst] = isset($arrValRegion[$lst]['Q2_4'])?number_format($arrValRegion[$lst]['Q2_4'],2):0;
	        $avgQR['Q2_5'][$lst] = isset($arrValRegion[$lst]['Q2_5'])?number_format($arrValRegion[$lst]['Q2_5'],2):0;
	        $avgQR['Q2_6'][$lst] = isset($arrValRegion[$lst]['Q2_6'])?number_format($arrValRegion[$lst]['Q2_6'],2):0;
	    }
	    
	    $tempDate = isset($_POST['dateEnd'])?$_POST['dateEnd']:date('Y-m-d');
	    $dateSelect = !empty($tempDate)?$tempDate:date('Y-m-d');
	    
	    if (!empty($dateSelect)) {
	        $arrKey = explode('-', $dateSelect);	        
	        $dateKey = intval($arrKey[1]);
	    }else {
	        $dateKey = '';
	    }

	    $year = explode(' ', $date);
	    $mouth2 = $q_m[(int)$dateKey-2].' '.$year[1];
	    $mouth1 = $q_m[(int)$dateKey-1].' '.$year[1];
	    $mouth0 = $q_m[(int)$dateKey].' '.$year[1];
	    $arrMouth = [$mouth2,$mouth1,$mouth0];
	    $arrMouthKey = [(int)$dateKey-2,(int)$dateKey-1,(int)$dateKey];
	    
	    $ShowAgent = $agency['agencyName'];
	    $ShowDivision = $agency['division'];
	    $ShowRegion = $agency['region'];
	    $ShowMouth = 'เดือน '.$date;

	    foreach ($arrMouthKey as $i) {
            $avgQAll_agency = [$ShowAgent, '','',$avgQ['Q1'][$i], $avgQ['Q2_1'][$i], $avgQ['Q2_2'][$i], $avgQ['Q2_3'][$i], $avgQ['Q2_4'][$i], $avgQ['Q2_5'][$i], $avgQ['Q2_6'][$i]];
            $sumMouthAC[$i] = ($avgQ['Q1'][$i] + $avgQ['Q2_1'][$i] + $avgQ['Q2_2'][$i] + $avgQ['Q2_3'][$i] + $avgQ['Q2_4'][$i] + $avgQ['Q2_5'][$i] + $avgQ['Q2_6'][$i])/7;
            //$sumMouthQD[$i] = ($avgQD['Q1'][$i] + $avgQD['Q2_1'][$i] + $avgQD['Q2_2'][$i] + $avgQD['Q2_3'][$i] + $avgQD['Q2_4'][$i] + $avgQD['Q2_5'][$i] + $avgQD['Q2_6'][$i])/7;
            $sumMouthQD[$i] = $avgQD['Q1'][$i];// + $avgQD['Q2_1'][$i] + $avgQD['Q2_2'][$i] + $avgQD['Q2_3'][$i] + $avgQD['Q2_4'][$i] + $avgQD['Q2_5'][$i] + $avgQD['Q2_6'][$i])/7;
            //$sumMouthQR[$i] = ($avgQR['Q1'][$i] + $avgQR['Q2_1'][$i] + $avgQR['Q2_2'][$i] + $avgQR['Q2_3'][$i] + $avgQR['Q2_4'][$i] + $avgQR['Q2_5'][$i] + $avgQR['Q2_6'][$i])/7;
            $sumMouthQR[$i] = $avgQR['Q1'][$i];// + $avgQR['Q2_1'][$i] + $avgQR['Q2_2'][$i] + $avgQR['Q2_3'][$i] + $avgQR['Q2_4'][$i] + $avgQR['Q2_5'][$i] + $avgQR['Q2_6'][$i])/7;
	    }

	    $sheetDivision = [$sumMouthQD[$arrMouthKey[0]],$sumMouthQD[$arrMouthKey[1]],$sumMouthQD[$arrMouthKey[2]]];
	    $sheetRegion = [$sumMouthQR[$arrMouthKey[0]],$sumMouthQR[$arrMouthKey[1]],$sumMouthQR[$arrMouthKey[2]]];

	    $sheetQ1 = [$avgQ['Q1'][$arrMouthKey[0]], $avgQ['Q1'][$arrMouthKey[1]],$avgQ['Q1'][$arrMouthKey[2]]];
	    $sheetQ2_1 = [$avgQ['Q2_1'][$arrMouthKey[0]], $avgQ['Q2_1'][$arrMouthKey[1]],$avgQ['Q2_1'][$arrMouthKey[2]]];
	    $sheetQ2_2 = [$avgQ['Q2_2'][$arrMouthKey[0]], $avgQ['Q2_2'][$arrMouthKey[1]],$avgQ['Q2_2'][$arrMouthKey[2]]];
	    $sheetQ2_3 = [$avgQ['Q2_3'][$arrMouthKey[0]], $avgQ['Q2_3'][$arrMouthKey[1]],$avgQ['Q2_3'][$arrMouthKey[2]]];
	    $sheetQ2_4 = [$avgQ['Q2_4'][$arrMouthKey[0]], $avgQ['Q2_4'][$arrMouthKey[1]],$avgQ['Q2_4'][$arrMouthKey[2]]];
	    $sheetQ2_5 = [$avgQ['Q2_5'][$arrMouthKey[0]], $avgQ['Q2_5'][$arrMouthKey[1]],$avgQ['Q2_5'][$arrMouthKey[2]]];
	    $sheetQ2_6 = [$avgQ['Q2_6'][$arrMouthKey[0]], $avgQ['Q2_6'][$arrMouthKey[1]],$avgQ['Q2_6'][$arrMouthKey[2]]];
	    $sheetCntSendSMS = [$cntSentSMS[$agency['agencyCD']][$arrMouthKey[0]], $cntSentSMS[$agency['agencyCD']][$arrMouthKey[1]], $cntSentSMS[$agency['agencyCD']][$arrMouthKey[2]]];
	    $sheetCntSMSSSS = [$cntSMSSS[$agency['agencyCD']][$arrMouthKey[0]], $cntSMSSS[$agency['agencyCD']][$arrMouthKey[1]], $cntSMSSS[$agency['agencyCD']][$arrMouthKey[2]]];
	    $sheetCountAns = [$countAns[$agency['agencyCD']][$arrMouthKey[0]], $countAns[$agency['agencyCD']][$arrMouthKey[1]], $countAns[$agency['agencyCD']][$arrMouthKey[2]]];
	    //var_dump($sheet3Agent[$agency['agencyCD']]);exit();
	    foreach ($sheet3Agent[$agency['agencyCD']] as $key => $val){
	        $sendSMS_3M_L = isset($val[$arrMouthKey[0]]['cntSMSSS'])?$val[$arrMouthKey[0]]['cntSMSSS']:0;
	        $sendSMS_3M_M = isset($val[$arrMouthKey[1]]['cntSMSSS'])?$val[$arrMouthKey[1]]['cntSMSSS']:0;
	        $sendSMS_3M_H = isset($val[$arrMouthKey[2]]['cntSMSSS'])?$val[$arrMouthKey[2]]['cntSMSSS']:0;
	        $sumSendSMS = $sendSMS_3M_L+$sendSMS_3M_M+$sendSMS_3M_H;
	        
	        $countAns_3M_L = isset($val[$arrMouthKey[0]]['countAns'])?$val[$arrMouthKey[0]]['countAns']:0;
	        $countAns_3M_M = isset($val[$arrMouthKey[1]]['countAns'])?$val[$arrMouthKey[1]]['countAns']:0;
	        $countAns_3M_H = isset($val[$arrMouthKey[2]]['countAns'])?$val[$arrMouthKey[2]]['countAns']:0;
	        $sumcountAns = $countAns_3M_L+$countAns_3M_M+$countAns_3M_H;
	        
	        $Q1_3M_L = isset($val[$arrMouthKey[0]]['Q1'])?$val[$arrMouthKey[0]]['Q1']:0;
	        $Q1_3M_M = isset($val[$arrMouthKey[1]]['Q1'])?$val[$arrMouthKey[1]]['Q1']:0;
	        $Q1_3M_H = isset($val[$arrMouthKey[2]]['Q1'])?$val[$arrMouthKey[2]]['Q1']:0;
	        $sumQ1 = ($Q1_3M_L+$Q1_3M_M+$Q1_3M_H);
	        
	        $Q21_3M_L = isset($val[$arrMouthKey[0]]['Q2_1'])?$val[$arrMouthKey[0]]['Q2_1']:0;
	        $Q21_3M_M = isset($val[$arrMouthKey[1]]['Q2_1'])?$val[$arrMouthKey[1]]['Q2_1']:0;
	        $Q21_3M_H = isset($val[$arrMouthKey[2]]['Q2_1'])?$val[$arrMouthKey[2]]['Q2_1']:0;
	        $sumQ21 = ($Q21_3M_L+$Q21_3M_M+$Q21_3M_H);
	        
	        $Q22_3M_L = isset($val[$arrMouthKey[0]]['Q2_2'])?$val[$arrMouthKey[0]]['Q2_2']:0;
	        $Q22_3M_M = isset($val[$arrMouthKey[1]]['Q2_2'])?$val[$arrMouthKey[1]]['Q2_2']:0;
	        $Q22_3M_H = isset($val[$arrMouthKey[2]]['Q2_2'])?$val[$arrMouthKey[2]]['Q2_2']:0;
	        $sumQ22 = ($Q22_3M_L+$Q22_3M_M+$Q22_3M_H);
	        
	        $Q23_3M_L = isset($val[$arrMouthKey[0]]['Q2_3'])?$val[$arrMouthKey[0]]['Q2_3']:0;
	        $Q23_3M_M = isset($val[$arrMouthKey[1]]['Q2_3'])?$val[$arrMouthKey[1]]['Q2_3']:0;
	        $Q23_3M_H = isset($val[$arrMouthKey[2]]['Q2_3'])?$val[$arrMouthKey[2]]['Q2_3']:0;
	        $sumQ23 = ($Q23_3M_L+$Q23_3M_M+$Q23_3M_H);
	        
	        $Q24_3M_L = isset($val[$arrMouthKey[0]]['Q2_4'])?$val[$arrMouthKey[0]]['Q2_4']:0;
	        $Q24_3M_M = isset($val[$arrMouthKey[1]]['Q2_4'])?$val[$arrMouthKey[1]]['Q2_4']:0;
	        $Q24_3M_H = isset($val[$arrMouthKey[2]]['Q2_4'])?$val[$arrMouthKey[2]]['Q2_4']:0;
	        $sumQ24 = ($Q24_3M_L+$Q24_3M_M+$Q24_3M_H);
	        
	        $Q25_3M_L = isset($val[$arrMouthKey[0]]['Q2_5'])?$val[$arrMouthKey[0]]['Q2_5']:0;
	        $Q25_3M_M = isset($val[$arrMouthKey[1]]['Q2_5'])?$val[$arrMouthKey[1]]['Q2_5']:0;
	        $Q25_3M_H = isset($val[$arrMouthKey[2]]['Q2_5'])?$val[$arrMouthKey[2]]['Q2_5']:0;
	        $sumQ25 = ($Q25_3M_L+$Q25_3M_M+$Q25_3M_H);
	        
	        $Q26_3M_L = isset($val[$arrMouthKey[0]]['Q2_6'])?$val[$arrMouthKey[0]]['Q2_6']:0;
	        $Q26_3M_M = isset($val[$arrMouthKey[1]]['Q2_6'])?$val[$arrMouthKey[1]]['Q2_6']:0;
	        $Q26_3M_H = isset($val[$arrMouthKey[2]]['Q2_6'])?$val[$arrMouthKey[2]]['Q2_6']:0;
	        $sumQ26 = ($Q26_3M_L+$Q26_3M_M+$Q26_3M_H);
	        
	        $sheet2agent3M[] = [
	            $arrAgent[$key]
	            , $sumSendSMS
	            , $sumcountAns
	            , ($sumcountAns>0)?$sumQ1/$sumcountAns:'-'
	            , ($sumcountAns>0)?$sumQ21/$sumcountAns:'-'
	            , ($sumcountAns>0)?$sumQ22/$sumcountAns:'-'
	            , ($sumcountAns>0)?$sumQ23/$sumcountAns:'-'
	            , ($sumcountAns>0)?$sumQ24/$sumcountAns:'-'
	            , ($sumcountAns>0)?$sumQ25/$sumcountAns:'-'
	            , ($sumcountAns>0)?$sumQ26/$sumcountAns:'-'
	        ];
	    }
	    
	    $sum3TotalQ1 = $arr[$arrMouthKey[0]]['TotalQ1']+$arr[$arrMouthKey[1]]['TotalQ1']+$arr[$arrMouthKey[2]]['TotalQ1'];
	    $sum3TotalQ2_1 = $arr[$arrMouthKey[0]]['TotalQ2_1']+$arr[$arrMouthKey[1]]['TotalQ2_1']+$arr[$arrMouthKey[2]]['TotalQ2_1'];
	    $sum3TotalQ2_2 = $arr[$arrMouthKey[0]]['TotalQ2_2']+$arr[$arrMouthKey[1]]['TotalQ2_2']+$arr[$arrMouthKey[2]]['TotalQ2_2'];
	    $sum3TotalQ2_3 = $arr[$arrMouthKey[0]]['TotalQ2_3']+$arr[$arrMouthKey[1]]['TotalQ2_3']+$arr[$arrMouthKey[2]]['TotalQ2_3'];
	    $sum3TotalQ2_4 = $arr[$arrMouthKey[0]]['TotalQ2_4']+$arr[$arrMouthKey[1]]['TotalQ2_4']+$arr[$arrMouthKey[2]]['TotalQ2_4'];
	    $sum3TotalQ2_5 = $arr[$arrMouthKey[0]]['TotalQ2_5']+$arr[$arrMouthKey[1]]['TotalQ2_5']+$arr[$arrMouthKey[2]]['TotalQ2_5'];
	    $sum3TotalQ2_6 = $arr[$arrMouthKey[0]]['TotalQ2_6']+$arr[$arrMouthKey[1]]['TotalQ2_6']+$arr[$arrMouthKey[2]]['TotalQ2_6'];
	    
	    $sum3Q1 = $arr[$arrMouthKey[0]]['Q1']+$arr[$arrMouthKey[1]]['Q1']+$arr[$arrMouthKey[2]]['Q1'];
	    $sum3Q2_1 = $arr[$arrMouthKey[0]]['Q2_1']+$arr[$arrMouthKey[1]]['Q2_1']+$arr[$arrMouthKey[2]]['Q2_1'];
	    $sum3Q2_2 = $arr[$arrMouthKey[0]]['Q2_2']+$arr[$arrMouthKey[1]]['Q2_2']+$arr[$arrMouthKey[2]]['Q2_2'];
	    $sum3Q2_3 = $arr[$arrMouthKey[0]]['Q2_3']+$arr[$arrMouthKey[1]]['Q2_3']+$arr[$arrMouthKey[2]]['Q2_3'];
	    $sum3Q2_4 = $arr[$arrMouthKey[0]]['Q2_4']+$arr[$arrMouthKey[1]]['Q2_4']+$arr[$arrMouthKey[2]]['Q2_4'];
	    $sum3Q2_5 = $arr[$arrMouthKey[0]]['Q2_5']+$arr[$arrMouthKey[1]]['Q2_5']+$arr[$arrMouthKey[2]]['Q2_5'];
	    $sum3Q2_6 = $arr[$arrMouthKey[0]]['Q2_6']+$arr[$arrMouthKey[1]]['Q2_6']+$arr[$arrMouthKey[2]]['Q2_6'];
	    
	    $arr3M_AGC = [
	        'Q1' => ($sum3TotalQ1>0)?$sum3Q1/$sum3TotalQ1:0
	        ,'Q2_1' => ($sum3TotalQ2_1>0)?$sum3Q2_1/$sum3TotalQ2_1:0
	        ,'Q2_2' => ($sum3TotalQ2_2>0)?$sum3Q2_2/$sum3TotalQ2_2:0
	        ,'Q2_3' => ($sum3TotalQ2_3>0)?$sum3Q2_3/$sum3TotalQ2_3:0
	        ,'Q2_4' => ($sum3TotalQ2_4>0)?$sum3Q2_4/$sum3TotalQ2_4:0
	        ,'Q2_5' => ($sum3TotalQ2_5>0)?$sum3Q2_5/$sum3TotalQ2_5:0
	        ,'Q2_6' => ($sum3TotalQ2_6>0)?$sum3Q2_6/$sum3TotalQ2_6:0
	    ];
	    
	    $sheet2sum3month = [
	        isset($arr3M_AGC['Q1'])?$arr3M_AGC['Q1']:'-'
	        , isset($arr3M_AGC['Q2_1'])?$arr3M_AGC['Q2_1']:'-'
	        , isset($arr3M_AGC['Q2_2'])?$arr3M_AGC['Q2_2']:'-'
	        , isset($arr3M_AGC['Q2_3'])?$arr3M_AGC['Q2_3']:'-'
	        , isset($arr3M_AGC['Q2_4'])?$arr3M_AGC['Q2_4']:'-'
	        , isset($arr3M_AGC['Q2_5'])?$arr3M_AGC['Q2_5']:'-'
	        , isset($arr3M_AGC['Q2_6'])?$arr3M_AGC['Q2_6']:'-'
	    ];
//////////////////////////////////////////////////////////////////////////////OverallPerformance Start
	    $styleTrue = [
	        'font'  => [	            
	            'color' => array('rgb' => '008000'),	            
	        ]
	    ];
	    $styleFals = [
	        'font'  => [
	            'color' => array('rgb' => 'FF0000'),
	        ]
	    ];
	    $objWorksheet = $this->objPHPExcel->getActiveSheet(0)->setTitle('OverallPerformance');	    
	    $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);
	    //Header nameAL Up left
	    $objWorksheet->setCellValue('A1',$ShowAgent);
	    //mouth bottom logo
	    $objWorksheet->setCellValue('J7',$ShowMouth);
	    //mouth show	    
	    $objWorksheet->setCellValue('F8',$mouth2);
	    $objWorksheet->setCellValue('H8',$mouth1);
	    $objWorksheet->setCellValue('J8',$mouth0);
	    
	    $objWorksheet->setCellValue('A10',$ShowAgent);
	    $objWorksheet->setCellValue('A11',$ShowDivision);
	    $objWorksheet->setCellValue('A12',$ShowRegion);
	    	    
	    $objWorksheet->setCellValue('F13',$mouth2);
	    $objWorksheet->setCellValue('H13',$mouth1);
	    $objWorksheet->setCellValue('J13',$mouth0);
	    //first gen
	    //$ic = 2;
	    //$columeQ = ['J'];
	    
	    //secrond gen
	    $ic = 1;
	    $columeQ = ['H','J'];
	    
	    //normal process
	    //$ic = 0;
	    //$columeQ = ['F','H','J'];
	    
	    //var_dump($sheetQ1);exit();
	    
	    foreach ($columeQ as $c){
	        $objWorksheet->setCellValue($c.'10',($sheetQ1[$ic]>0)?$sheetQ1[$ic]:'n/a');
	        if ($sheetQ1[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'10')->applyFromArray($styleTrue);
	        }elseif ($sheetQ1[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'10')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'11',($sheetDivision[$ic]>0)?$sheetDivision[$ic]:'n/a');
	        if ($sheetDivision[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'11')->applyFromArray($styleTrue);
	        }elseif ($sheetDivision[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'11')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'12',($sheetRegion[$ic]>0)?$sheetRegion[$ic]:'n/a');
	        if ($sheetRegion[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'12')->applyFromArray($styleTrue);
	        }elseif ($sheetRegion[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'12')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'14',($sheetQ2_1[$ic]>0)?$sheetQ2_1[$ic]:'n/a');
	        if ($sheetQ2_1[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'14')->applyFromArray($styleTrue);
	        }elseif ($sheetQ2_1[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'14')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'15',($sheetQ2_2[$ic]>0)?$sheetQ2_2[$ic]:'n/a');
	        if ($sheetQ2_2[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'15')->applyFromArray($styleTrue);
	        }elseif ($sheetQ2_2[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'15')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'16',($sheetQ2_3[$ic]>0)?$sheetQ2_3[$ic]:'n/a');
	        if ($sheetQ2_3[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'16')->applyFromArray($styleTrue);
	        }elseif ($sheetQ2_3[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'16')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'17',($sheetQ2_4[$ic]>0)?$sheetQ2_4[$ic]:'n/a');
	        if ($sheetQ2_4[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'17')->applyFromArray($styleTrue);
	        }elseif ($sheetQ2_4[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'17')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'18',($sheetQ2_5[$ic]>0)?$sheetQ2_5[$ic]:'n/a');
	        if ($sheetQ2_5[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'18')->applyFromArray($styleTrue);
	        }elseif ($sheetQ2_5[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'18')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'19',($sheetQ2_6[$ic]>0)?$sheetQ2_6[$ic]:'n/a');
	        if ($sheetQ2_6[$ic] >= 4) {
	            $objWorksheet->getStyle($c.'19')->applyFromArray($styleTrue);
	        }elseif ($sheetQ2_6[$ic] <= 2) {
	            $objWorksheet->getStyle($c.'19')->applyFromArray($styleFals);
	        }
	        
	        $objWorksheet->setCellValue($c.'21',isset($sheetCntSendSMS[$ic])?$sheetCntSendSMS[$ic]:0);
	        $objWorksheet->setCellValue($c.'22',isset($sheetCntSMSSSS[$ic])?$sheetCntSMSSSS[$ic]:0);
	        $objWorksheet->setCellValue($c.'23',isset($sheetCountAns[$ic])?$sheetCountAns[$ic]:0);
	        $ic++;
	    }
////////////////////////////////////////////////////////////////////////////// OverallPerformance End
//////////////////////////////////////////////////////////////////////////////// PerformanceByAgent Start
	    $objWorksheet->setCellValue('A44',$ShowAgent);
	    
	    $objWorksheet->fromArray(
	        [
	            $arrMouth,
	        ],
	        null,
	        'I40',
	        FALSE
	    );
	    
	    $columeQ2 = ['E','F','G','H','I','J','K'];
	    $ic2 = 0;
	    foreach ($columeQ2 as $c2){
	        $objWorksheet->setCellValue($c2.'44',($sheet2sum3month[$ic2]>0)?$sheet2sum3month[$ic2]:'n/a');
	        if ($sheet2sum3month[$ic2] >= 4) {
	            $objWorksheet->getStyle($c2.'44')->applyFromArray($styleTrue);
	        }elseif ($sheet2sum3month[$ic2] <= 2) {
	            $objWorksheet->getStyle($c2.'44')->applyFromArray($styleFals);
	        }
	        $ic2++;
	    }
	    
	    $row = 46;
	    foreach ($sheet2agent3M as $list){
	        if ($list[1] <= 3) {
	            $objWorksheet->setCellValue('A'.$row,$list[0]);
	            $objWorksheet->setCellValue('C'.$row,($list[1]>0)?$list[1]:'-');
	            $objWorksheet->setCellValue('D'.$row,($list[2]>0)?$list[2]:'-');
	            $objWorksheet->setCellValue('E'.$row,'-');
	            $objWorksheet->setCellValue('F'.$row,'-');
	            $objWorksheet->setCellValue('G'.$row,'-');
	            $objWorksheet->setCellValue('H'.$row,'-');
	            $objWorksheet->setCellValue('I'.$row,'-');
	            $objWorksheet->setCellValue('J'.$row,'-');
	            $objWorksheet->setCellValue('K'.$row,'-');
	       } else {
	            $objWorksheet->setCellValue('A'.$row,$list[0]);
	            $objWorksheet->setCellValue('C'.$row,($list[1]>0)?$list[1]:'-');
	            //$objWorksheet->setCellValue('C'.$row,$list[1]);
	            $objWorksheet->setCellValue('D'.$row,($list[2]>0)?$list[2]:'-');
	            //$objWorksheet->setCellValue('D'.$row,$list[2]);
	            $objWorksheet->setCellValue('E'.$row,$list[3]);
	            $objWorksheet->setCellValue('F'.$row,$list[4]);
	            $objWorksheet->setCellValue('G'.$row,$list[5]);
	            $objWorksheet->setCellValue('H'.$row,$list[6]);
	            $objWorksheet->setCellValue('I'.$row,$list[7]);
	            $objWorksheet->setCellValue('J'.$row,$list[8]);
	            $objWorksheet->setCellValue('K'.$row,$list[9]);
	        }
            $q = 3;
            foreach ($columeQ2 as $cc){
                if ($list[$q] >= 4) {
                    $objWorksheet->getStyle($cc.$row)->applyFromArray($styleTrue);
                }elseif ($list[$q] <= 2) {
                    $objWorksheet->getStyle($cc.$row)->applyFromArray($styleFals);
                }
                $q++;
            }
	        
	        $row++;
	    }   
//////////////////////////////////////////////////////////////////////////////// PerformanceByAgent End
//////////////////////////////////////////////////////////////////////////////Excel Start
	    $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
	    $objWriter->setIncludeCharts(TRUE);
	    $objWriter->save('./application/aia_share/'.$this->rep_name);
//////////////////////////////////////////////////////////////////////////////Excel End
	    unset($this->objPHPExcel);
	    return TRUE;
	}
	
	public function pop_save_file() {
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=' . $this->rep_name);
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');		
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		
		exit;
	}
}
