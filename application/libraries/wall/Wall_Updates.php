<?php

//Srinivas Tamada http://9lessons.info
//Wall_Updates

class Wall_Updates extends CI_Model {

        public $perpage = 10; // Uploads perpage

        public function Login_Check($value, $type) {
                $username_email = $this->db->escape_str($value);
                if ($type) {
                        $query = $this->db->query("SELECT userID FROM t_ma_user WHERE username='$username_email' ");
                } else {
                        $query = $this->db->query("SELECT userID FROM t_ma_user WHERE email='$username_email' ");
                }

                return mysql_num_rows($query);
        }

        public function User_ID($username) {
                $username = $this->db->escape_str($username);
                $query = $this->db->query("SELECT userID FROM t_ma_user WHERE username='$username' AND status='1'");
                if ($query->num_rows() == 1) {
                        $row = $query->row();
                        return $row['userID'];
                } else {
                        return false;
                }
        }

        public function User_Details($userID) {
                $username = $this->db->escape_str($userID);
                $query = $this->db->query("SELECT userID,username,email,friend_count FROM t_ma_user WHERE userID='$userID' AND status='1'");
                $data = $query->result_array();
                return $data;
        }

        // User Search   	
        public function User_Search($searchword) {
                $q = $this->db->escape_str($_POST['searchword']);
                $query = $this->db->query("select username,userID from t_ma_user where username like '%$q%' order by userID LIMIT 5");
//                while ($row = mysql_fetch_array($query))
                foreach ($query->result_array() as $row)
                        $data[] = $row;
                return $data;
        }

        // Updates   	
        public function Updates($userID, $lastid) {
                // More Button
                $morequery = "";
                if ($lastid)
                        $morequery = " and M.msg_id<'" . $lastid . "' ";

                // More Button End
//                $sql = "SELECT M.msg_id, M.uid_fk, M.message, M.created, U.username,M.uploads FROM t_wall_messages M, t_ma_user U  WHERE U.display='1' AND M.uid_fk=U.userID and M.uid_fk='$userID' $morequery order by M.msg_id desc limit " . $this->perpage;
//                $sql = "SELECT M.msg_id, M.uid_fk, M.message, M.created, U.username,M.uploads, M.is_public FROM t_wall_messages M, t_ma_user U  WHERE U.display='1' AND (M.uid_fk=U.userID and is_public='1') or (M.uid_fk='".$userID."' and is_public='0') order by M.msg_id desc limit " . $this->perpage;
//
//                $query = $this->db->query($sql);
                $this->db->select('msg_id,uid_fk,message,created,username,uploads,is_public')->from('v_wall_message');
                $this->db->where('(is_public=1 or (is_public=0 and owner_id="' . $userID . '"))');

                if ($lastid)
                        $this->db->where('msg_id<', $lastid);
                $this->db->orderBy('msg_id', 'desc');
                $this->db->limit($this->perpage);
                $query = $this->db->get();

                $data = array();
                if ($query->num_rows() > 0) {

                        foreach ($query->result_array() as $row) {
                                $this->db->select('*')->from('t_wall_private_message')->where('message_id', $row['msg_id'])->where($this->config->item('display'), 1);
                                $query1 = $this->db->get();
                                $rows = $query1->row_array();
                                $row['message_data'] = $rows;

                                $this->db->select('*')->from('v_ass_task_evaluation_result')->where('message_id', $row['msg_id'])->where($this->config->item('display'), 1);
                                $query1 = $this->db->get();
                                $rows = $query1->row_array();
                                $row['output_data'] = $rows;

                                $this->db->select('event_id,title,start_date,end_date,color,description,leave_name,boss_id,employee_id,employee_name,boss_name,request_employee_id,request_employee_name')->from('v_wall_leave_event')->where('is_active', 1)->where('display', 1)->where('message_id', $row['msg_id']);
                                $query1 = $this->db->get();

                                $row_ = $query1->row_array();

                                $employee_id = explode(',', $row_['request_employee_id']);

                                $rows = array();
                                $i = 1;
                                foreach ($employee_id as $k => $v) {

                                        $this->db->select('name,employee_parent_name')->from('v_ma_employee')->where('employee_id', $v);
                                        $query = $this->db->get();
                                        $row_1 = $query->row_array();
                                        if (count($employee_id) > 1) {
                                                $name = '';
                                        } else {
                                                $name = $row_1['name'];
                                        }
                                        $boss_name = $row_1['employee_parent_name'];
                                        $rows[] = array('event_id' => $row_['event_id'], 'title' => $row_['title'], 'start_date' => $row_['start_date'], 'end_date' => $row_['end_date'], 'color' => $row_['color'], 'description' => $row_['description'], 'leave_name' => $row_['leave_name'], 'request_employee_id' => $v, 'request_employee_name' => $name, 'boss_name' => $boss_name);
                                        $i++;
                                }
                                $row['leave_data'] = $rows;

                                $this->db->select('*')->from('t_wall_user_uploads')->where('message_id', $row['msg_id']);
                                $query1 = $this->db->get();
                                $rows = $query1->result_array();
                                $row['uploads'] = $rows;
                                $data[] = $row;
                        }
                }

                return $data;
        }

        function get_leave_data() {
                $event_id = $_POST['event_id'];
                $this->db->select('event_id, event_id as id, start_date as start, end_date as end, leave_type_id,employee_id as request_employee_id,message_id,description')->from('t_wall_leave_event')->where('event_id', $event_id);
                $query = $this->db->get();
                print json_encode($query->row_array());
        }

        // Total Updates   	
        public function Total_Updates($userID) {
                if (!isset($morequery))
                        $morequery = '';
//                $sql = "SELECT M.msg_id, M.uid_fk, M.message, M.created, U.username,M.uploads FROM t_wall_messages M, t_ma_user U  WHERE U.display='1' AND M.uid_fk=U.userID and M.uid_fk='$userID' $morequery order by M.msg_id ";
                $sql = "SELECT M.msg_id, M.uid_fk, M.message, M.created, U.username,M.uploads FROM t_wall_messages M, t_ma_user U  WHERE U.display='1' AND M.uid_fk=U.userID order by M.msg_id ";

                $query = $this->db->query($sql);
                $data = $query->num_rows();
                return $data;
        }

        // Friends_Updates   	
        public function Friends_Updates($userID, $lastid) {
                // More Button
                $morequery = "";
                if ($lastid)
                        $morequery = " and M.msg_id<'" . $lastid . "' ";
                // More Button End

                $query = $this->db->query("SELECT DISTINCT M.msg_id, M.uid_fk, M.message, M.created, U.username,M.uploads FROM t_wall_messages M, t_ma_user U, t_wall_friends F  WHERE U.display='1' AND M.uid_fk=U.userID AND  M.uid_fk = F.friend_two AND F.friend_one='$userID' $morequery order by M.msg_id desc limit " . $this->perpage);

                $data = array();
//                while ($row = mysql_fetch_array($query))
                foreach ($query->result_array() as $row)
                        $data[] = $row;
                return $data;
        }

        //Total Friends Updates   	
        public function Total_Friends_Updates($userID) {


                $query = $this->db->query("SELECT DISTINCT M.msg_id, M.uid_fk, M.message, M.created, U.username,M.uploads FROM t_wall_messages M, t_ma_user U, t_wall_friends F  WHERE U.display='1' AND M.uid_fk=U.userID AND  M.uid_fk = F.friend_two AND F.friend_one='$userID' order by M.msg_id ");

                $data = $query->num_rows();
                return $data;
        }

        //Comments
        public function Comments($msg_id, $second_count) {
                $query = '';
                if ($second_count)
                        $query = "limit $second_count,2";
                $query = $this->db->query("SELECT C.com_id, C.uid_fk, C.comment, C.created, U.username, C.uploads FROM t_wall_comments C, t_ma_user U WHERE U.display='1' AND C.uid_fk=U.userID and C.msg_id_fk='$msg_id' order by C.com_id asc $query");
//                while ($row = mysql_fetch_array($query))
                foreach ($query->result_array() as $row) {

                        $com_id = $row['com_id'];
                        $this->db->select('*')->from('t_wall_user_uploads')->where('comment_id', $com_id);
                        $query1 = $this->db->get();
                        $uploads = array();
                        if ($query1->num_rows() > 0) {
                                $uploads = $query1->result_array();
                        }
                        $row['uploads'] = $uploads;
                        $data[] = $row;
                }


                if (!empty($data)) {
                        return $data;
                }
        }

        //Avatar Image
        //From database
        public function Profile_Pic($userID) {

                $query = $this->db->query("SELECT avatar FROM `t_ma_user` WHERE userID='$userID'");
//                $row = mysql_fetch_array($query);
                $row = $query->row_array();
//                vd::d($row);
                if (!empty($row['avatar'])) {
//                        $profile_pic_path = base_url() . 'avatar/';
                        $profile_pic_path = './upload/avatar/';
                        $data = $profile_pic_path . $row['avatar'];

                        return $data;
                } else {
                        $data = "./upload/avatar/default.jpg";
                        return $data;
                }
        }

        //  Gravatar Image
        public function Gravatar($userID) {
                $query = $this->db->query("SELECT email FROM `t_ma_user` WHERE userID='$userID'");
                $row = $query->row_array();
                if (!empty($row)) {
                        $email = $row['email'];
                        $lowercase = strtolower($email);
                        $imagecode = md5($lowercase);
                        $data = "http://www.gravatar.com/avatar.php?gravatar_id=$imagecode";
                        return $data;
                } else {
                        $data = "./upload/avatar/default.jpg";
                        return $data;
                }
        }

        //Insert Update
        public function Insert_Update($userID, $update, $uploads) {
                $update = $this->db->escape_str($update);
                $time = time();
                $ip = $_SERVER['REMOTE_ADDR'];
                $query = $this->db->query("SELECT msg_id,message FROM `t_wall_messages` WHERE uid_fk='$userID' order by msg_id desc limit 1");
                $result = $query->result_array();

//                if ($update != $result['message']) {
                if (is_array($uploads) && count($uploads) > 0) {

//                        $uploads_array = explode(',', $uploads);
                        $uploads = implode(',', array_unique($uploads));
                } else {
                        $uploads = '';
                }
                $query = $this->db->query("INSERT INTO `t_wall_messages` (message, uid_fk, ip,created,uploads) VALUES (N'$update', '$userID', '$ip','$time','$uploads')");
                $newquery = $this->db->query("SELECT M.msg_id, M.uid_fk, M.message, M.created, M.uploads, U.username, M.is_public FROM t_wall_messages M, t_ma_user U where M.uid_fk=U.userID and M.uid_fk='$userID' order by M.msg_id desc limit 1 ");
                $result = $newquery->row_array();

//                $result1 = $this->Get_Upload_Image_Id($result['uploads']);
//                $result['image_path'] = $result1['image_path'];
                return $result;
//                } else {
//                        return false;
//                }
        }

        //Delete update
        public function Delete_Update($userID, $msg_id) {
                $query = $this->db->query("DELETE FROM `t_wall_comments` WHERE msg_id_fk = '.$msg_id.' and uid_fk='.$userID.' ");
                $query = $this->db->query("DELETE FROM `t_wall_messages` WHERE msg_id = '$msg_id' and uid_fk='$userID'");

                $this->mdb->r_delete('t_wall_leave_event', array('message_id' => $msg_id));
                $this->mdb->r_delete('t_ass_task_evaluation_result', array('message_id' => $msg_id));
                $this->mdb->r_delete('t_wall_private_message', array('message_id' => $msg_id));
                return true;
        }

        //Image Upload
        public function Image_Upload($userID, $image) {
                //Base64 encoding
                $path = "uploads/";
                $img_src = $path . $image;
                $imgbinary = fread(fopen($img_src, "r"), filesize($img_src));
                $img_base = base64_encode($imgbinary);
                $ids = 0;
                $query = $this->db->query("insert into t_wall_user_uploads (image_path,uid_fk) values('$image' ,'$userID')");
                $ids = mysql_insert_id();
                return $ids;
        }

        function upload_file($userID, $message_id, $document_file, $document_original_file) {

                if (strlen($document_file) > 0 && $document_file !== 'undefined') {
                        if (strpos($document_file, ',') !== false) {
                                $a = explode(',', $document_file);
                                $b = explode(',', $document_original_file);
                                for ($i = 0; $i < count($a); $i++) {
                                        $data = array('uid_fk' => $userID, 'document_file' => $a[$i], 'document_original_file' => $b[$i], 'message_id' => $message_id);
                                        $this->db->insert('t_wall_user_uploads', $data);
                                }
                        } else {
                                $data = array('uid_fk' => $userID, 'document_file' => $document_file, 'document_original_file' => $document_original_file, 'message_id' => $message_id);
                                $this->db->insert('t_wall_user_uploads', $data);
                        }
                }
                $this->db->select('*')->from('t_wall_user_uploads')->where('message_id', $message_id);
                $query = $this->db->get();
                return $query->result_array();
        }

        //get Image Upload
        public function Get_Upload_Image($userID, $image) {
                if ($image) {
                        $query = $this->db->query("select id,image_path from t_wall_user_uploads where image_path='$image'");
                } else {
                        $query = $this->db->query("select id,image_path from t_wall_user_uploads where uid_fk='$userID' order by id desc ");
                }

                $result = $query->result_array();

                return $result;
        }

        //Id Image Upload
        public function Get_Upload_Image_Id($id) {
                $query = $this->db->query("select image_path from t_wall_user_uploads where id='$id'");
                $result = $query->row_array();

                return $result;
        }

        //Insert Comments
        public function Insert_Comment($userID, $msg_id, $comment, $ip, $uploads) {
                $comment = $this->mcl->htmlcode($comment);

                $time = time();
                $ip = $_SERVER['REMOTE_ADDR'];
                $query = $this->db->query("SELECT com_id,comment FROM `t_wall_comments` WHERE uid_fk='$userID' and msg_id_fk='$msg_id' order by com_id desc limit 1 ");
                $result = $query->result_array();

                if (is_array($uploads) && count($uploads) > 0) {
                        $uploads = implode(',', array_unique($uploads));
                } else {
                        $uploads = '';
                }
//                if ($comment != $result['comment']) {
                $query = $this->db->query("INSERT INTO `t_wall_comments` (comment, uid_fk,msg_id_fk,ip,created,uploads) VALUES (N'$comment', '$userID','$msg_id', '$ip','$time','$uploads')");
                $newquery = $this->db->query("SELECT C.com_id, C.uid_fk, C.comment, C.msg_id_fk, C.created, U.username, C.uploads FROM t_wall_comments C, t_ma_user U where C.uid_fk=U.userID and C.uid_fk='$userID' and C.msg_id_fk='$msg_id' order by C.com_id desc limit 1 ");
                $result = $newquery->row_array();

                return $result;
//                } else {
//                        return false;
//                }
        }

        //Delete Comments
        public function Delete_Comment($userID, $com_id) {
                $userID = $this->db->escape_str($userID);
                $com_id = $this->db->escape_str($com_id);
//print($userID);
//print($com_id);
                $q = $this->db->query("SELECT M.uid_fk FROM t_wall_comments C, t_wall_messages M WHERE C.msg_id_fk = M.msg_id AND C.com_id='$com_id'");
                $d = $q->row_array();
                $oid = $d['uid_fk'];

                if ($userID == $oid) {
                        $query = $this->db->query("DELETE FROM `t_wall_comments` WHERE com_id='$com_id'");
                        return true;
                } else {
                        $query = $this->db->query("DELETE FROM `t_wall_comments` WHERE uid_fk='$userID' and com_id='$com_id'");
                        return true;
                }
        }

        //Friends List
        public function Friends_List($userID, $page, $offset, $rowsPerPage) {
                $userID = $this->db->escape_str($userID);
                $page = $this->db->escape_str($page);
                $offset = $this->db->escape_str($offset);
                $rowsPerPage = $this->db->escape_str($rowsPerPage);

                if ($page)
                        $con = $offset . "," . $rowsPerPage;
                else
                        $con = $rowsPerPage;



                $query = $this->db->query("SELECT U.username, U.userID FROM t_ma_user U, t_wall_friends F WHERE U.display='1' AND U.userID=F.friend_two AND F.friend_one='$userID' AND F.role='fri' ORDER BY F.friend_id DESC LIMIT $con")or die(mysql_error());
//                while ($row = mysql_fetch_array($query))
                foreach ($query->result_array() as $row)
                        $data[] = $row;
                return $data;
        }

        public function Friends_Check($userID, $fid) {
                $query = $this->db->query("SELECT role FROM t_wall_friends WHERE friend_one='$userID' AND friend_two='$fid'");
                $num = $query->result_array();
                return $num['role'];
        }

        public function Friends_Check_Count($userID, $fid) {
                $query = $this->db->query("SELECT friend_id FROM t_wall_friends WHERE friend_one='$userID' AND friend_two='$fid'");
                $num = $query->result_array();
                return $num;
        }

        // Add Friend
        public function Add_Friend($userID, $fid) {
                $fid = $this->db->escape_str($fid);
                $q = $this->db->query("SELECT friend_id FROM t_wall_friends WHERE friend_one='$userID' AND friend_two='$fid' AND role='fri'");
                if ($q->num_rows() == 0) {
                        $query = $this->db->query("INSERT INTO t_wall_friends(friend_one,friend_two,role) VALUES ('$userID','$fid','fri')");
                        $query = $this->db->query("UPDATE t_ma_user SET friend_count=friend_count+1 WHERE userID='$userID'");
                        return true;
                }
        }

        // Remove Friend
        public function Remove_Friend($userID, $fid) {
                $fid = $this->db->escape_str($fid);
                $q = $this->db->query("SELECT friend_id FROM t_wall_friends WHERE friend_one='$userID' AND friend_two='$fid' AND role='fri'");
                if ($q->num_rows() == 0) {
                        $query = $this->db->query("DELETE FROM t_wall_friends WHERE friend_one='$userID' AND friend_two='$fid'");
                        $query = $this->db->query("UPDATE t_ma_user SET friend_count=friend_count-1 WHERE userID='$userID'");
                        return true;
                }
        }

}

?>
