<?php
defined('BASEPATH') or die('Acceso Restringido');

class Tree_Menu extends CI_Model {

        var $tree = array();
        var $trees = array();
        var $ptree = array();

        public function __construct() {
                parent::__construct();
                $this->load->library('auth_level');
        }

        function get_group_tree($position, $groupID) {

                $new = array();
                $r = $this->get_data_group($groupID);
                $privileges = $r['privileges'];
                $visible_functions = $r['visible_functions'];

                if (count($visible_functions) > 0) {
                        $arr = $this->get_data($position, 'functionID', $visible_functions);

                        foreach ($arr as $a) {
                                $new[$a['parentID']][] = $a;
                        }
//                        $tree = $this->createTree($new, array($arr[0]));
                        foreach ($arr as $k => $v) {
                                if ($v['parentID'] == 0) {
                                        $tree = $this->createTree($new, array($v));

                                        array_push($this->tree, $tree[0]);
                                }
                        }

                        return $this->tree;
                } else {
                        return array();
                }
        }

        function get_generic_tree($table_name, $column_name, $key = '', $where = array()) {
                $new = array();
                $arr = $this->get_generic_data($key, $where, $table_name, $column_name);

                foreach ($arr as $a) {
                        $new[$a['parentID']][] = $a;
                }

                foreach ($arr as $k => $v) {
                        if ($v['parentID'] == 0 || $v['parentID'] == -1 || is_null($v['parentID'])) {

                                $tree = $this->createTree($new, array($v), $key);

                                array_push($this->tree, $tree[0]);
                        }
                }


                return $this->tree;
        }

        function get_tree($position) {

                $new = array();
                $arr = $this->get_data($position);

                foreach ($arr as $a) {
                        $new[$a['parentID']][] = $a;
                }

                foreach ($arr as $k => $v) {
                        if ($v['parentID'] == 0) {
                                $tree = $this->createTree($new, array($v));

                                array_push($this->tree, $tree[0]);
                        }
                }


                return $this->tree;
        }

        function get_data($position = "top", $key = '', $where = array(), $table_name = '', $column_name = 'functionID,code,name, functionUri as url,parentID,icon,level') {
                if (strlen($table_name) == 0)
                        $table_name = $this->config->item('function_table');
                if (strlen($key) > 0 && count($where) > 0) {
                        $this->db->select($column_name)->from($table_name)->where($this->config->item('display'), '1')->where('position', $position)->where_in($key, $where)->orderBy('sortOrder', 'asc'); //->orderBy('parentID', 'asc')->orderBy('name', 'asc');
                } else {
                        $this->db->select($column_name)->from($table_name)->where($this->config->item('display'), '1')->where('position', $position)->orderBy('sortOrder', 'asc'); //->orderBy('parentID', 'asc');                        
                }
                $query = $this->db->get();

                return $query->result_array();
        }

        function get_generic_data($key = '', $where = array(), $table_name, $column_name) {

                if (strlen($key) > 0 && count($where) > 0) {
                        $this->db->select($column_name)->from($table_name)->where($this->config->item('display'), '1')->where_in($key, $where)->orderBy('name', 'asc'); //->orderBy('parentID', 'asc')->orderBy('name', 'asc');
                } else {
                        $this->db->select($column_name)->from($table_name)->where($this->config->item('display'), '1')->orderBy('name', 'asc'); //->orderBy('parentID', 'asc');
                }

                $query = $this->db->get();
//vd::d($query->result_array());
                return $query->result_array();
        }

        function get_data_group($groupID) {

                $a = $this->auth_level->get_auth($groupID);
                if ($a !== false) {
                        $a = explode(',', $a);

                        $r = array();
                        $vf = array();

                        if (is_array($a)) {
                                foreach ($a as $k => $v) {
                                        $vv = explode('-', $v);
                                        if (count($vv) == 2) {
                                                $v0 = $vv[0];
                                                $a = explode('_', $v0);
                                                $functionID = $a[1];
                                                $vv = $vv[1];
                                                if (substr($vv, 0, 1) == '1') {
                                                        array_push($vf, $functionID);
                                                }
                                        }
                                }
                                return array('privileges' => $r, 'visible_functions' => $vf);
                        } else {
                                return array('privileges' => array(), 'visible_functions' => array());
                        }
                } else {
                        return array('privileges' => array(), 'visible_functions' => array());
                }
        }

        function createTree(&$list, $parent, $indexColumn = '') {
                if (strlen($indexColumn) == 0)
                        $indexColumn = 'functionID';
                $tree = array();
                foreach ($parent as $k => $l) {
                        if (isset($list[$l[$indexColumn]])) {
                                $l['sub'] = $this->createTree($list, $list[$l[$indexColumn]], $indexColumn);
                        }
                        $tree[] = $l;
                }

                return $tree;
        }

        function print_tree($a, $is_ignore_id = false) {

                $o = '<ul class="jquery-tree">';
                foreach ($a as $key => $nav_item) {
                        //process parent nav
                        $nav_htm = '';
                        $nav_title = isset($nav_item["name"]) ? $nav_item["name"] : "(No Name)";

                        $key = $nav_item['id'];
                        if ($is_ignore_id == true) {
                                $nav_htm .= '<span class="menu-item-parent" id=' . $key . '>' . $nav_title . '</span>';
                        } else {
                                $nav_htm .= '<span class="menu-item-parent" id=' . $key . '>' . $key . '-' . $nav_title . '</span>';
                        }
                        if (isset($nav_item["sub"]) && $nav_item["sub"])
                                $nav_htm .= $this->process_sub_nav($nav_item["sub"], $is_ignore_id);

                        $o .= '<li>' . $nav_htm . '</li>';
                }

                $o .= '</ul>';

                return $o;
//                return $this->ptree;
        }

        function bootstrapItems($items) {

                // Starting from items at root level
                if (!is_array($items)) {
                        $items = $items->roots();
                }

                foreach ($items as $item) {
                        ?>
                        <li <? if (isset($item['sub'])): ?> class="dropdown" <? endif ?>>
                                <a href="<? echo '#' . $item['url'] ?>" <? if (isset($item['sub'])): ?> class="dropdown-toggle" data-toggle="dropdown" <? endif ?>>
                                        <? echo $item['name'] ?> <? if (isset($item['sub'])): ?> <b class="caret"></b> <? endif ?></a>
                                        <? if (isset($item['sub'])): ?>
                                        <ul class="dropdown-menu">
                                                <? if (isset($item['sub'])) $this->bootstrapItems($item['sub']) ?>
                                        </ul> 
                                <? endif ?>
                        </li>
                        <?
                }
        }

        function draw_function_tree($nav_item) {
                $sub_item_htm = '';
                if (isset($nav_item["sub"]) && $nav_item["sub"]) {
                        $sub_nav_item = $nav_item["sub"];
                        $sub_item_htm = $this->draw_function_tree($sub_nav_item);
                } else {

                        $sub_item_htm .= '<ul>';
                        foreach ($nav_item as $key => $sub_item) {
                                if (array_key_exists('sub', $sub_item)) {
                                        $url = '#';
                                } else {
                                        if ($sub_item["url"] !== '#')
                                                $url = '#' . $sub_item["url"];
                                        else
                                                $url = '#';
                                }

                                $url_target = isset($sub_item["url_target"]) ? 'target="' . $sub_item["url_target"] . '"' : "";
                                $icon = isset($sub_item["icon"]) ? '<i class="fa fa-lg fa-fw ' . $sub_item["icon"] . '"></i>' : "";
                                $code = isset($sub_item["code"]) ? $sub_item["code"] : "";
                                $nav_title = isset($sub_item["name"]) ? $sub_item["name"] : "";

                                $label_htm = isset($sub_item["label_htm"]) ? $sub_item["label_htm"] : "";
                                if (isset($sub_item["sub"])) {
                                        $sub_item_htm .= '<li ' . (isset($sub_item["active"]) ? 'class = "active"' : '') . '>
											<a href="' . $url . '" ' . $url_target . '>' . $icon . ' ' . $code . '-' . $nav_title . $label_htm . '</a>' . (isset($sub_item["sub"]) ? $this->draw_function_tree($sub_item["sub"]) : '') . '
										</li>';
                                } else {
                                        $sub_item_htm .= '<li ' . (isset($sub_item["active"]) ? 'class = "active"' : '') . '>
											<a href="' . $url . '" ' . $url_target . '>' . $icon . ' ' . $code . '-' . $nav_title . $label_htm . '</a>' . (isset($sub_item["sub"]) ? $this->draw_function_tree($sub_item["sub"]) : '') . '
										</li>';
                                }
                        }
                        $sub_item_htm .= '</ul>';
                }
                return $sub_item_htm;
        }

        function process_sub_nav0($nav_item) {
                $sub_item_htm = "";
                if (isset($nav_item["sub"]) && $nav_item["sub"]) {
                        $sub_nav_item = $nav_item["sub"];
                        $sub_item_htm = $this->process_sub_nav($sub_nav_item);
                } else {
                        $sub_item_htm .= '<ul>';
                        foreach ($nav_item as $key => $sub_item) {
                                if (isset($sub_item['url']) && strlen($sub_item['url']) == 0) {
                                        $url = '#';
                                } else {
                                        if ($sub_item["url"] !== '#')
                                                $url = $sub_item["url"];
                                        else
                                                $url = '#';
                                }

                                $url_target = isset($sub_item["url_target"]) ? 'target="' . $sub_item["url_target"] . '"' : "";
                                $icon = isset($sub_item["icon"]) ? '<i class="fa fa-lg fa-fw ' . $sub_item["icon"] . '"></i>' : "";
                                $code = isset($sub_item["code"]) ? $sub_item["code"] : "";
                                $nav_title = isset($sub_item["name"]) ? $sub_item["name"] : "";
                                $label_htm = isset($sub_item["label_htm"]) ? $sub_item["label_htm"] : "";
//                                $url = base_url() . $url;
                                $url = '#' . $url;
                                if (isset($sub_item["sub"])) {
                                        $sub_item_htm .= '<li class="has_sub"><a href="#" class="waves-effect waves-primary" onclick="javascript:void(0)">' . $icon . '<span>' . $code . ' ' . $nav_title . $label_htm . '</span></a>' . (isset($sub_item["sub"]) ? $this->process_sub_nav0($sub_item["sub"]) : '') . '</li>';
                                } else {

                                        $sub_item_htm .= '<li class=""><a class="waves-effect waves-primary" href="' . $url . '">' . $icon . '<span>' . $code . ' ' . $nav_title . $label_htm . '</span></a>' . (isset($sub_item["sub"]) ? $this->process_sub_nav0($sub_item["sub"]) : '') . '</li>';
                                }
                        }
                        $sub_item_htm .= '</ul>';
                }
                return $sub_item_htm;
        }

        function process_sub_nav($nav_item, $is_ignore_id) {
//                vd::d($nav_item);
                $sub_item_htm = "";
                if (isset($nav_item["sub"]) && $nav_item["sub"]) {
                        $sub_nav_item = $nav_item["sub"];
                        $sub_item_htm = $this->process_sub_nav($sub_nav_item, $is_ignore_id);
                } else {
                        $sub_item_htm .= '<ul>';

                        foreach ($nav_item as $key => $sub_item) {

                                $nav_title = isset($sub_item["name"]) ? $sub_item["name"] : "(No Name)";

                                $key = $sub_item['functionID'];

//                                if (isset($sub_item['sub']) && $sub_item['sub']) {
//                                        $sub_item_htm .= $this->process_sub_nav($sub_item, $is_ignore_id);
//                                } else {

                                if ($is_ignore_id == true) {
                                        $sub_item_htm .= '<li class="menu-title"><span id=' . $key . '>' . $nav_title . '</span>' . (isset($sub_item["sub"]) ? $this->process_sub_nav($sub_item["sub"], $is_ignore_id) : '') . '</li>';
                                } else {
                                        $sub_item_htm .= '<li class="menu-title"><span id=' . $key . '>' . $key . '-' . $nav_title . '</span>' . (isset($sub_item["sub"]) ? $this->process_sub_nav($sub_item["sub"], $is_ignore_id) : '') . '</li>';
                                }

//                                }
                        }
                        $sub_item_htm .= '</ul>';
                }
//                vd::d($sub_item_htm);
                return $sub_item_htm;
        }

        function get_leaf_node($view, $column, $indexColumn, $where = '') {
                $sWhere = " left join " . $view . " t2 on t1." . $indexColumn . " = t2.parentID WHERE t1." . $this->config->item('display') . "='1' and (t2." . $indexColumn . " is null or t2." . $indexColumn . " = '-1' or t2." . $indexColumn . " = '0')";
                if (strlen($where) > 0) {
                        $sWhere .= " and " . $where;
                }

                $columns = explode(",", $column);
                $columns_0 = $columns;

                foreach ($columns as $k => $v) {
                        $columns[$k] = 't1.' . trim($v);
                        $columns_0[$k] = trim($v);
                }
                $sQuery = "SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $columns)) . "
		FROM   $view t1
		$sWhere
                        ";

                $query = $this->db->query($sQuery);
                $output = array();

                if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                                foreach ($columns_0 as $k => $v) {
                                        $a[$v] = $row->$v;
                                }
                                array_push($output, $a);
                        }
                }

                return $output;
        }

        function buildTreeFromFlatArray(array &$elements, $indexColumn = 'id', $parent_column = 'parentID', $parentId = 0) {
                $branch = array();

                foreach ($elements as $element) {
                        if ($element[$parent_column] == $parentId) {
                                $children = $this->buildTreeFromFlatArray($elements, $indexColumn, $parent_column, $element[$indexColumn]);
                                if ($children) {
                                        $element['children'] = $children;
                                }
                                $branch[$element[$indexColumn]] = $element;
//                                unset($elements[$element[$indexColumn]]);
                        }
                }
                return $branch;
        }

        function getChildrenSum($array) {
                $sum = 0;

                if (count($array) > 0) {
                        foreach ($array as $item) {
                                $sum += str_replace(',', '', $item['value']);
                                if (isset($item['children']))
                                        $sum += str_replace(',', '', $this->getChildrenSum($item['children']));
                        }
                } else {
                        $sum = 0;
                }
                return $sum;
        }

        function getSumFromArray($tree_array, $guid, $indexColumn = 'id') {
                foreach ($tree_array as $item) {
                        if ($item[$indexColumn] == $guid) {
                                return $this->getChildrenSum($item['children']);
                        } else {
                                if (isset($item['children'])) {
                                        foreach ($item['children'] as $item1) {
                                                if ($item1[$indexColumn] == $guid) {
                                                        return $this->getChildrenSum($item1['children']);
                                                }
                                        }
                                }
                        }
                }

                return 0;
        }

}
?>