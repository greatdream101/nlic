<?php

defined('BASEPATH') or die('Acceso Restringido');

class Tree_Menu_admin extends CI_Model {

        var $_CI;
        var $_forest;
        var $_settings;
        var $_parent;
        var $output;

        function Tree_Menu_admin() {
//                $this->_CI = & get_instance();
                $this->load->library('tree/tree_node');

                //$this->load->database();

                $this->_forest = array();
                $this->_settings = array();

                $this->config->load('tree_menu', TRUE, TRUE);
                $config = $this->config->item('tree_menu');

                $this->initialize($config);
        }

        function initialize($config = array()) {
                if (empty($config)) {
                        $config['pk'] = 'functionID';
                        $config['fk'] = 'function_parentID';
                        $config['text_column_eng'] = 'function_name_eng';
                        $config['text_column'] = 'function_name';
                        $config['code_column'] = 'function_code';
                        $config['table'] = 'v_at_admin_function';
                        $config['menu_list_class'] = 'menu_esp';
                        $config['userID_column'] = 'userID';
                        if (isset($_SESSION['userID']))
                                $config['userID'] = $_SESSION['userID'];
                }
                $this->_settings = $config;
        }

        function _get_id(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['pk'], $vars))
                        return $vars[$this->_settings['pk']];

                return 0;
        }

        function _get_text(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['text_column'], $vars)) {
                        $text = $vars[$this->_settings['text_column']];
                        return htmlspecialchars($text);
                }
                return '';
        }

        function _get_text_eng(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['text_column_eng'], $vars)) {
                        $text = $vars[$this->_settings['text_column_eng']];
                        return htmlspecialchars($text);
                }
                return '';
        }

        function _get_code(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['code_column'], $vars)) {
                        $text = $vars[$this->_settings['code_column']];
                        return htmlspecialchars($text);
                }
                return '';
        }

        function _get_roots() {
                $this->db->from($this->_settings['table']);
                $this->db->where($this->_settings['fk'] . ' IS NULL');
                $this->db->or_where($this->_settings['fk'], '0');
                $this->db->where('display', '1');
                $this->db->orderBy($this->_settings['text_column'], "asc");
                $query = $this->db->get();
                return $query->result();
        }

        function _get_children($id) {
                $this->db->from($this->_settings['table']);
                $this->db->where($this->_settings['fk'] . ' IS NOT NULL');
                $this->db->where($this->_settings['fk'], $id);
                $this->db->where('display', '1');
                $this->db->orderBy($this->_settings['text_column'], "asc");
                $query = $this->db->get();
                return $query->result();
        }

        function _has_children($id) {
                $query = $this->db->query(
                        "SELECT COUNT(*) AS total FROM "
                        . $this->db->protect_identifiers($this->_settings['table'])
                        . " WHERE " . $this->db->protect_identifiers($this->_settings['fk'])
                        . " IS NOT NULL "
                        . " AND " . $this->db->protect_identifiers($this->_settings['fk'])
                        . " = " . $this->db->escape($id)
                        . " and display='1'"
                );

                $result = $query->row();

                return $result->total > 0;
        }

        function _build_tree(&$tree) {
                $id = $this->_get_id($tree);

                if ($this->_has_children($id)) {
                        $children = $this->_get_children($id);

                        foreach ($children as $child) {
                                $child_tree = Tree::factory($child);

                                $this->_build_tree($child_tree);

                                $tree->insert($child_tree);
                        }
                }
        }

        function _print_sublist(&$tree) {
                $id = $this->_get_id($tree);
                $text = $this->_get_text($tree);
//            $link = strtolower($this->_get_text_eng($tree));
                $link = strtolower($this->_get_code($tree));
                $link = str_replace(' ', '_', $link);
                $link_code = $link;
//            $link = anchor($this->_parent . '/' . $link, $text, array('class' => 'link_menu', 'id' => $link));
                $link = anchor($link, $text, array('class' => 'link_menu_admin', 'function_code' => $link_code, 'id' => $this->_get_id($tree)));

//            $link = anchor($id, $text);                
                if (!$tree->has_children()) {
                        //print "<li>\n<div class=\'file\'>". $link. "</div>\n</li>\n";
                        $this->output .= "<li><span class=file>" . $link . "</div></li>";
                } else {

                        $this->_parent = strtolower($text);
                        //print "<li>\n<span class=\'folder\'>".$text."</span>\n";
                        $this->output .= "<li><span class=folder>" . $text . "</span>";

                        //print "<ul>\n";
                        $this->output .= "<ul>";
                        $children = & $tree->children();
                        foreach ($children as $child) {
                                $this->_print_sublist($child);
                        }
                        //print "</ul>\n";
                        $this->output .= "</ul>";
                }
        }

        function &build() {
                $roots = $this->_get_roots();
                foreach ($roots as $root) {
                        $tree = Tree::factory($root);
                        $this->_build_tree($tree);
                        $this->_forest[] = $tree;
                }

                return $this->_forest;
        }

        function print_menu() {

                $this->output = "";
                $this->output .= '<ul id="exampletreeview_admin" class="filetree">';

                foreach ($this->_forest as $tree) {
                        $this->_print_sublist($tree);
                }
                $this->output .='</ul>';

                return $this->output;
        }

}

?>