<?php
require_once dirname(__FILE__) . '/../'.'libraries/spout-3.0.1/Box/src/Spout/Autoloader/autoload.php';

use Box\Spout\Autoloader\Psr4Autoloader;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

class Spout_excel extends Psr4Autoloader {

    public function excel_raw($params){
        
        $writer = WriterEntityFactory::createXLSXWriter();
        $fileName = $params['rep_name'];        
        $writer->openToBrowser($fileName); // stream data directly to the browser
        $row_header = $params['data_array'][0];
        
        foreach ($row_header as $key => $value) {
            $cells[] = WriterEntityFactory::createCell($key);
        }
        
        /** add a row at a time */
        $singleRow = WriterEntityFactory::createRow($cells);
        $writer->addRow($singleRow);
        
        /** Shortcut: add a row from an array of values */
        foreach ($params['data_array'] as $keys => $values) {
            $arrValue = '';
            foreach ($values as $key => $value) {
                $arrValue[] = $value;
            }           
            $rowFromValues = WriterEntityFactory::createRowFromArray($arrValue);
            $writer->addRow($rowFromValues);
        }        
        unset($params['data_array']);
        unset($rowFromValues);
        $writer->close();
    }
}