<?php 

class QcSurvey_lib extends CI_Model{

    function get_qcHistory($surveyerID,$seriesID,$branchID){

        $this->db->select('qcStatusName,createdDate,questionID,remark')->from('v_qcHistory')
            ->where('userID',$surveyerID)
            ->where('seriesID',$seriesID)
            ->where('branchID',$branchID)
            ->orderby('createdDate','DESC');
        $query = $this->db->get();    
        $data = [];
        foreach($query->result_array() as $val){ 
            $data[$val['questionID']][] = array('qcStatusName'=>$val['qcStatusName'],'createdDate'=>$val['createdDate'],'remark'=>$val['remark']);
        }
        //vd::d($data);
        $dataTable = [];
        foreach($data as $kk => $vTable){
            $dataTable[$kk] = $this->mcl->table($vTable);
        }
        return $dataTable;
    }
}