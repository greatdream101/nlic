<?php
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Scripts_lib extends Mcl {

        public function __construct() {
                parent::__construct();
        }

        function genScriptsTable($data = [], $url = [], $class = '',$modal_func='',$target_id='') {
                //$data = array(), $url = array(), $class = ''

                $o = '';
                if (count($data) != 0) {
                        $index_column = array_key_exists('index_column', $url) ? $url['index_column'] : '';
                        // $columns = array_keys($data[0]);
                        $columns = array_keys(reset($data));
                        $o = '';
                        $o .= '<table class = "table table-bordered table-striped table-hover table-condensed ' . $class . '">';
                        $o .= '<thead>';

                        $o .= '<tr>';
                        if (strpos($class, 'runNumber') !== false) {
                                $o .= ' <th>' . $this->gl('no') . ' </ th>';
                        }
                        if (count($columns) > 0) {
                                foreach ($columns as $k => $v) {
                                        $o .= ' <th>' . $this->gl($v) . ' </ th>';
                                }
                        }
                        if($modal_func != '')
                        {
                             $o .= '<th class="text-center">'.$this->mcl->gl($modal_func).'</th>';
                        }

                        $o .= '</tr>';

                        if (strpos($class, 'no-filter') == 0) {
                                $o .= '<tr class="second" role="row">';
                                if (strpos($class, 'runNumber') !== false) {
                                        $o .= '<td><input class="form-control search_init" name="search_no" value="" type="text"></td>';
                                }
                                if (count($columns) > 0) {
                                        foreach ($columns as $k => $v) {
                                                $o .= '<td><input class="form-control search_init" name="search_' . $v . '" value="" type="text"></td>';
                                        }
                                }
                                if($modal_func != '')
                                {
                                     $o .= '<td><input class="form-control search_init" name="search_no" value="" type="text"></td>';
                                }

                                $o .= '</tr>';
                        }

                        $o .= '</thead>';
                        $o .= '<tbody>';
                        $rn = 0;
                        foreach ($data as $k => $v) {
                                $rn++;
                                $o .= '<tr>';
                                if (strpos($class, 'runNumber') !== false) {
                                        $o .= ' <td>' . $rn . ' </ td>';
                                }
                                $i = 0;
                                foreach ($columns as $kk => $vv) {
                                        if ($i == 0 & !empty($url)) {
                                                //row_action row_action_edit btn btn-xs btn-default
                                                $o .= '<td>';
                                                for ($li = 0; $li < count($url['url']); $li ++) {
                                                        if (isset($url['attr_id'][$li]) and ! empty($url['attr_id'][$li])) {
                                                                $tkid = $url['attr_id'][$li];
                                                        } else {
                                                                $tkid = "";
                                                        }

                                                        if (strpos($url['url'][$li], 'http') !== false) {
                                                                if (strlen($index_column) > 0) {
                                                                        $o .= '<a id="' . $v[trim($vv)] . '" class="row_action row_action_edit btn btn-xs btn-default ' . $tkid . '" href="' . $url['url'][$li] . '/?' . $index_column . '=' . $v[trim($vv)] . '" target="_blank"><i class="fa fa-edit"></i></a>';
                                                                } else {
                                                                        $o .= '<a id="' . $v[trim($vv)] . '" class="row_action row_action_edit btn btn-xs btn-default ' . $tkid . '" href="' . $url['url'][$li] . '/' . $v[trim($vv)] . '"><i class="fa fa-edit"></i></a>';
                                                                }
                                                        } else {
                                                                $o .= '<a id="' . $v[trim($vv)] . '" class="row_action row_action_edit btn btn-xs btn-default ' . $tkid . '" href="#' . $url['url'][$li] . '/' . $v[trim($vv)] . '"><i class="fa fa-edit"></i></a>';
                                                        }
                                                }
                                                $o .= '</td>';
                                        } else {
                                                $o .= '<td>' . $v[trim($vv)] . ' </ td>';
                                        }
                                        $i++;
                                }
                                if($modal_func != '')
                                {
                                     $o .= '<td class="text-center">
                                                    <button class="btn btn-primary btn-rounded btn-pop-modal" data-target=".bd-example-modal-sm" type="button"><i class="fa fa-info-circle"></i> '.$this->mcl->gl($modal_func).'</button>
                                                    <input type="hidden" value="'.$v[$target_id].'" />
                                               </td>';
                                }

                                $o .= '</tr>';
                        }

                        $o .= '</tbody>';
                        $o .= '</table>';
                }
                return $o;
        }

        function getScriptsGreeting($seriesID)
        {
             $this->load->model('addon/Scripts_mod');
             $greetingScripts = $this->Scripts_mod->getSurveyScripts(1,$seriesID);

             $o = "";
             if(!empty($greetingScripts))
             {
                  $o .= '<div class="card-box greeting-scripts-area text-center">';
                       $o .= $greetingScripts[0]['scriptsText'];
                       $o .= "</p></p>";
                       $o .= "<button class='btn-begin-survey'>".$this->mcl->gl('begin')."</button>";
                  $o .= '</div>';
             }

             return $o;
        }

        function getScriptsEnding($seriesID)
        {
             $this->load->model('addon/Scripts_mod');
             $greetingScripts = $this->Scripts_mod->getSurveyScripts(2,$seriesID);

             $o = "";
             if(!empty($greetingScripts))
             {
                $o .= '<div class="card-box ending-scripts-area text-center" style="margin-top:4%;">';
                $o .= $greetingScripts[0]['scriptsText'];
                // $o .= "</p></p>";
                // $o .= "<button class='btn-end-survey'>".$this->mcl->gl('end')."</button>";
                $o .= '</div>';
             }

             return $o;
        }

        function getScriptsTerminate($seriesID)
        {
             $this->load->model('addon/Scripts_mod');
             $terminateScripts = $this->Scripts_mod->getSurveyScripts(3,$seriesID);

             $o = "";
             if(!empty($terminateScripts))
             {
                $o .= '<div class="card-box ending-scripts-area text-center">';
                $o .= $terminateScripts[0]['scriptsText'];
                $o .= '</div>';
             }

             return $o;
        }

        function getScriptsNotFound($seriesID=0)
        {
             $this->load->model('addon/Scripts_mod');
             $scriptsText = $this->Scripts_mod->getSurveyScripts(4,$seriesID);

             $o = "";
             if(!empty($scriptsText))
             {
                $o .= '<div class="card-box ending-scripts-area text-center">';
                $o .= $scriptsText[0]['scriptsText'];
                $o .= '</div>';
             }

             return $o;
        }
}
