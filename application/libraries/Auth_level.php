<?

class Auth_level extends CI_Model {

        function in($input) {
                $result = array();
                $oo = array();
                foreach ($input as $k => $v) {
                        if (strpos($k, '_') !== false) {
                                $data = explode("_", $k);
//                                var_dump($data);
                                if (count($data) > 0 && (int) $data[0] > 0) {
                                        list($groupID, $functionID) = $data;
                                        if (is_numeric($groupID) && is_numeric($functionID)) {
                                                if (isset($oo[$groupID])) {
                                                        $oo[$groupID] .= ',' . $k . '-' . $v;
                                                } else {
                                                        $oo[$groupID] = $k . '-' . $v;
                                                }
                                        }
                                }
                        }
                }
//                var_dump($oo);
                return $oo;
        }

        function get_auth($group) {
                if ((int) $group > 0) {
                        $sql = "select authLevel from " . $this->config->item('group_table') . " where groupID='" . $group . "'";
                        $query = $this->db->query($sql);

                        if ($query->num_rows() > 0) {
                                $data = $query->row();
                                if (!is_null($data->authLevel)) {
                                        $authLevel = $this->encryption->decrypt($data->authLevel);
                                        return $authLevel;
                                } else {
                                        return false;
                                }
                        } else {
                                return false;
                        }
                } else {
                        return false;
                }
        }

}

?>
