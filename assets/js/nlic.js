$(document).ready(function(){

    handleChart();

    function handleChart(){

        var host = window.location.origin;
        var url  = host+'/mol/sso_summary_data';

        $.ajax({
            type: "POST",
            url: url,
            cache: false,
            async: false,
            data: {'year':'2019'},
            success: function (html) {
                output = $.parseJSON(html);
                handleHighChart('chartColumn', output['data']);
            }
        });
    }

    $('#select_year').on('change', function(){
        // console.log('data : '+$(this).val());
        var year = $(this).val();
        var host = window.location.origin;
        var url  = host+'/mol/sso_summary_data';

        $.ajax({
            type: "POST",
            url: url,
            cache: false,
            async: false,
            data: {'year':year},
            success: function (html) {
                output = $.parseJSON(html);
                console.log(output['data']['A33']);
                handleHighChart('chartColumn', output['data']);
                }
        });
    });

        function handleHighChart(elementid, data){

        var ctx = $("#"+elementid);
                var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                            datasets: [{
                                label: ['มาตรา 33'],
                                data: data['A33'],
                            backgroundColor: 
                                [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                ],
                            borderColor:
                                [
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                ],
                                borderWidth: 1
                            },{
                                label: ['มาตรา 39'],
                                data: data['A39'],
                            backgroundColor: 
                                [
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                ],
                                borderColor: 
                                [
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(255, 206, 86, 1)',
                                ],
                                borderWidth: 1
                            },{
                                label: ['มาตรา 40'],
                                data: data['A40'],
                            backgroundColor: 
                                [
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                ],
                                borderColor: 
                                [
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(153, 102, 255, 1)',
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                }
                            }]
                        },
                         responsive: true,
                         title: {
                            display: true,
                        //     text: 'THAI STATISTICS HOSPITAL PATIENT OF OPD'
                        },
                    }
                });
        }//end chart

        $('.open_modal_document').each(function(){
            $(this).on('click', function(){

                var host       = window.location.origin;
                var url        = host+'/mol/get_document_list';
                var service_id = $(this).attr('documentid');
                var titleName  = $(this).attr('link_name');

                $.ajax({
                    method: "POST",
                    url   : url,
                    data  : {'service_id':service_id},
                    success:function(result){
                        $('#modal_header').html(titleName);
                        $('#document_list').html(result);
                        // $('#document_list').append(result);
                        $('#document_modal').modal('show');

                        setTimeout(() => {
                            $(".document_file_list").hover(function(){
                                $(this).css("background-color", "#dddddd");
                            }, function(){
                                $(this).css("background-color", "#fafafa");
                              });
                        }, 100);
                    }
                });
            });
        });
});