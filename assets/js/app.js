/**
 * Theme: Minton Admin Template
 * Author: Coderthemes
 * Module/App: Main Js
 * host        www.Example.com:8080
 * hostname    www.Example.com
 * port        8080
 * protocol    http:
 * pathname    index.php
 * href        http://www.Example.com:8080/index.php#tab2
 * hash        #tab2
 * search      ?foo=789
 * var x = $(location).attr('<property>');
 */
var str = $(location).attr('href');
var res = str.split("/");
var url = res[0] + '//' + res[2] + '/' + res[3] + '/';
var host = $(location).attr('host');
var ajax_data = '';
var locale = new Object();
var selected = [];
var current_hash = '';
var cc = $('#content');
var excel_url = 'mct/export_excel';
var fc = '';
var fo = '';
var hot = {};
var hot_data = [];
var hot_header = [];
var hot_schema = [];
var hot_addon = [];
var iMaxFilesize = 20971520; // 20MB
var auto_save_in_days = 3; //3 days
var num_cols_overflow = 5;
var full_calendar_url = 'mct/get_full_calendar';
var user_full_calendar_url = 'mct/get_full_calendar_user';
var tree_inner_url = 'addon/ajax_load_treeview/get_tree_inner?display=1&is_semi=0';
var _mode;
var cc;
$.timeelapse.settings.showMsecs = true;
toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
}

function executeFunctionByName(functionName, context /*, args */) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
                context = context[namespaces[i]];
        }
        return context[func].apply(this, args);
}
var w, h, dw, dh;
var changeptype = function () {
        w = $(window).width();
        h = $(window).height();
        dw = $(document).width();
        dh = $(document).height();

        if (jQuery.browser.mobile === true) {
                $("body").addClass("mobile").removeClass("fixed-left");
        }

        if (!$("#wrapper").hasClass("forced")) {
                if (w > 1024) {
                        $("body").removeClass("smallscreen").addClass("widescreen");
                        $("#wrapper").removeClass("enlarged");
                } else {
                        $("body").removeClass("widescreen").addClass("smallscreen");
                        $("#wrapper").addClass("enlarged");
                        $(".left ul").removeAttr("style");
                }
                if ($("#wrapper").hasClass("enlarged") &&
                        $("body").hasClass("fixed-left")) {
                        $("body").removeClass("fixed-left").addClass("fixed-left-void");
                } else if (!$("#wrapper").hasClass("enlarged") &&
                        $("body").hasClass("fixed-left-void")) {
                        $("body").removeClass("fixed-left-void").addClass("fixed-left");
                }

        }
        toggle_slimscroll(".slimscrollleft");
}

var debounce = function (func, wait, immediate) {
        var timeout, result;
        return function () {
                var context = this,
                        args = arguments;
                var later = function () {
                        timeout = null;
                        if (!immediate)
                                result = func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow)
                        result = func.apply(context, args);
                return result;
        };
}

function resizeitems() {
        if ($.isArray(resizefunc)) {
                for (i = 0; i < resizefunc.length; i++) {
                        window[resizefunc[i]]();
                }
        }
}

function initscrolls() {
        if (jQuery.browser.mobile !== true) {
                //SLIM SCROLL
                $('.slimscroller').slimscroll({
                        height: 'auto',
                        size: "5px"
                });

                $('.slimscrollleft').slimScroll({
                        height: 'auto',
                        position: 'right',
                        size: "5px",
                        color: '#dcdcdc',
                        wheelStep: 5
                });
        }
}

function toggle_slimscroll(item) {
        if ($("#wrapper").hasClass("enlarged")) {
                $(item).css("overflow", "inherit").parent().css("overflow", "inherit");
                $(item).siblings(".slimScrollBar").css("visibility", "hidden");
        } else {
                $(item).css("overflow", "hidden").parent().css("overflow", "hidden");
                $(item).siblings(".slimScrollBar").css("visibility", "visible");
        }
}

//============================

var checkURL = function () {
        //get the url by removing the hash
        //var url = location.hash.replace(/^#/, '');
        var url = location.href.split('#').splice(1).join('#');

        //BEGIN: IE11 Work Around
        if (!url) {

                try {
                        var documentUrl = window.document.URL;
                        if (documentUrl) {
                                if (documentUrl.indexOf('#', 0) > 0 && documentUrl.indexOf('#', 0) < (documentUrl.length + 1)) {
                                        url = documentUrl.substring(documentUrl.indexOf('#', 0) + 1);

                                }

                        }

                } catch (err) {
                }
        }
        //END: IE11 Work Around

        var container = $('#ajax_content');

        // Do this if url exists (for page refresh, etc...)
        if (url) {
                // remove all active class
                $('nav li.active').removeClass("active");
                // match the url and add the active class

                $('nav li:has(a[href="#' + url + '"])').addClass("active");
                var title = ($('nav a[href="#' + url + '"]').attr('title'));

                // change page title from global var
                document.title = (title || document.title);

                // parse url to jquery
                loadURL(url + location.search, container);

        }
}


var end_loading = function (container) {
        var container = which_container(container);


        var b = $(cc).find('.sticky');
        if (b.length > 0) {
                enableControl(1);
                if (typeof (b.attr('pass_validation')) == 'undefined')
                        show_footer(0);
        } else {
                enableControl(1);
        }
        // scrollTo();
}
var enableControl = function (t) {
        if (t == 0) {
                $(cc).find('button:not(.always_disabled)').attr('disabled', 'disabled'); //.fadeTo('fast', 0.2);
                $(cc).find('button:not(.always_disabled)').attr('disabled', 'disabled'); //.fadeTo('fast', 0.2);
                $(cc).find('input:not(.always_disabled):not([type=hidden])').attr(
                        'disabled', 'disabled'); //.fadeTo('fast', 0.2);
                $(cc).find('textarea:not(.always_disabled)').attr('disabled',
                        'disabled'); //.fadeTo('fast', 0.2);
                $(cc).find('.select2:not(.always_disabled)').prop("disabled", true);
                $(
                        '.btn-header a, span[id=demo-setting], div[id=function_tree_area] li a, input.search, button.btn-search,a.show-shortcut')
                        .addClass('disabled');
        } else {
                $(cc).find('button:not(.always_disabled)').removeAttr('disabled'); //.fadeTo('fast', 1);
                $(cc).find('button:not(.always_disabled)').removeAttr('disabled'); //.fadeTo('fast', 1);
                $(cc).find('input:not(.always_disabled):not([type=hidden])')
                        .removeAttr('disabled'); //.fadeTo('fast', 1);
                $(cc).find('.select2:not(.always_disabled)').prop("disabled", false);
                $(cc).find('textarea:not(.always_disabled)').removeAttr('disabled'); //.fadeTo('fast', 0.2);
                $(
                        '.btn-header a, span[id=demo-setting], div[id=function_tree_area] li a, input.search, button.btn-search,a.show-shortcut')
                        .removeClass('disabled');
        }
        $(cc).find('input[name=checkbox_all]').prop('checked', false);
}

var get_selectbox_placeholder = function (is_multiple) {
        var placeholder;
        if (get_lang() == 'english') {
                if (is_multiple == false) {
                        placeholder = "Type or Select (one item)"
                } else {
                        placeholder = "Type or Select (more than one item)"
                }
        } else {
                if (is_multiple == false) {
                        placeholder = "สามารถพิมพ์หรือเลือกได้เพียงรายการเดียว"
                } else {
                        placeholder = "สามารถพิมพ์หรือเลือกได้หลายรายการ"
                }
        }
        return placeholder
}

var get_lang = function () {
        var lang = $('body').find('input[type=hidden][name=site_lang]').val();
        return lang;
}

var is_use_cookie = function (o) {
        if ($('.input_page').length > 0) {
                return false;
        } else {
                if ($(o).hasClass('no-cookie')) {
                        return false;
                }
                return true;
        }
}

var htmlspecialchars_decode = function (string, quote_style) {
        //       discuss at: http://phpjs.org/functions/htmlspecialchars_decode/
        //      original by: Mirek Slugen
        //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        //      bugfixed by: Mateusz "loonquawl" Zalega
        //      bugfixed by: Onno Marsman
        //      bugfixed by: Brett Zamir (http://brett-zamir.me)
        //      bugfixed by: Brett Zamir (http://brett-zamir.me)
        //         input by: ReverseSyntax
        //         input by: Slawomir Kaniecki
        //         input by: Scott Cariss         //         input by: Francois
        //         input by: Ratheous
        //         input by: Mailfaker (http://www.weedem.fr/)
        //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // reimplemented by: Brett Zamir (http://brett-zamir.me)
        //        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
        //        returns 1: '<p>this -> &quot;</p>'
        //        example 2: htmlspecialchars_decode("&amp;quot;");                         //        returns 2: '&quot;'
        if (string == null)
                return '';
        var optTemp = 0,
                i = 0,
                noquotes = false;
        if (typeof quote_style === 'undefined') {
                quote_style = 2;
        }
        string = string.toString().replace(/&lt;/g, '<').replace(/&gt;/g, '>');
        var OPTS = {
                'ENT_NOQUOTES': 0,
                'ENT_HTML_QUOTE_SINGLE': 1,
                'ENT_HTML_QUOTE_DOUBLE': 2,
                'ENT_COMPAT': 2,
                'ENT_QUOTES': 3,
                'ENT_IGNORE': 4
        };
        if (quote_style === 0) {
                noquotes = true;
        }
        if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
                quote_style = [].concat(quote_style);
                for (i = 0; i < quote_style.length; i++) {
                        // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
                        if (OPTS[quote_style[i]] === 0) {
                                noquotes = true;
                        } else if (OPTS[quote_style[i]]) {
                                optTemp = optTemp | OPTS[quote_style[i]];
                        }
                }
                quote_style = optTemp;
        }
        if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
                string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
                // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
        }
        if (!noquotes) {
                string = string.replace(/&quot;/g, '"');
        }
        // Put this in last place to avoid escape being double-decoded
        string = string.replace(/&amp;/g, '&');
        return string;
}

var show_hint = function (o) {
        var name = o.attr('name');
        var max_length = attr(o.attr('maxlength'), '65,535');
        var sys_validation = attr(
                ucFirstAllWords($(
                        'input[type=hidden][name=sys_validation\\[' + name + '\\]]')
                        .val()), '-');
        var format = '';
        var ins = '';
        var wysiwyg = false;
        if (o.hasClass('wysiwyg')) {
                wysiwyg = true;
        }
        var select2 = false;
        if (o.hasClass('select2')) {
                select2 = true;
        }

        var datepicker = false;
        if (o.hasClass('datepicker')) {
                datepicker = true;
        }

        var announce_header = false
        if (o.hasClass('announce_header')) {
                announce_header = true;
        }

        var document_download = false;
        if (o.hasClass('document_download')) {
                document_download = true;
        }

        var button = false;
        if (o.hasClass('btn')) {
                button = true;
        }

        switch (o.attr('type')) {
                case 'search':
                        ins = 'Type the text to filter this column data';
                        max_length = '-';
                        break;
                case 'password':
                        ins = 'Enter for password. The password strength is show below this hint';
                        break;
                case 'text':
                        if (datepicker) {
                                ins = 'Click on the white textbox and select the year, month then date. The dialog will be disappear after clicking on the desired date.';
                                max_length = '-';
                                format = 'YYYY-mm-dd';
                        } else {
                                format = 'Alphabets & Number';
                                ins = 'Please enter values according to its max. length, validation and format.';
                        }
                        break;
                default: //textarea
                        format = 'Alphabets & Number';
                        if (wysiwyg) {
                                ins = 'Click on it, and wait for a seconds. It will turn into HTML inputs.'
                        } else if (select2) {
                                var multiple = attr(o.attr('multiple'), '');
                                if (multiple.length > 0) {
                                        ins = 'Plase select one or more choices. You can use the filter box above to find any related item. ';
                                        ins += 'Click the cross symbol on the right to erase the previous selected values.';
                                } else {
                                        ins = 'Plase select only one choice. You can use the filter box above to find any related item. '
                                        ins += 'Click the cross symbol on the right of each selected items to remove.';
                                }

                                max_length = '-';
                                format = '-';
                        } else if (button) {
                                ins = 'Click this button to submit the above conditions to query for related data.';
                                format = '-';
                                max_length = '-';
                        } else if (announce_header) {
                                ins = 'Click on the title of each announcements to show/ hide it\'s detail.';
                                format = '-';
                                max_length = '-';
                        } else if (document_download) {
                                ins = 'Click on each lists and then the save file dialog will appear.';
                                format = '-';
                                max_length = '-';
                        } else {
                                ins = 'Please enter long description in this area. You can also write in HTML format.';
                        }
                        break;
        }

        var h = $('<div class="hint-text">' + ins +
                '<div class=spacer-10></div><ul><li>Max. Characters: ' +
                max_length + '</li><li>Validation: ' + sys_validation +
                '</li><li>Format: ' + format + '</li></ul></div>')
        $('#sidebar-hint').find('.inner-padding').html(h);
}

var ucFirstAllWords = function (str) {
        if ($.inArray(" ", str) !== -1) {
                var pieces = str.split(" ");
                for (var i = 0; i < pieces.length; i++) {
                        var j = pieces[i].charAt(0).toUpperCase();
                        pieces[i] = j + pieces[i].substr(1);
                }
                return pieces.join(" ");
        } else if ($.inArray("_", str) !== -1) {
                pieces = str.split("_");
                for (i = 0; i < pieces.length; i++) {
                        j = pieces[i].charAt(0).toUpperCase();
                        pieces[i] = j + pieces[i].substr(1);
                }
                return pieces.join(" ");
        } else {
                if (typeof (str) !== 'undefined' && str.length > 0) {
                        var a = str.substr(0, 1);
                        var b = str.substr(1);
                        return a.toUpperCase() + b;
                } else {
                        return '';
                }
        }
}

var log_events = function (btn_name) {
        var page = window.location.href;
        var data = 'btn_name=' + btn_name + '&page=' + page;
        $.ajax({
                type: "POST",
                url: get_base_url() + 'mct/log_events',
                data: data,
                cache: false,
                async: false,
                beforeSend: function () {

                },
                success: function (html) {

                }
        });
}

var get_cookie = function (name) {
        return $.cookie(name);
}

var clear_input = function (container) {
        var cc = which_container(container);
        $(cc).find('.tab_asterisk').remove();
        $(cc).find('em.invalid').remove();
        $(cc).find('.datatable').find('input[type=checkbox]').prop('checked', false);
        $(cc).find('input[type=hidden][name=id]').val(0);
        $(cc).find('input[type=text]:not(".do_not_clear")').val('');
        $(cc).find('textarea:not(".do_not_clear")').val('');
        $(cc).find('input[type=hidden][name=id]').remove();
        $(cc).find('upload_row_edit').empty();
        $(cc).find('input[type=checkbox]:not(.do_not_clear)').prop('checked', false);
        $(cc).find('table.table-checkbox').find('input[type=hidden]:not(".do_not_clear")').remove();
        $(cc).find('select:not(".do_not_clear")').each(function () { //added a each loop here
                $(this).val('');
        });
        $(cc).find('.select2').removeClass('validation-required');
        $(cc).find('input[type=hidden][class=sortOrder]').remove();
        $(cc).find('table.table-detail tbody tr').each(function (k, v) {
                var id = $(v).attr('id');
                if (id !== 'row0') {
                        $(v).remove();
                }
                id = null;
        })
        $(cc).find('.fileinput').find('input[type=hidden]').remove();
        var a = $(cc).find('.document_original_file');
        a.next().remove();
        a.remove();
        $(cc).find('table[id$=main_list_view] tr').removeClass('dt_selected'); //.removeClass('dt_selected_edit');
        $(cc).find('a.my_file_upload').remove();
        $(cc).find('textarea.wysiwyg:not(".do_not_clear")').each(function (k, v) {
                $(v).code('');
        });
        $(cc).find('input[id^=sys_validation]').each(
                function () {
                        var id = $(this).attr('id').replace('sys_validation', '').replace('[', '').replace(']', '');
                        if (id.length > 0) {
                                $(cc).find('input[type=hidden][name=' + id + ']:not(".do_not_clear")').remove();
                                $(cc).find('input[type=hidden][id=' + id + ']:not(".do_not_clear")').remove();
                        }
                        id = null;
                });
        a = null;
}

var clear_error_tab = function () {
        $(cc).find('em.invalid').remove();
        $(cc).find('.validation-required').removeClass('validation-required');
}

var startUploadings = function () {
        // passUpload = true;
        var p = [];
        $('body').find('input[type=file]:not(.upload_row0):not(.note-image-input)')
                .each(
                        function (k, v) {
                                var name = $(v).attr('name');
                                var time = get_current_datetime();
                                var fileName = $(this).val();

                                if (fileName.length > 0) {
                                        var fileNameExt = fileName.substr(fileName
                                                .lastIndexOf('.') + 1);
                                        fileNameExt = fileNameExt.toLowerCase();
                                        var validExtensions = getValidExtension(name);
                                        if ($.inArray(fileNameExt, validExtensions) == -1) {
                                                alert("Invalid file type '." + fileNameExt +
                                                        "'");
                                                p.push(0);
                                        } else {
                                                if ($(this).val()) {
                                                        startUploading(name, time, fileName);
                                                        p.push(1);
                                                }
                                        }
                                        validExtensions, fileNameExt = null;
                                }
                                name, time, fileName = null;
                        });
        if (p.length > 0) {
                if ($.inArray(0, p) !== -1)
                        passUpload = false;
                else
                        passUpload = true;
        } else {
                passUpload = true;
        }
}

var uploadProgress = function (e) { // upload process in progress
        var obj = $('#content').find(
                'input[type=file]:not(.pass-upload):not(.upload_row0):first');
        var name = $(obj).attr('name');
        if (e.lengthComputable && obj) {
                try {
                        iBytesUploaded = e.loaded;
                        iBytesTotal = e.total;
                        var iPercentComplete = Math.round(e.loaded * 100 / e.total);
                        var iBytesTransfered = bytesToSize(iBytesUploaded);
                        document.getElementById(name + '_progress_percent').innerHTML = iPercentComplete
                                .toString() +
                                '%';
                        document.getElementById(name + '_progress').style.width = (iPercentComplete * 4)
                                .toString() +
                                'px';
                        document.getElementById(name + '_b_transfered').innerHTML = iBytesTransfered;
                        if (iPercentComplete == 100) {
                                var oUploadResponse = document.getElementById(name +
                                        '_upload_response');
                                oUploadResponse.innerHTML = '<h1>Please wait...processing</h1>';
                                oUploadResponse.style.display = 'block';
                        }
                } catch (e) {
                }
        } else {
                document.getElementById(name + '_progress').innerHTML = 'unable to compute';
        }
}

var uploadFinish = function (e) { // upload successfully finished
        try {
                var obj = $('#content').find(
                        'input[type=file]:not(.pass-upload):not(.upload_row0):first');
                var name = $(obj).attr('name');
                if (obj) {
                        var oUploadResponse = document.getElementById(name +
                                '_upload_response');
                        oUploadResponse.innerHTML = e.target.responseText;
                        oUploadResponse.style.display = 'block';
                        document.getElementById(name + '_progress_percent').innerHTML = '100%';
                        document.getElementById(name + '_progress').style.width = '100%';
                        document.getElementById(name + '_filesize').innerHTML = sResultFileSize;
                        document.getElementById(name + '_remaining').innerHTML = '| 00:00:00';
                        $('.start-upload').addClass('hide');
                        $('.' + name + '_upload_stat').fadeOut('slow');
                        $(obj).addClass('pass-upload');
                        var upload_dir = $('input[type=hidden][name=upload_dir]').val();
                        var result = e.target.responseText.split(','); //                var from_file = result[1];
                        var to_file = result[0];
                        var o = '<a href=' + download_url + upload_dir + '/' + to_file +
                                '>' + to_file + '</a>';
                        var t = name.split('_');
                        t = t[t.length - 1];
                        var t1 = t.substr(0, t.length - 1);
                        var t2 = t.substr(t.length - 1);
                        if ($.isNumeric(t1) && t2 == 'a') {
                                $(obj).parent().parent().parent().html(o);
                        } else {
                                var l = download_url + upload_dir + '/' + to_file;
                                $('a.my_file_upload').attr('href', l).html(to_file);
                        }
                        clearInterval(oTimer);
                }
        } catch (e) {
        }
}

var uploadError = function (e) { // upload error
        try {
                var obj = $('#content').find(
                        'input[type=file]:not(.upload_row0):not(.pass-upload):first')
                        .attr('name');
                if (obj) {
                        document.getElementById(name + '_error2').style.display = 'block';
                        clearInterval(oTimer);
                }
        } catch (e) {
        }
}

var uploadAbort = function (e) { // upload abort
        try {
                var obj = $('#content').find(
                        'input[type=file]:not(.upload_row0):not(.pass-upload):first')
                        .attr('name');
                if (obj) {
                        document.getElementById(name + '_abort').style.display = 'block';
                        clearInterval(oTimer);
                }
                loading
        } catch (e) {
        }
}

var startUploading = function (name, time, original_file_name) { // cleanup all temp states
        if (name.length > 0) {
                iPreviousBytesLoaded = 0;
                var vFD = new FormData(document.getElementById('my_form'));
                if (vFD == null)
                        var vFD = new FormData(getElementsStartsWithId('my_form'));
                // create XMLHttpRequest object, adding few event listeners, and POSTing our data
                var oXHR = new XMLHttpRequest();
                oXHR.upload.addEventListener('progress', uploadProgress, false);
                oXHR.addEventListener('load', uploadFinish, false);
                oXHR.addEventListener('error', uploadError, false);
                oXHR.addEventListener('abort', uploadAbort, false);
                var upload_dir = $('input[type=hidden][name=upload_dir]').val();
                //                var table_name = $('input[type=hidden][name=table_name]').val();
                var file_save_name = time;
                //                var params = "upload_dir=" + upload_dir + '&table_name=' + table_name + '&file_save_name=' + file_save_name + '&upload_name=' + name;
                var params = 'file_save_name=' + file_save_name + '&upload_name=' +
                        name + '&original_file_name=' + original_file_name +
                        '&upload_dir=' + upload_dir;
                oXHR.open('POST', get_base_url() + 'mct/upload?' + params);
                oXHR.send(vFD);
                var oFile = document.getElementById(name).files[0];
                if (typeof (oFile) !== 'undefined') {
                        var ext = CheckExtension(oFile, name);
                        var h = '<input type=hidden name=' + name + ' value="' +
                                file_save_name + '.' + ext + '">';
                        $('input[name=' + name + ']').after(h);
                }
        }
}

var get_data_serialize = function (container) {
        container = which_container(container);
        var data = '';
        var dd = $(container).find('input:not(.row0):not([validation_rules*=money]), textarea:not(.row0)').serializeArray();
        var c = [];

        $.each(dd, function (i, d) {
                if ($.trim(d['value']).length > 0 && $.trim(d['name']).length > 0) {
                        if ($.inArray(d['name'], c) == -1) {
                                data += d['name'] + '=' + d['value'] + '&';
                                c.push(d['name']);
                        }
                }
        })

        var serializedData = $(container).find('input[validation_rules*=money]').serializeArray();
        $.each(serializedData, function (i, d) {
                if ($.trim(d['value'])) {
                        data += '&' + d['name'] + '=' + replace_all(d['value'], ',', '');
                }
        });

        $(container).find('.wysiwyg').each(function (k, v) {
                var name = $(v).attr('name');
                var detail = $.htmlClean($(v).code(), {
                        format: true
                });
                data += '&' + name + '=' + encodeURIComponent(detail);
                name, detail = null;
        });

        serializedData = null;
//      alert(data);
        return data;
}

var deserialize = function (query) {
        var pairs, i, keyValuePair, key, value, map = {};
        // remove leading question mark if its there
        if (query.slice(0, 1) === '?') {
                query = query.slice(1);
        }
        if (query !== '') {
                pairs = query.split('&');
                for (i = 0; i < pairs.length; i += 1) {
                        keyValuePair = pairs[i].split('=');
                        key = decodeURIComponent(keyValuePair[0]);
                        value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) :
                                undefined;
                        map[key] = value;
                }
        }

        return map;
}

var end_saving = function (container) {
        container = which_container(container);
        clear_cookie();
        setTimeout(function () {
                toastr["success"]("Save data complete.", 'System Message');
                enableControl(1);
        }, 1000);
        $('em.invalid').remove();
        //        $(container).find('button.search:not([id^=btn_save])').trigger('click');
        $(container).find('a.row_detail_undelete:visible').each(function (k, v) {
                $(this).closest('tr').remove();
        });
}

var start_saving = function () {
        enableControl(0);
}

var print_cookie = function () {
        var a = '';
        a = document.cookie.split(/; */);
        $(a).each(function (k, v) {
                print(v);
        });
}

var clear_cookie = function () {
        var a = '';
        a = document.cookie.split(/; */);
        $(a).each(function (k, v) {
                var b = v.split('=');
                if (b[0].substr(0, fc.length + 1) == fc + '-')
                        $.removeCookie(b[0], {
                                path: '/'
                        });
        });
        var cookies = $.cookie();
        for (var cookie in cookies) {
                if (cookie.substr(0, fc.length + 1) == fc + '-')
                        $.removeCookie(cookie);
        }
}

var refresh = function (refresh_now) {

        if (refresh_now) {
                var is_search = $('.search-box');

                if (is_search.length > 0) {
                        $(is_search).find('button.search').trigger('click');
                } else {
                        $('button[id=btn_cancel]').trigger('click');
                }
        }
}

var reload = function () {
        if ($(cc).find('button[id=btn_cancel]').length > 0) {
                $(cc).find('button[id=btn_cancel]').trigger('click');
        } else {
                location.reload();
        }
}

var refresh_delete_select2 = function (id) {
        $(cc).find('select.is_parent').each(function (k, v) {
                var a = $(this).find('option[value=' + id + ']');
                if ($(a).length > 0)
                        $(a).remove();
                a = null;
        });
}

var scrollTo = function (el, offeset) {
        try {
                var pos = (el && el.size() > 0) ? el.offset().top : 0;
                $('html,body').animate({
                        scrollTop: pos + (offeset ? offeset : 0)
                }, 'slow');
        } catch (error) {
        }
}

var get_level = function (sb, val) {
        var table_name = $(sb).attr('table_name');
        var name = $(sb).attr('name');
        $.ajax({
                type: "POST",
                url: get_base_url() + 'mct/get_depth_level',
                data: 'table_name=' + table_name + '&val=' + val + '&parent_column=' +
                        name,
                cache: true,
                success: function (data) {
                        var _level = data;
                        $(cc).find('input[type=hidden][name=level]').remove();
                        var lv = '<input type="hidden" name="level" id="level" value="' +
                                _level + '">';
                        $(sb).after(lv);
                }
        });
}

var can_be_parent = function (sb, val) {
        if (val.length > 0) {
                var table_name = $(sb).attr('table_name');
                var name = $(sb).attr('name');
                var page = window.location.href;
                var id = $(sb).attr('name');
                $
                        .ajax({
                                type: "POST",
                                url: get_base_url() + 'mct/can_be_parent',
                                data: 'table_name=' + table_name + '&val=' + val +
                                        '&parent_column=' + name + '&page=' + page,
                                cache: true,
                                success: function (data) {
                                        if (data == 0) {
                                                $('button[id^=btn_save]').removeClass('hide');
                                                $('em[id=can_be_parent_' + id + ']').remove();
                                        } else {
                                                $('button[id^=btn_save]').addClass('hide');
                                                var h = '<em id="can_be_parent_' +
                                                        id +
                                                        '" class=invalid>Can not be parent due to existing references</em>';
                                                $(sb).before(h);
                                        }
                                }
                        });
        }
}

$.fn.digits = function () {
        return this.each(function () {
                var val = $(this).val();
                val = val.replace(',', '');
                $(this).val(val.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        })
}


var replaceAt = function (s, n, t) {
        return s.substring(0, n) + t + s.substring(n + 1);
}

var handleElapsedTime = function (start) {
        var deltaMsecs = $.now() - start;
        var str = 'Page rendered in ';
        str += '<span id="elapsed">';
        str += '<strong>' + $.timeelapse(deltaMsecs) + '</strong>';
        str += '</span>';
        $('p[id=render_time]').html(str);
}

var pass_validation = function (container) {
        var fa = 0;
        container = which_container(container);

        $(container).find('span.invalid').remove();
        $(container).find('input[type=hidden][name^=sys_validation]').each(function (k, v) {
                var name = $(v).attr('name').replace('sys_validation[', '');
                name = name.replace(']', '');

                if ($(container).find('input[type=hidden][name=' + name + ']').length > 0) {
                        var val = attr($(container).find('input[type=hidden][name=' + name + ']').val(), '');


                        if (val.length == 0)
                                val = '';
                } else if ($(container).find('input[type=text][name=' + name + ']').length > 0) {
                        var val = attr($(container).find('input[type=text][name=' + name + ']').val(), '');
                        if (val.length == 0) {
                                val = '';
                        }
                } else if ($(container).find('textarea[name=' + name + ']').length > 0) {
                        var val = attr($(container).find('textarea[name=' + name + ']').val(), '');
                        if (val.length == 0) {
                                val = '';
                        }
                } else {
                        val = $(this).val();
                }


                if (val.length == 0) {

                        var error_text = 'The ' + name +
                                ' field is required.';

                        var err = $('<span id=error_text_' + name +
                                ' class=invalid>' + error_text +
                                '</span>');
                        if ($(v).closest('.form-group').length > 0) {
                                if ($(v).closest('.form-group').find('label').length > 0) {
                                        if ($(container).find('#error_text_' + name).length == 0) {
                                                if ($('label[for=' + name + ']').length > 0) {
                                                        $('label[for=' + name + ']').after(
                                                                err);
                                                } else {
                                                        $(v).closest('.form-group').find(
                                                                'label:first').after(err);
                                                }
                                        }
                                } else {
                                        if ($(container).find('#error_text_' + name).length == 0)
                                                $(v).closest('.form-group')
                                                        .prepend(err);
                                }
                        } else {
                                if ($(container).find('#error_text_' + name).length == 0)
                                        $(v).after(err);
                        }
                        fa++;
                }
        });

        if (fa > 0) {
                return false;
        } else {
                return true;
        }
}

var show_footer = function (show) {

        if (show) {
                $('#footer-container').find('button, .dropup').removeClass('hide')
                        .fadeIn();
        } else {
                $('#footer-container').find('button,.dropup').addClass('hide').hide();
        }
}

var open_excel_window = function () {
        start_loading();
        //        var b = $('.output-area').html();
        var b = '';
        $('.tab-pane.active').find('.output-area').each(function (k, v) {
                b += $(this).html()
        });
        $(b).find('table').attr('border', 1);
        if (typeof (b) !== 'undefined') {
                var php_path = get_base_url() + '/' + excel_url;
                var pathname = window.location.href;
                var file_name = pathname.split('/');
                file_name = file_name[file_name.length - 1];
                var form = "<form name='csvexportform' action='" + php_path +
                        "' method='post' accept-charset='ISO-8859-1'>";
                form = form + "<input type='hidden' name='csvBuffer' value='" + b +
                        "'>";
                form = form + "<input type='hidden' name='file_name' value='" +
                        file_name + "'>";
                form = form +
                        "</form><script>document.csvexportform.submit();</script>";
                OpenWindow = window.open('', '');
                OpenWindow.document.write(form);
                //                setTimeout(function () {
                //                        OpenWindow.close();
                //                }, 30000);
                form, file_name, pathname, php_path = null;
        } else {
                show_error('Nothing to be printed.');
        }
        b = null;
        end_loading();
        return false;
}

var getValidExtension = function(name) {
        var a = $("body").find(
            "input[type=hidden][name=valid_extension_" + name + "]"
        );

        var b = [];
        if ($(a).length == 0) {
            a = $("#" + name)
                .attr("valid_extension")
                .split(",");
        } else {
            a = $(a)
                .val()
                .split(","); //getValidExtension(name);
        }
        $.each(a, function(k, v) {
            b.push(v.trim());
        });
        return b;
    };

var CheckExtension = function (file, name) {
        var filePath = file.name;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var validExtensions = getValidExtension(name);
        if ($.inArray(ext, validExtensions) == -1) {
                return '';
        } else {
                return ext;
        }

        validExtensions, filePath, ext = null;
}

var fileSelected = function (name) {
        name = $.trim(name);

        var oFile = document.getElementById(name).files[0];
        if (typeof (oFile) !== 'undefined') {
                var _cc = $('#' + name).closest('.row');
                var a = $(_cc).find('input[type=hidden][name=' + name + ']').remove();
                var ext = CheckExtension(oFile, name);
                $(_cc).find('em[id=error_extension_' + name + ']').remove();

                if (ext.length == 0) {
                        var invalid_extension = '<em id="error_extension_' +
                                name +
                                '" class="invalid invalid-file-extension validation-required" for="' +
                                name +
                                '">Invalid file extension (' +
                                oFile.name.substring(oFile.name.lastIndexOf('.') + 1)
                                .toLowerCase() + ')</em>';

                        if ($(_cc).find('label[for=' + name + ']').length > 0) {
                                $(_cc).find('label[for=' + name + ']').after(invalid_extension);
                        } else {
                                $(_cc).find('input[name=' + name + ']').closest('.form-group')
                                        .append(invalid_extension);
                        }
                } else {
                        $(_cc).find('em.invalid-file-extension').remove();
                }

                if (oFile.size > iMaxFilesize) {
                        var invalid_extension = '<em id="error_extension_' +
                                name +
                                '" class="invalid invalid-file-size validation-required" for="' +
                                name + '">File is too large (' + bytesToSize(oFile.size) +
                                ' > ' + bytesToSize(iMaxFilesize) + ')</em>';
                        if ($(_cc).find('label[for=' + name + ']').length > 0) {
                                $(_cc).find('label[for=' + name + ']').after(invalid_extension);
                        } else {
                                $(_cc).find('input[name=' + name + ']').closest('.form-group')
                                        .append(invalid_extension);
                        }
                } else {
                        $(_cc).find('em.invalid-file-size').remove();
                }

                //                // get preview element
                //                var oImage = document.getElementById(name + '_preview');
                //                if (oImage !== null) {
                //                        // prepare HTML5 FileReader
                //                        var oReader = new FileReader();
                //                        oReader.onload = function (e) {
                //                                // e.target.result contains the DataURL which we will use as a source of the image
                //                                oImage.src = e.target.result;
                //                                oImage.onload = function () { // binding onload event
                //                                        // we are going to display some custom image information here
                //                                        sResultFileSize = bytesToSize(oFile.size);
                //                                        document.getElementById(name + '_fileinfo').style.display = 'block';
                //                                        document.getElementById(name + '_filename').innerHTML = 'Name: ' + oFile.name;
                //                                        document.getElementById(name + '_filesize').innerHTML = 'Size: ' + sResultFileSize;
                //                                        document.getElementById(name + '_filetype').innerHTML = 'Type: ' + oFile.type;
                //                                        document.getElementById(name + '_filedim').innerHTML = 'Dimension: ' + oImage.naturalWidth + ' x ' + oImage.naturalHeight;
                //                                };
                //                        };
                //                        // read selected file as DataURL
                //                        oReader.readAsDataURL(oFile);
                //                }
//                if ($('body').find('em.invalid').length == 0) {
//                        $(
//                                'button[id=btn_save],button[id=btn_save_refresh],button.btn_upload')
//                                .removeClass('hide');
//                } else {
//                        $(
//                                'button[id=btn_save],button[id=btn_save_refresh],button.btn_upload')
//                                .addClass('hide');
//                        return;
//                }
                var t = name.split('_');
                t = t[t.length - 1];
                var t1 = t.substr(0, t.length - 1);
                var t2 = t.substr(t.length - 1);
                var doco = name.replace('document_file', 'document_original_file');
                $(_cc).find('input[type=hidden][name=' + doco + ']').remove();
                if ($.isNumeric(t1) && t2 == 'a') {
                        var h = '<input type=hidden class="document_original_file" id="' +
                                doco + '" name="' + doco + '" value="' + oFile['name'] +
                                '">';
                } else {
                        var h = '<input type=hidden name="' + doco + '" id="' + doco +
                                '" value="' + oFile['name'] + '">';
                }

                $(_cc).find('.btn-file').append(h);
                $(_cc).find('input[type=hidden][name=table_name]').remove();
                var table_name = attr($('input[type=file][name=' + name + ']').attr(
                        'table_name'), '');
                if (table_name.length > 0) {
                        h = '<input type=hidden name=table_name id=table_name value=' +
                                table_name + '>';
                        $(_cc).find('.btn-file').append(h);
                }
                h, t1, t2, t, doco, a, ext = null;
        }
}

var get_current_datetime = function () {
        var now = new Date();
        var months = new Array('01', '02', '03', '04', '05', '06', '07', '08',
                '09', '10', '11', '12');
        var date = ((now.getDate() < 10) ? "0" : "") + now.getDate();
        var year = now.getFullYear() + 543;
        var curr_hour = now.getHours();
        var curr_min = now.getMinutes();
        var curr_second = now.getSeconds();
        var curr_millisecond = now.getMilliseconds();
        var today = year + months[now.getMonth()] + date + curr_hour + curr_min +
                curr_second + curr_millisecond;
        return today;
}

var is_admin = function () {
        var v = 0;
        var n = 0;
        $(cc).find('input[type=hidden][name^=auth_data]').each(function () {
                v += parseInt($(this).val(), 10);
                n++;
        });

        if (v == n && v > 0) {
                return true;
        } else {
                var master = attr($(cc).find(
                        'input[type=hidden][name=auth_data\\[master\\]]').val(), 0);
                if (master == 1) {
                        return true;
                } else {
                        return false;
                }
        }
}

var set_cookie = function (name, value, expires) {
        $.cookie(name, value, {
                expires: 7,
                path: '/'
        });
}

var get_cookie = function (name) {
        return $.cookie(name);
}

var switchClass = function (target, classOne, classTwo, delay) {
        var delay = !isNaN(delay) ? delay : 0;
        setTimeout(function () {
                if (target.hasClass(classOne)) {
                        target.removeClass(classOne).addClass(classTwo);
                } else if (target.hasClass(classTwo)) {
                        target.removeClass(classTwo).addClass(classOne);
                }
        }, delay)
}

var is_fullscreen = function () {
        if (window.innerWidth == screen.width) {
                return true;
        } else {
                return false;
        }
}
var show_error = function (t) {
        showSystemMessage(2, t);
}

var showSystemMessage = function (mid, t) {
        switch (mid) {
                case 1: //success;
                        toastr["success"](t, 'System Message');
                        break;
                case 2: //warning;
                        toastr.error(t, 'Alerts!');
                        break;
        }
}

var start_loading = function () {
        var target = document.getElementById('tab-1')

        enableControl(0);
}

var getDatatableHeight = function () {
        var a = $(window).height() - $('header').height() - $('footer').height() -
                $('.actionbar').height() - 200;
        return a;
}
var which_container = function (area) {
        if (typeof area == 'undefined') {
                container = $('body');
                cc = container;
        } else {
                container = area;
        }

        return container;
}

var attr = function (a, default_value) {
        if (a == null) {
                if ($.type(default_value) !== 'undefined') {
                        return default_value;
                } else {
                        return '';
                }
        }

        if ($.type(a) !== 'undefined') {
                if ($.type(default_value) !== 'undefined') {
                        if (a == 'true') {
                                return true;
                        } else if (a == 'false') {
                                return false;
                        } else {
                                if (a.length > 0) {
                                        return a;
                                } else {
                                        return default_value;
                                }
                        }
                } else {
                        if (a == 'true') {
                                return true;
                        } else if (a == 'false') {
                                return false;
                        } else {
                                return a;
                        }
                }
        } else {
                if ($.type(default_value) !== 'undefined') {
                        return default_value;
                } else {
                        return '';
                }
        }
}

function loadURL(url, container) {

        if (url == 'undefined') {
                url = 'pages/home';
        } else {

                var url0 = url;
                set_cookie('uri', url);
                url = $('input[type=hidden][name=base_url]').val() + url;

                var data = 'uri=' + url0;

                $.ajax({
                        type: "POST",
                        data: data,
                        url: url,
                        dataType: 'html',
                        cache: true, // (warning: setting it to false will cause a timestamp and will call the request twice)
                        beforeSend: function () {

                                container.html('<h1 class="ajax-loading-animation"><i class="fa fa-cog fa-spin"></i>' + locale['loading'] + '</h1>');
                        },
                        success: function (data) {

                                // dump data to container
                                container.css({
                                        opacity: '0.0'
                                }).html(data).delay(50).animate({
                                        opacity: '1.0'
                                }, 300);

                                handleControls();
                                detect_mode();
                                // clear data var
                                data = null;
                                container = null;
                        },
                        error: function (xhr, status, thrownError, error) {
                                container.html('<h4 class="ajax-loading-error"><i class="fa fa-warning txt-color-orangeDark"></i> Error requesting <span class="txt-color-red">' + url + '</span>: ' + xhr.status + ' <span style="text-transform: capitalize;">' + thrownError + '</span></h4>');
                        },
                        async: true
                });
        }
}

var detect_mode = function () {
        var u = window.location.href;
        u = u.split('#')[1];
        var uu = u.split('/');
        var mode;
        if (uu.length == 1) {
                if ($('button.search').length > 0) {
                        mode = 'search';
                } else {
                        mode = 'list';
                }
        } else {
                mode = uu[1];
        }

        enable_mode(mode);
}

var enable_mode = function (mode) {
        switch (mode) {
                case 'search':
                        $('button[id=btn_new]').addClass('hide');
                        $('button[id=btn_save]').addClass('hide');
                        $('button[id=btn_save_refresh]').addClass('hide');
                        $('button[id=btn_delete]').addClass('hide');
                        $('button[id=btn_cancel]').addClass('hide');
                        break;
                case 'edit':
                        $('button[id=btn_new]').addClass('hide');
                        $('button[id=btn_save]').removeClass('hide');
                        $('button[id=btn_save_refresh]').removeClass('hide');
                        $('button[id=btn_delete]').removeClass('hide');
                        $('button[id=btn_cancel]').removeClass('hide');
                        break;
                case 'add':
                        $('button[id=btn_new]').addClass('hide');
                        $('button[id=btn_save]').removeClass('hide');
                        $('button[id=btn_save_refresh]').removeClass('hide');
                        $('button[id=btn_delete]').addClass('hide');
                        $('button[id=btn_cancel]').removeClass('hide');
                        break;
                case 'list':
                        $('button[id=btn_new]').removeClass('hide');
                        $('button[id=btn_save]').addClass('hide');
                        $('button[id=btn_save_refresh]').addClass('hide');
                        $('button[id=btn_delete]').addClass('hide');
                        $('button[id=btn_cancel]').addClass('hide');
                        break;
        }
}
//var loadURL = function (u) {
//
//      if (typeof (u) !== 'undefined' && u !== '#' && u.length > 0 && u !== current_hash) {
//
//            current_hash = u;
//            if (u.indexOf('edit') < 0 && u.indexOf('add') < 0
//                    && u.indexOf('undelete') < 0) {//view list
//
//                  get_ajax(get_base_url() + u, '');
//                  $('#ajax_content').html(ajax_data);
//
//                  window.location.hash = u;
////                        $('.side-menu').find('li').removeClass('subdrop');
////                        $('.side-menu').find('a[href=#' + u.split('/')[0] + ']').closest('li').find('a').addClass('sudrop');
////                        $('.side-menu').find('li.active').closest('a').addClass('subdrop');
////                        alert('k');
//                  if ($('.panel:first').find('.datatable').length == 0) {
//                        $('button[id=btn_new]').addClass('hide');
//                  } else {
//                        $('button[id=btn_new]').removeClass('hide');
//                  }
//                  $('button[id=btn_save], button[id=btn_save_refresh], button[id=btn_delete],button[id=btn_undelete], button[id=btn_cancel]').addClass('hide');
//            } else {//add, edit
//
//                  u = u.split('/');
//                  var id = u[u.length - 1];
//                  var uri = u[1];
//
//                  u = u[0] + '/' + u[1];
//                  get_ajax(get_base_url() + u + '/' + id, '');
//                  $('#ajax_content').html(ajax_data);
//                  window.location.hash = u + '/' + id;
//                  $('button[id=btn_new]').addClass('hide');
//                  switch (uri) {
//                        case 'add':
//                              $(
//                                      'button[id=btn_save], button[id=btn_save_refresh], button[id=btn_cancel]')
//                                      .removeClass('hide');
//                              $('button[id=btn_delete], button[id=btn_undelete]')
//                                      .addClass('hide');
//                              break;
//                        case 'edit':
//                              $(
//                                      'button[id=btn_save], button[id=btn_save_refresh], button[id=btn_cancel], button[id=btn_delete]')
//                                      .removeClass('hide');
//                              $('button[id=btn_undelete]').addClass('hide');
//                              break;
//                        case 'undelete':
//                              $(
//                                      'button[id=btn_save], button[id=btn_save_refresh], button[id=btn_delete]')
//                                      .add('hide');
//                              $('button[id=btn_undelete],button[id=btn_cancel]')
//                                      .removeClass('hide');
//                              break;
//                  }
//
//            }
//            $('#ajax_content').after($('input[type=hidden][name=mode]'));
//            $('input[type=hidden][name=mode]:not(:first)').remove();
//
//
//            handleControls();
//      }
//}

var fnResetAllFilters = function (o) {
        var oSettings = o.fnSettings();
        for (var iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
                oSettings.aoPreSearchCols[iCol].sSearch = '';
        }
        o.fnStandingRedraw();
        //                o.fnDraw();
}

var handleDataTablesLocal = function (container) {
        container = which_container(container);

        /* // DOM Position key index //
         
         l - Length changing (dropdown)
         f - Filtering input (search)
         t - The Table! (datatable)
         i - Information (records)
         p - Pagination (paging)
         r - pRocessing
         < and > - div elements
         <"#id" and > - div with an id
         <"class" and > - div with a class
         <"#id.class" and > - div with an id and class
         
         Also see: http://legacy.datatables.net/usage/features
         */

        var colvis = locale['colvis'];
        var zero_record = locale['zero_record'];
        var page_info = locale['page_info'];
        var info_filter = locale['info_filter'];
        var sFirst = locale['sFirst'];
        var sLast = locale['sLast'];
        var sNext = locale['sNext'];
        var sPrevious = locale['sPrevious'];
        var loading_list_data = locale['loading_list_data'];


        //        var h = def_tab_height; //handleTabHeight();
        //print($(area).find('.datatable'));
        var h = $(window).height() - $('#search-content').height() - 300;
        //        print(h);

        $(container).find('.datatable_local').each(function (k, v) {
                var table_id = $(this).attr('id');
                //                print(table_id);
                var table_name = attr($(this).attr('table_name'), 'Export Data');

                var view_name = $(this).attr('view_name');
                var where_value = attr($(this).attr('whereData'), '');
                var indexColumn = $(this).attr('indexColumn');
                var column_name = $(this).attr('column_name');
                var colspan = attr($(this).attr('colspan'), 0);
                var is_deletable = parseInt($('input[type=hidden][name=auth_data\\[delete\\]]').val(), 10);
                var is_edittable = parseInt($('input[type=hidden][name=auth_data\\[edit\\]]').val(), 10);
                var is_searchable = parseInt($('input[type=hidden][name=auth_data\\[search\\]]').val(), 10);
                var is_master = attr($(cc).find('input[type=hidden][name=auth_data\\[master\\]]').val(), 0);
                var is_printable = parseInt($('input[type=hidden][name=auth_data\\[print\\]]').val(), 10);

                if (is_master == 0 && where_value.length > 0) {
                        var wheres = where_value.split('&');
                        var owheres = [];
                        var e;
                        $.each(wheres, function (k, v) {
                                e = v.split('=');
                                if (e[1].indexOf(']') > 0) {
                                        var a = e[1].replace('[', '');
                                        a = a.replace(']', '');
                                        a = $(cc).find('input[type=hidden][name=' + a + ']').val();
                                        e[1] = a;
                                }
                                var b = e[0] + '=' + e[1];
                                owheres.push(b);
                        });
                        owheres = owheres.join(' and ');
                        where_value = owheres;
                } else {
                        where_value = '';
                }
                var url = '';
                //                url = 'mct/get_data?table_name=' + table_name + '&view_name=' + view_name + '&indexColumn=' + indexColumn + '&column_name=' + column_name + '&where_value=' + where_value
                oTable = $(this).dataTable({
                        "sPaginationType": "full_numbers",
                        "aLengthMenu": [
                                [10, 25, 50, 100, -1],
                                ['10 ' + locale['rows'], '25 ' + locale['rows'], '50 ' + locale['rows'], '100 ' + locale['rows'], locale['show_all_records']]
                        ],
                        "scrollX": true,
                        //"scrollY": h + "px",
                        "scrollY": false,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bSort": true,
                        "bProcessing": true,
                        "bServerSide": false,
                        "bDeferRender": true,
                        "bStateSave": true,
                        "bAutoWidth": false,
                        "bSortCellsTop": true,
                        // "pageLength":-1,

                        "oColVis": {
                                "buttonText": locale['colvis'], //Show / hide columns
                        },
                        "language": {
                                "zeroRecords": zero_record,
                                "info": page_info,
                                "infoEmpty": zero_record,
                                "infoFiltered": info_filter,
                                "sProcessing": "<div class='loading-datatable'><i class='fa fa-gear fa-spin'></i> " + loading_list_data + "</div>",
                                "oPaginate": {
                                        "sFirst": sFirst,
                                        "sLast": sLast,
                                        "sNext": sNext,
                                        "sPrevious": sPrevious
                                },
                                "sSearch": '<div class="input-group"><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>',
                        },
                        //                                "sDom": "<'dt-toolbar'<lrC>>" + "t" + "<'dt-toolbar-footer'<i><pS>>",
                        //                        "dom": "<'dt-toolbar'<'row'<'col-sm-12 col-xs-12 col-md-12 align-left'CB>>>" + "tr" + "<'dt-toolbar-footer'<'col-sm-12 col-xs-12 col-md-4 hidden-xs hidden-sm'i><'col-sm-12 col-xs-12 col-md-8'pS>>",
//                        "sDom": "Bfrt<'row'<'col-md-6'i><'col-md-6'p>>",
                        "sDom": "<'row'<'col-md-9'B><'col-md-3 pull-right'f>>rt<'row'<'col-md-6'i><'col-md-6'p>>",
                        //                       dom: 'T<"clear">lfrtip',
                        //                        "sAjaxSource": url,
                        buttons: [
                                //                                {
                                //                                        extend: 'print',
                                //                                        text: '<i class="fa fa-print"></i>',
                                //                                        text: '<i class="fa fa-file-excel-o"></i><span class="hidden-xs"> ' + locale['selected_print'] + '</span>',
                                //                                        autoPrint: false,
                                //
                                //                                        exportOptions: {
                                //                                                modifier: {
                                //                                                        selected: true
                                //                                                }
                                //                                        },
                                //                                        enabled: is_printable
                                //                                },
                                {
                                        extend: 'excelHtml5',
                                        title: table_name,
                                        text: '<i class="fa fa-file-excel-o"></i><span class="hidden-xs"> ' + locale['excel'] + '</span>',
                                        titleAttr: 'Excel',
                                        filename: table_name,
                                        customize: function (xlsx) {
                                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                                // jQuery selector to add a border
                                                $('row c[r]', sheet).attr('s', '25');
                                                $('row:first c', sheet).attr('s', '32');
                                        }
                                }, 'pageLength', {
                                        extend: 'colvis',
                                        collectionLayout: 'four-column',
                                        postfixButtons: ['colvisRestore']
                                }, 'selectAll',
                                'selectNone'
                        ],
                        select: false
                });
                //                        .fnFakeRowspan(colspan);
                //                fnResetAllFilters(oTable);

                $('a.buttons-select-all').on('click', function (e) {
                        //                        e.preventDefault();
                        selected = [-1];
                });

                $('a.buttons-select-none').on('click', function (e) {
                        //                        e.preventDefault();
                        selected = [];
                })

                $(container).find(".dataTables_scrollHead input").keyup(function (e) {
                        if (e.keyCode == 13) {
                                oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
                                return false;
                        }
                });


                $("table[id=" + table_id + "] tbody").on('click', 'a.row_action_edit', function (e) {
                        e.preventDefault();


                        start_loading();
                        var id = $(this).attr('id');
                        if ($(cc).find('button[id=btn_list_delete_' + table_id + ']').length == 0) {
                                _mode = 'edit';
                        } else {
                                _mode = $(cc).find('button[id=btn_list_delete_' + table_id + ']').attr('mode');
                        }

                        if (_mode == 'edit') {
                                window.location = window.location.hash + '/edit/' + id;
                        } else {
                                window.location = window.location.hash + '/undelete/' + id;
                        }

                        end_loading();
                });
                //                print($(area).find("table[id=" + table_id + "] tbody"));
                $(container).find("table[id=" + table_id + "] tbody").on('dblclick', 'tr', function () {
                        start_loading();
                        $(this).find('a.row_action').trigger('click');
                        end_loading();
                });
                $(container).find("table[id=" + table_id + "] tbody").on('click', 'tr', function () {
                        var id = this.id;

                        var index = $.inArray(id, selected);
                        if (selected[0] == -1)
                                selected = [];
                        $(cc).find('.row_action_edit').removeClass('btn-primary').addClass('btn-default');
                        if (index === -1) {
                                selected.push(id);
                        } else {
                                selected.splice(index, 1);
                        }
                        $(this).toggleClass('dt_selected');

                        if ($(this).closest('tr').find('input[type=checkbox][class^=delete]').prop('checked') == true) {
                                $(this).closest('tr').find('input[type=checkbox][class^=delete]').prop('checked', false);
                        } else {
                                $(this).closest('tr').find('input[type=checkbox][class^=delete]').prop('checked', true);
                        }
                        $(this).find('.row_action_edit').removeClass('btn-default').addClass('btn-primary');
                });
                $(container).find('#' + table_id + '_wrapper .dt-toolbar').find('button[id=btn_excel_' + table_id + ']').on('click', function (e) {
                        e.preventDefault();
                        php_path = excel_url;
                        var is_close = true;
                        var file_name = $.cookie('current_url');
                        $(this).attr('disabled', 'disabled');
                        var headers = [];
                        var row = $(container).find('.datatable').find('tr')[0];
                        var col = $(row).find('th');
                        $.each(col, function () {
                                headers.push($(this).html());
                        });
                        c = headers.join(',');
                        if ($('.output-area').length == 0 && typeof (oTable) !== 'undefined') {
                                //                        var name = $(this).attr('id');
                                var orientation = attr($(container).find('input[type=hidden][name=orientation]').val(), '');
                                if (orientation.length == 0)
                                        orientation = 'P';
                                var file_name = $.cookie('current_url');
                                var column_name = $(oTable).attr('column_name'); //$(cc).find('input[type=hidden][name=column_name]').val();
                                var view_name = $(oTable).attr('view_name'); //$(cc).find('input[type=hidden][name=view_name]').val();
                                var table_name = $(oTable).attr('table_name'); //$(cc).find('input[type=hidden][name=table_name]').val();
                                var indexColumn = $(oTable).attr('indexColumn'); //$(cc).find('input[type=hidden][name=indexColumn]').val();
                                var url = 'mct/get_data?table_name=' + table_name + '&view_name=' + view_name + '&indexColumn=' + indexColumn + '&column_name=' + column_name; //    + '&where_value=' + where_value;
                                if (column_name.indexOf(',') > 0) {
                                        $.ajax({
                                                type: "POST",
                                                url: url,
                                                cache: false,
                                                async: false,
                                                success: function (html) {
                                                        var data = $.parseJSON(html);
                                                        data = data['aaData'];
                                                        if (is_close)
                                                                var a = '<table border=1 class="table-csv">';
                                                        else
                                                                a = '';
                                                        var c = column_name.split(',');
                                                        var cc = [];
                                                        $.each(c, function (k, v) {
                                                                v = $.trim(v);
                                                                if (v == indexColumn) {
                                                                        cc.push(v);
                                                                }
                                                                if (v.substr(v.length - 3) !== '_id') {
                                                                        cc.push(v);
                                                                }
                                                        });
                                                        cc = cc.join(',');
                                                        $.each(data, function (k, v) {
                                                                a += '<tr>';
                                                                $.each(v, function (kk, vv) {
                                                                        a += '<td>' + vv + '</td>';
                                                                });
                                                                a += '</tr>';
                                                        });
                                                        if (is_close)
                                                                a += '</table>';
                                                        b = a;
                                                        var form = "<form name='csvexportform_all' action='" + php_path + "' method='post' accept-charset='ISO-8859-1'>";
                                                        form = form + "<input type='hidden' name='csvBuffer' value='" + b + "'>";
                                                        form = form + "<input type='hidden' name='headers' value='" + cc + "'>";
                                                        form = form + "<input type='hidden' name='file_name' value='" + file_name + "'>";
                                                        form = form + "<input type='hidden' name='orientation' value='" + orientation + "'>";
                                                        form = form + "</form><script>document.csvexportform_all.submit();</script>";
                                                        OpenWindow_all = window.open('', '');
                                                        OpenWindow_all.document.write(form);
                                                        if (is_close) {
                                                                setTimeout(function () {
                                                                        OpenWindow_all.close();
                                                                }, 5000);
                                                        }
                                                }
                                        });
                                }
                        } else {
                                var b = $('.output-area').clone();
                                $(b).find('.report-header, .hr-totop, .rt-menu').remove();
                                var c = $(b).find('input,textarea');
                                $.each(c, function (k, v) {
                                        if ($(this).attr('type') == 'checkbox') {
                                                if ($(this).hasClass('is_checked')) {
                                                        var val = 'T';
                                                } else {
                                                        var val = '';
                                                }
                                        } else {
                                                var val = $(this).val();
                                        }
                                        var p = $(this).closest('td');
                                        $(p).html(val);
                                })

                                $(b).find('tr.second').remove();
                                b = $(b).html();

                                var form = "<form name='csvexportform_all' action='" + php_path + "' method='post' accept-charset='ISO-8859-1'>";
                                form = form + "<input type='hidden' name='csvBuffer' value='" + b + "'>";
                                form = form + "<input type='hidden' name='file_name' value='" + file_name + "'>";
                                form = form + "<input type='hidden' name='orientation' value='" + orientation + "'>";
                                form = form + "</form><script>document.csvexportform_all.submit();</script>";
                                OpenWindow_all = window.open('', '');
                                OpenWindow_all.document.write(form);
                                if (is_close) {
                                        setTimeout(function () {
                                                OpenWindow_all.close();
                                        }, 5000);
                                }
                        }
                        $(this).removeAttr('disabled');
                });

                table_id, table_name, view_name, where_value, indexColumn, column_name, colspan, is_deletable, is_edittable, is_searchable, oSettings = null;
        });
        $('.dataTables_wrapper').find('button.ColVis_MasterButton').removeClass('ColVis_Button ColVis_MasterButton').addClass('btn btn-default hidden-xs'); //.addClass('form-control add-padding-left-4 hidden-xs hidden-sm');

        $('input.search_init').addClass('form-control');

        loading_list_data, zero_record, page_info, info_filter, colvis, sFirst, sLast, sNext, sPrevious, h = null;
}

var handleDataTables = function (container) {
        /* // DOM Position key index //
         
         l - Length changing (dropdown)
         f - Filtering input (search)
         t - The Table! (datatable)
         i - Information (records)
         p - Pagination (paging)
         r - pRocessing
         < and > - div elements
         <"#id" and > - div with an id
         <"class" and > - div with a class
         <"#id.class" and > - div with an id and class
         
         Also see: http://legacy.datatables.net/usage/features
         */

        container = which_container(container);
        var colvis = locale['colvis'];
        var zero_record = locale['zero_record'];
        var page_info = locale['page_info'];
        var info_filter = locale['info_filter'];
        var sFirst = locale['sFirst'];
        var sLast = locale['sLast'];
        var sNext = locale['sNext'];
        var sPrevious = locale['sPrevious'];
        var loading_list_data = locale['loading_list_data'];
        var search = locale['search'];
        var lengthMenu = locale['lengthMenu'];
        var h = getDatatableHeight();

        $(container)
                .find('.datatable').each(function (k, v) {
                var table_id = $(this).attr('id');
                var table_name = attr($(this).attr('table_name'), '');
                var view_name = attr($(this).attr('view_name'), '');
                var where_value = attr($(this).attr('whereData'), '');
                var indexColumn = attr($(this).attr('indexColumn'), '');
                var column_name = attr($(this).attr('column_name'), '');
                var colspan = attr($(this).attr('colspan'), 0);
                var orderBy = attr($(this).attr('orderBy'), '');
                var is_deletable = parseInt($('input[type=hidden][name=auth_data\\[delete\\]]').val(), 10) == 1 ? true : false;
                var is_edittable = parseInt($('input[type=hidden][name=auth_data\\[edit\\]]').val(), 10) == 1 ? true : false;
                var is_searchable = parseInt($('input[type=hidden][name=auth_data\\[search\\]]').val(), 10) == 1 ? true : false;
                var is_printable = parseInt($('input[type=hidden][name=auth_data\\[print\\]]').val(), 10) == 1 ? true : false;
                var is_master = attr($('input[type=hidden][name=auth_data\\[master\\]]').val(), 0) == 1 ? true : false;

                if (is_master == 0 && where_value.length > 0) {
                        var wheres = where_value.split('&');
                        var owheres = [];
                        var e;
                        $.each(wheres, function (k, v) {
                                e = v.split('=');
                                if (typeof (e[1]) != "undefined" && e[1] !== null) {
                                        if (e[1].indexOf(']') > 0) {
                                                var a = e[1].replace('[', '');
                                                a = a.replace(']', '');
                                                a = $(container).find('input[type=hidden][name=' + a + ']').val();
                                                e[1] = a;
                                        }
                                        var b = e[0] + '=' + e[1];
                                } else {
                                        var b = e;
                                }

                                owheres.push(b);
                        });
                        owheres = owheres.join(' and ');
                        where_value = owheres;
                } else {
                        where_value = '';
                }

                var url = '';
                url = get_base_url() + 'mct/get_data?table_name=' +
                        table_name + '&view_name=' + view_name +
                        '&indexColumn=' + indexColumn +
                        '&column_name=' + column_name +
                        '&where_value=' + where_value +
                        '&orderBy=' + orderBy;
                oTable = $(this)
                        .dataTable({
                                colReorder: true,
                                //               retrieve: true,
                                //               paging: false,
                                //                        rowReorder: true,                       
                                //                                                responsive: true,
                                select: true,
                                "destroy": true,
                                pageLength: 10,
                                "sPaginationType": "full_numbers",
                                lengthMenu: [
                                        [10, 25, 50, 100, -1],
                                        [
                                                '10 ' +
                                                        locale['rows'],
                                                '25 ' +
                                                        locale['rows'],
                                                '50 ' +
                                                        locale['rows'],
                                                '100 ' +
                                                        locale['rows'],
                                                locale['show_all_records']
                                        ]
                                ],
                                buttons: [{
                                                extend: 'print',
                                                text: '<i class="fa fa-print"></i>',
                                                titleAttr: locale['selected_print'],
                                                autoPrint: false,
                                                exportOptions: {
                                                        modifier: {
                                                                selected: true
                                                        }
                                                },
                                                enabled: is_printable
                                        },
                                        {
                                                extend: 'print',
                                                text: '<i class="fa fa-print"></i>',
                                                titleAttr: locale['print_all'],
                                                autoPrint: false,
                                                enabled: is_printable
                                        },
                                        {
                                                extend: 'excelHtml5',
                                                title: table_name,
                                                text: '<i class="fa fa-file-excel-o"></i>',
                                                titleAttr: 'Excel',
                                        },
                                        {
                                                extend: 'pdfHtml5',
                                                title: table_name,
                                                text: '<i class="fa fa-file-pdf-o"></i>',
                                                titleAttr: locale['pdf'],
                                                enabled: is_printable
                                        },
                                        'pageLength',
                                        {
                                                extend: 'colvis',
                                                collectionLayout: 'fixed two-column',
                                                postfixButtons: ['colvisRestore']
                                        },
                                        {
                                                text: '<i class="fa fa-refresh"></i>',
                                                action: function (e,
                                                        dt, node,
                                                        config) {
                                                        dt.ajax.reload();
                                                }
                                        },
                                        {
                                                text: '<i class="fa fa-star"></i>',
                                                enabled: is_admin(),
                                                action: function (e,
                                                        dt, node,
                                                        config) {
                                                        var _mode;
                                                        var mode = $(node)
                                                                .find(
                                                                        '.fa-star');
                                                        if (mode.length > 0) {
                                                                _mode = 'delete';
                                                                $(mode)
                                                                        .closest(
                                                                                'span')
                                                                        .empty()
                                                                        .append(
                                                                                '<i class="fa fa-sun-o"></i>');
                                                                url = get_base_url() +
                                                                        'mct/get_data_delete?table_name=' +
                                                                        table_name +
                                                                        '&view_name=' +
                                                                        view_name +
                                                                        '&indexColumn=' +
                                                                        indexColumn +
                                                                        '&column_name=' +
                                                                        column_name +
                                                                        '&where_value=' +
                                                                        where_value

                                                        } else {
                                                                mode = $(node)
                                                                        .find(
                                                                                '.fa-sun-o');
                                                                _mode = 'edit';
                                                                $(mode)
                                                                        .closest(
                                                                                'span')
                                                                        .empty()
                                                                        .append(
                                                                                '<i class="fa fa-star"></i>');
                                                                url = get_base_url() +
                                                                        'mct/get_data?table_name=' +
                                                                        table_name +
                                                                        '&view_name=' +
                                                                        view_name +
                                                                        '&indexColumn=' +
                                                                        indexColumn +
                                                                        '&column_name=' +
                                                                        column_name +
                                                                        '&where_value=' +
                                                                        where_value
                                                        }

                                                        $(
                                                                'input[name=mode]')
                                                                .val(_mode);
                                                        oTable
                                                                .fnReloadAjax(url);
                                                }
                                        }
                                ],
                                "scrollX": true,
                                "scrollY": h + "px",
                                lengthChange: false,
                                "bFilter": true,
                                "bSort": true,
                                "bProcessing": true,
                                "bServerSide": true,
                                "bDeferRender": true,
                                stateSave: false,
                                "bAutoWidth": false,
                                "bSortCellsTop": true,
                                "columnDefs": [{
                                                "width": "80px",
                                                "targets": 0,
                                                "fnCreatedCell": function (nTd,
                                                        sData, oData, iRow,
                                                        iCol) {
                                                        var id = oData[0];
                                                        if (id.length > 3) {
                                                                var a = ' <span class=row_number_center>' +
                                                                        id +
                                                                        '</span><br/>';
                                                        } else {
                                                                var a = ' <span class=row_number>' +
                                                                        id +
                                                                        '</span>';
                                                        }
                                                        $(nTd).empty();
                                                        if (is_edittable) {
                                                                var b = $('<a id=' +
                                                                        id +
                                                                        ' class="row_action row_action_edit btn btn-xs btn-default"><i class="fa fa-edit"></i></a>');
                                                                $(nTd).append(b);
                                                        }
                                                        $(nTd).append(a);
                                                }
                                        }],
                                "oColVis": {
                                        "buttonText": colvis, //Show / hide columns
                                },
                                "language": {
                                        "lengthMenu": lengthMenu,
                                        "search": '',
                                        "zeroRecords": zero_record,
                                        "info": page_info,
                                        "infoEmpty": zero_record,
                                        "infoFiltered": info_filter,
                                        "sSearch": '<div class="input-group"><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>',
                                        "sProcessing": "<div class='loading-datatable'><i class='fa fa-gear fa-spin'></i> " +
                                                loading_list_data +
                                                "</div>",
                                        "oPaginate": {
                                                "sFirst": sFirst,
                                                "sLast": sLast,
                                                "sNext": sNext,
                                                "sPrevious": sPrevious
                                        },
                                        buttons: {
                                                colvis: colvis
                                        }
                                },
                                //                        "sDom": "<B<t><'margin-left-20'i><'margin-right-20'p>>",
                                //                        "sDom": "<'dt-toolbar'<'row'<'col'B><'col pull-right'C>>>" + "tr" + "<'row'<'dt-toolbar-footer'<'col hidden-xs hidden-sm'i><'col'pS>>>",
                                //                        "sDom": "Bfrt<'row'i<'float-right'p>>",
//                                "sDom": "Bfrt<'row'<'col-md-6'i><'col-md-6'p>>",
                                "sDom": "<'row'<'col-md-9'B><'col-md-3 pull-right'f>>rt<'row'<'col-md-6'i><'col-md-6'p>>",
                                "sAjaxSource": url,
                                "fnDrawCallback": function (
                                        oSettings) {

                                },
                                "fnCreatedRow": function (nRow,
                                        aData, iDataIndex) {},
                                "fnInitComplete": function (
                                        oSettings, json) {

                                },
                        }); //.fnFakeRowspan(colspan);
                //                fnResetAllFilters(oTable);

                if (is_searchable) {
                        $(container)
                                .find(".dataTables_scrollHead input")
                                .keyup(
                                        function (e) {
                                                if (e.keyCode == 13) {
                                                        oTable
                                                                .fnFilter(
                                                                        this.value,
                                                                        oTable.oApi
                                                                        ._fnVisibleToColumnIndex(
                                                                                oTable
                                                                                .fnSettings(),
                                                                                $(
                                                                                        "thead input")
                                                                                .index(
                                                                                        this)));
                                                        return false;
                                                }
                                        });
                } else {
                        $(container).find('input.search_init').val('');
                        $(container).find('input.search_init').addClass(
                                'hide');
                }

                $("table[id=" + table_id + "] tbody").on(
                        'click',
                        'a.row_action_edit',
                        function (e) {
                                e.preventDefault();
                                start_loading();
                                var id = $(this).attr('id');
                                var mode = attr($(
                                        'input[type=hidden][name=mode]')
                                        .val(), 'edit');

                                //                        if ($(container).find('button[id=btn_list_delete_' + table_id + ']').length == 0) {
                                //                                _mode = 'edit';
                                //                        } else {
                                //                                _mode = $(container).find('button[id=btn_list_delete_' + table_id + ']').attr('mode');
                                //                        }

                                if (mode == 'edit') {
                                        window.location = window.location.href +
                                                '/edit/' + id;
                                } else if (mode == 'delete') {
                                        window.location = window.location.href +
                                                '/undelete/' + id;
                                }
                                setTimeout(function () {

                                        if (mode == 'edit') {
                                                //                                        $('button[id *=delete]:first').attr('id', 'btn_delete').attr('name', 'btn_delete').attr('title', 'Delete').attr('onClick', 'log_events(\'delete\')').removeClass('hide').empty().append('<i class="fa fa-trash-o"></i> Delete');
                                        } else if (mode == 'delete') {
                                                //                                        $('button[id *=delete]:first').attr('id', 'btn_undelete').attr('name', 'btn_undelete').attr('title', 'Un Delete').attr('onClick', 'log_events(\'undelete\')').removeClass('hide').empty().append('<i class="fa fa-trash-o"></i> Undelete');
                                        }
                                }, 1000)

                                //                        } else {
                                //                                window.location = window.location.href + '/undelete/' + id;
                                //                        }
                                end_loading();
                        });

                $(container)
                        .find("table[id=" + table_id + "] tbody")
                        .on(
                                'click',
                                'tr',
                                function () {
                                        var id = this.id;
                                        var index = $.inArray(id, selected);
                                        $(container).find(
                                                "table[id=" + table_id +
                                                "] tbody tr")
                                                .removeClass('dt_selected');
                                        $(container).find(
                                                '.row_action_edit')
                                                .removeClass('btn-invert')
                                                .addClass('btn-default');
                                        if (index === -1) {
                                                selected.push(id);
                                        } else {
                                                selected.splice(index, 1);
                                        }
                                        $(this).toggleClass('dt_selected');
                                        $(this).find('.row_action_edit')
                                                .removeClass('btn-default')
                                                .addClass('btn-invert');
                                });

                $("table[id=" + table_id + "] tbody").on('dblclick', 'tr', function () {
                        //                        start_loading();
                        var id = $(this).find('td:first').text().trim();

                        if (id.length > 0) {

                                window.location = window.location.hash + '/edit/' + id;
                                setTimeout(function () {
                                        if (_mode == 'delete') {
                                                $('button[id=btn_delete]').addClass('hide');
                                                $('button[id=btn_undelete]').removeClass('hide');
                                        } else {
                                                $('button[id=btn_delete]').removeClass('hide');
                                                $('button[id=btn_undelete]').addClass('hide');
                                        }
                                }, 1000);

                        }
                        //                        end_loading();
                });
                //                $(cc).find('#' + table_id + '_wrapper .dt-buttons').closest('div').after('<div class="pull-right colvis"></div>');
                $(container).find(
                        '#' + table_id + '_wrapper .dt-toolbar .row')
                        .find('div:nth-child(2)').append(
                        $('a.buttons-colvis')).addClass(
                        'hidden-xs');

                var oSettings = oTable.fnSettings();
                $(container)
                        .find("table[id=" + table_id + "]")
                        .find("thead input")
                        .each(
                                function (i) {
                                        if (oSettings.aoPreSearchCols[i]['sSearch'] != '') {
                                                $(this)
                                                        .val(
                                                                oSettings.aoPreSearchCols[i]['sSearch']);
                                        }
                                });
                table_id, table_name, view_name, where_value,
                        indexColumn, column_name, colspan,
                        is_deletable, is_edittable, is_searchable,
                        oSettings = null;
        });
        $('.dataTables_wrapper').find('button.ColVis_MasterButton').removeClass(
                'ColVis_Button ColVis_MasterButton').addClass(
                'btn btn-default hidden-xs'); //.addClass('form-control add-padding-left-4 hidden-xs hidden-sm');

        $('input.search_init').addClass('form-control').addClass('max-width')
                .closest('label.input').css('margin-bottom', '0').css('display',
                'block');
        //        $(cc).find('.dataTables_length').find('select').select2().addClass('hidden-xs');
        //        $(cc).find('.delete-undelete').addClass('hidden-xs');

        loading_list_data, zero_record, page_info, info_filter, colvis, sFirst,
                sLast, sNext, sPrevious, h = null;
}

var handle_ex = function () {
        var b = '';
        $('.panel-body').find('table').each(function (k, v) {

                $(this).find('input[type=hidden]').remove();
                $(this).find('input[type=radio]').remove();
                $(this).find('textarea').remove();
                $(this).find('input[type=text]').remove();
                $(this).find('img').remove();
                $(this).find('label').remove();
                $(this).find('select').remove();
                $(this).find('button').remove();

                b += '<table border=1><tbody>' + $(this).html() + '</tbody></table>';
                b += '<table><tr></tr></table>';
                //b += $(this).html();

        });

        var file_name = "Task_detail";
        var form = "<form name='csvexportform_all' action='" + get_base_url() + excel_url + "' method='post' accept-charset='ISO-8859-1'>";
        form = form + "<input type='hidden' name='csvBuffer' value='" + b + "'>";
        form = form + "<input type='hidden' name='file_name' value='" + file_name + "'>";
        form = form + "</form><script>document.csvexportform_all.submit();</script>";

        OpenWindow_all = window.open('', '');
        OpenWindow_all.document.write(form);

        setTimeout(function () {
                OpenWindow_all.close();
        }, 5000);
}

var handle_task_report = function () {
        var b = '';
        $('#fte').find('table').each(function (k, v) {

                $(this).find('input[type=hidden]').remove();
                $(this).find('input[type=radio]').remove();
                $(this).find('textarea').remove();
                $(this).find('input[type=text]').remove();
                $(this).find('img').remove();
                $(this).find('label').remove();
                $(this).find('select').remove();
                $(this).find('button').remove();

                b += '<table border=1><tbody>' + $(this).html() + '</tbody></table>';
                b += '<table><tr></tr></table>';
                //b += $(this).html();

        });

        var file_name = "Task_Report";
        var form = "<form name='csvexportform_all' action='" + get_base_url() + excel_url + "' method='post' accept-charset='ISO-8859-1'>";
        form = form + "<input type='hidden' name='csvBuffer' value='" + b + "'>";
        form = form + "<input type='hidden' name='file_name' value='" + file_name + "'>";
        form = form + "</form><script>document.csvexportform_all.submit();</script>";

        OpenWindow_all = window.open('', '');
        OpenWindow_all.document.write(form);

        setTimeout(function () {
                OpenWindow_all.close();
        }, 5000);
}


var handleTableSorter = function (container) {
        container = which_container(container);
        $(container).find('.sort-table').each(function (k, v) {
                // call the tablesorter plugin and apply the uitheme widget
                $(this).tablesorter({
                        // this will apply the bootstrap theme if "uitheme" widget is included
                        // the widgetOptions.uitheme is no longer required to be set

                        theme: "bootstrap",
                        widthFixed: true,
                        headerTemplate: '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

                        // widget code contained in the jquery.tablesorter.widgets.js file
                        // use the zebra stripe widget if you plan on hiding any rows (filter widget)
                        widgets: ["uitheme", "filter", "zebra"],
                        widgetOptions: {
                                // using the default zebra striping class name, so it actually isn't included in the theme variable above
                                // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
                                zebra: ["even", "odd"],
                                // reset filters button
                                filter_reset: ".reset",
                                // extra css class name (string or array) added to the filter element (input or select)
                                filter_cssFilter: "form-control",
                                // set the uitheme widget to use the bootstrap theme class names
                                // this is no longer required, if theme is set
                                // ,uitheme : "bootstrap"
                                filter_placeholder: {
                                        search: 'Search...'
                                },
                                filter_ignoreCase: true,
                                // class added to filtered rows (rows that are not showing); needed by pager plugin
                                filter_filteredRow: 'filtered',
                        }
                }).tablesorterPager({
                        // target the pager markup - see the HTML block below
                        container: $(this).find(".ts-pager"),
                        // target the pager page select dropdown - choose a page
                        cssGoto: ".pagenum",
                        // remove rows from the table to speed up the sort of large tables.
                        // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
                        removeRows: false,
                        // output string - default is '{page}/{totalPages}';
                        // possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
                        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})',
                        // if true, the table will remain the same height no matter how many records are displayed. The space is made up by an empty
                        // table row set to a height to compensate; default is false
                        fixedHeight: true,
                });

        })

        $(cc).find('.tablesorter-filter').on('click', function (e) {
                e.preventDefault();
                show_hint($(this));
        })
}

var handleTextBoxes = function (container) {
        handleSearchTextBox(container);
        handleTextBox(container);
}

var handleSearchBox = function () {
        $('.tab-pane').each(function (k, v) {
                var id = $(this).attr('id').replace('tab_', '');
                //                var column_width = attr($(this).find('input[type=hidden][name=column_width]').val(), 12);
                if ($(this).find('button.search').length > 0) {
                        $('.hr-totop:first')
                        //                        $(this).prepend('<div id="search-box-' + id + '" class=search-box></div>');
                        //                        $(this).find('.search-box').append($(this).find('.form-group'));
                        //                        $(this).find('.search-box').find('.form-group:not(.sub_header)').wrap('<div class=row><div class="col-md-' + column_width + '"></div></div>');
                        //                        $(this).find('.search-box').append($(this).find('button.btn'));
                        //                        $(this).find('button.btn').wrapAll('<div class=row><div class="col-md-12"></div></div>');
                        $(this).children().wrapAll('<div id="search-box-' + id + '" class=search-box></div>');
                        var a = $('.search-box').find('.output-area');
                        $('.search-box').after(a);
                }
        })

        if ($('button.search').length > 0) {
                $('#footer-container').find('button').addClass('hide');
        }
}

var handleSearchTextBox = function (container) {
        container = which_container(container);

        $(container).find('input[id=search_tree]').each(function (k, v) {
                $(v).on('keypress', function (e) {
                        if (e.keyCode == 13) {
                                var table_target = 'sidebar-menu';
                                filterText($(v).val(), table_target);
                                filterRow($(v).val(), table_target);
                                table_target = null;
                                return false;
                        }
                });
        });
}

var filterRow = function (search, row_target) {
        if (search.length == 0) {
                $('div[' + row_target + '], tr[' + row_target + ']')
                        .removeClass('hide');
        } else {
                var a = $('div[' + row_target + '], tr[' + row_target + ']');
                $(a).removeClass('hide');
                $.each(a, function (k, v) {
                        var s = $(v).attr(row_target);
                        if (s.indexOf(search) >= 0) {

                        } else {
                                $(v).addClass('hide');
                        }
                })
        }
}

var filterText = function (search, table_target) {

        var $allListElements = $('#' + table_target).find('ul > li');
        if (search.length == 0) {
                $("#" + table_target).removeClass('hide');
                $('#' + table_target + ' div.row').removeClass('hide');
                $('table[id=' + table_target + '] tbody tr').removeClass('hide');
                $allListElements.show().removeClass('menu-open');
        } else {

                var $matchingListElements = $allListElements.filter(function (i, li) {
                        var listItemText = $(li).text().toUpperCase(),
                                searchText = search
                                .toUpperCase();
                        return ~listItemText.indexOf(searchText);
                });
                $allListElements.hide();
                $matchingListElements.show();
                $matchingListElements.addClass('menu-open');
                $("#" + table_target + ' div[data-search]').addClass('hide');
                $("#" + table_target + ' div[data-search]').filter(function () {
                        return $(this).text().indexOf(search) >= 0;
                }).closest("div.row").each(function (k, v) {
                        $(v).removeClass('hide');
                });
                $("#" + table_target + ' div.row').addClass('hide');
                $('#' + table_target + ' div.row').filter(function () {
                        return $(this).text().indexOf(search) >= 0;
                }).closest("div.row").each(function (k, v) {
                        $(v).removeClass('hide');
                });
                $('table[id=' + table_target + '] tbody tr').addClass('hide');
                $('table[id=' + table_target + '] tbody tr').find("td").filter(
                        function () {
                                return $(this).text().indexOf(search) >= 0
                        }).closest("tr").each(function (k, v) {
                        $(v).removeClass('hide');
                });
                $matchingListElements = null;
        }
        $allListElements = null;
}

var handleTextBox = function (container) {

        container = which_container(container);
        $(container)
                .find(
                        'input[type=text]:not(.row0):not(.date), input[type=password]:not(.row0):not(.date)')
                .each(function (k, v) {

                        if ($(v).hasClass('always_disabled')) {
                                $(v).attr('disabled', 'disabled');
                        }

                        var data_limit = $(this).attr('data-limit');
                        if (parseInt(data_limit, 10) > 0) {
                                var counter = '#' + $(this).attr('name') + '_counter';
                                $(this).simplyCountable({
                                        'maxCount': data_limit,
                                        'counter': counter
                                });
                        }

                        //                var val = get_cookie($(this).attr('name'));
                        //                if (typeof (val) !== 'undefined')
                        //                        $(this).val(val);
                        if ($(this).hasClass('is_money'))
                                $(this).digits();
                        data_limit = null;
                        counter = null;
                }).on('click', function (e) {
                e.preventDefault();
                show_hint($(this));
        }).on('blur', function (e) {
                e.preventDefault();
                if ($(this).hasClass('is_money'))
                        $(this).digits();
        }).on('keypress', function (e) {
                if ($(this).hasClass('is-number')) {
                        var charcode = e.which;
                        if (charcode > 31 && (charcode < 48 || charcode > 57)) {
                                return false;
                        } else {
                                return true;
                        }
                } else if ($(this).hasClass('is-ch-en')) {
                        //debugger;
                        var charcode = e.which;
                        if (charcode > 31 && (charcode < 65 || charcode > 90) &&
                                (charcode < 97 || charcode > 122)) {
                                return false;
                        } else {
                                return true;
                        }
                }

        });

}

var handleRadioBox = function (container) {
        container = which_container(container);
        $(container).find('input[type=radio]').on('click', function (e) {
                var id = $(this).attr('id');
                var name = $(this).attr('name');
                var h = '<input type=hidden name=' + id + ' value=' + $(this).val() + '>';

                $(container).find('input[type=hidden][name=' + id + ']').remove();
                $(this).closest('.form-group').append(h);
                if (is_use_cookie($(this))) {
                        $.cookie(name, $(this).val());
                }
        });
//      var rb = [];
//      $(container).find('input[type=radio]').each(function (k, v) {
//            var name = $(this).attr('name');
//            if ($.inArray(name, rb) == -1) {
//                  rb.push(name);
//            }
//      });
//
//      $.each(rb, function (k, v) {
//            if (typeof (v) !== 'undefined') {
//                  var name = v;
//                  if (is_use_cookie($(this)) && name.length > 0) {
//                        if ($.type(get_cookie(name)) !== 'undefined' && name.length > 0) {
//                              val = attr(get_cookie(name), '0');
//                              if (val.length == 0 && $(this).prop('checked') == true)
//                                    val = $(this).val();
//                              $(container).find('input[type=hidden][name=' + name + ']:not(.do_no_clear)').remove();
//                              var h = '<input type=hidden name=' + name + ' value=' + val + '>';
//                              if (attr($(container).find('input[type=radio][name=' + name + ']').val(), '') == val) {
//                                    $(this).prop('checked', true);
//
//                                    $(container).find('input[type=radio][name=' + name + ']').after(h);
//
//                              }
//                        }
//                  } else {
//
//                        $('input[type=radio][name=' + name + ']').each(function (k, v) {
//                              if ($(this).prop('checked') == true) {
//                                    val = $(this).val();
//                                    var h = '<input type=hidden name=' + name + ' value=' + val + '>';
//                                    $(container).find('input[type=hidden][name=' + name + ']').remove();
//                                    $(container).find('input[type=radio][name=' + name + ']').after(h);
//                              }
//                        })
//
//                  }
//            }
//      });

}

var handleCheckBox = function (container) {
        container = which_container(container);
        $(container).find('input[type=checkbox]').on('click', function (e) {

                if ($(this).hasClass('cb-privilege')) {
                        var v = $(this).val();
                        var hidden = $(this).closest('td').find(
                                'input[type=hidden]');
                        var nv;
                        if ($(this).prop('checked') == true) {
                                nv = replaceAt($(hidden).val(), v - 1, 1);
                        } else {
                                nv = replaceAt($(hidden).val(), v - 1, 0);
                        }
                        $(hidden).val(nv);
                } else if ($(this).hasClass('checkbox-master')) {
                        var table = attr($('#' + $(this).attr('name')), '');
                        var table_name = $(table).attr('table_name');
                        var is_group_function = $(this).hasClass(
                                'group-function');
                        if ($(this).prop('checked') == true) {

                                $(table).find('input[type=checkbox]').prop(
                                        'checked', true);
                                $(table)
                                        .find('tr')
                                        .each(
                                                function (k, v) {
                                                        if (!is_group_function) {
                                                                var cb = $(this)
                                                                        .find(
                                                                                "input[type=checkbox]:first");
                                                                var cb_name = $(cb)
                                                                        .attr('name');
                                                                var value = $(cb).val();
                                                                var hidden = '<input type=hidden name=' +
                                                                        table_name +
                                                                        '_' +
                                                                        cb_name +
                                                                        'i value=' +
                                                                        value + '>';
                                                                $(this)
                                                                        .find(
                                                                                'input[type=hidden][name=' +
                                                                                table_name +
                                                                                '_' +
                                                                                cb_name +
                                                                                'i]')
                                                                        .remove();
                                                                $(cb).after(hidden);
                                                        } else {
                                                                var hidden = $(v)
                                                                        .find(
                                                                                'input[type=hidden]');
                                                                if (hidden.length > 0) {
                                                                        var hv = $(hidden)
                                                                                .val()
                                                                                .toString();
                                                                        var new_v = "";
                                                                        for (var i = 0; i < hv.length; i++) {
                                                                                new_v += "1";
                                                                        }
                                                                        $(hidden)
                                                                                .val(new_v);
                                                                }
                                                        }
                                                });
                        } else {
                                $(table).find('input[type=checkbox]').prop(
                                        'checked', false);
                                var hidden = '<input type=hidden name="' +
                                        table_name + '_dummy_1i" value="">';
                                $(this).after(hidden);
                                $(table)
                                        .find('tr')
                                        .each(
                                                function () {
                                                        if (!is_group_function) {
                                                                var cb = $(this)
                                                                        .find(
                                                                                "input[type=checkbox]:first");
                                                                var cb_name = $(cb)
                                                                        .attr('name');
                                                                $(this)
                                                                        .find(
                                                                                'input[type=hidden][name=' +
                                                                                table_name +
                                                                                '_' +
                                                                                cb_name +
                                                                                'i]')
                                                                        .remove();
                                                        } else {
                                                                $(table)
                                                                        .find(
                                                                                'input[type=hidden]')
                                                                        .each(
                                                                                function () {
                                                                                        var name = $(
                                                                                                this)
                                                                                                .attr(
                                                                                                        'name');
                                                                                        var e = name
                                                                                                .split('_');
                                                                                        if (e.length == 2) {
                                                                                                var v = $(
                                                                                                        this)
                                                                                                        .val()
                                                                                                        .toString();
                                                                                                var new_v = "";
                                                                                                for (var i = 0; i < v.length; i++) {
                                                                                                        new_v += "0";
                                                                                                }
                                                                                                $(
                                                                                                        this)
                                                                                                        .val(
                                                                                                                new_v);
                                                                                        }
                                                                                })

                                                        }
                                                })
                        }
                } else {
                        var id = $(this).attr('id');
                        var controls = $(container).find('input[type=checkbox][name=' + id + ']');
                        var a = '';
                        $.each(controls, function (k, v) {
                                if ($(this).prop('checked') == true) {
                                        a += ',' + $(this).val();
                                }
                        });

                        a = a.substring(1);
                        //                                        var val = $(this).val();
                        $(container).find('input[type=hidden][name=' + id + ']').remove();
                        var h0 = '<input type=hidden name=' + id +
                                ' value="' + a + '">';
                        $(this).after(h0);
                        //                                        var h1 = '<input type=hidden name=' + id
                        //                                                + ' value="' + val + '">';
                        //                                        $(container).find(
                        //                                                'input[type=hidden][name=' + id + ']')
                        //                                                .remove();
                        //                                        if ($(this).prop('checked') == true) {
                        //                                                $(this).after(h1);
                        //                                        } else {
                        //                                                $(this).after(h0);
                        //                                        }

                }
        });
        $('.tablesorter').find('input[type=checkbox]').on(
                'click',
                function (e) {
                        //                var table_name = $(this).closest('table').attr('table_name');
                        var name = $(this).attr('name');
                        name = name.split('_');
                        var id = name[name.length - 1];
                        var indexColumn = $(this).attr('name').replace('_' + id, '');
                        var hidden = '';
                        if ($(this).prop('checked') == true) {
                                if ($(this).hasClass('tc')) {
                                        hidden = '<input type=hidden name=' + indexColumn +
                                                '_' + id + 'i value=' + id + '>';
                                } else {
                                        hidden = '<input type=hidden name=' + indexColumn +
                                                '_' + id + 'a value=' + id + '>';
                                }
                        } else {
                                if ($(this).hasClass('tc')) {
                                        hidden = '';
                                } else {
                                        hidden = '<input type=hidden name=' + indexColumn +
                                                '_' + id + 'd value=' + id + '>';
                                }
                        }
                        $(cc).find(
                                'input[type=hidden][name^=' + indexColumn + '_' + id +
                                ']').remove();
                        $(this).after(hidden);
                });
}

var handleResponsiveTable = function (container) {
        container = which_container(container);
        $(container).find('table.responsive-table').each(
                function () {
                        $(this).responsiveTables({
                                columnManage: true,
                                exclude: '',
                                menuIcon: '<i class="fa fa-bars"></i>',
                                startBreakpoint: function (ui) {
                                        //ui.item(element)
                                },
                                endBreakpoint: function (ui) {
                                        //ui.item(element)
                                },
                                onColumnManage: function () {}
                        });
                        var i = 1;
                        $(container).find('td.rt-clone-td').remove();
                        $(container).find('input[type=hidden][name*=_sortOrder_]')
                                .each(function (k, v) {
                                        $(this).val(i);
                                        i++;
                                })

                        num_cols = $(this).find('thead').find('th').length;
                        if (num_cols < num_cols_overflow) {
                                $(this).closest('.rt-table').css('overflow-x', 'hidden');
                        } else {
                                $(this).closest('.rt-table').css('overflow-x', 'auto');
                        }
                });
}

var handleFixedHeaderTable = function (container) {
        var sh = $(window).height();
        container = which_container(container);
        $(container)
                .find('.fixed_headers')
                .each(
                        function (k, v) {
                                var header = $(this).find('thead').clone();
                                var header0 = $(this).find('thead').clone();
                                var fixed_column = $(this).attr('fixed-columns');
                                var fixed_column_1 = fixed_column - 1;
                                $(header).find('th:lt(' + fixed_column + ')').remove();
                                $(header0).find('th:gt(' + fixed_column_1 + ')')
                                        .remove();
                                var id = $(this).attr('id');
                                var month = id.split('-')[1];
                                header = '<div id="' +
                                        id +
                                        '" class="fixedTable"><header class="fixedTable-header-aside"><table month="' +
                                        month +
                                        '" class="table table-bordered my_month"><thead>' +
                                        $(header0).html() +
                                        '</thead></table></header><header class="fixedTable-header"><table class="table table-bordered"><thead>' +
                                        $(header).html() +
                                        '</thead></table></header>';
                                var rows = $(this).find('tbody').find('tr').clone();
                                var aside = '<aside class="fixedTable-sidebar"><table class="table table-bordered"><tbody>';
                                $.each(rows, function (kk, vv) {
                                        $(vv).find('td:gt(' + fixed_column_1 + ')')
                                                .remove();
                                        aside += '<tr>' + $(vv).html() + '</tr>';
                                })
                                aside += '</tbody></table></aside>';
                                $(this).find('thead').remove()
                                var rows = $(this).find('tbody').find('tr').clone();
                                var body = '<div class="fixedTable-body"><table class="table table-bordered"><tbody>';
                                $.each(rows, function (kk, vv) {
                                        var row_id = $(vv).attr('id');
                                        $(vv).find('td:lt(' + fixed_column + ')').remove();
                                        body += '<tr id=' + row_id + '>' + $(vv).html() +
                                                '</tr>';
                                });
                                body += '</tbody></table></div>';
                                var output = header + aside + body;
                                $(this).parent().append(output);
                                $(this).remove();
                                fixedTable($('#' + id));
                                $('.output-area').css('overflow-x', 'hidden');
                        });

        $('.fixedTable-body, .fixedTable-sidebar').css('height', sh * 0.8);

}


var handleSelectBox = function (container) {
        container = which_container(container);
        $(container)
                .find('select.select2').each(function () {
                if (!$(this).hasClass('row0') &&
                        !$(this).hasClass('multiselect')) {
                        var name = $(this).attr('name');
                        var width = $(this).attr('data-select-width') ||
                                '100%';
                        var sclass = $(this).attr('class');
                        var is_multiple = false;
                        if (sclass.indexOf('is_multiple') !== -1)
                                is_multiple = true;
                        if (sclass.indexOf('required') !== -1) {
                                $(this).attr('validation_rules',
                                        'trim|required');
                        }
                        var placeholder = get_selectbox_placeholder(is_multiple);
                        var val = '';
                        var opt_o = '<option></option>';
                        $(this).prepend(opt_o);
                        if (is_use_cookie($(this))) {
                                val = attr(get_cookie(name), '');

                                if (val.length == 0) {
                                        val = attr($(container).find(
                                                'input[type=hidden][name=' + name +
                                                ']').val(), '');
                                }
                                $('input[type=hidden][name=' + name + ']')
                                        .remove();
                                var h = '<input type=hidden name=' + name +
                                        ' id=' + name + ' value="' + val +
                                        '">';
                                $('select[name=' + name + ']').after(h);

                        } else {
                                val = attr($(container)
                                        .find(
                                                'input[type=hidden][name=' +
                                                name + ']').val(), '');
                        }

                        $(this).val(val).trigger("change");

                        $(this)
                                .select2({
                                        allowClear: true,
                                        width: width,
                                        placeholder: placeholder
                                })
                                .on(
                                        "change",
                                        function (e) {
                                                var selected_element = $(e.currentTarget);
                                                var val = selected_element
                                                        .val();
                                                if (is_use_cookie($(this))) {
                                                        $.cookie(name, val);
                                                }
                                                h = '<input type=hidden name="' +
                                                        name + '" id="' +
                                                        name + '" value="' +
                                                        val + '">';
                                                $(container).find(
                                                        'input[type=hidden][name=' +
                                                        name + ']')
                                                        .remove();
                                                $(this).after(h);
                                                if ($(this).hasClass(
                                                        'is_parent') ||
                                                        (name
                                                                .indexOf('parentID') >= 0)) {
                                                        get_level($(this), val);
                                                        can_be_parent($(this), val);
                                                }

                                                if ($(this).closest(
                                                        '.form-group').find(
                                                        'span.data-subtext[id=' +
                                                        name + ']').length > 0) {
                                                        $(
                                                                'span.data-subtext[id=' +
                                                                name +
                                                                ']')
                                                                .find('span')
                                                                .addClass('hide');
                                                        $(this)
                                                                .closest(
                                                                        '.form-group')
                                                                .find(
                                                                        'span.data-subtext[id=' +
                                                                        name +
                                                                        ']')
                                                                .find(
                                                                        'span[id=' +
                                                                        val +
                                                                        ']')
                                                                .removeClass('hide')
                                                                .find('span')
                                                                .removeClass('hide');
                                                }
                                        }).on(
                                'select2:unselecting',
                                function (e) {
                                        $(container).find(
                                                'input[type=hidden][name=' +
                                                name + ']')
                                                .val('');
                                });
                        if ($(this).hasClass('is_parent') ||
                                (name.indexOf('parentID') >= 0)) {
                                val = attr($(this).val(), '');
                                get_level($(this), val);
                                can_be_parent($(this), val);
                        }
                }
        })


}

var handleMultiSelectBox = function (container) {
        container = which_container(container);
        $(container)
                .find('select.multiselect')
                .each(
                        function () {
                                if (!$(this).hasClass('row0')) {
                                        var ob = $(this);
                                        var name = $(this).attr('name');
                                        var sclass = $(this).attr('class');
                                        if (sclass.indexOf('required') !== -1) {
                                                $(this).attr('validation_rules',
                                                        'trim|required');
                                        }

                                        var val = attr($.cookie(name), '');
                                        if (is_use_cookie($(this))) {
                                                val = attr(get_cookie(name), '');
                                        } else {
                                                val = attr($(container)
                                                        .find(
                                                                'input[type=hidden][name=' +
                                                                name + ']').val(), '');
                                        }

                                        if (val.length > 0) {
                                                var h = '<input type=hidden name=' + name +
                                                        ' id=' + name + ' value=' + val + '>';
                                                $(ob).after(h);
                                                if (val.indexOf(',') >= 0) {
                                                        val = val.split(',');
                                                } else {
                                                        val = [val];
                                                }

                                        } else {
                                                var val = attr($(container)
                                                        .find(
                                                                'input[type=hidden][name=' +
                                                                name + ']').val(), '');
                                                if (val.indexOf(',') >= 0) {
                                                        val = val.split(',');
                                                } else {
                                                        if (val.length > 0)
                                                                val = [val];
                                                        else
                                                                val = [];
                                                }
                                        }
                                        $(this)
                                                .multiselect({
                                                        enableCollapsibleOptGroups: true,
                                                        enableClickableOptGroups: true,
                                                        enableFiltering: true,
                                                        includeSelectAllOption: true,
                                                        buttonWidth: '100%',
                                                        maxHeight: 250,
                                                        selectedClass: 'multiselect-selected',
                                                        enableCaseInsensitiveFiltering: true,
                                                        inheritClass: true,
                                                        optionClass: function (element) {
                                                                var value = $(element)
                                                                        .index();
                                                                if (value % 2 == 0) {
                                                                        return 'even';
                                                                } else {
                                                                        return 'odd';
                                                                }
                                                        },
                                                        onChange: function (option,
                                                                checked, select) {
                                                                if ($(ob).val() !== null) {
                                                                        var v = $(ob).val()
                                                                                .join(',');
                                                                } else {
                                                                        var v = '';
                                                                }
                                                                var h = '<input type=hidden name=' +
                                                                        name +
                                                                        ' id=' +
                                                                        name +
                                                                        ' value=' +
                                                                        v + '>';
                                                                $(container).find(
                                                                        'input[type=hidden][name=' +
                                                                        name +
                                                                        ']')
                                                                        .remove();
                                                                $(ob).after(h);
                                                                $.cookie(name, v);
                                                        },
                                                        onSelectAll: function () {
                                                                var selected = [];
                                                                var v = $(
                                                                        'select[name=' +
                                                                        name +
                                                                        '] option:selected')
                                                                        .filter(
                                                                                function () {
                                                                                        if ($
                                                                                                .inArray(
                                                                                                        $(
                                                                                                                this)
                                                                                                        .val(),
                                                                                                        selected) == -1) {
                                                                                                selected
                                                                                                        .push($(
                                                                                                                this)
                                                                                                                .val());
                                                                                        }
                                                                                });

                                                                selected = selected
                                                                        .join(',');
                                                                var h = '<input type=hidden name=' +
                                                                        name +
                                                                        ' id=' +
                                                                        name +
                                                                        ' value=' +
                                                                        selected + '>';
                                                                $(container).find(
                                                                        'input[type=hidden][name=' +
                                                                        name +
                                                                        ']')
                                                                        .remove();
                                                                $(ob).after(h);

                                                                $.cookie(name, selected);
                                                        },
                                                        onDeselectAll: function () {
                                                                var v = '';
                                                                var h = '<input type=hidden name=' +
                                                                        name +
                                                                        ' id=' +
                                                                        name +
                                                                        ' value=' +
                                                                        v + '>';
                                                                $(container).find(
                                                                        'input[type=hidden][name=' +
                                                                        name +
                                                                        ']')
                                                                        .remove();
                                                                $(ob).after(h);
                                                                $.cookie(name, v);
                                                                //                                alert('onDeselectAll triggered!');
                                                        }
                                                }).multiselect('select', val);
                                }
                        });
}

var handleFooter = function (container) {
        container = which_container(container);
        $('.button_area').find('button.footer').remove();
        $(container).find('button.footer').each(function (k, v) {
                $('.button_area').append($(this));
        });
}

var handleTextArea = function (container) {

        container = which_container(container);

        $(container)
                .find('textarea')
                .each(
                        function (k, v) {

                                var name = $(this).attr('name');
                                if ($(this).hasClass('wysiwyg')) {
                                        var ot = $(this).val();
                                        $(this).summernote({
                                                height: 200,
                                                codemirror: {
                                                        mode: 'text/html',
                                                        htmlMode: true,
                                                        lineNumbers: true,
                                                        //                                theme: 'monokai'
                                                },

                                        });
                                        $(this).code(htmlspecialchars_decode(ot));
                                        $('.note-toolbar').find('button').removeClass(
                                                'btn-small btn-sm');
                                } else if ($(this).hasClass('wysiwyg-message')) {
                                        var ot = $(this).val();
                                        $(this).summernote({
                                                height: 200,
                                                codemirror: {
                                                        mode: 'text/html',
                                                        htmlMode: true,
                                                        lineNumbers: true,
                                                        //                                theme: 'monokai'
                                                },
                                                toolbar: [
                                                        ['style', ['style']],
                                                        [
                                                                'font', ['bold', 'italic',
                                                                        'underline',
                                                                        'superscript',
                                                                        'subscript',
                                                                        'clear'
                                                                ]
                                                        ],
                                                        //                                        ['help', ['help']],
                                                        ['fontname', ['fontname']],
                                                        //                                        ['fontsize', ['fontsize']], // Still buggy
                                                        ['color', ['color']],
                                                                //                                        ['view', ['fullscreen', 'codeview']],
                                                                //                                        ['para', ['ul', 'ol', 'paragraph']],
                                                                //                                        ['height', ['height']],
                                                                //                                        ['table', ['table']],
                                                                //                                ['insert', ['link', 'picture', 'video', 'hr']],
                                                ],
                                                defaultFontName: 'Tahoma',
                                                // fontName
                                                //                                fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Helvetica Neue', 'Impact', 'Lucida Grande', 'Tahoma', 'Times New Roman', 'Verdana'],
                                                focus: true,
                                        });
                                        $(this).code(htmlspecialchars_decode(ot));
                                        $('.note-toolbar').find('button').removeClass(
                                                'btn-small btn-sm');
                                } else {
                                        $(this)
                                                .on(
                                                        'dblclick',
                                                        function () {
                                                                var a = '<div class=clone id=clone_' +
                                                                        name + '>';
                                                                a += $(this).closest('td')
                                                                        .clone().html();
                                                                a += '<button class="btn btn-default"><i class="fa fa-save">Save</i></button>';
                                                                a += '</div>';
                                                                $(this).closest('td').append(a);
                                                                $('#clone_' + name)
                                                                        .PopupWindow({
                                                                                title: "Edit " +
                                                                                        name,
                                                                                modal: true,
                                                                                statusBar: false,
                                                                                height: 400,
                                                                                width: 600,
                                                                                buttons: {
                                                                                        close: true,
                                                                                        maximize: false,
                                                                                        collapse: false,
                                                                                        minimize: false
                                                                                }
                                                                        });
                                                                $('#clone_' + name)
                                                                        .find('button')
                                                                        .off('click')
                                                                        .on(
                                                                                'click',
                                                                                function () {
                                                                                        var t = $(
                                                                                                '#clone_' +
                                                                                                name)
                                                                                                .find(
                                                                                                        'textarea')
                                                                                                .val();
                                                                                        $(cc)
                                                                                                .find(
                                                                                                        '#' +
                                                                                                        name)
                                                                                                .val(
                                                                                                        t)
                                                                                                .text(
                                                                                                        t);
                                                                                        $(
                                                                                                '#clone_' +
                                                                                                name)
                                                                                                .PopupWindow(
                                                                                                        "close")
                                                                                                .remove();
                                                                                })
                                                        })
                                }
                                show_hint($(this));

                        })
}

var handleDateTimePicker = function (container) {
//      var lang = get_lang();
//      lang = lang.toLowerCase();
//      lang = lang.substr(0, 2);

        container = which_container(container);

        $(container).find('input[type=date]').each(function (k, v) {
                if (!$(this).find('input:first').hasClass('row0')) {
                        var dataDateFormat = attr($(this).find('input').attr('data-dateformat'), 'YYYY-MM-DD');
//                  $(this).closest('section').find('.note').append('<span>' + locale['please_enter_the_date_in_format_of'] + ' ' + dataDateFormat + '</span>');
                        if (dataDateFormat == 'YYYY-MM-DD') {
                                var addtime = false;
                        } else {
                                var addtime = true;
                        }
                        
                        $(this).datetimepicker({
//                        locale: lang,
                                showClear: true,
                                showClose: true,
                                calendarWeeks: false,
                                useCurrent: false,
                                sideBySide: addtime,
                                format: dataDateFormat, //'YYYY-MM-DD HH:mm',
                        });

                        dataDateFormat = null;
                }
        });

        $(container).find('input[type=datetimepicker]').each(function (k, v) {
                if (!$(this).find('input:first').hasClass('row0')) {
                        var dataDateFormat = attr($(v).attr('data-dateformat'), 'YYYY-MM-DD');
                        if (dataDateFormat == 'YYYY-MM-DD') {
                                var addtime = false;
                        } else {
                                var addtime = true;
                        }
                        
                        $(this).datetimepicker({
//                        locale: lang,
                                showClear: true,
                                showClose: true,
                                calendarWeeks: false,
                                useCurrent: true,
                                sideBySide: addtime,
                                format: dataDateFormat, //'YYYY-MM-DD HH:mm',
                        });

                        // dataDateFormat = null;
                }
        });
}

var handleTimePicker = function (container) {

        container = which_container(container);

        $(container).find('div.timepicker').each(function (k, v) {
                var a = $(this).find('input.timepicker');
                if (!$(a).hasClass('row0')) {
                        $(a).timepicker({
                                showMeridian: false
                        }).on("tp.change", function (e) {
                                var name = $(this).find('input').attr('name');
                                var dd = new Date(e.date);
                                if (null !== dd) {
                                        var h = addZero(dd.getHours());
                                        var mm = addZero(dd.getMinutes());
                                        var ss = addZero(dd.getSec());
                                        var v1 = h + ':' + mm + ':' + ss;
//                          if (h !== 0) {
//                                v1 += ' ' + h + ':' + mm + ':00';
//                          }

                                        $(container).find('input[type=hidden][name=' + name + ']').remove();
                                        var h = '<input type=hidden name="' + name + '" value="' + v1 + '">';
                                        $(this).after(h);
                                        v1, name, h = null;
                                }
                        });
                }
        });
}


var handleTab = function () {
        $('.ext-tabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
        });
}

var handleTables = function (container) {
        handleDataTables(container);
        handleTableSorter(container);
        handleResponsiveTable(container);
        handleFixedHeaderTable(container);
}

var handleSelectBoxes = function (container) {
        handleSelectBox(container);
        handleMultiSelectBox(container);
        handleRefreshSelectBox(container);
}

var handleRefreshSelectBox = function (container) {

        $('i.show_refresh').off('click').on('click', function (e) {
                e.preventDefault();

                var container = $(this).closest('.form-group');

                var function_id = $(this).closest('.form-group').find('select').attr('function_id');
                var table_name = $(this).closest('.form-group').find('select').attr('table_name');

                $(this).closest('.form-group').find('.select2-container').remove();
                var dataString = 'function_id=' + function_id + '&table_name=' + table_name;
                $.ajax({
                        type: "POST",
                        url: get_base_url() + "mct/get_select_data",
                        data: dataString,
                        cache: false,
                        async: false,
                        success: function (html) {

                                var data = $.parseJSON(html);
                                var o = '<option value=0></option>';
                                $.each(data, function (k, v) {
                                        o += '<option value=' + k + '>' + v['name'] + '</option>';
                                });

                                $(container).find('select').empty().append(o);
                                handleSelectBox(container);
                        }
                });

        });
}
var handleButtonDetail = function (container) {
        container = which_container(container);
        $(container).find('a.refresh-file').off('click').on('click', function (k, v) {
                var table_name = $(this).closest('table').attr('id');
                var id = $(this).closest('tr').attr('id');
                var indexColumn = $('#' + table_name).attr('indexColumn');
                var dataString = 'table_name=' + table_name + '&indexColumn=' + indexColumn + '&id=' + id;
                var target = $(this).prev();
                $.ajax({
                        type: "POST",
                        url: "mct/get_refresh_file",
                        data: dataString,
                        cache: false,
                        async: false,
                        success: function (html) {
                                var data = $.parseJSON(html);
                                var href = $(target).attr('href').split('/')
                                href.pop(); //.join('/')+data.document_file;
                                href = href.join('/') + '/' + data.document_file;
                                href += '&table_name=' + table_name;
                                $(target).attr('href', href);
                                data, href = null;
                        }
                });
                table_name, id, indexColumn, dataString, target = null;
        })

        $(container).find('a.row_add_detail').each(function (k, v) {
                $(this).off('click').on('click', function (e) {
                        e.preventDefault();
                        var counter = $(this).closest('.table-wrapper').find('tbody').find('tr').length;
                        var row0 = $(this).closest('.table-wrapper').find('#row0').clone().removeClass('hide');
                        $(row0).find('input[type=file]').each(function (k, v) {
                                var source_name = $(v).attr('id');
                                var new_name = source_name + '_' + counter + 'a';
                                $(v).attr('id', new_name).attr('name', new_name).attr('source_name', source_name).attr('onchange', 'fileSelected(\'' + new_name + '\')').removeClass('row0');
                                $(v).closest('td').find('input[id^=valid_extension_]').attr('id', 'valid_extension_' + new_name).attr('name', 'valid_extension_' + new_name);
                        })

                        $(row0).find('textarea').each(function (k, v) {
                                var source_name = $(v).attr('id');
                                var new_name = source_name + '_' + counter + 'a';
                                $(v).attr('id', new_name).attr('name', new_name).attr('source_name', source_name).removeClass('row0');
                        })

                        $(row0).find('select').each(function (k, v) {
                                var source_name = $(v).attr('id');
                                var new_name = source_name + '_' + counter + 'a';
                                if ($(this).hasClass('msb')) {
                                        $(v).addClass('multiselect').removeClass('row0').removeClass('msb').removeClass('select2');
                                } else {
                                        $(v).addClass('select2').removeClass('row0');
                                }
                                $(v).attr('id', new_name).attr('name', new_name).attr('source_name', source_name);
                                $(v).closest('[class^=col-]').find('input[type=hidden][name^=sys_validation]').attr('id', 'sys_validation[' + new_name + ']').attr('name', 'sys_validation[' + new_name + ']');
                        })

                        $(row0).find('input[type=text]').each(function (k, v) {
                                if (!$(this).hasClass('.date')) {
                                        var data_limit = $(v).attr('maxlength');
                                        var new_name = $(v).attr('id') + '_' + counter + 'a';
                                        $(v).attr('data-limit', data_limit);
                                        $(v).removeClass('row0');
                                        $(v).attr('id', new_name).attr('name', new_name);
                                        $(this).closest('[class^=col-]').find('.safe').attr('id', new_name + '_counter');
                                        $(this).closest('[class^=col-]').find('.helper-text-box').attr('id', 'helper-' + new_name).attr('name', 'helper-' + new_name);
                                        $(this).closest('[class^=col-]').find('input[type=hidden][name^=sys_validation]').attr('id', 'sys_validation[' + new_name + ']').attr('name', 'sys_validation[' + new_name + ']').removeClass('row0');
                                } else {
                                        $(v).addClass('form_date');
                                        var new_name = $(v).find('input[type=text]').attr('id') + '_' + counter + 'a';
                                        $(v).attr('data-link-field', new_name);
                                        $(v).find('input[type=text]').attr('id', new_name).attr('name', new_name);
                                        $(this).closest('[class^=col-]').find('input[type=hidden]:not([name^=sys_validation])').attr('id', new_name).attr('name', new_name).removeClass('row0');
                                }
                        })


                        $(row0).attr('id', counter);
                        $(this).closest('.table-wrapper').find('tbody').prepend(row0);
                        handleSelectBox(row0);
                        handleMultiSelectBox(row0);
                        handleDateTimePicker(row0);
                        handleTextBox(row0);
                        handleValidationRules(row0);
                        handleTextArea(row0);
                        handleButtonDetail(row0);
                });
        });
        $(container).find('a.row0_detail_delete').each(function (k, v) {
                $(this).off('click').on('click', function (e) {
                        e.preventDefault();
                        start_loading();
                        var p = $(this).parent().parent();
                        $(p).remove();
                        end_loading();
                        p = null;
                });
        });
        //        print( $(container).find('a.row_detail_down, .row_detail_up'));
        $(container).find('a.row_detail_down, .row_detail_up').each(function (k, v) {
                $(this).off('click').on('click', function (e) {
                        e.preventDefault();
                        start_loading();
                        var row = $(this).closest('tr');
                        if ($(this).is(".row_detail_up")) { //move up
                                var s1 = attr(row.find('input[type=hidden][name*=sortOrder]').val(), 0);
                                var s2 = attr(row.prev().find('input[type=hidden][name*=sortOrder]').val(), 0);
                                if (s1 > 0 && s2 > 0) {
                                        row.find('input[type=hidden][name*=sortOrder]').val(s2);
                                        row.prev().find('input[type=hidden][name*=sortOrder]').val(s1);
                                        row.insertBefore(row.prev());
                                }
                                s1, s2 = null;
                        } else { //move down
                                var s1 = attr(row.find('input[type=hidden][name*=sortOrder]').val(), 0);
                                var s2 = attr(row.next().find('input[type=hidden][name*=sortOrder]').val(), 0);
                                if (s1 > 0 && s2 > 0) {
                                        row.find('input[type=hidden][name*=sortOrder]').val(s2);
                                        row.next().find('input[type=hidden][name*=sortOrder]').val(s1);
                                        row.insertAfter(row.next());
                                }
                                s1, s2 = null;
                        }
                        end_loading();
                })
        });


        $(container).find('a.row_detail_delete').each(function (k, v) {

                $(this).off('click').on('click', function (e) {
                        e.preventDefault();
                        var p = $(this).closest('tr');
                        var ttype;
                        if ($(p).hasClass('row_new')) {
                                $(p).delay(1000).fadeOut(300, function () {
                                        $(this).remove();
                                });
                                return;
                        }

                        var row = $(this).closest('tr');
                        var row0 = $(this).closest('.table-wrapper').find('#row0').clone().removeClass('hide');

                        $(row).find('input[type=hidden],input.datepicker').each(function (k, v) {
                                var a = $(v).attr('name');
                                var last = parseInt(a.substr(a.length - 1), 10);
                                if (last >= 0) {
                                        a = a + 'd';
                                } else {
                                        a = a.substr(0, a.length - 1) + 'd';
                                }
                                $(v).attr('name', a).attr('id', a);
                        });
                        $(row).find('input[type=text], input[type=radio], input[type=checkbox], .fileinput, textarea, a.refresh-file, .row_detail_up,.row_detail_down').addClass('disabled').attr('disabled', 'disabled');
                        $(row).find('.select2:not(.always_disabled)').prop("disabled", true);
                        $(this).addClass('hide');
                        $(row).find('a.row_detail_undelete').removeClass('hide');
                        p, row, ttype = null;
                });
        });


        $(container).find('a.row_detail_undelete').each(function (k, v) {
                $(this).off('click').on('click', function (e) {
                        e.preventDefault();
                        start_loading();
                        var ttype;
                        var row = $(this).closest('tr');
                        $(this).addClass('hide');
                        $(row).find('a.row_detail_delete').removeClass('hide');
                        $(row).find('input[type=hidden], input.datepicker').each(function (k, v) {
                                var a = $(v).attr('name');
                                a = a.substr(0, a.length - 1);
                                $(v).attr('name', a).attr('id', a);
                        });
                        $(row).find('input[type=text], input[type=radio], input[type=checkbox], .fileinput, textarea, a.refresh-file, .row_detail_up,.row_detail_down').removeClass('disabled');
                        end_loading();
                        row, ttype = null;
                });
        });
        $(container).find('a.row_detail_edit').each(function (k, v) {
                $(this).off('click').on('click', function (e) {
                        e.preventDefault();
                        start_loading();
                        var a = $(this).attr('target_id') + '_modal';
                        var row = $(this).parent().parent();
                        var table = row.parent().parent();
                        var tableNameId = $(table).attr('savetable');
                        var rowId = $(row).attr('id').replace(tableNameId + "_datarow_", "");
                        var data = handleGetRowData(row, table);
                        data = data['input_data'];
                        $('#' + table.attr("editModal")).attr('targetId', rowId);
                        $('#' + table.attr("editModal")).modal('show');
                        end_loading();
                        a, row, table, tableNameId, rowId, data = null;
                })
        })

        $(container).find('a.row_detail_remove').each(function (k, v) {
                $(this).off('click').on('click', function (e) {
                        e.preventDefault();
                        start_loading();
                        var target_row = $(this).attr('target_row');
                        var table_id = $(this).attr('table_id');
                        var row = $('table[id=' + table_id + ']').find('tr[id=' + target_row + ']'); //
                        var row_id = '';
                        $(row).find('input[type=hidden], textarea').each(function (k, v) {
                                var a = $(v).attr('name');
                                var last = parseInt(a.substr(a.length - 1), 10);
                                if (last >= 0) {
                                        a = a + 'd';
                                } else {
                                        a = a.substr(0, a.length - 1) + 'd';
                                }
                                $(v).attr('name', a).attr('id', a);
                                row_id = a;
                        });
                        $(row).attr('id', row_id).find('td').fadeTo('slow', 0.1);
                        end_loading();
                        target_row, table_id, row, row_id = null;
                })
        });
}



var handleButton = function (container) {
        var container = which_container(container);
        $('body')
                .find('button[id=btn_export_all_data]')
                .off('click')
                .on(
                        'click',
                        function (e) {
                                e.preventDefault();
                                php_path = excel_url;
                                var is_close = true;

                                $(this).attr('disabled', 'disabled');
                                var headers = [];
                                var row = $(container).find('.datatable').find('tr')[0];
                                var col = $(row).find('th');
                                $.each(col, function () {
                                        headers.push($(this).html());
                                });
                                c = headers.join(',');
                                var file_name = window.location.href.split('/')[4];
                                if ($('.output-area').length == 0 &&
                                        typeof (oTable) !== 'undefined') {
                                        //                        var name = $(this).attr('id');
                                        var orientation = attr($(container).find(
                                                'input[type=hidden][name=orientation]')
                                                .val(), '');
                                        if (orientation.length == 0)
                                                orientation = 'P';

                                        var column_name = $(oTable).attr('column_name'); //$(cc).find('input[type=hidden][name=column_name]').val();
                                        var view_name = $(oTable).attr('view_name'); //$(cc).find('input[type=hidden][name=view_name]').val();
                                        var table_name = $(oTable).attr('table_name'); //$(cc).find('input[type=hidden][name=table_name]').val();
                                        var indexColumn = $(oTable).attr('indexColumn'); //$(cc).find('input[type=hidden][name=indexColumn]').val();
                                        var url = get_base_url() +
                                                'mct/get_data?table_name=' + table_name +
                                                '&view_name=' + view_name +
                                                '&indexColumn=' + indexColumn +
                                                '&column_name=' + column_name; //    + '&where_value=' + where_value;
                                        if (column_name.indexOf(',') > 0) {
                                                $
                                                        .ajax({
                                                                type: "POST",
                                                                url: url,
                                                                cache: false,
                                                                async: false,
                                                                success: function (html) {
                                                                        var data = $.parseJSON(html);
                                                                        data = data['aaData'];
                                                                        if (is_close)
                                                                                var a = '<table border=1 class="table-csv">';
                                                                        else
                                                                                var a = '';
                                                                        var c = column_name.split(',');
                                                                        var cc = [];
                                                                        $
                                                                                .each(
                                                                                        c,
                                                                                        function (k, v) {
                                                                                                v = $
                                                                                                        .trim(v);
                                                                                                if (v == indexColumn) {
                                                                                                        cc
                                                                                                                .push(v);
                                                                                                }
                                                                                                if (v
                                                                                                        .substr(v.length - 3) !== '_id') {
                                                                                                        cc
                                                                                                                .push(v);
                                                                                                }
                                                                                        });
                                                                        cc = cc.join(',');
                                                                        $.each(data, function (k, v) {
                                                                                a += '<tr>';
                                                                                $.each(v, function (kk, vv) {
                                                                                        a += '<td>' + vv +
                                                                                                '</td>';
                                                                                });
                                                                                a += '</tr>';
                                                                        });
                                                                        if (is_close)
                                                                                a += '</table>';
                                                                        b = a;
                                                                        var form = "<form name='csvexportform_all' action='" +
                                                                                php_path +
                                                                                "' method='post' accept-charset='ISO-8859-1'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='csvBuffer' value='" +
                                                                                b + "'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='headers' value='" +
                                                                                cc + "'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='file_name' value='" +
                                                                                file_name + "'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='orientation' value='" +
                                                                                orientation + "'>";
                                                                        form = form +
                                                                                "</form><script>document.csvexportform_all.submit();</script>";
                                                                        OpenWindow_all = window.open(
                                                                                '', '');
                                                                        OpenWindow_all.document
                                                                                .write(form);
                                                                        if (is_close) {
                                                                                setTimeout(function () {
                                                                                        OpenWindow_all.close();
                                                                                }, 30000);
                                                                        }
                                                                }
                                                        });
                                        }
                                } else {
                                        var b = $('.output-area').clone();
                                        $(b).find('.report-header, .hr-totop, .rt-menu')
                                                .remove();
                                        var c = $(b).find('input,textarea');

                                        $.each(c, function (k, v) {
                                                if ($(this).attr('type') == 'checkbox') {
                                                        if ($(this).hasClass('is_checked')) {
                                                                var val = 'T';
                                                        } else {
                                                                var val = '';
                                                        }
                                                } else {
                                                        var val = $(this).val();
                                                }
                                                var p = $(this).closest('td');
                                                $(p).html(val);
                                        })

                                        b = $(b).html();

                                        var form = "<form name='csvexportform_all' action='" +
                                                php_path +
                                                "' method='post' accept-charset='ISO-8859-1'>";
                                        form = form +
                                                "<input type='hidden' name='csvBuffer' value='" +
                                                b + "'>";
                                        form = form +
                                                "<input type='hidden' name='file_name' value='" +
                                                file_name + "'>";
                                        form = form +
                                                "<input type='hidden' name='orientation' value='" +
                                                orientation + "'>";
                                        form = form +
                                                "</form><script>document.csvexportform_all.submit();</script>";
                                        OpenWindow_all = window.open('', '');
                                        OpenWindow_all.document.write(form);
                                        if (is_close) {
                                                setTimeout(function () {
                                                        OpenWindow_all.close();
                                                }, 30000);
                                        }
                                }
                                $(this).removeAttr('disabled');
                        })

        $('body')
                .find('button[id=btn_pdf_this_data]')
                .off('click')
                .on(
                        'click',
                        function (e) {
                                e.preventDefault();
                                php_path = pdf_url;
                                var is_close = false;
                                var file_name = $.cookie('current_url');
                                var headers = [];
                                var row = $(container).find('.datatable').find('tr')[0];
                                var col = $(row).find('th');
                                $.each(col, function () {
                                        headers.push($(this).html());
                                });
                                c = headers.join(',');
                                $(this).attr('disabled', 'disabled');
                                $
                                        .when(fn_get_rep_table())
                                        .done(
                                                function () {
                                                        if (typeof (b) !== 'undefined') {
                                                                var form = "<form name='csvexportform_this' action='" +
                                                                        php_path +
                                                                        "' method='post' accept-charset='ISO-8859-1'>";
                                                                form = form +
                                                                        "<input type='hidden' name='csvBuffer' value='" +
                                                                        b + "'>";
                                                                form = form +
                                                                        "<input type='hidden' name='headers' value='" +
                                                                        c + "'>";
                                                                form = form +
                                                                        "<input type='hidden' name='file_name' value='" +
                                                                        file_name + "'>";
                                                                form = form +
                                                                        "</form><script>document.csvexportform_this.submit();</script>";
                                                                OpenWindow = window
                                                                        .open('', '');
                                                                OpenWindow.document.write(form);
                                                                if (is_close) {
                                                                        setTimeout(function () {
                                                                                OpenWindow.close();
                                                                        }, 30000);
                                                                }
                                                        }
                                                });
                                $(this).removeAttr('disabled');
                        })

        $('body')
                .find('button[id=btn_pdf_all_data]')
                .off('click')
                .on(
                        'click',
                        function (e) {
                                e.preventDefault();
                                php_path = pdf_url;
                                var is_close = false;
                                var file_name = $.cookie('current_url');
                                var headers = [];
                                var row = $(container).find('.datatable').find('tr')[0];
                                var col = $(row).find('th');
                                $.each(col, function () {
                                        headers.push($(this).html());
                                });
                                c = headers.join(',');
                                $(this).attr('disabled', 'disabled');
                                if ($('.output-area').length == 0 &&
                                        typeof (oTable) !== 'undefined') {
                                        var name = $(this).attr('id');
                                        var orientation = attr($(container).find(
                                                'input[type=hidden][name=orientation]')
                                                .val(), '');
                                        if (orientation.length == 0)
                                                orientation = 'P';
                                        var file_name = $.cookie('current_url');
                                        var column_name = $(oTable).attr('column_name'); //$(cc).find('input[type=hidden][name=column_name]').val();
                                        var view_name = $(oTable).attr('view_name'); //$(cc).find('input[type=hidden][name=view_name]').val();
                                        var table_name = $(oTable).attr('table_name'); //$(cc).find('input[type=hidden][name=table_name]').val();
                                        var indexColumn = $(oTable).attr('indexColumn'); //$(cc).find('input[type=hidden][name=indexColumn]').val();
                                        var url = get_base_url() +
                                                'mct/get_data?table_name=' + table_name +
                                                '&view_name=' + view_name +
                                                '&indexColumn=' + indexColumn +
                                                '&column_name=' + column_name; //    + '&where_value=' + where_value;
                                        if (column_name.indexOf(',') > 0) {
                                                $
                                                        .ajax({
                                                                type: "POST",
                                                                url: url,
                                                                cache: false,
                                                                async: false,
                                                                success: function (html) {
                                                                        var data = $.parseJSON(html);
                                                                        data = data['aaData'];
                                                                        if (is_close)
                                                                                var a = '<table border=1>';
                                                                        else
                                                                                a = '';
                                                                        var c = column_name.split(',');
                                                                        var cc = [];
                                                                        $
                                                                                .each(
                                                                                        c,
                                                                                        function (k, v) {
                                                                                                v = $
                                                                                                        .trim(v);
                                                                                                if (v == indexColumn)
                                                                                                        cc
                                                                                                                .push(v);
                                                                                                if (v
                                                                                                        .substr(v.length - 3) !== '_id') {
                                                                                                        cc
                                                                                                                .push(v);
                                                                                                }
                                                                                        });
                                                                        cc = cc.join(',');
                                                                        $.each(data, function (k, v) {
                                                                                a += '<tr>';
                                                                                $.each(v, function (kk, vv) {
                                                                                        a += '<td>' + vv +
                                                                                                '</td>';
                                                                                });
                                                                                a += '</tr>';
                                                                        });
                                                                        if (is_close)
                                                                                a += '</table>';
                                                                        b = a;
                                                                        var form = "<form name='csvexportform_all' action='" +
                                                                                php_path +
                                                                                "' method='post' accept-charset='ISO-8859-1'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='csvBuffer' value='" +
                                                                                b + "'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='headers' value='" +
                                                                                cc + "'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='file_name' value='" +
                                                                                file_name + "'>";
                                                                        form = form +
                                                                                "<input type='hidden' name='orientation' value='" +
                                                                                orientation + "'>";
                                                                        form = form +
                                                                                "</form><script>document.csvexportform_all.submit();</script>";
                                                                        OpenWindow_all = window.open(
                                                                                '', '');
                                                                        OpenWindow_all.document
                                                                                .write(form);
                                                                        if (is_close) {
                                                                                setTimeout(function () {
                                                                                        OpenWindow_all.close();
                                                                                }, 30000);
                                                                        }
                                                                }
                                                        });
                                        }
                                } else {
                                        b = $('.output-area').clone();
                                        var c = $(b).find('input,textarea');
                                        $.each(c, function (k, v) {
                                                var val = $(this).val();
                                                var p = $(this).closest('td');
                                                $(p).html(val);
                                        })
                                        b = $(b).html();
                                        if (b.length > 0) {
                                                var form = "<form name='csvexportform_all' action='" +
                                                        php_path +
                                                        "' method='post' accept-charset='ISO-8859-1'>";
                                                form = form +
                                                        "<input type='hidden' name='csvBuffer' value='" +
                                                        b + "'>";
                                                //                                        form = form + "<input type='hidden' name='headers' value='" + cc + "'>";
                                                form = form +
                                                        "<input type='hidden' name='file_name' value='" +
                                                        file_name + "'>";
                                                form = form +
                                                        "<input type='hidden' name='orientation' value='" +
                                                        orientation + "'>";
                                                form = form +
                                                        "</form><script>document.csvexportform_all.submit();</script>";
                                                OpenWindow_all = window.open('', '');
                                                OpenWindow_all.document.write(form);
                                                if (is_close) {
                                                        setTimeout(function () {
                                                                OpenWindow_all.close();
                                                        }, 30000);
                                                }
                                        }
                                }
                                $(this).removeAttr('disabled');
                        })

        $(container)
                .find('button[id=btn_save], button[id=btn_save_refresh]')
                .each(
                        function () {
                                var button_name = $(this).attr('name');
                                $(this)
                                        .off('click')
                                        .on(
                                                'click',
                                                function (e) {
                                                        e.preventDefault();
                                                        $
                                                                .when(startUploadings())
                                                                .done(
                                                                        function () {
                                                                                if (passUpload) {
                                                                                        var saveTypeID = attr(
                                                                                                $(
                                                                                                        container)
                                                                                                .find(
                                                                                                        'input[type=hidden][name=saveTypeID]')
                                                                                                .val(),
                                                                                                '0');
                                                                                        var url = '';
                                                                                        //                                        saveTypeID = parseInt(saveTypeID, 10);

                                                                                        //                                        switch (saveTypeID) {
                                                                                        //                                                default:
                                                                                        //                                                        url = ['/mct/save_data'];
                                                                                        //                                                        break;
                                                                                        //                                                case 1:
                                                                                        //                                                        url = ['/mct/save_data_custom'];
                                                                                        //                                                        break;
                                                                                        //                                                case 2:
                                                                                        //                                                        url = ['/mct/save_data', '/mct/save_data_custom'];
                                                                                        //                                                        break;
                                                                                        //                                        }
                                                                                        var data = get_data_serialize(container);

                                                                                        var url = get_base_url() +
                                                                                                get_uri() +
                                                                                                '/save_data';
                                                                                        $
                                                                                                .ajax({
                                                                                                        url: url,
                                                                                                        type: 'post',
                                                                                                        dataType: 'json',
                                                                                                        data: data,
                                                                                                        cache: false,
                                                                                                        async: false,
                                                                                                        beforeSend: function () {
                                                                                                                start_saving();
                                                                                                                enableControl(0);
                                                                                                        },
                                                                                                        success: function (
                                                                                                                html) {
                                                                                                                $(
                                                                                                                        container)
                                                                                                                        .find(
                                                                                                                                'em.invalid')
                                                                                                                        .remove();
                                                                                                                $(
                                                                                                                        container)
                                                                                                                        .find(
                                                                                                                                'input,.select2')
                                                                                                                        .removeClass(
                                                                                                                                'validation-required');

                                                                                                                if (parseInt(
                                                                                                                        html,
                                                                                                                        10) > 0) {
                                                                                                                        var data = $
                                                                                                                                .parseJSON(html);

                                                                                                                        clear_error_tab();
                                                                                                                        end_saving();
                                                                                                                        if (button_name == 'btn_save_refresh') {
                                                                                                                                refresh(true);
                                                                                                                        } else {

                                                                                                                                refresh(false);
                                                                                                                                //                                                                                                                                scrollTo();
                                                                                                                        }

                                                                                                                        return false;
                                                                                                                } else {
                                                                                                                        if (html.length == 0) {
                                                                                                                                //                                                                                window.location.href = "/auth/logout";
                                                                                                                                return false;
                                                                                                                        } else {
                                                                                                                                enableControl(1);
                                                                                                                                data = html;
                                                                                                                                $
                                                                                                                                        .each(
                                                                                                                                                data,
                                                                                                                                                function (
                                                                                                                                                        key,
                                                                                                                                                        value) {
                                                                                                                                                        var a = $(
                                                                                                                                                                container)
                                                                                                                                                                .find(
                                                                                                                                                                        'label[for=' +
                                                                                                                                                                        key +
                                                                                                                                                                        ']')
                                                                                                                                                                .text();
                                                                                                                                                        if ($
                                                                                                                                                                .inArray(
                                                                                                                                                                        '.',
                                                                                                                                                                        a) > 0)
                                                                                                                                                                a = $(
                                                                                                                                                                        container)
                                                                                                                                                                        .find(
                                                                                                                                                                                'label[for=' +
                                                                                                                                                                                key +
                                                                                                                                                                                ']')
                                                                                                                                                                        .attr(
                                                                                                                                                                                'alt');
                                                                                                                                                        switch (key) {
                                                                                                                                                                case 'upload':
                                                                                                                                                                        if (pass_grid_upload &&
                                                                                                                                                                                pass_single_upload) {
                                                                                                                                                                                $
                                                                                                                                                                                        .when(
                                                                                                                                                                                                do_upload(value))
                                                                                                                                                                                        .done(
                                                                                                                                                                                                function () {
                                                                                                                                                                                                        setTimeout(
                                                                                                                                                                                                                function () {
                                                                                                                                                                                                                        do_success_upload();
                                                                                                                                                                                                                },
                                                                                                                                                                                                                1000);
                                                                                                                                                                                                });
                                                                                                                                                                        }
                                                                                                                                                                        break;
                                                                                                                                                                case 'custom':
                                                                                                                                                                        show_info(value);
                                                                                                                                                                        break;
                                                                                                                                                                case 'info':
                                                                                                                                                                        show_dialog_info(value);
                                                                                                                                                                        break;
                                                                                                                                                                default:
                                                                                                                                                                        var b = value
                                                                                                                                                                                .split(' ');
                                                                                                                                                                        if (b.length > 0) {
                                                                                                                                                                                var c = $
                                                                                                                                                                                        .trim(b[1]);
                                                                                                                                                                                var text_error = '<em id=' +
                                                                                                                                                                                        c +
                                                                                                                                                                                        ' for="name" class="invalid">' +
                                                                                                                                                                                        value +
                                                                                                                                                                                        '</em>';
                                                                                                                                                                                var label = $(
                                                                                                                                                                                        cc)
                                                                                                                                                                                        .find(
                                                                                                                                                                                                'label[for=' +
                                                                                                                                                                                                c +
                                                                                                                                                                                                ']');
                                                                                                                                                                                if ($(label).length > 0) {
                                                                                                                                                                                        $(
                                                                                                                                                                                                label)
                                                                                                                                                                                                .after(
                                                                                                                                                                                                        text_error);
                                                                                                                                                                                } else {
                                                                                                                                                                                        $(
                                                                                                                                                                                                '#' +
                                                                                                                                                                                                c)
                                                                                                                                                                                                .closest(
                                                                                                                                                                                                        '.form-group')
                                                                                                                                                                                                .prepend(
                                                                                                                                                                                                        text_error);
                                                                                                                                                                                }
                                                                                                                                                                        }

                                                                                                                                                                        break;
                                                                                                                                                        }
                                                                                                                                                });
                                                                                                                                set_error_tab();
                                                                                                                                toastr
                                                                                                                                        .error(
                                                                                                                                                'Please take care of some of system errors as shown in dashed border.',
                                                                                                                                                'Alerts!');
                                                                                                                                return false;
                                                                                                                        }
                                                                                                                        end_saving();
                                                                                                                        return false;
                                                                                                                }
                                                                                                        },
                                                                                                        error: function (
                                                                                                                xhr,
                                                                                                                ajaxOptions,
                                                                                                                thrownError) {
                                                                                                                end_saving();
                                                                                                                //                                                        window.location.href = "/auth/logout";
                                                                                                                return false;
                                                                                                        }
                                                                                                });

                                                                                        //                                        for (var ho in hot) {
                                                                                        //                                                var hd = hot[ho].getData();
                                                                                        //                                                if (hd.length > 0) {
                                                                                        //                                                        var id = $('input[type=hidden][name=id]').val();
                                                                                        //                                                        var fc = ho;
                                                                                        //                                                        $.ajax({
                                                                                        //                                                                url: 'mct/save_hanson',
                                                                                        //                                                                dataType: 'json',
                                                                                        //                                                                type: 'POST',
                                                                                        //                                                                data: {
                                                                                        //                                                                        data: hd,
                                                                                        //                                                                        fc: fc,
                                                                                        //                                                                        id: id,
                                                                                        //                                                                        uri: uri
                                                                                        //                                                                }, // contains changed cells' data
                                                                                        //                                                                success: function () {
                                                                                        //
                                                                                        //                                                                }
                                                                                        //                                                        });
                                                                                        //                                                }
                                                                                        //                                        }

                                                                                        setTimeout(
                                                                                                function () {
                                                                                                        var id = attr(
                                                                                                                $(
                                                                                                                        container)
                                                                                                                .find(
                                                                                                                        'input[type=hidden][name=id]')
                                                                                                                .val(),
                                                                                                                0);
                                                                                                        //                                                                                                        drawGanttView(id);
                                                                                                },
                                                                                                1000);
                                                                                        data,
                                                                                                url,
                                                                                                saveTypeID = null;
                                                                                }
                                                                        });
                                                        return false; // blocks redirect after submission via ajax
                                                });
                        });

        $(container).find('button[id=btn_cancel]').off('click').on('click',
                function (e) {
                        e.preventDefault();
                        start_loading();
                        var page_history = attr(get_cookie('page_history'), '');
                        if (page_history.length > 0) {
                                redirect(page_history);
                        } else {
                                var u = window.location.hash.split('/');
                                window.location = u[0];
                                u = null;
                        }
                        page_history = null;
                        end_loading();
                });

        $(container).find('button[id^=btn_new]').off('click')
                .on(
                        'click',
                        function (e) {
                                e.preventDefault();
                                start_loading();
                                var u = window.location.hash.split('/');
                                var id = $(this).attr('id');
                                if (id !== 'btn_new') {
                                        var new_id = id.split('_');
                                        new_id = new_id[new_id.length - 1];
                                        set_cookie('ref_id', new_id);
                                }

                                window.location.hash = u[0] + '/add/0';
                                clear_input();
                                clear_error_tab();
//                            $(container).find('button[id=btn_save]').removeAttr(
//                                    'disabled');
                                $(container).find('table[id$=main_list_view] tr').removeClass('dt_selected');
                                u, id = null;
                                end_loading();
                                return false;
                        });

        $(container).find('button[id=btn_excel]').off('click').on('click',
                function (e) {
                        e.preventDefault();
                        open_excel_window();
                });

        $(container).find('button[id=btn_undelete]').off('click').on('click',
                function (e) {
                        e.preventDefault();
                        start_loading();
                        undelete_data();
                        end_loading();
                });

        $(container)
                .find('button[id=btn_delete]')
                .off('click')
                .on(
                        'click',
                        function (e) {
                                e.preventDefault();
                                var code = attr($(container).find('#code').val(), '');
                                var name = attr($(container).find('#name').val(), '');
                                if (code.length > 0)
                                        name = code + ' - ' + name;

                                var a = '';
                                a += '<div class="popup"><h3 class="panel-title">Are you sure to delete the data of ' +
                                        name + '?</h3>';
                                a += '<br/><br/><br/>';
                                a += '<hr/>';
                                a += '</br>';
                                a += '<a class="btn btn-default delete_now"><i class="fa fa-trash"></i> Confirm</a> ';
                                a += '<a class="btn btn-default delete_cancel"><i class="fa fa-ban"></i> Cancel</a>';
                                a += '</div>';

                                $(container).append(a);
                                if ($(container).find('.popup').PopupWindow("getState"))
                                        $(container).find('.popup').PopupWindow("destroy");
                                $(container).find('.popup').PopupWindow({
                                        title: "Please confirm to delete",
                                        modal: true,
                                        statusBar: false,
                                        height: 300,
                                        width: 600,
                                        buttons: {
                                                close: true,
                                                maximize: false,
                                                collapse: false,
                                                minimize: false
                                        }
                                });

                                $('.popup').find('a.btn-warning, a.delete_cancel').on(
                                        'click',
                                        function (e) {
                                                e.preventDefault();
                                                $('.popup').PopupWindow('destroy');
                                                $('.popup').remove();
                                        })

                                $('.popup').find('a.btn-danger, a.delete_now').on(
                                        'click',
                                        function (e) {
                                                e.preventDefault();
                                                delete_data();
                                                $('.popup').PopupWindow('destroy');
                                                $('.popup').remove();
                                        })
                        });
}

var delete_data = function (uri, id) {
        var id = $(cc).find('input[type=hidden][name=id]').val();
        var uri = $(cc).find('input[type=hidden][name=uri]').val();
        $.ajax({
                type: "POST",
                url: get_base_url() + get_uri() + '/delete_data',
                data: 'uri=' + uri + '&id=' + id,
                cache: false,
                async: false,
                beforeSend: function () {
                        start_loading();
                },
                success: function (html) {
                        if (html.length > 0) {
                                end_loading();
                                refresh(true);
                                refresh_delete_select2(html);
                        } else {
                                //                                window.location.href = "/auth/logout";
                                return false;
                        }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        return false;
                }
        });
        id, uri, indexColumn = null;
}

var set_error_tab = function () {
        $(cc).find('.tab_asterisk').remove();
        $(cc).find('a[data-toggle=tab]').each(function () {
                var id = $(this).attr('href');
                var em = $(id).find('em');
                var label = $(this).html();
                if (em.length > 0) {
                        var r = '<div id="' + id + '_asterisk" class="asterisk tab_asterisk"></div>';
                        var new_label = label + ' ' + r;
                        $(this).html(new_label);
                        r = null;
                }
                id, em, label = null;
        });
}

var handleValidationRules = function (container) {

        container = which_container(container);
        var a = $(container).find('[validation_rules]');

        if ($(container).find('input[name^=sys_validation][type=hidden]').length)
                $(container).find('input[name^=sys_validation][type=hidden]').remove();

        if (a.length > 0) {
                $.each(a, function (k, v) {
                        var b = $(v).attr('validation_rules');

                        if (typeof (b) !== 'undefined' && !$(this).hasClass('always_disabled')) {
                                var bb;
                                if ($.inArray('|', b) !== -1) {
                                        bb = b.split('|');
                                } else {
                                        bb = [b];
                                }
                                if (b.length > 0) {
                                        var vname = v.name;
                                        var h = '';

                                        h = '<input type=hidden id="sys_validation[' + vname +
                                                ']" name="sys_validation[' + vname +
                                                ']" value="' + b + '">';

                                        $(container).find(
                                                'input[type=hidden][name=sys_validation\\[' +
                                                vname + '\\]]').remove();
                                        $(v).closest('.form-group').after(h);
                                        vname = null;
                                }
                                bb = null;
                        } else {
                                if ($(this).hasClass('datepicker')) {
                                        var vname = v.name;
                                        var h = '';
                                        h = '<input type=hidden id="sys_validation[' + vname +
                                                ']" name="sys_validation[' + vname +
                                                ']" value="' + b + '">';
                                        $(container).find(
                                                'input[type=hidden][name=sys_validation\\[' +
                                                vname + '\\]]').remove();
                                        $(v).after(h);
                                }
                        }
                        b = null;
                });
        }
        a = null;
}

var handleControls = function (container) {
        handleWidget();
        handleFooter(container);
        handleTextBox(container);
        handleButton(container);
        handleTables(container);
        handleSelectBoxes(container);
        handleButtonDetail(container);
        handleCheckBox(container);
        handleRadioBox(container);
        handleDateTimePicker(container);
        handleTimePicker(container);
        handleTextArea(container);
        handleValidationRules(container);
        handleSearchBox();
        handleRating();

        handleTab();

}

var handleRating = function () {
        $('select.rating').each(function () {
                //                var name = $(this).attr('name');
                var id = $(this).attr('id');
                $(this).barrating({
                        theme: 'fontawesome-stars',
                        showValues: true,
                        onSelect: function (value, text, event) {
                                if (typeof (event) !== 'undefined') {
                                        // rating was selected by a user

                                        var h = '<input type=hidden name=' + id + ' id=' + id + ' value=' + value + '>';

                                        $(cc).find('input[type=hidden][name=' + id + ']').remove();
                                        $(this).after(h);
                                } else {
                                        // rating was selected programmatically
                                        // by calling `set` method
                                }
                        }
                }); //.closest('.form-group').find('.br-widget:last').hide();

        });
        //        $('.br-widget').hide();
}

var get_ajax = function (url, ajax_input) { //send the ajax request and return the response
        if (ajax_input.length) {
                $.ajax({
                        type: 'POST',
                        cache: false,
                        async: false,
                        //                        timeout: 30000,
                        url: url,
                        data: ajax_input,
                        beforeSend: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                                return false;
                        },
                        success: function (data) {
                                ajax_data = data;
                        }
                });
        } else {
                $.ajax({
                        type: 'POST',
                        cache: false,
                        async: false,
                        url: url,
                        //                        timeout: 30000,
                        beforeSend: function () {},
                        error: function (xhr, ajaxOptions, thrownError) {
                                return false;
                        },
                        success: function (data) {
                                ajax_data = data;
                        }
                });
        }
}

var get_base_url = function () {
        return url;
        //        return attr($('input[type=hidden][name=base_url]').val(), '');
}

var get_uri = function () {
        return attr($('input[type=hidden][name=uri]').val(), '');
}

var handleWidget = function () {
        $('#widget-grid').jarvisWidgets('destroy').jarvisWidgets({
                grid: 'article',
                widgets: '.jarviswidget',
                localStorage: true,
                deleteSettingsKey: '#deletesettingskey-options',
                settingsKeyLabel: 'Reset settings?',
                deletePositionKey: '#deletepositionkey-options',
                positionKeyLabel: 'Reset position?',
                sortable: true,
                buttonsHidden: false,
                //
                toggleButton: true,
                toggleClass: 'min-10 | plus-10',
                toggleSpeed: 200,
                onToggle: function () {},
                //
                deleteButton: true,
                deleteClass: 'fa fa-trash',
                deleteSpeed: 200,
                onDelete: function () {},
                //
                editButton: true,
                editPlaceholder: '.jarviswidget-editbox',
                editClass: 'icon-pencil | icon-trash',
                editSpeed: 200,
                onEdit: function () {},
                //
                fullscreenButton: true,
                fullscreenClass: 'fullscreen-10 | normalscreen-10',
                fullscreenDiff: 3,
                onFullscreen: function () {},
                //
                customButton: false,
                customClass: 'folder-10 | next-10',
                customStart: function () {
                        alert('Hello you, this is a custom button...')
                },
                customEnd: function () {
                        alert('bye, till next time...')
                },
                //
                buttonOrder: '%refresh% %delete% %custom% %edit% %fullscreen% %toggle%',
                opacity: 1.0,
                dragHandle: '> header',
                placeholderClass: 'jarviswidget-placeholder',
                indicator: true,
                indicatorTime: 600,
                ajax: true,
                timestampPlaceholder: '.jarviswidget-timestamp',
                timestampFormat: 'Last update: %m%/%d%/%y% %h%:%i%:%s%',
                refreshButton: true,
                refreshButtonClass: 'icon-refresh icon-spin',
                labelError: 'Sorry but there was a error:',
                labelUpdated: 'Last Update:',
                labelRefresh: 'Refresh',
                labelDelete: 'Delete widget:',
                afterLoad: function () {},
                rtl: false

        });
}

$.fn.extend({

        //pass the options variable to the function
        jarvismenu: function (options) {

                var defaults = {
                        accordion: 'true',
                        speed: 200,
                        closedSign: '[+]',
                        openedSign: '[-]'
                },
                        // Extend our default options with those provided.
                        opts = $.extend(defaults, options),
                        //Assign current element to variable, in this case is UL element
                        $this = $(this);

                //add a mark [+] to a multilevel menu

                $this.find("li").each(function () {
                        if ($(this).find("ul").length !== 0) {
                                //add the multilevel sign next to the link
                                $(this).find("a:first").append("<b class='collapse-sign'>" + opts.closedSign + "</b>");

                                //avoid jumping to the top of the page when the href is an #
                                if ($(this).find("a:first").attr('href') == "#") {
                                        $(this).find("a:first").click(function () {
                                                return false;
                                        });
                                }
                        }
                });
//
//                //open active level
//                $this.find("li.active").each(function () {
//                        $(this).parents("ul").slideDown(opts.speed);
//                        $(this).parents("ul").parent("li").find("b:first").html(opts.openedSign);
//                        $(this).parents("ul").parent("li").addClass("open");
//                });
//
//                $this.find("li a").click(function () {
//
//                        if ($(this).parent().find("ul").length !== 0) {
//
//                                if (opts.accordion) {
//                                        //Do nothing when the list is open
//                                        if (!$(this).parent().find("ul").is(':visible')) {
//                                                parents = $(this).parent().parents("ul");
//                                                visible = $this.find("ul:visible");
//                                                visible.each(function (visibleIndex) {
//                                                        var close = true;
//                                                        parents.each(function (parentIndex) {
//                                                                if (parents[parentIndex] == visible[visibleIndex]) {
//                                                                        close = false;
//                                                                        return false;
//                                                                }
//                                                        });
//                                                        if (close) {
//                                                                if ($(this).parent().find("ul") != visible[visibleIndex]) {
//                                                                        $(visible[visibleIndex]).slideUp(opts.speed, function () {
//                                                                                $(this).parent("li").find("b:first").html(opts.closedSign);
//                                                                                $(this).parent("li").removeClass("open");
//                                                                        });
//
//                                                                }
//                                                        }
//                                                });
//                                        }
//                                } // end if
//                                if ($(this).parent().find("ul:first").is(":visible") && !$(this).parent().find("ul:first").hasClass("active")) {
//                                        $(this).parent().find("ul:first").slideUp(opts.speed, function () {
//                                                $(this).parent("li").removeClass("open");
//                                                $(this).parent("li").find("b:first").delay(opts.speed).html(opts.closedSign);
//                                        });
//
//                                } else {
//                                        $(this).parent().find("ul:first").slideDown(opts.speed, function () {
//                                                /*$(this).effect("highlight", {color : '#616161'}, 500); - disabled due to CPU clocking on phones*/
//                                                $(this).parent("li").addClass("open");
//                                                $(this).parent("li").find("b:first").delay(opts.speed).html(opts.openedSign);
//                                        });
//                                } // end else
//                        } // end if
//                });
        } // end function
});

$(window).on('hashchange', function () {
        checkURL();
});

// === following js will activate the menu in left side bar based on url ====
$(document).ready(function () {
        // INITIALIZE LEFT NAV
        loadLang();
        $('nav ul').jarvismenu({
//                accordion: true,
//                speed: true,
                closedSign: '<em class="fa fa-plus-square-o"></em>',
                openedSign: '<em class="fa fa-minus-square-o"></em>'
        });
        //open active level
        setTimeout(function () {
                $('nav').find("li.active").each(function () {
//                        alert($(this));
                        $(this).parents("ul").slideDown();
                        $(this).parents("ul").parent("li").find("b:first");//.html(opts.openedSign);
                        $(this).parents("ul").parent("li").addClass("open");
                });
        }, 1000);

        setTimeout(function () {

                checkURL();

        }, 500);
        handleSearchTextBox();
});

var print = function (k) {
        console.log(k);
}

var loadLang = function () {

        if (('ontouchstart' in window) || window.DocumentTouch &&
                document instanceof DocumentTouch) {
                var clickEvent = 'touchstart';
                var useTap = true;
        } else {
                clickEvent = 'click';
                useTap = false;
        }

        $(
                '.dropdown-menu:not(.datepicker, .rt-table .dropdown-menu, .wysihtml5-toolbar .dropdown-menu, .bootstrap-timepicker-widget)')
                .on(clickEvent, '*', function (e) {
                        //                e.stopPropagation();
                        var href = attr($(this).attr('href'));
                        if (href.length > 0)
                                window.location.href = href;
                });

        var lang = attr($('input[type=hidden][name=site_lang]').val(), 'english');

        var base_url = get_base_url();

        if (base_url == null) {
                base_url = window.location.origin + window.location.pathname;
        }
        $.ajax({
                async: false,
                type: 'GET',
                url: base_url + 'assets/xml/' + lang + '.xml',
                //url: './assets/xml/' + lang + '.xml',
                dataType: "text",
                success: function (xml) {
                        $(xml).each(function (k, v) {
                                locale[$(this).attr('id')] = $(this).text();
                        });
                }
        });

}

var replace_all = function (txt, replace, with_this) {
        try {
                if (typeof (txt) !== 'undefined' && txt !== false) {
                        return txt.replace(new RegExp(replace, 'g'), with_this);
                } else {
                        return txt;
                }
        } catch (e) {
        }
}

var addZero = function (i) {
        if (i < 10) {
                i = "0" + i;
        }
        return i;
}

var transpose_table = function (objTable) {
        if (typeof objTable != 'undefined') {

                objTable.each(function () {
                        var $this = $(this);
                        var newrows = [];
                        $this.find("tbody tr, thead tr").each(function () {
                                var i = 0;
                                $(this).find("td, th").each(function () {
                                        i++;
                                        if (newrows[i] === undefined) {
                                                newrows[i] = $("<tr></tr>");
                                        }
                                        newrows[i].append($(this));
                                });
                        });
                        $this.find("tr").remove();
                        $.each(newrows, function () {
                                $this.append(this);
                        });
                });
                //switch old th to td
                objTable.find('th').wrapInner('<td />').contents().unwrap();
                //move first tr into thead
                var thead = objTable.find("thead");
                var thRows = objTable.find("tr:first");
                var copy = thRows.clone(true).appendTo("thead");
                thRows.remove();
                //switch td in thead into th
                objTable.find('thead tr td').wrapInner('<th />').contents().unwrap();
                //add tr back into tfoot
                objTable.find('tfoot').append("<tr></tr>");
                //add tds into tfoot
                objTable.find('tbody tr:first td').each(function () {
                        objTable.find('tfoot tr').append("<td>&nbsp;</td>");
                });
                return false;
        }
}

var HandleHighchartTable = function (container) {
        $(container).highchartTable();
}


var handleHighChart = function (chart_id, data) {

        var title = attr($('#' + chart_id).attr('title'), '');
        var x_axis = attr($('#' + chart_id).attr('x-axis'), '');
        var x_axis_short = attr($('#' + chart_id).attr('x-axis-short'), '');

        var chart_type = attr($('#' + chart_id).attr('chart_type'), '');
        var series_column = attr($('#' + chart_id).attr('series-column'), '');

        var yasix_name = data['yasix_name'];
        if (yasix_name == '') {
                yasix_name = 'Value';
        } else {
                yasix_name = yasix_name;
        }
        var series = data[series_column];
        var y_axis = attr(data['y_axis'], []);
        var sub_tooltip = [];
        sub_tooltip = data['tooltip'];
        var sub_title = data['targets'];
        var sub = '';
        $.each(sub_title, function (k, v) {
                sub += k + ': ' + v + '; ';
        });

        $(this).empty();


        Highcharts.setOptions({
                lang: {
                        thousandsSep: ','
                }
        })


        switch (chart_type) {
                case 'bar':
                        $('#' + chart_id).highcharts({
                                colors: ['#7cb5ec', '#f7a35c', '#90ed7d', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                                chart: {
                                        type: 'bar',
                                        height: 500
                                },
                                title: {
                                        text: title
                                },
                                xAxis: {
                                        categories: data[x_axis],
                                },
                                yAxis: {
                                        min: 0,
                                        title: {
                                                text: 'Value'
                                        }
                                },
                                legend: {
                                        reversed: true
                                },
                                plotOptions: {
                                        bar: {
                                                stacking: 'percent',
                                                dataLabels: {
                                                        enabled: true
                                                }
                                        }
                                },
                                series: series
                        });
                        trend_line(series, chart_id);
                        break;
                case 'pie':
                        $.each(series, function (kk, vv) {
                                $('.highchart').append('<div class="row"><div class="col-md-12"><div id=pie-' + kk + '></div></div></div>');
                                $('#pie-' + kk).highcharts({
                                        chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false,
                                                type: 'pie'
                                        },
                                        title: {
                                                text: title
                                        },
                                        tooltip: {

                                        },
                                        plotOptions: {
                                                pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                                enabled: true,
                                                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                                style: {
                                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                }
                                                        }
                                                }
                                        },
                                        series: [{
                                                        data: vv['data']
                                                }]

                                });
                        });
                        trend_line(series, chart_id);
                        break;
                case 'line':
                        $('#' + chart_id).highcharts({
                                colors: ['#7cb5ec', '#f7a35c', '#90ed7d', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                                title: {
                                        text: title,
                                        x: -20 //center
                                },
                                subtitle: {
                                        text: '',
                                        x: -20
                                },
                                xAxis: {
                                        categories: data[x_axis_short],

                                },
                                yAxis: {
                                        min: 0,
                                        title: {
                                                text: 'score'
                                        },
                                        plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#808080'
                                                }]
                                },
                                tooltip: {
                                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                                        footerFormat: '</table>',
                                        shared: true,
                                        useHTML: true
                                },
                                legend: {
                                        layout: 'vertical',
                                        align: 'right',
                                        verticalAlign: 'middle',
                                        borderWidth: 0
                                },
                                plotOptions: {
                                        line: {
                                                dataLabels: {
                                                        enabled: true,
                                                        format: '{point.y:.1f}%'
                                                },
                                                enableMouseTracking: false,
                                                connectNulls: true
                                        }
                                },
                                series: series
                        });
                        trend_line(series, chart_id);
                        break;
                case 'column':

                        $('#' + chart_id).highcharts({
                                colors: ['#7cb5ec', '#f7a35c', '#90ed7d', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],

                                chart: {
                                        type: chart_type,
                                },

                                title: {
                                        text: title
                                },
                                subtitle: {
                                        text: sub
                                },
                                xAxis: {
                                        categories: data[x_axis],
                                        crosshair: true
                                },
                                yAxis: {
                                        tickInterval : 1,
                                        min: 0,
                                        title: {
                                                text: data['yasix_name']

                                        }
                                },
                                tooltip: {
                                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                        footerFormat: '</table>',
                                        shared: true,
                                        useHTML: true
                                },
                                plotOptions: {
                                        series: {
                                                borderWidth: 0,
                                                dataLabels: {
                                                        enabled: true,
                                                        format: '{point.y}',
                                                        style: {
                                                                fontSize: '10px'
                                                        }
                                                }

                                        }
                                },

                                series: series
                        });
                        trend_line(series, chart_id);
                        break;
                case 'multiple':
                        $('#' + chart_id).highcharts({
                                colors: ['#7cb5ec', '#f7a35c', '#90ed7d', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                                title: {
                                        text: title,
                                        x: -20 //center
                                },
                                subtitle: {
                                        text: '',
                                        x: -20
                                },
                                xAxis: {
                                        categories: data[x_axis_short],

                                },
                                yAxis: y_axis,
                                tooltip: {

                                        shared: true,
                                },
                                legend: {
                                        reversed: true
                                },

                                series: series,
                                /*tooltip: {
                                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                 '<td style="padding:0"><b>{point.y:,.2f}</b></td></tr>',
                                 footerFormat: '</table>',
                                 shared: true,
                                 useHTML: true
                                 },*/
                                tooltip: {
                                        formatter: function () {

                                                var s = '<b>' + this.x + '</b>';

                                                $.each(this.points, function () {
                                                        s += '<table><tr><td style="color:' + this.series.color + ';padding:0">' + this.series.name + ' : </td>';
                                                        s += '<td style="padding:0"> <b>' + number_format(this.y, 2) + '</b></td>';
                                                        s += '</tr></table>';
                                                });
                                                // Add more tooltip
                                                /**
                                                 *  Demo in function get_report_shop()
                                                 */
                                                if (data['check_tooltip'] == 1) { // check have tooltip? (1:yes)
                                                        var index = data[x_axis_short].indexOf(this.x); // get index
                                                        s += '<br/><b>' + sub_tooltip['name'] + '</b> : ' + sub_tooltip['data'][index];
                                                }
                                                return s;
                                        },
                                        shared: true,
                                        useHTML: true
                                },
                                plotOptions: {
                                        series: {
                                                borderWidth: 0,
                                                dataLabels: {
                                                        enabled: true,
                                                        format: '{point.y:,.2f}',
                                                        style: {
                                                                fontSize: '10px'
                                                        }
                                                }

                                        }
                                },
                        });
                        trend_line(series, chart_id);
                        break;

                case 'trend':
                        $('#' + chart_id).highcharts({
                                colors: ['#7cb5ec', '#f7a35c', '#90ed7d', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                                title: {
                                        text: title,
                                        x: -20 //center
                                },
                                subtitle: {
                                        text: '',
                                        x: -20
                                },
                                xAxis: {
                                        categories: data[x_axis_short],

                                },
                                yAxis: y_axis,
                                tooltip: {

                                        shared: true,
                                },
                                legend: {
                                        reversed: true
                                },

                                series: series,
                                /*tooltip: {
                                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                 '<td style="padding:0"><b>{point.y:,.2f}</b></td></tr>',
                                 footerFormat: '</table>',
                                 shared: true,
                                 useHTML: true
                                 },*/
                                tooltip: {
                                        formatter: function () {

                                                var s = '<b>' + this.x + '</b>';

                                                $.each(this.points, function () {
                                                        s += '<table><tr><td style="color:' + this.series.color + ';padding:0">' + this.series.name + ' : </td>';
                                                        s += '<td style="padding:0"> <b>' + number_format(this.y, 2) + '</b></td>';
                                                        s += '</tr></table>';
                                                });
                                                // Add more tooltip
                                                /**
                                                 *  Demo in function get_report_shop()
                                                 */
                                                if (data['check_tooltip'] == 1) { // check have tooltip? (1:yes)
                                                        var index = data[x_axis_short].indexOf(this.x); // get index
                                                        s += '<br/><b>' + sub_tooltip['name'] + '</b> : ' + sub_tooltip['data'][index];
                                                }
                                                return s;
                                        },
                                        shared: true,
                                        useHTML: true
                                },
                                plotOptions: {
                                        series: {
                                                borderWidth: 0,
                                                dataLabels: {
                                                        enabled: true,
                                                        format: '{point.y:,.2f}',
                                                        style: {
                                                                fontSize: '10px'
                                                        }
                                                }

                                        }
                                },
                        });
                        trend_line(series, chart_id);
                        break;


                case 'dual' :
                        $('#' + chart_id).highcharts({
                                colors: ['#7cb5ec', '#f7a35c', '#90ed7d', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                                chart: {
                                        zoomType: 'xy'
                                },
                                title: {
                                        text: 'Average Monthly Temperature and Rainfall in Tokyo'
                                },
                                subtitle: {
                                        text: 'Source: WorldClimate.com'
                                },
                                xAxis: [{
                                                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                                                crosshair: true
                                        }],
                                yAxis: [{// Primary yAxis
                                                labels: {
                                                        format: '{value}°C',
                                                        style: {
                                                                color: Highcharts.getOptions().colors[1]
                                                        }
                                                },
                                                title: {
                                                        text: 'Temperature',
                                                        style: {
                                                                color: Highcharts.getOptions().colors[1]
                                                        }
                                                }
                                        }, {// Secondary yAxis
                                                title: {
                                                        text: 'Rainfall',
                                                        style: {
                                                                color: Highcharts.getOptions().colors[0]
                                                        }
                                                },
                                                labels: {
                                                        format: '{value} mm',
                                                        style: {
                                                                color: Highcharts.getOptions().colors[0]
                                                        }
                                                },
                                                opposite: true
                                        }],
                                tooltip: {
                                        shared: true
                                },
                                legend: {
                                        layout: 'vertical',
                                        align: 'left',
                                        x: 120,
                                        verticalAlign: 'top',
                                        y: 100,
                                        floating: true,
                                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                                },
                                series: [{
                                                name: 'Rainfall',
                                                type: 'column',
                                                yAxis: 1,
                                                data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
                                                tooltip: {
                                                        valueSuffix: ' mm'
                                                }

                                        }, {
                                                name: 'Temperature',
                                                type: 'spline',
                                                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
                                                tooltip: {
                                                        valueSuffix: '°C'
                                                }
                                        }]
                        });
                        break;
        }

        //        })
}

var trend_line = function (series, chart_id) {
        /* add regression line dynamically */
        var trend_linear = $('#' + chart_id).highcharts();
        $.each(series, function (i, val) {

                if (typeof val['trend'] == "undefined") {
                        var trend_active = false;
                } else {
                        var trend_active = val['trend'];
                }

                if (trend_active == true) {
                        var sourceData = val['data'];
                        trend_linear.addSeries({
                                type: 'line',
                                name: 'Trend ' + val['name'],
                                marker: {enabled: false},
                                /* function returns data for trend-line */
                                data:
                                        (function () {
                                                return fitOneDimensionalData(sourceData, val['year']);
                                        })()
                        });
                }
        });

        function fitOneDimensionalData(source_data, year) {
                var trend_source_data = [];
                var d = new Date();
                var m = d.getMonth();
                var y = d.getFullYear();
                var sizedata = source_data.length;
                if (year == y) {
                        //sourceData.splice(m+1);
                        sizedata = m + 1;
                }
                for (var i = sizedata; i-- > 0; ) {
                        trend_source_data[i] = [i, source_data[i]];
                }
                var regression_data = fitData(trend_source_data).data;
                var trend_line_data = [];
                for (var i = regression_data.length; i-- > 0; ) {
                        trend_line_data[i] = regression_data[i][1];
                }
                return trend_line_data;
        }
}


var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 50, // distance to the element when triggering the animation (default is 0)
        mobile: false
                // trigger animations on mobile devices (true is default)
});
wow.init();